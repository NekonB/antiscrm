<?php
 
require './vendor/autoload.php';
 
require_once './connectionCDR.php';
function foo($seconds) {
    $t = round($seconds);
    return sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60);
  }
    

    ob_start();
    session_start();
    $dtb = $_GET['start'];
    $dte = $_GET['end'];
    $number = $_SESSION['number'];

    $strOperators ="";
    
    $iSecDtb = ($dtb/1000);
    $iSecDte = ($dte/1000);
    $db =date("Y-m-d H:i:s", $iSecDtb);
    $de =date("Y-m-d H:i:s", $iSecDte);
    $data = (object) array();
    $data->agents = $_SESSION['operators'];

    /**
     * query - all calls
     */
    $sql="";
    $sql .= "SELECT " ;
        $sql .= "count(c.lastapp ='Queue' or null) + count(c.lastapp='Playback' or null) as allCalls, ";
        $sql .= "count(c.lastapp ='Playback' or null) as wellcomeLoss, ";
        $sql .= "count(c.lastapp='Queue' and c.disposition='NO ANSWER' or null) as lossInQueue, ";
        $sql .= "count(c.dcontext='not-work-time-gorod'  or null) as lossNotWorktime, ";
//        $sql .= "count(c.lastapp = 'Dial' and c.disposition='ANSWERED' or null) as confirm, ";
        $sql .= "count(c.disposition='BUSY' or null) as busyLoss ";
    $sql .= "FROM cdr as c ";
    $sql .= "WHERE ";
        $sql .= "c.calldate between '".$db."' and '".$de."' and c.did = '".$number."'";
    
    $stmt = $cdr->query($sql);
    $data -> allCalls = $stmt->fetchObject();
    $data -> allCalls -> loss = ((int)$data -> allCalls->wellcomeLoss + (int)$data -> allCalls->lossInQueue + (int)$data -> allCalls->lossNotWorktime);
    $data -> allCalls -> confirm =0;
    $stmt->closeCursor();

    /**
     * query - by operators
     */
    
    $byOperCall = array();
    foreach($_SESSION['operators'] as $oper) {
        $sql = "SELECT count(*) as allCalls, count(c.disposition = 'ANSWERED' and c.lastapp='Dial' or null) as confirm, count(c.disposition = 'NO ANSWER' or c.disposition='BUSY' or c.lastapp != 'Dial' or null) as loss, count(c.disposition = 'NO ANSWER' and c.lastapp='Dial' or null) as operatorLoss, count(c.lastapp != 'Dial' or null) as overload, c.dst as agent  FROM cdr as c WHERE c.calldate between ? and ? and c.dst = ? ";
        $stmt = $cdr->prepare($sql);
        $stmt->execute(array($db,$de,$oper));
        array_push($byOperCall , $stmt->fetchObject());
    }
    $data-> byOperCall =$byOperCall;
    $stmt->closeCursor();


    foreach($data->byOperCall as $el) {
        $data->allCalls->confirm+=$el->confirm;
    } 

    /**
     * All AVG Q
     */
    $data -> avgQ = (object)array();
    $sql = "SELECT avg(c.duration) as avgQ FROM cdr as c WHERE c.calldate between ? and ? and c.did = ? and c.lastapp ='Queue'";
    $stmt = $cdr->prepare($sql);
    $stmt->execute(array($db,$de,$number));
    $data -> avgQ -> avgQ = $stmt->fetchObject();
    $sql = "SELECT max(time(c.duration)) as avgQ FROM cdr as c WHERE c.calldate between ? and ? and c.did = ? and c.lastapp ='Queue' ";
    $stmt = $cdr->prepare($sql);
    $stmt->execute(array($db,$de,$number));
    $data -> avgQ -> maxQ = $stmt->fetchObject();
    $sql = "SELECT min(time(c.duration)) as avgQ FROM cdr as c WHERE c.calldate between ? and ? and c.did = ? and c.lastapp ='Queue' ";
    $stmt = $cdr->prepare($sql);
    $stmt->execute(array($db,$de,$number));
    $data -> avgQ -> minQ = $stmt->fetchObject();
    $stmt->closeCursor();


    $data -> longAll = (object)array();
    $AL = array();
    $minL = array();
    $maxL = array();
    foreach($_SESSION['operators'] as $oper) {
        $sql = "SELECT avg(c.duration) as avgL , c.dst as agent FROM cdr as c WHERE c.calldate between ? and ?  and c.dst = ? and c.disposition = 'ANSWERED' and c.lastapp ='Dial'";
        $stmt = $cdr->prepare($sql);
        $stmt->execute(array($db,$de,$oper));
        array_push($AL , $stmt->fetchObject());
        $sql = "SELECT min(time(c.duration)) as avgL , c.dst as agent FROM cdr as c WHERE c.calldate between ? and ?  and c.dst = ? and c.disposition = 'ANSWERED' and c.lastapp ='Dial'";
        $stmt = $cdr->prepare($sql);
        $stmt->execute(array($db,$de,$oper));
        array_push($minL , $stmt->fetchObject());
        $sql = "SELECT max(time(c.duration)) as avgL , c.dst as agent FROM cdr as c WHERE c.calldate between ? and ?  and c.dst = ? and c.disposition = 'ANSWERED' and c.lastapp ='Dial'";
        $stmt = $cdr->prepare($sql);
        $stmt->execute(array($db,$de,$oper));
        array_push($maxL , $stmt->fetchObject());
    }
    $data-> longAll -> avgL = $AL;
    $data-> longAll -> maxL = $maxL;
    $data-> longAll -> minL = $minL;

    $avgByO = array();
    foreach($_SESSION['operators'] as $oper) {
        $sql="SELECT avg(c.billsec) as avgQ , max(time(c.billsec or null)) as maxQ , min(time(c.billsec or null)) as minQ , c.dst as agent FROM cdr as c WHERE c.calldate between ? and ? and c.dst = ?";
        $stmt= $cdr->prepare($sql);
        $stmt->execute(array($db,$de,$oper));
        array_push($avgByO , $stmt->fetchObject());
    }
    $data-> byOperAVGO = $avgByO;
    $longBy = array();
    foreach($_SESSION['operators'] as $oper) {
        $sql="SELECT avg(c.billsec) as avgL , max(time(c.billsec)) as maxL , min(time(c.billsec)) as minL , c.dst as agent FROM cdr as c WHERE c.calldate between ? and ? and c.dst = ?  and c.disposition ='ANSWERED' and c.lastapp = 'Dial' ";
        $stmt= $cdr->prepare($sql);
        $stmt->execute(array($db,$de,$oper));
        array_push($longBy , $stmt->fetchObject());
    }
    $data-> longByO = $longBy;
 
 
echo date('H:i:s') , " Create new PHPExcel object" , EOL;
$objPHPExcel = new PHPExcel();


$objPHPExcel->setActiveSheetIndex(0);
$active = $objPHPExcel->getActiveSheet();
$active->getColumnDimension('A')->setAutoSize(true);
$active->getColumnDimension('C')->setAutoSize(true);  
$active->getColumnDimension('E')->setAutoSize(true);  

$active->setCellValueByColumnAndRow(0,1,"Статистика Колл-центра за ".$db." - ".$de);
$active->mergeCells('A1:G2');
$active->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$active->getStyle('A1')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$active->getStyle('A1')->getFont()->setBold(true)->setSize(16);

$active-> getCell('A4')->setValue('Статистика звонков');
$active-> getStyle('A4')->getFont()->getColor()->setRGB('004f74');
$active->getStyle('A4')->getFont()->setBold(true)->setSize(14);
$active->getCell('A5')->setValue('Кол-во звонков, поступивших в коллцентр:');    
    $active->getCell('B5')->setValue($data-> allCalls-> allCalls)
        ->getStyle()
        ->getFont()
        ->getColor()
        ->setRGB('0a7ba8');
    $active->getCell('B5')->getStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);          
$active->getCell('A6')->setValue('Кол-во обработанных вызовов:');
    $active->getCell('B6')->setValue($data-> allCalls-> confirm)
        ->getStyle()
        ->getFont()
        ->getColor()
        ->setRGB('3ca80a');
    $active->getCell('B6')->getStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);                        //value B6
$active->getCell('A7')->setValue('Кол-во пропущенных вызовов:');
    $active->getCell('B7')->setValue($data-> allCalls -> loss)
        ->getStyle()
        ->getFont()
        ->getColor()
        ->setRGB('cf1313');
    $active->getCell('B7')->getStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);                            //value B7
$active->getCell('A8')->setValue('- потерянные на приветсвии');
    $active->getCell('B8')->setValue($data-> allCalls-> wellcomeLoss)
        ->getStyle()
        ->getFont()
        ->getColor()
        ->setRGB('cf1313');
    $active->getCell('B8')->getStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);                      //value B8
$active->getCell('A9')->setValue('- потерянные в очереди');  
    $active->getCell('B9')->setValue($data-> allCalls-> lossInQueue)
        ->getStyle()
        ->getFont()
        ->getColor()
        ->setRGB('cf1313');
    $active->getCell('B9')->getStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);                          //value B9
$active->getCell('A10')->setValue('- потерянные в не рабочее время');                       //value B10
    $active->getCell('B10')->setValue($data-> allCalls-> lossNotWorktime)
        ->getStyle()
        ->getFont()
        ->getColor()
        ->setRGB('cf1313');
    $active->getCell('B10')->getStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);  


$active->getCell('C4')->setValue('Статистика ожидания в очереди');
$active-> getStyle('C4')->getFont()->getColor()->setRGB('004f74');
$active->getStyle('C4')->getFont()->setBold(true)->setSize(14);
$active->getCell('C5')->setValue('Минимальное время ожидания в очереди:');
    $active->getCell('D5')->setValue($data-> avgQ-> minQ-> avgQ)
        ->getStyle()
        ->getFont()
        ->getColor()
        ->setRGB('0a7ba8');
    $active->getCell('B10')->getStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$active->getCell('C6')->setValue('Среднее время ожидания в очереди:');
    $active->getCell('D6')->setValue(foo(floor($data-> avgQ-> avgQ-> avgQ)))
        ->getStyle()
        ->getFont()
        ->getColor()
        ->setRGB('0a7ba8');
    $active->getCell('B10')->getStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$active->getCell('C7')->setValue('Максимальное время ожидания в очереди:');
    $active->getCell('D7')->setValue($data-> avgQ-> maxQ-> avgQ)
        ->getStyle()
        ->getFont()
        ->getColor()
        ->setRGB('0a7ba8');
    $active->getCell('B10')->getStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);




    $aMAX = array();
    $aMIN = array();
    $aAVG = array();
    foreach($data->longAll->maxL as $el) {
        if(isset($el->avgL)) {
            $hms = $el->avgL;
            $a = explode(':',$hms);
            $seconds = ($a[0])*60 *60 + ($a[1])*60 + ($a[2]);
            array_push($aMAX,$seconds);
        } 
    }
    foreach($data->longAll->minL as $el) {
        if(isset($el->avgL)) {
            $hms = $el->avgL;
            $a = explode(':',$hms);
            $seconds = ($a[0])*60 *60 + ($a[1])*60 + ($a[2]);
            array_push($aMIN,$seconds); 
            echo $seconds;
        }
        
    }
    foreach($data->longAll->avgL as $el) {
        if(isset($el->avgL)) {
            array_push($aAVG, $el->avgL);
        }
    }
    $aa= 0;
    foreach($aAVG as $el) {
        $aa+=$el;
    }
    $aa = $aa/count($aAVG);

$active->getCell('E4')->setValue('Продолжительность входящих вызовов');
$active-> getStyle('E4')->getFont()->getColor()->setRGB('004f74');
$active->getStyle('E4')->getFont()->setBold(true)->setSize(14);
$active->getCell('E5')->setValue('Минимальное время разговора:');
    $active->getCell('F5')->setValue(foo(min($aMIN)))
        ->getStyle()
        ->getFont()
        ->getColor()
        ->setRGB('0a7ba8');
    $active->getCell('B10')->getStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$active->getCell('E6')->setValue('Среднее время разговора:');
    $active->getCell('F6')->setValue(foo(floor($aa)))
        ->getStyle()
        ->getFont()
        ->getColor()
        ->setRGB('0a7ba8');
    $active->getCell('B10')->getStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
$active->getCell('E7')->setValue('Максимальное время разговора:');
    $active->getCell('F7')->setValue(foo(max($aMAX)))
        ->getStyle()
        ->getFont()
        ->getColor()
        ->setRGB('0a7ba8');
    $active->getCell('B10')->getStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);



$active->mergeCells('A12:G13');
$active->getCell('A12')->setValue('По операторам')
    ->getStyle()
    ->getFont()
    ->setSize(16)
    ->setBold(true);
$active->getStyle('A12')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

$row =15;

foreach($data->byOperCall as $el) {
    
    if(isset($el->agent)) {
        $active->getCellByColumnAndRow(0,$row)->setValue('Статистика звонков оператора '.$el->agent)
            ->getStyle()
            ->getFont()
            ->setSize(14)
            ->getColor()
            ->setRGB('0a7ba8');
        
        $row+=1;
        $active->getCellByColumnAndRow(0,$row)->setValue('Статистика звонков')
            ->getStyle()
            ->getFont()
            ->setSize(12)
            ->getColor()
            ->setRGB('0a7ba8');
        


        $orw+=1; 
        $active->getCellByColumnAndRow(0,$row)->setValue('Кол-во звонков, поступивших в коллцентр:');
        $active->getCellByColumnAndRow(1,$row)->setValue($el->allCalls)
            ->getStyle()
            ->getFont()
            ->getColor()
            ->setRGB('0a7ba8');;

        

        $row+=1;

        $active->getCellByColumnAndRow(0,$row)->setValue('Кол-во обработанных вызовов:');
        $active->getCellByColumnAndRow(1,$row)->setValue($el->confirm)
            ->getStyle()
            ->getFont()
            ->getColor()
            ->setRGB('0a7ba8');

        

        $row+=1;

        $active->getCellByColumnAndRow(0,$row)->setValue('Кол-во пропущенных вызовов:');
        $active->getCellByColumnAndRow(1,$row)->setValue($el->loss)
            ->getStyle()
            ->getFont()
            ->getColor()
            ->setRGB('0a7ba8');


        
        $row+=3;
    }
}
$row=15;
foreach($data->longByO as $el) {
    if(isset($el->agent)) {
        $active->getCellByColumnAndRow(2,$row)->setValue('Продолжительность входящих вызовов')
            ->getStyle()
            ->getFont()
            ->setSize(12)
            ->getColor()
            ->setRGB('0a7ba8');

            $row+=1;

            $active->getCellByColumnAndRow(2,$row)->setValue('Минимальное время разговора:');
            $active->getCellByColumnAndRow(3,$row)->setValue($el->minL)
                ->getStyle()
                ->getFont()
                ->getColor()
                ->setRGB('0a7ba8');
            $row+=1;
            $active->getCellByColumnAndRow(2,$row)->setValue('Среднее время разговора:');
            $active->getCellByColumnAndRow(3,$row)->setValue(foo(floor($el->avgL)))
                ->getStyle()
                ->getFont()
                ->getColor()
                ->setRGB('0a7ba8');

            $row+=1;

            $active->getCellByColumnAndRow(2,$row)->setValue('Максимальное время разговора:');
            $active->getCellByColumnAndRow(3,$row)->setValue($el->minL)
            ->getStyle()
            ->getFont()
            ->getColor()
            ->setRGB('0a7ba8');
            $row+=3;
    }
}

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save("report.xls");

echo "<pre>";
print_r($data);
echo "</pre>";
header('location: ./report.xls');