<aside class="text-white left-panel-sm d-lg-none col-sm-12 col-md-12 col-12" >
    <div class="logo-sm">
        <img src="assets/img/logo-head.png" class="img-fluid ${3|rounded-top,rounded-right,rounded-bottom,rounded-left,rounded-circle,|}" alt="">
    </div>
</aside>
<aside class="text-white left-panel-big col-xl-2 col-lg-3 d-none d-lg-block" >
    <nav class="col-12 text-center">
        <div class="navbar-header">
            <div class="logo">
                <a href="./">
                    <img src="assets/img/logo-head.png" class="img-fluid ${3|rounded-top,rounded-right,rounded-bottom,rounded-left,rounded-circle,|}" alt="">
                </a>
            </div>
        </div>
        <div class="main-menu ">
            <ul class="menu-elements nav navbar-nav">
                <li class="menu-title"><a href="./"><?php echo $trans->onlinemonitoring ?></a></li>
                <li class="menu-title"><a href="./cc_state.php"><?php echo $trans->statecallcenter?></a></li>
                <li class="menu-title"><a href="./allinfo.php"><?php echo $trans->statops ?></a></li>
                <li class="menu-title"><a href="./logout.php"><?php echo $trans->logout ?></a></li>
            </ul>
        </div>
    </nav>
</aside>