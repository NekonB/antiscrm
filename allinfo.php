<?php 
    ob_start();
    session_start();
    if(!isset($_SESSION['number'])) {
        header("location: login.php");
    }
    $flocal= file_get_contents("./langs/".$_SESSION['locale'].".json");
    $trans=json_decode($flocal);
?>
<?php require_once './header.php';?>
<style>
    .ref-count:hover {
	cursor:pointer;
	text-decoration:underline;
    }
</style>
<body>
	<div class="container-fluid ">
        <div class=" row ">
            <?php  require_once './leftpanel.php';?>
            <div class="right-panel col-xl-10 offset-xl-2 col-lg-9 offset-lg-3 col-md-12 col-sm-12">
                <div class="content container-fluid row">
                <div class="header col-12 row d-flex justify-content-between" >
                    <div class="">
                        <h3 class="text-muted"><?php echo $trans->statops; ?> (<?php echo $_SESSION['number'] ?>)</h3>
                    </div>
                    <div class="">
                        <div class="btn-group btn-group-toggle container" data-toggle="buttons">
                            <label class="btn btn-secondary langch" data-lang="kk">
                                <input type="radio" name="lang" id="kk" autocomplete="off" checked> <img src="./icons/Kazakhstan.png"/> Қазақ
                            </label>
                            <label class="btn btn-secondary langch" data-lang="ru">
                                <input type="radio" name="lang" id="ru" autocomplete="off"> <img src="./icons/Russia.png"/> Русский
                            </label>
                            <label class="btn btn-secondary langch" data-lang="en">
                                <input type="radio" name="lang" id="en" autocomplete="off"><img src="./icons/United-Kingdom.png"/>  English
                            </label>
                        </div>
                    </div>   
                </div>
                <div class="items-block  container-fluid mt-3">
                    <div class="mt-2" >
                        <div class="">
                            <label for=""><?php echo $trans->on ?></label>
                            <div class="row">
                                <div class="intervals-button"><button data-diff="0" data-interval="hour" id="day" class=" btn btn-dark"><?php echo $trans->ontoday ?></button></div>
                                <div class="intervals-button"><button data-diff="604800" data-interval="day" id="week" class=" btn btn-dark"><?php echo $trans->onweek; ?></button></div>
                                <div class="intervals-button"><button data-diff="2419200" data-interval="day" id="month" class=" btn btn-dark"> <?php echo $trans->onmonth ?></button></div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2"></div>
                    <div class="row mt-2 col-6">
                        <label for="datepicker"><?php echo $trans->chooseinterval;?></label>
                        <div class=" input-daterange input-group" id="datepicker">
                            <span><?php echo $trans->date; ?></span>
                            <span class="text-datepicker"><?php echo $trans->to ?></span>
                            <input type="text" value="" class="input-sm ml-2 form-control form-control" id="picStart" /> 
                            <span class="text-datepicker"><?php echo $trans->do ?></span>
                            <input type="text" value="" class="input-sm  form-control form-control" id="picEnd" />
                            <!-- <button id="search" class="btn btn-primary">Найти</button> -->
                        </div>
                    </div>
                    <div class=" mt-2 ">
                        <button id="search" class="btn btn-primary"><?php echo $trans->search ?></button> <span style="display: none;" class=" btn  loader"></span>
                        <button id="report" class="btn btn-primary">Создать отчет в Excel</button>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-6 cc_all">
                            <canvas id="cc_all"></canvas>
                        </div>
                        <div class="col-6 cc_agent">
                            <canvas id="cc_agent"></canvas>
                        </div>
                    </div>
                    
                    <div class="dashboard mt-5 nb-5 row">
                        <div class=" col-12 col-xl-4 col-lg-4 col-sm-12">
                            <div class="col-12 title">
                                <h5><?php echo $trans->statcalls?></h5>
                            </div>
                            <div class="block counts">
                                <div class="info ref-count" data-param="">
                                    <?php echo $trans->countcallall ?><span class=" count">-</span>
                                </div>
                                <div class="info ref-count" data-param="status=ANSWERED">
                                    <?php echo $trans->countservcall ?><span class="  count-success">-</span>
                                </div>
                                <div class="info" data-param="status=NO%20ANSWER">
                                    <?php  echo $trans->countlosscall?><span class=" count-fail">-</span>
                                </div>
                                <Div class="info ref-count" data-param="status=wellcomeLoss">
                                    <?php  echo $trans->losswelcome_?><span class=" count-wl">-</span>
                                </Div>
                                <Div class="info ref-count" data-param="status=NO%20ANSWER">
                                    <?php  echo $trans->lossinqueue?><span class=" count-ql">-</span>
                                </Div>
                                <Div class="info ref-count" data-param="status=notWorkTimeLoss">
                                    <?php  echo $trans->lossnotworktime?><span class=" count-nwt">-</span>
                                </Div>
                            </div>
                        </div>
                        <div class=" col-12 col-xl-4 col-lg-4 col-sm-12">
                            <div class="col-12 title">
                                <h5><?php  echo $trans->statwaitqueue?></h5>
                            </div>
                            <div class="block queueTimes">
                                
                                <div class="info">
                                    <?php echo $trans->minwaitinqueue ?><span class="count min time">--:--:--</span>	
                                </div>
                                <div class="info">
                                    <?php  echo $trans->avgwaitinqueue?><span class="count avg time">--:--:--</span>	
                                </div>
                                <div class="info">
                                    <?php  echo $trans->maxwaitinqueue?><span class="count max time">--:--:--</span>	
                                </div>
                            </div>
                        </div>
                        <div class=" col-12 col-xl-4 col-lg-4 col-sm-12">
                            <div class="col-12   title">
                                <h5><?php  echo $trans->durationincalls?></h5>
                            </div>
                            <div class="block callTimes">
                                <div class="info">
                                    <?php echo $trans->mintimecall ?><span class="count time min">--:--:--</span>
                                </div>
                                <div class="info">
                                    <?php echo $trans->avgtimecall ?><span class="count time avg">--:--:--</span>
                                </div>
                                <div class="info">
                                    <?php echo $trans->maxtimecall ?><span class="count time max">--:--:--</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <h1 class=" center"><?php echo $trans->byops ?></h1>
                    <!-- <div class="diag row" >
                        <canvas height="300" id="allChart"></canvas>
                    </div> -->
                    <div class="by-operator col-12 dashboard">
    
                    </div>
                </div>
                   
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <script src="https://cdn.jsdelivr.net/gh/emn178/chartjs-plugin-labels/src/chartjs-plugin-labels.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.js"></script> -->
    <script>
    $(document).delegate('#report','click',function() {
        var start = $('#picStart').val();
        var end   = $('#picEnd').val(); 
        location.replace('./report.php?start='+new Date(start).getTime()+'&end='+new Date(end).getTime());
    });
     var toDT = function(date) {
        Date.prototype.toLocalISOString = function() {
    
        // Helper for padding
            function pad(n, len) {
            return ('000' + n).slice(-len);
            }
        
            // If not called on a Date instance, or timevalue is NaN, return undefined
            if (isNaN(this) || Object.prototype.toString.call(this) != '[object Date]') return;
        
            // Otherwise, return an ISO format string with the current system timezone offset
            var d = this;
            var os = d.getTimezoneOffset();
            var sign = (os > 0? '-' : '+');
            os = Math.abs(os);
            
            return pad(d.getFullYear(), 4) + '-' +
                pad(d.getMonth() + 1, 2) + '-' +
                pad(d.getDate(), 2) +
                'T' + 
                pad(d.getHours(), 2) + ':' +
                pad(d.getMinutes(), 2) + ':' +
                pad(d.getSeconds(), 2) 
                
                // Note sign of ECMASCript offsets are opposite to ISO 8601
        }

        var dt = date.toLocalISOString().replace('T', ' ');
        return dt;
    }

    $.ajaxSetup({
        url:'./ajax/getState.php',
        dataType:'json',
        beforeSend: function() {
            $('.loader').show();
        },
        complete: function() {
            $('.loader').hide();
        },
        error: function(jqXHR, textStatus, errorThrown) {
            $('.loader').hide();
        }
    })

    var toHHMMSS = (secs) => {
        var sec_num = parseInt(secs, 10)
        var hours   = Math.floor(sec_num / 3600)
        var minutes = Math.floor(sec_num / 60) % 60
        var seconds = sec_num % 60

        return [hours,minutes,seconds]
            .map(v => v < 10 ? "0" + v : v)
            .filter((v,i) => v !== "00" || i > 0)
            .join(":")
    }

    function byOperator(operators  , Q , L)  {
        var html = "";
        $('.by-operator').empty();
        console.log(operators);
        
        operators.forEach(function(el) {
            if (el.agent) {
                html+="<div class='row' data-op='"+el.agent+"'>";
                    html+="<div class='col-12 title-operator ref-count' data-param='agent="+el.agent+"'><h3><?php echo $trans->statcallops ?> "+el.agent+"</h3></div>";
                    html+='<div class="operator col-12 col-xl-4 col-lg-4 col-sm-12">'
                        html+='<div class="col-12 title">'
                            html+='<h5><?php echo $trans->statcalls ?></h5>'
                        html+='</div>'
                        html+='<div class="block">'
                            html+='<div class="info ref-count" data-param="agent='+el.agent+'">';
                                html+='<?php echo $trans->countcallall ?><span class="count">'+el.allCalls+'</span>'
                            html+='</div>'
                            html+='<div class="info ref-count" data-param="agent='+el.agent+'&status=ANSWERED">'
                                html+='<?php echo $trans->countservcall ?><span class="count-success">'+el.confirm+'</span>'
                            html+='</div>'
                            html+='<div class="info ref-count" data-param="agent='+el.agent+'&status=NO%20ANSWER">'
                                html+='<?php echo $trans->countlosscall ?><span class="count-fail">'+el.loss+'</span>'
                            html+='</div>'
                        html+='</div>'
                    html+='</div>'
                    
                    html+='<div class="L col-12 col-xl-4 col-lg-4 col-sm-12">'
                        html+='<div class="col-12   title">'
                            html+='<h5><?php echo $trans->durationincalls ?></h5>'
                        html+='</div>'
                        html+='<div class="block">'
                            html+='<div class="info">'
                                html+='<?php echo $trans->mintimecall ?><span class="count min time">'+el.minL+'</span>'
                            html+='</div>'
                            html+='<div class="info">'
                                html+='<?php echo $trans->avgtimecall ?><span class="count avg time">'+toHHMMSS(Math.floor(el.avgL))+'</span>'
                            html+='</div>'
                            html+='<div class="info">'
                                html+='<?php echo $trans->maxtimecall ?><span class="count max time">'+el.maxL+'</span>'
                            html+='</div>'
                        html+='</div>'
                    html+='</div>'
                    html+='<div class="d col-12 col-xl-4 col-lg-4 col-sm-12">'
                        html+='<canvas id=cc_agent_'+el.agent+'></canvas>'
                    html+='</div>'
                html+="</div>";
            }
        });
        $('.by-operator').html(html);
        Q.forEach(function(el) {
            if(el.agent) {
                // $("[data-op='"+el.agent+"'] .Q .min").text(el.minQ);
                // $("[data-op='"+el.agent+"'] .Q .avg").text(toHHMMSS(Math.floor(el.avgQ)));
                // $("[data-op='"+el.agent+"'] .Q .max").text(el.maxQ);
            }
        }) 
        L.forEach(function(el) {
            if(el.agent) {
                $("[data-op='"+el.agent+"'] .L .min").text(el.minL);
                $("[data-op='"+el.agent+"'] .L .avg").text(toHHMMSS(Math.floor(el.avgL)));
                $("[data-op='"+el.agent+"'] .L .max").text(el.maxL);
            }
        }) 

    }

    function AVGtimes(timesA , timesL) {

        $('.queueTimes .avg.time').text(toHHMMSS(Math.floor(timesA.avgQ.avgQ)));
        $('.queueTimes .min.time').text(timesA.minQ.avgQ);
        $('.queueTimes .max.time').text(timesA.maxQ.avgQ);

    
        var la = 0  , lmx = 0 , lmn = 0,
        aMAX = [],
        aMIN = [],
        aAVG = [];

       
        timesL.maxL.forEach(el => {
            if(el.avgL) {
                var hms = el.avgL;   // your input string
                var a = hms.split(':'); // split it at the colons
                var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]); 
                aMAX.push(seconds)
                console.log(seconds)      
            }
        });
        timesL.minL.forEach(el=>{
            if(el.avgL) {
                var hms = el.avgL;   // your input string
                var a = hms.split(':'); // split it at the colons
                var seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]); 
                aMIN.push(seconds)      
                console.log(seconds)
            }
        })
        timesL.avgL.forEach(el => {
            if(el.avgL) {
                aAVG.push(el.avgL);
            }
        });
        var ALLAVG =0;
        aAVG.forEach(function(el) {
            ALLAVG+=Number(el);
        })
        ALLAVG = ALLAVG/aAVG.length;
        

    console.log(aAVG)
        $('.callTimes .avg.time').text(toHHMMSS(Math.floor(ALLAVG)));
        $('.callTimes .min.time').text(toHHMMSS(Math.min.apply(null,aMIN)));
        $('.callTimes .max.time').text(toHHMMSS(Math.max.apply(null,aMAX)));
    }

    $(document).ready(function() {

        $('.langch[data-lang="<?php echo $_SESSION['locale'] ?>"]').addClass('active').addClass('focus');
        $('.langch').click(function () {
            $.ajax({
                type:'POST',
                url:'./setlocale.php',
                data:{local:$(this).data('lang')},
                dataType:'json',
                success:function(response) {
                    location.reload();
                    console.log(response);
                }
            })
        });


        jQuery.datetimepicker.setLocale('<?php echo $_SESSION['locale']?>');
        jQuery('#picStart').datetimepicker({
            dayOfWeekStart:1
        });

        jQuery('#picEnd').datetimepicker({
            dayOfWeekStart:1
        });
        
        var db = new Date().setHours(0,0,0);
        var de = new Date();
        if(new Date(de).getTime() > new Date(db).getTime()) {
            $.ajax({
                type:'POST',
                data:{db:toDT(new Date(new Date(db))) , de:toDT(new Date(new Date(de))) , interval:'hour'},
                success:function(response) {
                    console.log(response);
                    var counts = response.allCalls;
                    var operators = response.byOperCall;
                    var Qoper = response.byOperAVGO;
                    var Loper = response.longByO;
                    var timesA = response.avgQ;
                    var timesL = response.longAll; 
                    $('.counts .info .count').text(counts.allCalls);
                    $('.counts .info .count-success').text(Number(counts.confirm));
                    $('.counts .info .count-fail').text(Number(counts.loss));
                    $('.counts .info .count-wl').text(counts.wellcomeLoss);
                    $('.counts .info .count-ql').text(Number(counts.lossInQueue))
                    $('.counts .info .count-nwt').text(Number(counts.lossNotWorktime))
                    AVGtimes(timesA , timesL);
                    byOperator(operators ,Qoper , Loper)
                    renderAgents(operators);
                    
                    $('#picStart').val(toDT(new Date(db))); 
                    renderAll(operators , counts);
                    $('#picEnd').val(toDT(new Date(de)));
                }
            })
        }
        else  {

        }
        
        
    });

    $(document).delegate('.intervals-button button' , 'click' , function() {
        var interval = $(this).data('interval');
        var diff = $(this).data('diff');
        var db = new Date();
        var de = new Date();
        // if($(this).prop('id') == 'day') {
            var db = new Date().setHours(0,0);
            var de = new Date().setHours(23,59);
        // }
        
        db =  parseInt(new Date(db).getTime()) - parseInt(diff*1000);
        console.log(db);
        if(new Date(de).getTime() > new Date(db).getTime()) {
            $.ajax({
                type:'POST',
                data:{db:toDT(new Date(new Date(db))) , de:toDT(new Date(new Date(de))) , interval:interval},
                success:function(response) {
                    console.log(response);
                    var counts = response.allCalls;
                    var operators = response.byOperCall;
                    var Qoper = response.byOperAVGO;
                    var Loper = response.longByO;
                    var timesA = response.avgQ;
                    var timesL = response.longAll; 
                    $('.counts .info .count').text(counts.allCalls);
                    $('.counts .info .count-success').text(Number(counts.confirm));
                    $('.counts .info .count-fail').text(Number(counts.loss));
                    $('.counts .info .count-wl').text(counts.wellcomeLoss);
                    $('.counts .info .count-ql').text(Number(counts.lossInQueue))
                    $('.counts .info .count-nwt').text(Number(counts.lossNotWorktime))
                    AVGtimes(timesA , timesL);                
                    byOperator(operators ,Qoper , Loper)
                    renderAgents(operators);
                    renderAll(operators , counts);
                    $('#picStart').val(toDT(new Date(db))); 
                    $('#picEnd').val(toDT(new Date(de)));
                }
            })
        }
        else {

        }
        
    })

    $(document).delegate('#search' , 'click' , function() {
        var db = new Date($('#picStart').val());
            db = new Date(db);
        var de = new Date($('#picEnd').val());
            de = new Date(de);
        var interval ='day';

        var operators = [];

         if(new Date(de).getTime() > new Date(db).getTime()) {
            $.ajax({
                type:'POST',
                data:{db:toDT(new Date(new Date(db))) , de:toDT(new Date(new Date(de))) , interval:interval},
                success:function(response) {
                    console.log(response);
                    var counts = response.allCalls;
                    var operators = response.byOperCall;
                    var Qoper = response.byOperAVGO;
                    var Loper = response.longByO;
                    var timesA = response.avgQ;
                    var timesL = response.longAll; 
                    $('.counts .info .count').text(counts.allCalls);
                    $('.counts .info .count-success').text(Number(counts.confirm));
                    $('.counts .info .count-fail').text(Number(counts.loss));
                    $('.counts .info .count-wl').text(counts.wellcomeLoss);
                    $('.counts .info .count-ql').text(Number(counts.lossInQueue));
                    $('.counts .info .count-nwt').text(Number(counts.lossNotWorktime));
                    AVGtimes(timesA , timesL);
                    byOperator(operators ,Qoper , Loper)
                    renderAgents(operators);
                    renderAll(operators , counts);
                    $('#picStart').val(toDT(new Date(db))); 
                    $('#picEnd').val(toDT(new Date(de))); 
                }
            });
        }
        
    });
    
    var renderAgents = function(data) {
        
        data.forEach(el=> {
            if(el.agent!=null) {
                // $('[data-op="'+el.agent+'"] .d').html('');
                // $('[data-op="'+el.agent+'"] .d').html('<Canvas id="cc_agent_'+el.agent+'"></canvas>');
                
                var ctx = document.getElementById('cc_agent_'+el.agent).getContext('2d');
                var chart = new Chart(ctx, {
                    type:'pie',
                    data:{
                        labels:["<?php echo $trans->proccall ?>" ,"<?php echo $trans->loss?>"],
                        datasets:[
                            {
                                data:[el.confirm,el.loss],
                                backgroundColor:["#008000" , "#B22222"],
                                borderWidth:0
                            }
                        ]
                    },
                    options:{
                        plugins: {
                            labels: {
                                render: 'percentage',
                                fontColor:'#eee',
                                precision: 2
                            },
                        },
                    }
                });
            }
        });
    }

    var renderAll = function(dataAgent , allCalls) {
        var labels_cc_agent = [];
        var data_cc_agent = [];
        
        dataAgent.forEach(el => {
            if(el.agent) {
                labels_cc_agent.push(el.agent);
                data_cc_agent.push(el.allCalls);
            }   
        });

        

        var config_cc_all = {
            type:'pie',
            data: {
                labels: ["<?php echo $trans->serviced ?>" , "<?php echo $trans->lossinqueue_ ?>" , "<?php echo $trans->losswelcome ?>" ,"<?php  echo $trans->lossinnotworktime?>"],
                datasets: [
                    {
                        data:[
                            allCalls.confirm,
                            allCalls.lossInQueue,
                            allCalls.wellcomeLoss,
                            allCalls.lossNotWorktime
                        ],
                        backgroundColor:[
                            "#008000",
                            "#B22222",
                            "#FF8C00",
                            "#DAA520",
                            "#F0E68C"
                        ],
                        borderWidth:1
                    }
                ]
            },
            options:{
                
                plugins: {
                    labels: {
                        render: 'percentage',
                        fontColor:'#eee',
                        precision: 2
                    },
                },
                
                                
                legend:{ 
                    position:'left'
                },
                title:{
                    text:"<?php  echo $trans->allstatecalls?>",
                    display:true,
                }
            }
        };

        var config_cc_agent = {
            type:'pie',
            data: {
                labels: labels_cc_agent,
                datasets: [
                    {
                        data: data_cc_agent,
                        backgroundColor: [
                            '#6D9BC3',
                            '#CD607E',
                            '#6EAEA1',
                            '#DA2C43',
                            "#4997D0",
                            "#6B3FA0",
                            "#CEC8EF",
                            "#ABAD48"
                        ],
                        borderWidth:1
                        
                    }
                    
                ]
            },
            options:{
                plugins: {
                    labels: {
                        render: 'percentage',
                        fontColor:'#eee',
                        precision: 2
                    },
                },
                legend: {
                    // display: true,
                    position:'left',
                    align:'start',
                },
                title: {
                   
                    display: true,
                    text: '<?php  echo $trans->countcallbyops?>'
                }
            }
        };


        $('div.cc_all').html('<canvas id="cc_all"></canvas>');
        $('div.cc_agent').html('<canvas id="cc_agent"></canvas>');
        var ctx_cc_all = document.getElementById('cc_all').getContext('2d');
        var ctx_cc_agent = document.getElementById('cc_agent').getContext('2d');

        var chart_cc_all = new Chart(ctx_cc_all , config_cc_all);
        var chart_cc_agent = new Chart(ctx_cc_agent , config_cc_agent);
    }

    $(document).delegate('.ref-count' , 'click' ,function () {
        var param = $(this).data('param');
        var db = $('#picStart').val();
        var de = $('#picEnd').val();
        location.replace(`./cc_state.php?redirect=1&db=${db}&de=${de}&${param}`);
    });

</script>
</body>
</html>