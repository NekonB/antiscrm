function pad(t){var e=t+"";return e.length<2?"0"+e:e}function randomDate(t,e){return new Date(t.getTime()+Math.random()*(e.getTime()-t.getTime()))}function randomTime(){var t=Math.floor(500*Math.random()),e=pad(1+Math.floor(t/60)%12),n=pad(t%60);return e+":"+n}!function(t,e){"use strict";"object"==typeof module&&"object"==typeof module.exports?module.exports=t.document?e(t,!0):function(t){if(!t.document)throw new Error("jQuery requires a window with a document");return e(t)}:e(t)}("undefined"!=typeof window?window:this,function(t,e){"use strict";function n(t,e,n){n=n||lt;var i,r,o=n.createElement("script");if(o.text=t,e)for(i in wt)r=e[i]||e.getAttribute&&e.getAttribute(i),r&&o.setAttribute(i,r);n.head.appendChild(o).parentNode.removeChild(o)}function i(t){return null==t?t+"":"object"==typeof t||"function"==typeof t?pt[gt.call(t)]||"object":typeof t}function r(t){var e=!!t&&"length"in t&&t.length,n=i(t);return!_t(t)&&!xt(t)&&("array"===n||0===e||"number"==typeof e&&e>0&&e-1 in t)}function o(t,e){return t.nodeName&&t.nodeName.toLowerCase()===e.toLowerCase()}function a(t,e,n){return _t(e)?St.grep(t,function(t,i){return!!e.call(t,i,t)!==n}):e.nodeType?St.grep(t,function(t){return t===e!==n}):"string"!=typeof e?St.grep(t,function(t){return ft.call(e,t)>-1!==n}):St.filter(e,t,n)}function s(t,e){for(;(t=t[e])&&1!==t.nodeType;);return t}function l(t){var e={};return St.each(t.match(Ft)||[],function(t,n){e[n]=!0}),e}function u(t){return t}function c(t){throw t}function h(t,e,n,i){var r;try{t&&_t(r=t.promise)?r.call(t).done(e).fail(n):t&&_t(r=t.then)?r.call(t,e,n):e.apply(void 0,[t].slice(i))}catch(t){n.apply(void 0,[t])}}function d(){lt.removeEventListener("DOMContentLoaded",d),t.removeEventListener("load",d),St.ready()}function f(t,e){return e.toUpperCase()}function p(t){return t.replace(Wt,"ms-").replace(Bt,f)}function g(){this.expando=St.expando+g.uid++}function m(t){return"true"===t||"false"!==t&&("null"===t?null:t===+t+""?+t:zt.test(t)?JSON.parse(t):t)}function v(t,e,n){var i;if(void 0===n&&1===t.nodeType)if(i="data-"+e.replace(Yt,"-$&").toLowerCase(),n=t.getAttribute(i),"string"==typeof n){try{n=m(n)}catch(r){}qt.set(t,e,n)}else n=void 0;return n}function y(t,e,n,i){var r,o,a=20,s=i?function(){return i.cur()}:function(){return St.css(t,e,"")},l=s(),u=n&&n[3]||(St.cssNumber[e]?"":"px"),c=t.nodeType&&(St.cssNumber[e]||"px"!==u&&+l)&&Xt.exec(St.css(t,e));if(c&&c[3]!==u){for(l/=2,u=u||c[3],c=+l||1;a--;)St.style(t,e,c+u),(1-o)*(1-(o=s()/l||.5))<=0&&(a=0),c/=o;c=2*c,St.style(t,e,c+u),n=n||[]}return n&&(c=+c||+l||0,r=n[1]?c+(n[1]+1)*n[2]:+n[2],i&&(i.unit=u,i.start=c,i.end=r)),r}function b(t){var e,n=t.ownerDocument,i=t.nodeName,r=ee[i];return r?r:(e=n.body.appendChild(n.createElement(i)),r=St.css(e,"display"),e.parentNode.removeChild(e),"none"===r&&(r="block"),ee[i]=r,r)}function _(t,e){for(var n,i,r=[],o=0,a=t.length;o<a;o++)i=t[o],i.style&&(n=i.style.display,e?("none"===n&&(r[o]=Ut.get(i,"display")||null,r[o]||(i.style.display="")),""===i.style.display&&Zt(i)&&(r[o]=b(i))):"none"!==n&&(r[o]="none",Ut.set(i,"display",n)));for(o=0;o<a;o++)null!=r[o]&&(t[o].style.display=r[o]);return t}function x(t,e){var n;return n="undefined"!=typeof t.getElementsByTagName?t.getElementsByTagName(e||"*"):"undefined"!=typeof t.querySelectorAll?t.querySelectorAll(e||"*"):[],void 0===e||e&&o(t,e)?St.merge([t],n):n}function w(t,e){for(var n=0,i=t.length;n<i;n++)Ut.set(t[n],"globalEval",!e||Ut.get(e[n],"globalEval"))}function D(t,e,n,r,o){for(var a,s,l,u,c,h,d=e.createDocumentFragment(),f=[],p=0,g=t.length;p<g;p++)if(a=t[p],a||0===a)if("object"===i(a))St.merge(f,a.nodeType?[a]:a);else if(ae.test(a)){for(s=s||d.appendChild(e.createElement("div")),l=(ie.exec(a)||["",""])[1].toLowerCase(),u=oe[l]||oe._default,s.innerHTML=u[1]+St.htmlPrefilter(a)+u[2],h=u[0];h--;)s=s.lastChild;St.merge(f,s.childNodes),s=d.firstChild,s.textContent=""}else f.push(e.createTextNode(a));for(d.textContent="",p=0;a=f[p++];)if(r&&St.inArray(a,r)>-1)o&&o.push(a);else if(c=Qt(a),s=x(d.appendChild(a),"script"),c&&w(s),n)for(h=0;a=s[h++];)re.test(a.type||"")&&n.push(a);return d}function S(){return!0}function C(){return!1}function T(t,e){return t===k()==("focus"===e)}function k(){try{return lt.activeElement}catch(t){}}function A(t,e,n,i,r,o){var a,s;if("object"==typeof e){"string"!=typeof n&&(i=i||n,n=void 0);for(s in e)A(t,s,n,i,e[s],o);return t}if(null==i&&null==r?(r=n,i=n=void 0):null==r&&("string"==typeof n?(r=i,i=void 0):(r=i,i=n,n=void 0)),r===!1)r=C;else if(!r)return t;return 1===o&&(a=r,r=function(t){return St().off(t),a.apply(this,arguments)},r.guid=a.guid||(a.guid=St.guid++)),t.each(function(){St.event.add(this,e,r,i,n)})}function I(t,e,n){return n?(Ut.set(t,e,!1),void St.event.add(t,e,{namespace:!1,handler:function(t){var i,r,o=Ut.get(this,e);if(1&t.isTrigger&&this[e]){if(o.length)(St.event.special[e]||{}).delegateType&&t.stopPropagation();else if(o=ct.call(arguments),Ut.set(this,e,o),i=n(this,e),this[e](),r=Ut.get(this,e),o!==r||i?Ut.set(this,e,!1):r={},o!==r)return t.stopImmediatePropagation(),t.preventDefault(),r.value}else o.length&&(Ut.set(this,e,{value:St.event.trigger(St.extend(o[0],St.Event.prototype),o.slice(1),this)}),t.stopImmediatePropagation())}})):void(void 0===Ut.get(t,e)&&St.event.add(t,e,S))}function E(t,e){return o(t,"table")&&o(11!==e.nodeType?e:e.firstChild,"tr")?St(t).children("tbody")[0]||t:t}function O(t){return t.type=(null!==t.getAttribute("type"))+"/"+t.type,t}function M(t){return"true/"===(t.type||"").slice(0,5)?t.type=t.type.slice(5):t.removeAttribute("type"),t}function P(t,e){var n,i,r,o,a,s,l,u;if(1===e.nodeType){if(Ut.hasData(t)&&(o=Ut.access(t),a=Ut.set(e,o),u=o.events)){delete a.handle,a.events={};for(r in u)for(n=0,i=u[r].length;n<i;n++)St.event.add(e,r,u[r][n])}qt.hasData(t)&&(s=qt.access(t),l=St.extend({},s),qt.set(e,l))}}function N(t,e){var n=e.nodeName.toLowerCase();"input"===n&&ne.test(t.type)?e.checked=t.checked:"input"!==n&&"textarea"!==n||(e.defaultValue=t.defaultValue)}function L(t,e,i,r){e=ht.apply([],e);var o,a,s,l,u,c,h=0,d=t.length,f=d-1,p=e[0],g=_t(p);if(g||d>1&&"string"==typeof p&&!bt.checkClone&&de.test(p))return t.each(function(n){var o=t.eq(n);g&&(e[0]=p.call(this,n,o.html())),L(o,e,i,r)});if(d&&(o=D(e,t[0].ownerDocument,!1,t,r),a=o.firstChild,1===o.childNodes.length&&(o=a),a||r)){for(s=St.map(x(o,"script"),O),l=s.length;h<d;h++)u=o,h!==f&&(u=St.clone(u,!0,!0),l&&St.merge(s,x(u,"script"))),i.call(t[h],u,h);if(l)for(c=s[s.length-1].ownerDocument,St.map(s,M),h=0;h<l;h++)u=s[h],re.test(u.type||"")&&!Ut.access(u,"globalEval")&&St.contains(c,u)&&(u.src&&"module"!==(u.type||"").toLowerCase()?St._evalUrl&&!u.noModule&&St._evalUrl(u.src,{nonce:u.nonce||u.getAttribute("nonce")}):n(u.textContent.replace(fe,""),u,c))}return t}function F(t,e,n){for(var i,r=e?St.filter(e,t):t,o=0;null!=(i=r[o]);o++)n||1!==i.nodeType||St.cleanData(x(i)),i.parentNode&&(n&&Qt(i)&&w(x(i,"script")),i.parentNode.removeChild(i));return t}function R(t,e,n){var i,r,o,a,s=t.style;return n=n||ge(t),n&&(a=n.getPropertyValue(e)||n[e],""!==a||Qt(t)||(a=St.style(t,e)),!bt.pixelBoxStyles()&&pe.test(a)&&me.test(e)&&(i=s.width,r=s.minWidth,o=s.maxWidth,s.minWidth=s.maxWidth=s.width=a,a=n.width,s.width=i,s.minWidth=r,s.maxWidth=o)),void 0!==a?a+"":a}function j(t,e){return{get:function(){return t()?void delete this.get:(this.get=e).apply(this,arguments)}}}function H(t){for(var e=t[0].toUpperCase()+t.slice(1),n=ve.length;n--;)if(t=ve[n]+e,t in ye)return t}function W(t){var e=St.cssProps[t]||be[t];return e?e:t in ye?t:be[t]=H(t)||t}function B(t,e,n){var i=Xt.exec(e);return i?Math.max(0,i[2]-(n||0))+(i[3]||"px"):e}function V(t,e,n,i,r,o){var a="width"===e?1:0,s=0,l=0;if(n===(i?"border":"content"))return 0;for(;a<4;a+=2)"margin"===n&&(l+=St.css(t,n+Kt[a],!0,r)),i?("content"===n&&(l-=St.css(t,"padding"+Kt[a],!0,r)),"margin"!==n&&(l-=St.css(t,"border"+Kt[a]+"Width",!0,r))):(l+=St.css(t,"padding"+Kt[a],!0,r),"padding"!==n?l+=St.css(t,"border"+Kt[a]+"Width",!0,r):s+=St.css(t,"border"+Kt[a]+"Width",!0,r));return!i&&o>=0&&(l+=Math.max(0,Math.ceil(t["offset"+e[0].toUpperCase()+e.slice(1)]-o-l-s-.5))||0),l}function U(t,e,n){var i=ge(t),r=!bt.boxSizingReliable()||n,o=r&&"border-box"===St.css(t,"boxSizing",!1,i),a=o,s=R(t,e,i),l="offset"+e[0].toUpperCase()+e.slice(1);if(pe.test(s)){if(!n)return s;s="auto"}return(!bt.boxSizingReliable()&&o||"auto"===s||!parseFloat(s)&&"inline"===St.css(t,"display",!1,i))&&t.getClientRects().length&&(o="border-box"===St.css(t,"boxSizing",!1,i),a=l in t,a&&(s=t[l])),s=parseFloat(s)||0,s+V(t,e,n||(o?"border":"content"),a,i,s)+"px"}function q(t,e,n,i,r){return new q.prototype.init(t,e,n,i,r)}function z(){Ce&&(lt.hidden===!1&&t.requestAnimationFrame?t.requestAnimationFrame(z):t.setTimeout(z,St.fx.interval),St.fx.tick())}function Y(){return t.setTimeout(function(){Se=void 0}),Se=Date.now()}function G(t,e){var n,i=0,r={height:t};for(e=e?1:0;i<4;i+=2-e)n=Kt[i],r["margin"+n]=r["padding"+n]=t;return e&&(r.opacity=r.width=t),r}function X(t,e,n){for(var i,r=(Q.tweeners[e]||[]).concat(Q.tweeners["*"]),o=0,a=r.length;o<a;o++)if(i=r[o].call(n,e,t))return i}function K(t,e,n){var i,r,o,a,s,l,u,c,h="width"in e||"height"in e,d=this,f={},p=t.style,g=t.nodeType&&Zt(t),m=Ut.get(t,"fxshow");n.queue||(a=St._queueHooks(t,"fx"),null==a.unqueued&&(a.unqueued=0,s=a.empty.fire,a.empty.fire=function(){a.unqueued||s()}),a.unqueued++,d.always(function(){d.always(function(){a.unqueued--,St.queue(t,"fx").length||a.empty.fire()})}));for(i in e)if(r=e[i],Te.test(r)){if(delete e[i],o=o||"toggle"===r,r===(g?"hide":"show")){if("show"!==r||!m||void 0===m[i])continue;g=!0}f[i]=m&&m[i]||St.style(t,i)}if(l=!St.isEmptyObject(e),l||!St.isEmptyObject(f)){h&&1===t.nodeType&&(n.overflow=[p.overflow,p.overflowX,p.overflowY],u=m&&m.display,null==u&&(u=Ut.get(t,"display")),c=St.css(t,"display"),"none"===c&&(u?c=u:(_([t],!0),u=t.style.display||u,c=St.css(t,"display"),_([t]))),("inline"===c||"inline-block"===c&&null!=u)&&"none"===St.css(t,"float")&&(l||(d.done(function(){p.display=u}),null==u&&(c=p.display,u="none"===c?"":c)),p.display="inline-block")),n.overflow&&(p.overflow="hidden",d.always(function(){p.overflow=n.overflow[0],p.overflowX=n.overflow[1],p.overflowY=n.overflow[2]})),l=!1;for(i in f)l||(m?"hidden"in m&&(g=m.hidden):m=Ut.access(t,"fxshow",{display:u}),o&&(m.hidden=!g),g&&_([t],!0),d.done(function(){g||_([t]),Ut.remove(t,"fxshow");for(i in f)St.style(t,i,f[i])})),l=X(g?m[i]:0,i,d),i in m||(m[i]=l.start,g&&(l.end=l.start,l.start=0))}}function $(t,e){var n,i,r,o,a;for(n in t)if(i=p(n),r=e[i],o=t[n],Array.isArray(o)&&(r=o[1],o=t[n]=o[0]),n!==i&&(t[i]=o,delete t[n]),a=St.cssHooks[i],a&&"expand"in a){o=a.expand(o),delete t[i];for(n in o)n in t||(t[n]=o[n],e[n]=r)}else e[i]=r}function Q(t,e,n){var i,r,o=0,a=Q.prefilters.length,s=St.Deferred().always(function(){delete l.elem}),l=function(){if(r)return!1;for(var e=Se||Y(),n=Math.max(0,u.startTime+u.duration-e),i=n/u.duration||0,o=1-i,a=0,l=u.tweens.length;a<l;a++)u.tweens[a].run(o);return s.notifyWith(t,[u,o,n]),o<1&&l?n:(l||s.notifyWith(t,[u,1,0]),s.resolveWith(t,[u]),!1)},u=s.promise({elem:t,props:St.extend({},e),opts:St.extend(!0,{specialEasing:{},easing:St.easing._default},n),originalProperties:e,originalOptions:n,startTime:Se||Y(),duration:n.duration,tweens:[],createTween:function(e,n){var i=St.Tween(t,u.opts,e,n,u.opts.specialEasing[e]||u.opts.easing);return u.tweens.push(i),i},stop:function(e){var n=0,i=e?u.tweens.length:0;if(r)return this;for(r=!0;n<i;n++)u.tweens[n].run(1);return e?(s.notifyWith(t,[u,1,0]),s.resolveWith(t,[u,e])):s.rejectWith(t,[u,e]),this}}),c=u.props;for($(c,u.opts.specialEasing);o<a;o++)if(i=Q.prefilters[o].call(u,t,c,u.opts))return _t(i.stop)&&(St._queueHooks(u.elem,u.opts.queue).stop=i.stop.bind(i)),i;return St.map(c,X,u),_t(u.opts.start)&&u.opts.start.call(t,u),u.progress(u.opts.progress).done(u.opts.done,u.opts.complete).fail(u.opts.fail).always(u.opts.always),St.fx.timer(St.extend(l,{elem:t,anim:u,queue:u.opts.queue})),u}function J(t){var e=t.match(Ft)||[];return e.join(" ")}function Z(t){return t.getAttribute&&t.getAttribute("class")||""}function tt(t){return Array.isArray(t)?t:"string"==typeof t?t.match(Ft)||[]:[]}function et(t,e,n,r){var o;if(Array.isArray(e))St.each(e,function(e,i){n||je.test(t)?r(t,i):et(t+"["+("object"==typeof i&&null!=i?e:"")+"]",i,n,r)});else if(n||"object"!==i(e))r(t,e);else for(o in e)et(t+"["+o+"]",e[o],n,r)}function nt(t){return function(e,n){"string"!=typeof e&&(n=e,e="*");var i,r=0,o=e.toLowerCase().match(Ft)||[];if(_t(n))for(;i=o[r++];)"+"===i[0]?(i=i.slice(1)||"*",(t[i]=t[i]||[]).unshift(n)):(t[i]=t[i]||[]).push(n)}}function it(t,e,n,i){function r(s){var l;return o[s]=!0,St.each(t[s]||[],function(t,s){var u=s(e,n,i);return"string"!=typeof u||a||o[u]?a?!(l=u):void 0:(e.dataTypes.unshift(u),r(u),!1)}),l}var o={},a=t===$e;return r(e.dataTypes[0])||!o["*"]&&r("*")}function rt(t,e){var n,i,r=St.ajaxSettings.flatOptions||{};for(n in e)void 0!==e[n]&&((r[n]?t:i||(i={}))[n]=e[n]);return i&&St.extend(!0,t,i),t}function ot(t,e,n){for(var i,r,o,a,s=t.contents,l=t.dataTypes;"*"===l[0];)l.shift(),void 0===i&&(i=t.mimeType||e.getResponseHeader("Content-Type"));if(i)for(r in s)if(s[r]&&s[r].test(i)){l.unshift(r);break}if(l[0]in n)o=l[0];else{for(r in n){if(!l[0]||t.converters[r+" "+l[0]]){o=r;break}a||(a=r)}o=o||a}if(o)return o!==l[0]&&l.unshift(o),n[o]}function at(t,e,n,i){var r,o,a,s,l,u={},c=t.dataTypes.slice();if(c[1])for(a in t.converters)u[a.toLowerCase()]=t.converters[a];for(o=c.shift();o;)if(t.responseFields[o]&&(n[t.responseFields[o]]=e),!l&&i&&t.dataFilter&&(e=t.dataFilter(e,t.dataType)),l=o,o=c.shift())if("*"===o)o=l;else if("*"!==l&&l!==o){if(a=u[l+" "+o]||u["* "+o],!a)for(r in u)if(s=r.split(" "),s[1]===o&&(a=u[l+" "+s[0]]||u["* "+s[0]])){a===!0?a=u[r]:u[r]!==!0&&(o=s[0],c.unshift(s[1]));break}if(a!==!0)if(a&&t["throws"])e=a(e);else try{e=a(e)}catch(h){return{state:"parsererror",error:a?h:"No conversion from "+l+" to "+o}}}return{state:"success",data:e}}var st=[],lt=t.document,ut=Object.getPrototypeOf,ct=st.slice,ht=st.concat,dt=st.push,ft=st.indexOf,pt={},gt=pt.toString,mt=pt.hasOwnProperty,vt=mt.toString,yt=vt.call(Object),bt={},_t=function(t){return"function"==typeof t&&"number"!=typeof t.nodeType},xt=function(t){return null!=t&&t===t.window},wt={type:!0,src:!0,nonce:!0,noModule:!0},Dt="3.4.1",St=function(t,e){return new St.fn.init(t,e)},Ct=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;St.fn=St.prototype={jquery:Dt,constructor:St,length:0,toArray:function(){return ct.call(this)},get:function(t){return null==t?ct.call(this):t<0?this[t+this.length]:this[t]},pushStack:function(t){var e=St.merge(this.constructor(),t);return e.prevObject=this,e},each:function(t){return St.each(this,t)},map:function(t){return this.pushStack(St.map(this,function(e,n){return t.call(e,n,e)}))},slice:function(){return this.pushStack(ct.apply(this,arguments))},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},eq:function(t){var e=this.length,n=+t+(t<0?e:0);return this.pushStack(n>=0&&n<e?[this[n]]:[])},end:function(){return this.prevObject||this.constructor()},push:dt,sort:st.sort,splice:st.splice},St.extend=St.fn.extend=function(){var t,e,n,i,r,o,a=arguments[0]||{},s=1,l=arguments.length,u=!1;for("boolean"==typeof a&&(u=a,a=arguments[s]||{},s++),"object"==typeof a||_t(a)||(a={}),s===l&&(a=this,s--);s<l;s++)if(null!=(t=arguments[s]))for(e in t)i=t[e],"__proto__"!==e&&a!==i&&(u&&i&&(St.isPlainObject(i)||(r=Array.isArray(i)))?(n=a[e],o=r&&!Array.isArray(n)?[]:r||St.isPlainObject(n)?n:{},r=!1,a[e]=St.extend(u,o,i)):void 0!==i&&(a[e]=i));return a},St.extend({expando:"jQuery"+(Dt+Math.random()).replace(/\D/g,""),isReady:!0,error:function(t){throw new Error(t)},noop:function(){},isPlainObject:function(t){var e,n;return!(!t||"[object Object]"!==gt.call(t))&&(!(e=ut(t))||(n=mt.call(e,"constructor")&&e.constructor,"function"==typeof n&&vt.call(n)===yt))},isEmptyObject:function(t){var e;for(e in t)return!1;return!0},globalEval:function(t,e){n(t,{nonce:e&&e.nonce})},each:function(t,e){var n,i=0;if(r(t))for(n=t.length;i<n&&e.call(t[i],i,t[i])!==!1;i++);else for(i in t)if(e.call(t[i],i,t[i])===!1)break;return t},trim:function(t){return null==t?"":(t+"").replace(Ct,"")},makeArray:function(t,e){var n=e||[];return null!=t&&(r(Object(t))?St.merge(n,"string"==typeof t?[t]:t):dt.call(n,t)),n},inArray:function(t,e,n){return null==e?-1:ft.call(e,t,n)},merge:function(t,e){for(var n=+e.length,i=0,r=t.length;i<n;i++)t[r++]=e[i];return t.length=r,t},grep:function(t,e,n){for(var i,r=[],o=0,a=t.length,s=!n;o<a;o++)i=!e(t[o],o),i!==s&&r.push(t[o]);return r},map:function(t,e,n){var i,o,a=0,s=[];if(r(t))for(i=t.length;a<i;a++)o=e(t[a],a,n),null!=o&&s.push(o);else for(a in t)o=e(t[a],a,n),null!=o&&s.push(o);return ht.apply([],s)},guid:1,support:bt}),"function"==typeof Symbol&&(St.fn[Symbol.iterator]=st[Symbol.iterator]),St.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "),function(t,e){pt["[object "+e+"]"]=e.toLowerCase()});var Tt=function(t){function e(t,e,n,i){var r,o,a,s,l,u,c,d=e&&e.ownerDocument,p=e?e.nodeType:9;if(n=n||[],"string"!=typeof t||!t||1!==p&&9!==p&&11!==p)return n;if(!i&&((e?e.ownerDocument||e:B)!==P&&M(e),e=e||P,L)){if(11!==p&&(l=bt.exec(t)))if(r=l[1]){if(9===p){if(!(a=e.getElementById(r)))return n;if(a.id===r)return n.push(a),n}else if(d&&(a=d.getElementById(r))&&H(e,a)&&a.id===r)return n.push(a),n}else{if(l[2])return Z.apply(n,e.getElementsByTagName(t)),n;if((r=l[3])&&w.getElementsByClassName&&e.getElementsByClassName)return Z.apply(n,e.getElementsByClassName(r)),n}if(w.qsa&&!G[t+" "]&&(!F||!F.test(t))&&(1!==p||"object"!==e.nodeName.toLowerCase())){if(c=t,d=e,1===p&&ht.test(t)){for((s=e.getAttribute("id"))?s=s.replace(Dt,St):e.setAttribute("id",s=W),u=T(t),o=u.length;o--;)u[o]="#"+s+" "+f(u[o]);c=u.join(","),d=_t.test(t)&&h(e.parentNode)||e}try{return Z.apply(n,d.querySelectorAll(c)),n}catch(g){G(t,!0)}finally{s===W&&e.removeAttribute("id")}}}return A(t.replace(lt,"$1"),e,n,i)}function n(){function t(n,i){return e.push(n+" ")>D.cacheLength&&delete t[e.shift()],t[n+" "]=i}var e=[];return t}function i(t){return t[W]=!0,t}function r(t){var e=P.createElement("fieldset");try{return!!t(e)}catch(n){return!1}finally{e.parentNode&&e.parentNode.removeChild(e),e=null}}function o(t,e){for(var n=t.split("|"),i=n.length;i--;)D.attrHandle[n[i]]=e}function a(t,e){var n=e&&t,i=n&&1===t.nodeType&&1===e.nodeType&&t.sourceIndex-e.sourceIndex;if(i)return i;if(n)for(;n=n.nextSibling;)if(n===e)return-1;return t?1:-1}function s(t){return function(e){var n=e.nodeName.toLowerCase();return"input"===n&&e.type===t}}function l(t){return function(e){var n=e.nodeName.toLowerCase();return("input"===n||"button"===n)&&e.type===t}}function u(t){return function(e){return"form"in e?e.parentNode&&e.disabled===!1?"label"in e?"label"in e.parentNode?e.parentNode.disabled===t:e.disabled===t:e.isDisabled===t||e.isDisabled!==!t&&Tt(e)===t:e.disabled===t:"label"in e&&e.disabled===t}}function c(t){return i(function(e){return e=+e,i(function(n,i){for(var r,o=t([],n.length,e),a=o.length;a--;)n[r=o[a]]&&(n[r]=!(i[r]=n[r]))})})}function h(t){return t&&"undefined"!=typeof t.getElementsByTagName&&t}function d(){}function f(t){for(var e=0,n=t.length,i="";e<n;e++)i+=t[e].value;return i}function p(t,e,n){var i=e.dir,r=e.next,o=r||i,a=n&&"parentNode"===o,s=U++;return e.first?function(e,n,r){for(;e=e[i];)if(1===e.nodeType||a)return t(e,n,r);return!1}:function(e,n,l){var u,c,h,d=[V,s];if(l){for(;e=e[i];)if((1===e.nodeType||a)&&t(e,n,l))return!0}else for(;e=e[i];)if(1===e.nodeType||a)if(h=e[W]||(e[W]={}),c=h[e.uniqueID]||(h[e.uniqueID]={}),r&&r===e.nodeName.toLowerCase())e=e[i]||e;else{if((u=c[o])&&u[0]===V&&u[1]===s)return d[2]=u[2];if(c[o]=d,d[2]=t(e,n,l))return!0}return!1}}function g(t){return t.length>1?function(e,n,i){for(var r=t.length;r--;)if(!t[r](e,n,i))return!1;return!0}:t[0]}function m(t,n,i){for(var r=0,o=n.length;r<o;r++)e(t,n[r],i);return i}function v(t,e,n,i,r){for(var o,a=[],s=0,l=t.length,u=null!=e;s<l;s++)(o=t[s])&&(n&&!n(o,i,r)||(a.push(o),u&&e.push(s)));return a}function y(t,e,n,r,o,a){return r&&!r[W]&&(r=y(r)),o&&!o[W]&&(o=y(o,a)),i(function(i,a,s,l){var u,c,h,d=[],f=[],p=a.length,g=i||m(e||"*",s.nodeType?[s]:s,[]),y=!t||!i&&e?g:v(g,d,t,s,l),b=n?o||(i?t:p||r)?[]:a:y;if(n&&n(y,b,s,l),r)for(u=v(b,f),r(u,[],s,l),c=u.length;c--;)(h=u[c])&&(b[f[c]]=!(y[f[c]]=h));if(i){if(o||t){if(o){for(u=[],c=b.length;c--;)(h=b[c])&&u.push(y[c]=h);o(null,b=[],u,l)}for(c=b.length;c--;)(h=b[c])&&(u=o?et(i,h):d[c])>-1&&(i[u]=!(a[u]=h))}}else b=v(b===a?b.splice(p,b.length):b),o?o(null,a,b,l):Z.apply(a,b)})}function b(t){for(var e,n,i,r=t.length,o=D.relative[t[0].type],a=o||D.relative[" "],s=o?1:0,l=p(function(t){return t===e},a,!0),u=p(function(t){return et(e,t)>-1},a,!0),c=[function(t,n,i){var r=!o&&(i||n!==I)||((e=n).nodeType?l(t,n,i):u(t,n,i));return e=null,r}];s<r;s++)if(n=D.relative[t[s].type])c=[p(g(c),n)];else{if(n=D.filter[t[s].type].apply(null,t[s].matches),n[W]){for(i=++s;i<r&&!D.relative[t[i].type];i++);return y(s>1&&g(c),s>1&&f(t.slice(0,s-1).concat({value:" "===t[s-2].type?"*":""})).replace(lt,"$1"),n,s<i&&b(t.slice(s,i)),i<r&&b(t=t.slice(i)),i<r&&f(t))}c.push(n)}return g(c)}function _(t,n){var r=n.length>0,o=t.length>0,a=function(i,a,s,l,u){var c,h,d,f=0,p="0",g=i&&[],m=[],y=I,b=i||o&&D.find.TAG("*",u),_=V+=null==y?1:Math.random()||.1,x=b.length;for(u&&(I=a===P||a||u);p!==x&&null!=(c=b[p]);p++){if(o&&c){for(h=0,a||c.ownerDocument===P||(M(c),s=!L);d=t[h++];)if(d(c,a||P,s)){l.push(c);break}u&&(V=_)}r&&((c=!d&&c)&&f--,i&&g.push(c))}if(f+=p,r&&p!==f){for(h=0;d=n[h++];)d(g,m,a,s);if(i){if(f>0)for(;p--;)g[p]||m[p]||(m[p]=Q.call(l));m=v(m)}Z.apply(l,m),u&&!i&&m.length>0&&f+n.length>1&&e.uniqueSort(l)}return u&&(V=_,I=y),g};return r?i(a):a}var x,w,D,S,C,T,k,A,I,E,O,M,P,N,L,F,R,j,H,W="sizzle"+1*new Date,B=t.document,V=0,U=0,q=n(),z=n(),Y=n(),G=n(),X=function(t,e){return t===e&&(O=!0),0},K={}.hasOwnProperty,$=[],Q=$.pop,J=$.push,Z=$.push,tt=$.slice,et=function(t,e){for(var n=0,i=t.length;n<i;n++)if(t[n]===e)return n;return-1},nt="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",it="[\\x20\\t\\r\\n\\f]",rt="(?:\\\\.|[\\w-]|[^\0-\\xa0])+",ot="\\["+it+"*("+rt+")(?:"+it+"*([*^$|!~]?=)"+it+"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|("+rt+"))|)"+it+"*\\]",at=":("+rt+")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|"+ot+")*)|.*)\\)|)",st=new RegExp(it+"+","g"),lt=new RegExp("^"+it+"+|((?:^|[^\\\\])(?:\\\\.)*)"+it+"+$","g"),ut=new RegExp("^"+it+"*,"+it+"*"),ct=new RegExp("^"+it+"*([>+~]|"+it+")"+it+"*"),ht=new RegExp(it+"|>"),dt=new RegExp(at),ft=new RegExp("^"+rt+"$"),pt={ID:new RegExp("^#("+rt+")"),CLASS:new RegExp("^\\.("+rt+")"),TAG:new RegExp("^("+rt+"|[*])"),ATTR:new RegExp("^"+ot),PSEUDO:new RegExp("^"+at),CHILD:new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+it+"*(even|odd|(([+-]|)(\\d*)n|)"+it+"*(?:([+-]|)"+it+"*(\\d+)|))"+it+"*\\)|)","i"),bool:new RegExp("^(?:"+nt+")$","i"),needsContext:new RegExp("^"+it+"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+it+"*((?:-\\d)?\\d*)"+it+"*\\)|)(?=[^-]|$)","i")},gt=/HTML$/i,mt=/^(?:input|select|textarea|button)$/i,vt=/^h\d$/i,yt=/^[^{]+\{\s*\[native \w/,bt=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,_t=/[+~]/,xt=new RegExp("\\\\([\\da-f]{1,6}"+it+"?|("+it+")|.)","ig"),wt=function(t,e,n){var i="0x"+e-65536;return i!==i||n?e:i<0?String.fromCharCode(i+65536):String.fromCharCode(i>>10|55296,1023&i|56320)},Dt=/([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,St=function(t,e){return e?"\0"===t?"�":t.slice(0,-1)+"\\"+t.charCodeAt(t.length-1).toString(16)+" ":"\\"+t},Ct=function(){M()},Tt=p(function(t){return t.disabled===!0&&"fieldset"===t.nodeName.toLowerCase()},{dir:"parentNode",next:"legend"});try{Z.apply($=tt.call(B.childNodes),B.childNodes),$[B.childNodes.length].nodeType}catch(kt){Z={apply:$.length?function(t,e){J.apply(t,tt.call(e))}:function(t,e){for(var n=t.length,i=0;t[n++]=e[i++];);t.length=n-1}}}w=e.support={},C=e.isXML=function(t){var e=t.namespaceURI,n=(t.ownerDocument||t).documentElement;return!gt.test(e||n&&n.nodeName||"HTML")},M=e.setDocument=function(t){var e,n,i=t?t.ownerDocument||t:B;return i!==P&&9===i.nodeType&&i.documentElement?(P=i,N=P.documentElement,L=!C(P),B!==P&&(n=P.defaultView)&&n.top!==n&&(n.addEventListener?n.addEventListener("unload",Ct,!1):n.attachEvent&&n.attachEvent("onunload",Ct)),w.attributes=r(function(t){return t.className="i",!t.getAttribute("className")}),w.getElementsByTagName=r(function(t){return t.appendChild(P.createComment("")),!t.getElementsByTagName("*").length}),w.getElementsByClassName=yt.test(P.getElementsByClassName),w.getById=r(function(t){return N.appendChild(t).id=W,!P.getElementsByName||!P.getElementsByName(W).length}),w.getById?(D.filter.ID=function(t){var e=t.replace(xt,wt);return function(t){return t.getAttribute("id")===e}},D.find.ID=function(t,e){if("undefined"!=typeof e.getElementById&&L){var n=e.getElementById(t);return n?[n]:[]}}):(D.filter.ID=function(t){var e=t.replace(xt,wt);return function(t){var n="undefined"!=typeof t.getAttributeNode&&t.getAttributeNode("id");return n&&n.value===e}},D.find.ID=function(t,e){if("undefined"!=typeof e.getElementById&&L){var n,i,r,o=e.getElementById(t);if(o){if(n=o.getAttributeNode("id"),n&&n.value===t)return[o];for(r=e.getElementsByName(t),i=0;o=r[i++];)if(n=o.getAttributeNode("id"),n&&n.value===t)return[o]}return[]}}),D.find.TAG=w.getElementsByTagName?function(t,e){return"undefined"!=typeof e.getElementsByTagName?e.getElementsByTagName(t):w.qsa?e.querySelectorAll(t):void 0}:function(t,e){var n,i=[],r=0,o=e.getElementsByTagName(t);if("*"===t){for(;n=o[r++];)1===n.nodeType&&i.push(n);return i}return o},D.find.CLASS=w.getElementsByClassName&&function(t,e){if("undefined"!=typeof e.getElementsByClassName&&L)return e.getElementsByClassName(t)},R=[],F=[],(w.qsa=yt.test(P.querySelectorAll))&&(r(function(t){N.appendChild(t).innerHTML="<a id='"+W+"'></a><select id='"+W+"-\r\\' msallowcapture=''><option selected=''></option></select>",t.querySelectorAll("[msallowcapture^='']").length&&F.push("[*^$]="+it+"*(?:''|\"\")"),t.querySelectorAll("[selected]").length||F.push("\\["+it+"*(?:value|"+nt+")"),t.querySelectorAll("[id~="+W+"-]").length||F.push("~="),t.querySelectorAll(":checked").length||F.push(":checked"),t.querySelectorAll("a#"+W+"+*").length||F.push(".#.+[+~]")}),r(function(t){t.innerHTML="<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";var e=P.createElement("input");e.setAttribute("type","hidden"),t.appendChild(e).setAttribute("name","D"),t.querySelectorAll("[name=d]").length&&F.push("name"+it+"*[*^$|!~]?="),2!==t.querySelectorAll(":enabled").length&&F.push(":enabled",":disabled"),N.appendChild(t).disabled=!0,2!==t.querySelectorAll(":disabled").length&&F.push(":enabled",":disabled"),t.querySelectorAll("*,:x"),F.push(",.*:")})),(w.matchesSelector=yt.test(j=N.matches||N.webkitMatchesSelector||N.mozMatchesSelector||N.oMatchesSelector||N.msMatchesSelector))&&r(function(t){w.disconnectedMatch=j.call(t,"*"),j.call(t,"[s!='']:x"),R.push("!=",at)}),F=F.length&&new RegExp(F.join("|")),R=R.length&&new RegExp(R.join("|")),e=yt.test(N.compareDocumentPosition),H=e||yt.test(N.contains)?function(t,e){var n=9===t.nodeType?t.documentElement:t,i=e&&e.parentNode;return t===i||!(!i||1!==i.nodeType||!(n.contains?n.contains(i):t.compareDocumentPosition&&16&t.compareDocumentPosition(i)))}:function(t,e){if(e)for(;e=e.parentNode;)if(e===t)return!0;return!1},X=e?function(t,e){if(t===e)return O=!0,0;var n=!t.compareDocumentPosition-!e.compareDocumentPosition;return n?n:(n=(t.ownerDocument||t)===(e.ownerDocument||e)?t.compareDocumentPosition(e):1,1&n||!w.sortDetached&&e.compareDocumentPosition(t)===n?t===P||t.ownerDocument===B&&H(B,t)?-1:e===P||e.ownerDocument===B&&H(B,e)?1:E?et(E,t)-et(E,e):0:4&n?-1:1)}:function(t,e){if(t===e)return O=!0,0;var n,i=0,r=t.parentNode,o=e.parentNode,s=[t],l=[e];if(!r||!o)return t===P?-1:e===P?1:r?-1:o?1:E?et(E,t)-et(E,e):0;if(r===o)return a(t,e);for(n=t;n=n.parentNode;)s.unshift(n);for(n=e;n=n.parentNode;)l.unshift(n);for(;s[i]===l[i];)i++;return i?a(s[i],l[i]):s[i]===B?-1:l[i]===B?1:0},P):P},e.matches=function(t,n){return e(t,null,null,n)},e.matchesSelector=function(t,n){if((t.ownerDocument||t)!==P&&M(t),w.matchesSelector&&L&&!G[n+" "]&&(!R||!R.test(n))&&(!F||!F.test(n)))try{var i=j.call(t,n);if(i||w.disconnectedMatch||t.document&&11!==t.document.nodeType)return i}catch(r){G(n,!0)}return e(n,P,null,[t]).length>0},e.contains=function(t,e){return(t.ownerDocument||t)!==P&&M(t),H(t,e)},e.attr=function(t,e){(t.ownerDocument||t)!==P&&M(t);var n=D.attrHandle[e.toLowerCase()],i=n&&K.call(D.attrHandle,e.toLowerCase())?n(t,e,!L):void 0;return void 0!==i?i:w.attributes||!L?t.getAttribute(e):(i=t.getAttributeNode(e))&&i.specified?i.value:null},e.escape=function(t){return(t+"").replace(Dt,St)},e.error=function(t){throw new Error("Syntax error, unrecognized expression: "+t)},e.uniqueSort=function(t){var e,n=[],i=0,r=0;if(O=!w.detectDuplicates,E=!w.sortStable&&t.slice(0),t.sort(X),O){for(;e=t[r++];)e===t[r]&&(i=n.push(r));for(;i--;)t.splice(n[i],1)}return E=null,t},S=e.getText=function(t){var e,n="",i=0,r=t.nodeType;if(r){if(1===r||9===r||11===r){if("string"==typeof t.textContent)return t.textContent;for(t=t.firstChild;t;t=t.nextSibling)n+=S(t)}else if(3===r||4===r)return t.nodeValue}else for(;e=t[i++];)n+=S(e);return n},D=e.selectors={cacheLength:50,createPseudo:i,match:pt,attrHandle:{},find:{},relative:{">":{dir:"parentNode",first:!0}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:!0},"~":{dir:"previousSibling"}},preFilter:{ATTR:function(t){return t[1]=t[1].replace(xt,wt),t[3]=(t[3]||t[4]||t[5]||"").replace(xt,wt),"~="===t[2]&&(t[3]=" "+t[3]+" "),t.slice(0,4)},CHILD:function(t){return t[1]=t[1].toLowerCase(),"nth"===t[1].slice(0,3)?(t[3]||e.error(t[0]),t[4]=+(t[4]?t[5]+(t[6]||1):2*("even"===t[3]||"odd"===t[3])),t[5]=+(t[7]+t[8]||"odd"===t[3])):t[3]&&e.error(t[0]),t},PSEUDO:function(t){var e,n=!t[6]&&t[2];return pt.CHILD.test(t[0])?null:(t[3]?t[2]=t[4]||t[5]||"":n&&dt.test(n)&&(e=T(n,!0))&&(e=n.indexOf(")",n.length-e)-n.length)&&(t[0]=t[0].slice(0,e),t[2]=n.slice(0,e)),t.slice(0,3))}},filter:{TAG:function(t){var e=t.replace(xt,wt).toLowerCase();return"*"===t?function(){return!0}:function(t){return t.nodeName&&t.nodeName.toLowerCase()===e}},CLASS:function(t){var e=q[t+" "];return e||(e=new RegExp("(^|"+it+")"+t+"("+it+"|$)"))&&q(t,function(t){return e.test("string"==typeof t.className&&t.className||"undefined"!=typeof t.getAttribute&&t.getAttribute("class")||"")})},ATTR:function(t,n,i){return function(r){var o=e.attr(r,t);return null==o?"!="===n:!n||(o+="","="===n?o===i:"!="===n?o!==i:"^="===n?i&&0===o.indexOf(i):"*="===n?i&&o.indexOf(i)>-1:"$="===n?i&&o.slice(-i.length)===i:"~="===n?(" "+o.replace(st," ")+" ").indexOf(i)>-1:"|="===n&&(o===i||o.slice(0,i.length+1)===i+"-"))}},CHILD:function(t,e,n,i,r){var o="nth"!==t.slice(0,3),a="last"!==t.slice(-4),s="of-type"===e;return 1===i&&0===r?function(t){return!!t.parentNode}:function(e,n,l){var u,c,h,d,f,p,g=o!==a?"nextSibling":"previousSibling",m=e.parentNode,v=s&&e.nodeName.toLowerCase(),y=!l&&!s,b=!1;if(m){if(o){for(;g;){for(d=e;d=d[g];)if(s?d.nodeName.toLowerCase()===v:1===d.nodeType)return!1;p=g="only"===t&&!p&&"nextSibling"}return!0}if(p=[a?m.firstChild:m.lastChild],a&&y){for(d=m,h=d[W]||(d[W]={}),c=h[d.uniqueID]||(h[d.uniqueID]={}),u=c[t]||[],f=u[0]===V&&u[1],b=f&&u[2],d=f&&m.childNodes[f];d=++f&&d&&d[g]||(b=f=0)||p.pop();)if(1===d.nodeType&&++b&&d===e){c[t]=[V,f,b];break}}else if(y&&(d=e,h=d[W]||(d[W]={}),c=h[d.uniqueID]||(h[d.uniqueID]={}),u=c[t]||[],f=u[0]===V&&u[1],b=f),b===!1)for(;(d=++f&&d&&d[g]||(b=f=0)||p.pop())&&((s?d.nodeName.toLowerCase()!==v:1!==d.nodeType)||!++b||(y&&(h=d[W]||(d[W]={}),c=h[d.uniqueID]||(h[d.uniqueID]={}),c[t]=[V,b]),d!==e)););return b-=r,b===i||b%i===0&&b/i>=0}}},PSEUDO:function(t,n){var r,o=D.pseudos[t]||D.setFilters[t.toLowerCase()]||e.error("unsupported pseudo: "+t);
return o[W]?o(n):o.length>1?(r=[t,t,"",n],D.setFilters.hasOwnProperty(t.toLowerCase())?i(function(t,e){for(var i,r=o(t,n),a=r.length;a--;)i=et(t,r[a]),t[i]=!(e[i]=r[a])}):function(t){return o(t,0,r)}):o}},pseudos:{not:i(function(t){var e=[],n=[],r=k(t.replace(lt,"$1"));return r[W]?i(function(t,e,n,i){for(var o,a=r(t,null,i,[]),s=t.length;s--;)(o=a[s])&&(t[s]=!(e[s]=o))}):function(t,i,o){return e[0]=t,r(e,null,o,n),e[0]=null,!n.pop()}}),has:i(function(t){return function(n){return e(t,n).length>0}}),contains:i(function(t){return t=t.replace(xt,wt),function(e){return(e.textContent||S(e)).indexOf(t)>-1}}),lang:i(function(t){return ft.test(t||"")||e.error("unsupported lang: "+t),t=t.replace(xt,wt).toLowerCase(),function(e){var n;do if(n=L?e.lang:e.getAttribute("xml:lang")||e.getAttribute("lang"))return n=n.toLowerCase(),n===t||0===n.indexOf(t+"-");while((e=e.parentNode)&&1===e.nodeType);return!1}}),target:function(e){var n=t.location&&t.location.hash;return n&&n.slice(1)===e.id},root:function(t){return t===N},focus:function(t){return t===P.activeElement&&(!P.hasFocus||P.hasFocus())&&!!(t.type||t.href||~t.tabIndex)},enabled:u(!1),disabled:u(!0),checked:function(t){var e=t.nodeName.toLowerCase();return"input"===e&&!!t.checked||"option"===e&&!!t.selected},selected:function(t){return t.parentNode&&t.parentNode.selectedIndex,t.selected===!0},empty:function(t){for(t=t.firstChild;t;t=t.nextSibling)if(t.nodeType<6)return!1;return!0},parent:function(t){return!D.pseudos.empty(t)},header:function(t){return vt.test(t.nodeName)},input:function(t){return mt.test(t.nodeName)},button:function(t){var e=t.nodeName.toLowerCase();return"input"===e&&"button"===t.type||"button"===e},text:function(t){var e;return"input"===t.nodeName.toLowerCase()&&"text"===t.type&&(null==(e=t.getAttribute("type"))||"text"===e.toLowerCase())},first:c(function(){return[0]}),last:c(function(t,e){return[e-1]}),eq:c(function(t,e,n){return[n<0?n+e:n]}),even:c(function(t,e){for(var n=0;n<e;n+=2)t.push(n);return t}),odd:c(function(t,e){for(var n=1;n<e;n+=2)t.push(n);return t}),lt:c(function(t,e,n){for(var i=n<0?n+e:n>e?e:n;--i>=0;)t.push(i);return t}),gt:c(function(t,e,n){for(var i=n<0?n+e:n;++i<e;)t.push(i);return t})}},D.pseudos.nth=D.pseudos.eq;for(x in{radio:!0,checkbox:!0,file:!0,password:!0,image:!0})D.pseudos[x]=s(x);for(x in{submit:!0,reset:!0})D.pseudos[x]=l(x);return d.prototype=D.filters=D.pseudos,D.setFilters=new d,T=e.tokenize=function(t,n){var i,r,o,a,s,l,u,c=z[t+" "];if(c)return n?0:c.slice(0);for(s=t,l=[],u=D.preFilter;s;){i&&!(r=ut.exec(s))||(r&&(s=s.slice(r[0].length)||s),l.push(o=[])),i=!1,(r=ct.exec(s))&&(i=r.shift(),o.push({value:i,type:r[0].replace(lt," ")}),s=s.slice(i.length));for(a in D.filter)!(r=pt[a].exec(s))||u[a]&&!(r=u[a](r))||(i=r.shift(),o.push({value:i,type:a,matches:r}),s=s.slice(i.length));if(!i)break}return n?s.length:s?e.error(t):z(t,l).slice(0)},k=e.compile=function(t,e){var n,i=[],r=[],o=Y[t+" "];if(!o){for(e||(e=T(t)),n=e.length;n--;)o=b(e[n]),o[W]?i.push(o):r.push(o);o=Y(t,_(r,i)),o.selector=t}return o},A=e.select=function(t,e,n,i){var r,o,a,s,l,u="function"==typeof t&&t,c=!i&&T(t=u.selector||t);if(n=n||[],1===c.length){if(o=c[0]=c[0].slice(0),o.length>2&&"ID"===(a=o[0]).type&&9===e.nodeType&&L&&D.relative[o[1].type]){if(e=(D.find.ID(a.matches[0].replace(xt,wt),e)||[])[0],!e)return n;u&&(e=e.parentNode),t=t.slice(o.shift().value.length)}for(r=pt.needsContext.test(t)?0:o.length;r--&&(a=o[r],!D.relative[s=a.type]);)if((l=D.find[s])&&(i=l(a.matches[0].replace(xt,wt),_t.test(o[0].type)&&h(e.parentNode)||e))){if(o.splice(r,1),t=i.length&&f(o),!t)return Z.apply(n,i),n;break}}return(u||k(t,c))(i,e,!L,n,!e||_t.test(t)&&h(e.parentNode)||e),n},w.sortStable=W.split("").sort(X).join("")===W,w.detectDuplicates=!!O,M(),w.sortDetached=r(function(t){return 1&t.compareDocumentPosition(P.createElement("fieldset"))}),r(function(t){return t.innerHTML="<a href='#'></a>","#"===t.firstChild.getAttribute("href")})||o("type|href|height|width",function(t,e,n){if(!n)return t.getAttribute(e,"type"===e.toLowerCase()?1:2)}),w.attributes&&r(function(t){return t.innerHTML="<input/>",t.firstChild.setAttribute("value",""),""===t.firstChild.getAttribute("value")})||o("value",function(t,e,n){if(!n&&"input"===t.nodeName.toLowerCase())return t.defaultValue}),r(function(t){return null==t.getAttribute("disabled")})||o(nt,function(t,e,n){var i;if(!n)return t[e]===!0?e.toLowerCase():(i=t.getAttributeNode(e))&&i.specified?i.value:null}),e}(t);St.find=Tt,St.expr=Tt.selectors,St.expr[":"]=St.expr.pseudos,St.uniqueSort=St.unique=Tt.uniqueSort,St.text=Tt.getText,St.isXMLDoc=Tt.isXML,St.contains=Tt.contains,St.escapeSelector=Tt.escape;var kt=function(t,e,n){for(var i=[],r=void 0!==n;(t=t[e])&&9!==t.nodeType;)if(1===t.nodeType){if(r&&St(t).is(n))break;i.push(t)}return i},At=function(t,e){for(var n=[];t;t=t.nextSibling)1===t.nodeType&&t!==e&&n.push(t);return n},It=St.expr.match.needsContext,Et=/^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;St.filter=function(t,e,n){var i=e[0];return n&&(t=":not("+t+")"),1===e.length&&1===i.nodeType?St.find.matchesSelector(i,t)?[i]:[]:St.find.matches(t,St.grep(e,function(t){return 1===t.nodeType}))},St.fn.extend({find:function(t){var e,n,i=this.length,r=this;if("string"!=typeof t)return this.pushStack(St(t).filter(function(){for(e=0;e<i;e++)if(St.contains(r[e],this))return!0}));for(n=this.pushStack([]),e=0;e<i;e++)St.find(t,r[e],n);return i>1?St.uniqueSort(n):n},filter:function(t){return this.pushStack(a(this,t||[],!1))},not:function(t){return this.pushStack(a(this,t||[],!0))},is:function(t){return!!a(this,"string"==typeof t&&It.test(t)?St(t):t||[],!1).length}});var Ot,Mt=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/,Pt=St.fn.init=function(t,e,n){var i,r;if(!t)return this;if(n=n||Ot,"string"==typeof t){if(i="<"===t[0]&&">"===t[t.length-1]&&t.length>=3?[null,t,null]:Mt.exec(t),!i||!i[1]&&e)return!e||e.jquery?(e||n).find(t):this.constructor(e).find(t);if(i[1]){if(e=e instanceof St?e[0]:e,St.merge(this,St.parseHTML(i[1],e&&e.nodeType?e.ownerDocument||e:lt,!0)),Et.test(i[1])&&St.isPlainObject(e))for(i in e)_t(this[i])?this[i](e[i]):this.attr(i,e[i]);return this}return r=lt.getElementById(i[2]),r&&(this[0]=r,this.length=1),this}return t.nodeType?(this[0]=t,this.length=1,this):_t(t)?void 0!==n.ready?n.ready(t):t(St):St.makeArray(t,this)};Pt.prototype=St.fn,Ot=St(lt);var Nt=/^(?:parents|prev(?:Until|All))/,Lt={children:!0,contents:!0,next:!0,prev:!0};St.fn.extend({has:function(t){var e=St(t,this),n=e.length;return this.filter(function(){for(var t=0;t<n;t++)if(St.contains(this,e[t]))return!0})},closest:function(t,e){var n,i=0,r=this.length,o=[],a="string"!=typeof t&&St(t);if(!It.test(t))for(;i<r;i++)for(n=this[i];n&&n!==e;n=n.parentNode)if(n.nodeType<11&&(a?a.index(n)>-1:1===n.nodeType&&St.find.matchesSelector(n,t))){o.push(n);break}return this.pushStack(o.length>1?St.uniqueSort(o):o)},index:function(t){return t?"string"==typeof t?ft.call(St(t),this[0]):ft.call(this,t.jquery?t[0]:t):this[0]&&this[0].parentNode?this.first().prevAll().length:-1},add:function(t,e){return this.pushStack(St.uniqueSort(St.merge(this.get(),St(t,e))))},addBack:function(t){return this.add(null==t?this.prevObject:this.prevObject.filter(t))}}),St.each({parent:function(t){var e=t.parentNode;return e&&11!==e.nodeType?e:null},parents:function(t){return kt(t,"parentNode")},parentsUntil:function(t,e,n){return kt(t,"parentNode",n)},next:function(t){return s(t,"nextSibling")},prev:function(t){return s(t,"previousSibling")},nextAll:function(t){return kt(t,"nextSibling")},prevAll:function(t){return kt(t,"previousSibling")},nextUntil:function(t,e,n){return kt(t,"nextSibling",n)},prevUntil:function(t,e,n){return kt(t,"previousSibling",n)},siblings:function(t){return At((t.parentNode||{}).firstChild,t)},children:function(t){return At(t.firstChild)},contents:function(t){return"undefined"!=typeof t.contentDocument?t.contentDocument:(o(t,"template")&&(t=t.content||t),St.merge([],t.childNodes))}},function(t,e){St.fn[t]=function(n,i){var r=St.map(this,e,n);return"Until"!==t.slice(-5)&&(i=n),i&&"string"==typeof i&&(r=St.filter(i,r)),this.length>1&&(Lt[t]||St.uniqueSort(r),Nt.test(t)&&r.reverse()),this.pushStack(r)}});var Ft=/[^\x20\t\r\n\f]+/g;St.Callbacks=function(t){t="string"==typeof t?l(t):St.extend({},t);var e,n,r,o,a=[],s=[],u=-1,c=function(){for(o=o||t.once,r=e=!0;s.length;u=-1)for(n=s.shift();++u<a.length;)a[u].apply(n[0],n[1])===!1&&t.stopOnFalse&&(u=a.length,n=!1);t.memory||(n=!1),e=!1,o&&(a=n?[]:"")},h={add:function(){return a&&(n&&!e&&(u=a.length-1,s.push(n)),function r(e){St.each(e,function(e,n){_t(n)?t.unique&&h.has(n)||a.push(n):n&&n.length&&"string"!==i(n)&&r(n)})}(arguments),n&&!e&&c()),this},remove:function(){return St.each(arguments,function(t,e){for(var n;(n=St.inArray(e,a,n))>-1;)a.splice(n,1),n<=u&&u--}),this},has:function(t){return t?St.inArray(t,a)>-1:a.length>0},empty:function(){return a&&(a=[]),this},disable:function(){return o=s=[],a=n="",this},disabled:function(){return!a},lock:function(){return o=s=[],n||e||(a=n=""),this},locked:function(){return!!o},fireWith:function(t,n){return o||(n=n||[],n=[t,n.slice?n.slice():n],s.push(n),e||c()),this},fire:function(){return h.fireWith(this,arguments),this},fired:function(){return!!r}};return h},St.extend({Deferred:function(e){var n=[["notify","progress",St.Callbacks("memory"),St.Callbacks("memory"),2],["resolve","done",St.Callbacks("once memory"),St.Callbacks("once memory"),0,"resolved"],["reject","fail",St.Callbacks("once memory"),St.Callbacks("once memory"),1,"rejected"]],i="pending",r={state:function(){return i},always:function(){return o.done(arguments).fail(arguments),this},"catch":function(t){return r.then(null,t)},pipe:function(){var t=arguments;return St.Deferred(function(e){St.each(n,function(n,i){var r=_t(t[i[4]])&&t[i[4]];o[i[1]](function(){var t=r&&r.apply(this,arguments);t&&_t(t.promise)?t.promise().progress(e.notify).done(e.resolve).fail(e.reject):e[i[0]+"With"](this,r?[t]:arguments)})}),t=null}).promise()},then:function(e,i,r){function o(e,n,i,r){return function(){var s=this,l=arguments,h=function(){var t,h;if(!(e<a)){if(t=i.apply(s,l),t===n.promise())throw new TypeError("Thenable self-resolution");h=t&&("object"==typeof t||"function"==typeof t)&&t.then,_t(h)?r?h.call(t,o(a,n,u,r),o(a,n,c,r)):(a++,h.call(t,o(a,n,u,r),o(a,n,c,r),o(a,n,u,n.notifyWith))):(i!==u&&(s=void 0,l=[t]),(r||n.resolveWith)(s,l))}},d=r?h:function(){try{h()}catch(t){St.Deferred.exceptionHook&&St.Deferred.exceptionHook(t,d.stackTrace),e+1>=a&&(i!==c&&(s=void 0,l=[t]),n.rejectWith(s,l))}};e?d():(St.Deferred.getStackHook&&(d.stackTrace=St.Deferred.getStackHook()),t.setTimeout(d))}}var a=0;return St.Deferred(function(t){n[0][3].add(o(0,t,_t(r)?r:u,t.notifyWith)),n[1][3].add(o(0,t,_t(e)?e:u)),n[2][3].add(o(0,t,_t(i)?i:c))}).promise()},promise:function(t){return null!=t?St.extend(t,r):r}},o={};return St.each(n,function(t,e){var a=e[2],s=e[5];r[e[1]]=a.add,s&&a.add(function(){i=s},n[3-t][2].disable,n[3-t][3].disable,n[0][2].lock,n[0][3].lock),a.add(e[3].fire),o[e[0]]=function(){return o[e[0]+"With"](this===o?void 0:this,arguments),this},o[e[0]+"With"]=a.fireWith}),r.promise(o),e&&e.call(o,o),o},when:function(t){var e=arguments.length,n=e,i=Array(n),r=ct.call(arguments),o=St.Deferred(),a=function(t){return function(n){i[t]=this,r[t]=arguments.length>1?ct.call(arguments):n,--e||o.resolveWith(i,r)}};if(e<=1&&(h(t,o.done(a(n)).resolve,o.reject,!e),"pending"===o.state()||_t(r[n]&&r[n].then)))return o.then();for(;n--;)h(r[n],a(n),o.reject);return o.promise()}});var Rt=/^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;St.Deferred.exceptionHook=function(e,n){t.console&&t.console.warn&&e&&Rt.test(e.name)&&t.console.warn("jQuery.Deferred exception: "+e.message,e.stack,n)},St.readyException=function(e){t.setTimeout(function(){throw e})};var jt=St.Deferred();St.fn.ready=function(t){return jt.then(t)["catch"](function(t){St.readyException(t)}),this},St.extend({isReady:!1,readyWait:1,ready:function(t){(t===!0?--St.readyWait:St.isReady)||(St.isReady=!0,t!==!0&&--St.readyWait>0||jt.resolveWith(lt,[St]))}}),St.ready.then=jt.then,"complete"===lt.readyState||"loading"!==lt.readyState&&!lt.documentElement.doScroll?t.setTimeout(St.ready):(lt.addEventListener("DOMContentLoaded",d),t.addEventListener("load",d));var Ht=function(t,e,n,r,o,a,s){var l=0,u=t.length,c=null==n;if("object"===i(n)){o=!0;for(l in n)Ht(t,e,l,n[l],!0,a,s)}else if(void 0!==r&&(o=!0,_t(r)||(s=!0),c&&(s?(e.call(t,r),e=null):(c=e,e=function(t,e,n){return c.call(St(t),n)})),e))for(;l<u;l++)e(t[l],n,s?r:r.call(t[l],l,e(t[l],n)));return o?t:c?e.call(t):u?e(t[0],n):a},Wt=/^-ms-/,Bt=/-([a-z])/g,Vt=function(t){return 1===t.nodeType||9===t.nodeType||!+t.nodeType};g.uid=1,g.prototype={cache:function(t){var e=t[this.expando];return e||(e={},Vt(t)&&(t.nodeType?t[this.expando]=e:Object.defineProperty(t,this.expando,{value:e,configurable:!0}))),e},set:function(t,e,n){var i,r=this.cache(t);if("string"==typeof e)r[p(e)]=n;else for(i in e)r[p(i)]=e[i];return r},get:function(t,e){return void 0===e?this.cache(t):t[this.expando]&&t[this.expando][p(e)]},access:function(t,e,n){return void 0===e||e&&"string"==typeof e&&void 0===n?this.get(t,e):(this.set(t,e,n),void 0!==n?n:e)},remove:function(t,e){var n,i=t[this.expando];if(void 0!==i){if(void 0!==e){Array.isArray(e)?e=e.map(p):(e=p(e),e=e in i?[e]:e.match(Ft)||[]),n=e.length;for(;n--;)delete i[e[n]]}(void 0===e||St.isEmptyObject(i))&&(t.nodeType?t[this.expando]=void 0:delete t[this.expando])}},hasData:function(t){var e=t[this.expando];return void 0!==e&&!St.isEmptyObject(e)}};var Ut=new g,qt=new g,zt=/^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,Yt=/[A-Z]/g;St.extend({hasData:function(t){return qt.hasData(t)||Ut.hasData(t)},data:function(t,e,n){return qt.access(t,e,n)},removeData:function(t,e){qt.remove(t,e)},_data:function(t,e,n){return Ut.access(t,e,n)},_removeData:function(t,e){Ut.remove(t,e)}}),St.fn.extend({data:function(t,e){var n,i,r,o=this[0],a=o&&o.attributes;if(void 0===t){if(this.length&&(r=qt.get(o),1===o.nodeType&&!Ut.get(o,"hasDataAttrs"))){for(n=a.length;n--;)a[n]&&(i=a[n].name,0===i.indexOf("data-")&&(i=p(i.slice(5)),v(o,i,r[i])));Ut.set(o,"hasDataAttrs",!0)}return r}return"object"==typeof t?this.each(function(){qt.set(this,t)}):Ht(this,function(e){var n;if(o&&void 0===e){if(n=qt.get(o,t),void 0!==n)return n;if(n=v(o,t),void 0!==n)return n}else this.each(function(){qt.set(this,t,e)})},null,e,arguments.length>1,null,!0)},removeData:function(t){return this.each(function(){qt.remove(this,t)})}}),St.extend({queue:function(t,e,n){var i;if(t)return e=(e||"fx")+"queue",i=Ut.get(t,e),n&&(!i||Array.isArray(n)?i=Ut.access(t,e,St.makeArray(n)):i.push(n)),i||[]},dequeue:function(t,e){e=e||"fx";var n=St.queue(t,e),i=n.length,r=n.shift(),o=St._queueHooks(t,e),a=function(){St.dequeue(t,e)};"inprogress"===r&&(r=n.shift(),i--),r&&("fx"===e&&n.unshift("inprogress"),delete o.stop,r.call(t,a,o)),!i&&o&&o.empty.fire()},_queueHooks:function(t,e){var n=e+"queueHooks";return Ut.get(t,n)||Ut.access(t,n,{empty:St.Callbacks("once memory").add(function(){Ut.remove(t,[e+"queue",n])})})}}),St.fn.extend({queue:function(t,e){var n=2;return"string"!=typeof t&&(e=t,t="fx",n--),arguments.length<n?St.queue(this[0],t):void 0===e?this:this.each(function(){var n=St.queue(this,t,e);St._queueHooks(this,t),"fx"===t&&"inprogress"!==n[0]&&St.dequeue(this,t)})},dequeue:function(t){return this.each(function(){St.dequeue(this,t)})},clearQueue:function(t){return this.queue(t||"fx",[])},promise:function(t,e){var n,i=1,r=St.Deferred(),o=this,a=this.length,s=function(){--i||r.resolveWith(o,[o])};for("string"!=typeof t&&(e=t,t=void 0),t=t||"fx";a--;)n=Ut.get(o[a],t+"queueHooks"),n&&n.empty&&(i++,n.empty.add(s));return s(),r.promise(e)}});var Gt=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,Xt=new RegExp("^(?:([+-])=|)("+Gt+")([a-z%]*)$","i"),Kt=["Top","Right","Bottom","Left"],$t=lt.documentElement,Qt=function(t){return St.contains(t.ownerDocument,t)},Jt={composed:!0};$t.getRootNode&&(Qt=function(t){return St.contains(t.ownerDocument,t)||t.getRootNode(Jt)===t.ownerDocument});var Zt=function(t,e){return t=e||t,"none"===t.style.display||""===t.style.display&&Qt(t)&&"none"===St.css(t,"display")},te=function(t,e,n,i){var r,o,a={};for(o in e)a[o]=t.style[o],t.style[o]=e[o];r=n.apply(t,i||[]);for(o in e)t.style[o]=a[o];return r},ee={};St.fn.extend({show:function(){return _(this,!0)},hide:function(){return _(this)},toggle:function(t){return"boolean"==typeof t?t?this.show():this.hide():this.each(function(){Zt(this)?St(this).show():St(this).hide()})}});var ne=/^(?:checkbox|radio)$/i,ie=/<([a-z][^\/\0>\x20\t\r\n\f]*)/i,re=/^$|^module$|\/(?:java|ecma)script/i,oe={option:[1,"<select multiple='multiple'>","</select>"],thead:[1,"<table>","</table>"],col:[2,"<table><colgroup>","</colgroup></table>"],tr:[2,"<table><tbody>","</tbody></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:[0,"",""]};oe.optgroup=oe.option,oe.tbody=oe.tfoot=oe.colgroup=oe.caption=oe.thead,oe.th=oe.td;var ae=/<|&#?\w+;/;!function(){var t=lt.createDocumentFragment(),e=t.appendChild(lt.createElement("div")),n=lt.createElement("input");n.setAttribute("type","radio"),n.setAttribute("checked","checked"),n.setAttribute("name","t"),e.appendChild(n),bt.checkClone=e.cloneNode(!0).cloneNode(!0).lastChild.checked,e.innerHTML="<textarea>x</textarea>",bt.noCloneChecked=!!e.cloneNode(!0).lastChild.defaultValue}();var se=/^key/,le=/^(?:mouse|pointer|contextmenu|drag|drop)|click/,ue=/^([^.]*)(?:\.(.+)|)/;St.event={global:{},add:function(t,e,n,i,r){var o,a,s,l,u,c,h,d,f,p,g,m=Ut.get(t);if(m)for(n.handler&&(o=n,n=o.handler,r=o.selector),r&&St.find.matchesSelector($t,r),n.guid||(n.guid=St.guid++),(l=m.events)||(l=m.events={}),(a=m.handle)||(a=m.handle=function(e){return"undefined"!=typeof St&&St.event.triggered!==e.type?St.event.dispatch.apply(t,arguments):void 0}),e=(e||"").match(Ft)||[""],u=e.length;u--;)s=ue.exec(e[u])||[],f=g=s[1],p=(s[2]||"").split(".").sort(),f&&(h=St.event.special[f]||{},f=(r?h.delegateType:h.bindType)||f,h=St.event.special[f]||{},c=St.extend({type:f,origType:g,data:i,handler:n,guid:n.guid,selector:r,needsContext:r&&St.expr.match.needsContext.test(r),namespace:p.join(".")},o),(d=l[f])||(d=l[f]=[],d.delegateCount=0,h.setup&&h.setup.call(t,i,p,a)!==!1||t.addEventListener&&t.addEventListener(f,a)),h.add&&(h.add.call(t,c),c.handler.guid||(c.handler.guid=n.guid)),r?d.splice(d.delegateCount++,0,c):d.push(c),St.event.global[f]=!0)},remove:function(t,e,n,i,r){var o,a,s,l,u,c,h,d,f,p,g,m=Ut.hasData(t)&&Ut.get(t);if(m&&(l=m.events)){for(e=(e||"").match(Ft)||[""],u=e.length;u--;)if(s=ue.exec(e[u])||[],f=g=s[1],p=(s[2]||"").split(".").sort(),f){for(h=St.event.special[f]||{},f=(i?h.delegateType:h.bindType)||f,d=l[f]||[],s=s[2]&&new RegExp("(^|\\.)"+p.join("\\.(?:.*\\.|)")+"(\\.|$)"),a=o=d.length;o--;)c=d[o],!r&&g!==c.origType||n&&n.guid!==c.guid||s&&!s.test(c.namespace)||i&&i!==c.selector&&("**"!==i||!c.selector)||(d.splice(o,1),c.selector&&d.delegateCount--,h.remove&&h.remove.call(t,c));a&&!d.length&&(h.teardown&&h.teardown.call(t,p,m.handle)!==!1||St.removeEvent(t,f,m.handle),delete l[f])}else for(f in l)St.event.remove(t,f+e[u],n,i,!0);St.isEmptyObject(l)&&Ut.remove(t,"handle events")}},dispatch:function(t){var e,n,i,r,o,a,s=St.event.fix(t),l=new Array(arguments.length),u=(Ut.get(this,"events")||{})[s.type]||[],c=St.event.special[s.type]||{};for(l[0]=s,e=1;e<arguments.length;e++)l[e]=arguments[e];if(s.delegateTarget=this,!c.preDispatch||c.preDispatch.call(this,s)!==!1){for(a=St.event.handlers.call(this,s,u),e=0;(r=a[e++])&&!s.isPropagationStopped();)for(s.currentTarget=r.elem,n=0;(o=r.handlers[n++])&&!s.isImmediatePropagationStopped();)s.rnamespace&&o.namespace!==!1&&!s.rnamespace.test(o.namespace)||(s.handleObj=o,s.data=o.data,i=((St.event.special[o.origType]||{}).handle||o.handler).apply(r.elem,l),void 0!==i&&(s.result=i)===!1&&(s.preventDefault(),s.stopPropagation()));return c.postDispatch&&c.postDispatch.call(this,s),s.result}},handlers:function(t,e){var n,i,r,o,a,s=[],l=e.delegateCount,u=t.target;if(l&&u.nodeType&&!("click"===t.type&&t.button>=1))for(;u!==this;u=u.parentNode||this)if(1===u.nodeType&&("click"!==t.type||u.disabled!==!0)){for(o=[],a={},n=0;n<l;n++)i=e[n],r=i.selector+" ",void 0===a[r]&&(a[r]=i.needsContext?St(r,this).index(u)>-1:St.find(r,this,null,[u]).length),a[r]&&o.push(i);o.length&&s.push({elem:u,handlers:o})}return u=this,l<e.length&&s.push({elem:u,handlers:e.slice(l)}),s},addProp:function(t,e){Object.defineProperty(St.Event.prototype,t,{enumerable:!0,configurable:!0,get:_t(e)?function(){if(this.originalEvent)return e(this.originalEvent)}:function(){if(this.originalEvent)return this.originalEvent[t]},set:function(e){Object.defineProperty(this,t,{enumerable:!0,configurable:!0,writable:!0,value:e})}})},fix:function(t){return t[St.expando]?t:new St.Event(t)},special:{load:{noBubble:!0},click:{setup:function(t){var e=this||t;return ne.test(e.type)&&e.click&&o(e,"input")&&I(e,"click",S),!1},trigger:function(t){var e=this||t;return ne.test(e.type)&&e.click&&o(e,"input")&&I(e,"click"),!0},_default:function(t){var e=t.target;return ne.test(e.type)&&e.click&&o(e,"input")&&Ut.get(e,"click")||o(e,"a")}},beforeunload:{postDispatch:function(t){void 0!==t.result&&t.originalEvent&&(t.originalEvent.returnValue=t.result)}}}},St.removeEvent=function(t,e,n){t.removeEventListener&&t.removeEventListener(e,n)},St.Event=function(t,e){return this instanceof St.Event?(t&&t.type?(this.originalEvent=t,this.type=t.type,this.isDefaultPrevented=t.defaultPrevented||void 0===t.defaultPrevented&&t.returnValue===!1?S:C,this.target=t.target&&3===t.target.nodeType?t.target.parentNode:t.target,this.currentTarget=t.currentTarget,this.relatedTarget=t.relatedTarget):this.type=t,e&&St.extend(this,e),this.timeStamp=t&&t.timeStamp||Date.now(),void(this[St.expando]=!0)):new St.Event(t,e)},St.Event.prototype={constructor:St.Event,isDefaultPrevented:C,isPropagationStopped:C,isImmediatePropagationStopped:C,isSimulated:!1,preventDefault:function(){var t=this.originalEvent;this.isDefaultPrevented=S,t&&!this.isSimulated&&t.preventDefault()},stopPropagation:function(){var t=this.originalEvent;this.isPropagationStopped=S,t&&!this.isSimulated&&t.stopPropagation()},stopImmediatePropagation:function(){var t=this.originalEvent;this.isImmediatePropagationStopped=S,t&&!this.isSimulated&&t.stopImmediatePropagation(),this.stopPropagation()}},St.each({altKey:!0,bubbles:!0,cancelable:!0,changedTouches:!0,ctrlKey:!0,detail:!0,eventPhase:!0,metaKey:!0,pageX:!0,pageY:!0,shiftKey:!0,view:!0,"char":!0,code:!0,charCode:!0,key:!0,keyCode:!0,button:!0,buttons:!0,clientX:!0,clientY:!0,offsetX:!0,offsetY:!0,pointerId:!0,pointerType:!0,screenX:!0,screenY:!0,targetTouches:!0,toElement:!0,touches:!0,which:function(t){var e=t.button;return null==t.which&&se.test(t.type)?null!=t.charCode?t.charCode:t.keyCode:!t.which&&void 0!==e&&le.test(t.type)?1&e?1:2&e?3:4&e?2:0:t.which}},St.event.addProp),St.each({focus:"focusin",blur:"focusout"},function(t,e){St.event.special[t]={setup:function(){return I(this,t,T),!1},trigger:function(){return I(this,t),!0},delegateType:e}}),St.each({mouseenter:"mouseover",mouseleave:"mouseout",pointerenter:"pointerover",pointerleave:"pointerout"},function(t,e){St.event.special[t]={delegateType:e,bindType:e,handle:function(t){var n,i=this,r=t.relatedTarget,o=t.handleObj;return r&&(r===i||St.contains(i,r))||(t.type=o.origType,n=o.handler.apply(this,arguments),t.type=e),n}}}),St.fn.extend({on:function(t,e,n,i){return A(this,t,e,n,i)},one:function(t,e,n,i){return A(this,t,e,n,i,1)},off:function(t,e,n){var i,r;if(t&&t.preventDefault&&t.handleObj)return i=t.handleObj,St(t.delegateTarget).off(i.namespace?i.origType+"."+i.namespace:i.origType,i.selector,i.handler),this;if("object"==typeof t){for(r in t)this.off(r,e,t[r]);return this}return e!==!1&&"function"!=typeof e||(n=e,e=void 0),n===!1&&(n=C),this.each(function(){St.event.remove(this,t,n,e)})}});var ce=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,he=/<script|<style|<link/i,de=/checked\s*(?:[^=]|=\s*.checked.)/i,fe=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;St.extend({htmlPrefilter:function(t){return t.replace(ce,"<$1></$2>")},clone:function(t,e,n){var i,r,o,a,s=t.cloneNode(!0),l=Qt(t);if(!(bt.noCloneChecked||1!==t.nodeType&&11!==t.nodeType||St.isXMLDoc(t)))for(a=x(s),o=x(t),i=0,r=o.length;i<r;i++)N(o[i],a[i]);if(e)if(n)for(o=o||x(t),a=a||x(s),i=0,r=o.length;i<r;i++)P(o[i],a[i]);else P(t,s);return a=x(s,"script"),a.length>0&&w(a,!l&&x(t,"script")),s},cleanData:function(t){for(var e,n,i,r=St.event.special,o=0;void 0!==(n=t[o]);o++)if(Vt(n)){if(e=n[Ut.expando]){if(e.events)for(i in e.events)r[i]?St.event.remove(n,i):St.removeEvent(n,i,e.handle);n[Ut.expando]=void 0}n[qt.expando]&&(n[qt.expando]=void 0)}}}),St.fn.extend({detach:function(t){return F(this,t,!0)},remove:function(t){return F(this,t)},text:function(t){return Ht(this,function(t){return void 0===t?St.text(this):this.empty().each(function(){1!==this.nodeType&&11!==this.nodeType&&9!==this.nodeType||(this.textContent=t)})},null,t,arguments.length)},append:function(){return L(this,arguments,function(t){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var e=E(this,t);e.appendChild(t)}})},prepend:function(){return L(this,arguments,function(t){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var e=E(this,t);e.insertBefore(t,e.firstChild)}})},before:function(){return L(this,arguments,function(t){this.parentNode&&this.parentNode.insertBefore(t,this)})},after:function(){return L(this,arguments,function(t){this.parentNode&&this.parentNode.insertBefore(t,this.nextSibling)})},empty:function(){for(var t,e=0;null!=(t=this[e]);e++)1===t.nodeType&&(St.cleanData(x(t,!1)),t.textContent="");return this},clone:function(t,e){return t=null!=t&&t,e=null==e?t:e,this.map(function(){return St.clone(this,t,e)})},html:function(t){return Ht(this,function(t){var e=this[0]||{},n=0,i=this.length;if(void 0===t&&1===e.nodeType)return e.innerHTML;if("string"==typeof t&&!he.test(t)&&!oe[(ie.exec(t)||["",""])[1].toLowerCase()]){t=St.htmlPrefilter(t);try{for(;n<i;n++)e=this[n]||{},1===e.nodeType&&(St.cleanData(x(e,!1)),e.innerHTML=t);e=0}catch(r){}}e&&this.empty().append(t)},null,t,arguments.length)},replaceWith:function(){var t=[];return L(this,arguments,function(e){var n=this.parentNode;St.inArray(this,t)<0&&(St.cleanData(x(this)),n&&n.replaceChild(e,this))},t)}}),St.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(t,e){St.fn[t]=function(t){for(var n,i=[],r=St(t),o=r.length-1,a=0;a<=o;a++)n=a===o?this:this.clone(!0),St(r[a])[e](n),dt.apply(i,n.get());return this.pushStack(i)}});var pe=new RegExp("^("+Gt+")(?!px)[a-z%]+$","i"),ge=function(e){var n=e.ownerDocument.defaultView;return n&&n.opener||(n=t),n.getComputedStyle(e)},me=new RegExp(Kt.join("|"),"i");!function(){function e(){if(u){l.style.cssText="position:absolute;left:-11111px;width:60px;margin-top:1px;padding:0;border:0",u.style.cssText="position:relative;display:block;box-sizing:border-box;overflow:scroll;margin:auto;border:1px;padding:1px;width:60%;top:1%",$t.appendChild(l).appendChild(u);var e=t.getComputedStyle(u);i="1%"!==e.top,s=12===n(e.marginLeft),u.style.right="60%",a=36===n(e.right),r=36===n(e.width),u.style.position="absolute",o=12===n(u.offsetWidth/3),$t.removeChild(l),u=null}}function n(t){return Math.round(parseFloat(t))}var i,r,o,a,s,l=lt.createElement("div"),u=lt.createElement("div");u.style&&(u.style.backgroundClip="content-box",u.cloneNode(!0).style.backgroundClip="",bt.clearCloneStyle="content-box"===u.style.backgroundClip,St.extend(bt,{boxSizingReliable:function(){return e(),r},pixelBoxStyles:function(){return e(),a},pixelPosition:function(){return e(),i},reliableMarginLeft:function(){return e(),s},scrollboxSize:function(){return e(),o}}))}();var ve=["Webkit","Moz","ms"],ye=lt.createElement("div").style,be={},_e=/^(none|table(?!-c[ea]).+)/,xe=/^--/,we={position:"absolute",visibility:"hidden",display:"block"},De={letterSpacing:"0",fontWeight:"400"};St.extend({cssHooks:{opacity:{get:function(t,e){if(e){var n=R(t,"opacity");return""===n?"1":n}}}},cssNumber:{animationIterationCount:!0,columnCount:!0,fillOpacity:!0,flexGrow:!0,flexShrink:!0,fontWeight:!0,gridArea:!0,gridColumn:!0,gridColumnEnd:!0,gridColumnStart:!0,gridRow:!0,gridRowEnd:!0,gridRowStart:!0,lineHeight:!0,opacity:!0,order:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{},style:function(t,e,n,i){if(t&&3!==t.nodeType&&8!==t.nodeType&&t.style){var r,o,a,s=p(e),l=xe.test(e),u=t.style;return l||(e=W(s)),a=St.cssHooks[e]||St.cssHooks[s],void 0===n?a&&"get"in a&&void 0!==(r=a.get(t,!1,i))?r:u[e]:(o=typeof n,"string"===o&&(r=Xt.exec(n))&&r[1]&&(n=y(t,e,r),o="number"),null!=n&&n===n&&("number"!==o||l||(n+=r&&r[3]||(St.cssNumber[s]?"":"px")),bt.clearCloneStyle||""!==n||0!==e.indexOf("background")||(u[e]="inherit"),a&&"set"in a&&void 0===(n=a.set(t,n,i))||(l?u.setProperty(e,n):u[e]=n)),void 0)}},css:function(t,e,n,i){var r,o,a,s=p(e),l=xe.test(e);return l||(e=W(s)),a=St.cssHooks[e]||St.cssHooks[s],a&&"get"in a&&(r=a.get(t,!0,n)),void 0===r&&(r=R(t,e,i)),"normal"===r&&e in De&&(r=De[e]),""===n||n?(o=parseFloat(r),n===!0||isFinite(o)?o||0:r):r}}),St.each(["height","width"],function(t,e){St.cssHooks[e]={get:function(t,n,i){if(n)return!_e.test(St.css(t,"display"))||t.getClientRects().length&&t.getBoundingClientRect().width?U(t,e,i):te(t,we,function(){return U(t,e,i)})},set:function(t,n,i){var r,o=ge(t),a=!bt.scrollboxSize()&&"absolute"===o.position,s=a||i,l=s&&"border-box"===St.css(t,"boxSizing",!1,o),u=i?V(t,e,i,l,o):0;return l&&a&&(u-=Math.ceil(t["offset"+e[0].toUpperCase()+e.slice(1)]-parseFloat(o[e])-V(t,e,"border",!1,o)-.5)),u&&(r=Xt.exec(n))&&"px"!==(r[3]||"px")&&(t.style[e]=n,n=St.css(t,e)),B(t,n,u)}}}),St.cssHooks.marginLeft=j(bt.reliableMarginLeft,function(t,e){if(e)return(parseFloat(R(t,"marginLeft"))||t.getBoundingClientRect().left-te(t,{marginLeft:0},function(){return t.getBoundingClientRect().left}))+"px"}),St.each({margin:"",padding:"",border:"Width"},function(t,e){St.cssHooks[t+e]={expand:function(n){for(var i=0,r={},o="string"==typeof n?n.split(" "):[n];i<4;i++)r[t+Kt[i]+e]=o[i]||o[i-2]||o[0];return r}},"margin"!==t&&(St.cssHooks[t+e].set=B)}),St.fn.extend({css:function(t,e){return Ht(this,function(t,e,n){var i,r,o={},a=0;if(Array.isArray(e)){for(i=ge(t),r=e.length;a<r;a++)o[e[a]]=St.css(t,e[a],!1,i);return o}return void 0!==n?St.style(t,e,n):St.css(t,e)},t,e,arguments.length>1)}}),St.Tween=q,q.prototype={constructor:q,init:function(t,e,n,i,r,o){this.elem=t,this.prop=n,this.easing=r||St.easing._default,this.options=e,this.start=this.now=this.cur(),this.end=i,this.unit=o||(St.cssNumber[n]?"":"px")},cur:function(){var t=q.propHooks[this.prop];return t&&t.get?t.get(this):q.propHooks._default.get(this)},run:function(t){var e,n=q.propHooks[this.prop];return this.options.duration?this.pos=e=St.easing[this.easing](t,this.options.duration*t,0,1,this.options.duration):this.pos=e=t,this.now=(this.end-this.start)*e+this.start,this.options.step&&this.options.step.call(this.elem,this.now,this),n&&n.set?n.set(this):q.propHooks._default.set(this),this}},q.prototype.init.prototype=q.prototype,q.propHooks={_default:{get:function(t){var e;return 1!==t.elem.nodeType||null!=t.elem[t.prop]&&null==t.elem.style[t.prop]?t.elem[t.prop]:(e=St.css(t.elem,t.prop,""),e&&"auto"!==e?e:0)},set:function(t){St.fx.step[t.prop]?St.fx.step[t.prop](t):1!==t.elem.nodeType||!St.cssHooks[t.prop]&&null==t.elem.style[W(t.prop)]?t.elem[t.prop]=t.now:St.style(t.elem,t.prop,t.now+t.unit)}}},q.propHooks.scrollTop=q.propHooks.scrollLeft={set:function(t){t.elem.nodeType&&t.elem.parentNode&&(t.elem[t.prop]=t.now)}},St.easing={linear:function(t){return t},swing:function(t){return.5-Math.cos(t*Math.PI)/2},_default:"swing"},St.fx=q.prototype.init,St.fx.step={};var Se,Ce,Te=/^(?:toggle|show|hide)$/,ke=/queueHooks$/;St.Animation=St.extend(Q,{
tweeners:{"*":[function(t,e){var n=this.createTween(t,e);return y(n.elem,t,Xt.exec(e),n),n}]},tweener:function(t,e){_t(t)?(e=t,t=["*"]):t=t.match(Ft);for(var n,i=0,r=t.length;i<r;i++)n=t[i],Q.tweeners[n]=Q.tweeners[n]||[],Q.tweeners[n].unshift(e)},prefilters:[K],prefilter:function(t,e){e?Q.prefilters.unshift(t):Q.prefilters.push(t)}}),St.speed=function(t,e,n){var i=t&&"object"==typeof t?St.extend({},t):{complete:n||!n&&e||_t(t)&&t,duration:t,easing:n&&e||e&&!_t(e)&&e};return St.fx.off?i.duration=0:"number"!=typeof i.duration&&(i.duration in St.fx.speeds?i.duration=St.fx.speeds[i.duration]:i.duration=St.fx.speeds._default),null!=i.queue&&i.queue!==!0||(i.queue="fx"),i.old=i.complete,i.complete=function(){_t(i.old)&&i.old.call(this),i.queue&&St.dequeue(this,i.queue)},i},St.fn.extend({fadeTo:function(t,e,n,i){return this.filter(Zt).css("opacity",0).show().end().animate({opacity:e},t,n,i)},animate:function(t,e,n,i){var r=St.isEmptyObject(t),o=St.speed(e,n,i),a=function(){var e=Q(this,St.extend({},t),o);(r||Ut.get(this,"finish"))&&e.stop(!0)};return a.finish=a,r||o.queue===!1?this.each(a):this.queue(o.queue,a)},stop:function(t,e,n){var i=function(t){var e=t.stop;delete t.stop,e(n)};return"string"!=typeof t&&(n=e,e=t,t=void 0),e&&t!==!1&&this.queue(t||"fx",[]),this.each(function(){var e=!0,r=null!=t&&t+"queueHooks",o=St.timers,a=Ut.get(this);if(r)a[r]&&a[r].stop&&i(a[r]);else for(r in a)a[r]&&a[r].stop&&ke.test(r)&&i(a[r]);for(r=o.length;r--;)o[r].elem!==this||null!=t&&o[r].queue!==t||(o[r].anim.stop(n),e=!1,o.splice(r,1));!e&&n||St.dequeue(this,t)})},finish:function(t){return t!==!1&&(t=t||"fx"),this.each(function(){var e,n=Ut.get(this),i=n[t+"queue"],r=n[t+"queueHooks"],o=St.timers,a=i?i.length:0;for(n.finish=!0,St.queue(this,t,[]),r&&r.stop&&r.stop.call(this,!0),e=o.length;e--;)o[e].elem===this&&o[e].queue===t&&(o[e].anim.stop(!0),o.splice(e,1));for(e=0;e<a;e++)i[e]&&i[e].finish&&i[e].finish.call(this);delete n.finish})}}),St.each(["toggle","show","hide"],function(t,e){var n=St.fn[e];St.fn[e]=function(t,i,r){return null==t||"boolean"==typeof t?n.apply(this,arguments):this.animate(G(e,!0),t,i,r)}}),St.each({slideDown:G("show"),slideUp:G("hide"),slideToggle:G("toggle"),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(t,e){St.fn[t]=function(t,n,i){return this.animate(e,t,n,i)}}),St.timers=[],St.fx.tick=function(){var t,e=0,n=St.timers;for(Se=Date.now();e<n.length;e++)t=n[e],t()||n[e]!==t||n.splice(e--,1);n.length||St.fx.stop(),Se=void 0},St.fx.timer=function(t){St.timers.push(t),St.fx.start()},St.fx.interval=13,St.fx.start=function(){Ce||(Ce=!0,z())},St.fx.stop=function(){Ce=null},St.fx.speeds={slow:600,fast:200,_default:400},St.fn.delay=function(e,n){return e=St.fx?St.fx.speeds[e]||e:e,n=n||"fx",this.queue(n,function(n,i){var r=t.setTimeout(n,e);i.stop=function(){t.clearTimeout(r)}})},function(){var t=lt.createElement("input"),e=lt.createElement("select"),n=e.appendChild(lt.createElement("option"));t.type="checkbox",bt.checkOn=""!==t.value,bt.optSelected=n.selected,t=lt.createElement("input"),t.value="t",t.type="radio",bt.radioValue="t"===t.value}();var Ae,Ie=St.expr.attrHandle;St.fn.extend({attr:function(t,e){return Ht(this,St.attr,t,e,arguments.length>1)},removeAttr:function(t){return this.each(function(){St.removeAttr(this,t)})}}),St.extend({attr:function(t,e,n){var i,r,o=t.nodeType;if(3!==o&&8!==o&&2!==o)return"undefined"==typeof t.getAttribute?St.prop(t,e,n):(1===o&&St.isXMLDoc(t)||(r=St.attrHooks[e.toLowerCase()]||(St.expr.match.bool.test(e)?Ae:void 0)),void 0!==n?null===n?void St.removeAttr(t,e):r&&"set"in r&&void 0!==(i=r.set(t,n,e))?i:(t.setAttribute(e,n+""),n):r&&"get"in r&&null!==(i=r.get(t,e))?i:(i=St.find.attr(t,e),null==i?void 0:i))},attrHooks:{type:{set:function(t,e){if(!bt.radioValue&&"radio"===e&&o(t,"input")){var n=t.value;return t.setAttribute("type",e),n&&(t.value=n),e}}}},removeAttr:function(t,e){var n,i=0,r=e&&e.match(Ft);if(r&&1===t.nodeType)for(;n=r[i++];)t.removeAttribute(n)}}),Ae={set:function(t,e,n){return e===!1?St.removeAttr(t,n):t.setAttribute(n,n),n}},St.each(St.expr.match.bool.source.match(/\w+/g),function(t,e){var n=Ie[e]||St.find.attr;Ie[e]=function(t,e,i){var r,o,a=e.toLowerCase();return i||(o=Ie[a],Ie[a]=r,r=null!=n(t,e,i)?a:null,Ie[a]=o),r}});var Ee=/^(?:input|select|textarea|button)$/i,Oe=/^(?:a|area)$/i;St.fn.extend({prop:function(t,e){return Ht(this,St.prop,t,e,arguments.length>1)},removeProp:function(t){return this.each(function(){delete this[St.propFix[t]||t]})}}),St.extend({prop:function(t,e,n){var i,r,o=t.nodeType;if(3!==o&&8!==o&&2!==o)return 1===o&&St.isXMLDoc(t)||(e=St.propFix[e]||e,r=St.propHooks[e]),void 0!==n?r&&"set"in r&&void 0!==(i=r.set(t,n,e))?i:t[e]=n:r&&"get"in r&&null!==(i=r.get(t,e))?i:t[e]},propHooks:{tabIndex:{get:function(t){var e=St.find.attr(t,"tabindex");return e?parseInt(e,10):Ee.test(t.nodeName)||Oe.test(t.nodeName)&&t.href?0:-1}}},propFix:{"for":"htmlFor","class":"className"}}),bt.optSelected||(St.propHooks.selected={get:function(t){var e=t.parentNode;return e&&e.parentNode&&e.parentNode.selectedIndex,null},set:function(t){var e=t.parentNode;e&&(e.selectedIndex,e.parentNode&&e.parentNode.selectedIndex)}}),St.each(["tabIndex","readOnly","maxLength","cellSpacing","cellPadding","rowSpan","colSpan","useMap","frameBorder","contentEditable"],function(){St.propFix[this.toLowerCase()]=this}),St.fn.extend({addClass:function(t){var e,n,i,r,o,a,s,l=0;if(_t(t))return this.each(function(e){St(this).addClass(t.call(this,e,Z(this)))});if(e=tt(t),e.length)for(;n=this[l++];)if(r=Z(n),i=1===n.nodeType&&" "+J(r)+" "){for(a=0;o=e[a++];)i.indexOf(" "+o+" ")<0&&(i+=o+" ");s=J(i),r!==s&&n.setAttribute("class",s)}return this},removeClass:function(t){var e,n,i,r,o,a,s,l=0;if(_t(t))return this.each(function(e){St(this).removeClass(t.call(this,e,Z(this)))});if(!arguments.length)return this.attr("class","");if(e=tt(t),e.length)for(;n=this[l++];)if(r=Z(n),i=1===n.nodeType&&" "+J(r)+" "){for(a=0;o=e[a++];)for(;i.indexOf(" "+o+" ")>-1;)i=i.replace(" "+o+" "," ");s=J(i),r!==s&&n.setAttribute("class",s)}return this},toggleClass:function(t,e){var n=typeof t,i="string"===n||Array.isArray(t);return"boolean"==typeof e&&i?e?this.addClass(t):this.removeClass(t):_t(t)?this.each(function(n){St(this).toggleClass(t.call(this,n,Z(this),e),e)}):this.each(function(){var e,r,o,a;if(i)for(r=0,o=St(this),a=tt(t);e=a[r++];)o.hasClass(e)?o.removeClass(e):o.addClass(e);else void 0!==t&&"boolean"!==n||(e=Z(this),e&&Ut.set(this,"__className__",e),this.setAttribute&&this.setAttribute("class",e||t===!1?"":Ut.get(this,"__className__")||""))})},hasClass:function(t){var e,n,i=0;for(e=" "+t+" ";n=this[i++];)if(1===n.nodeType&&(" "+J(Z(n))+" ").indexOf(e)>-1)return!0;return!1}});var Me=/\r/g;St.fn.extend({val:function(t){var e,n,i,r=this[0];{if(arguments.length)return i=_t(t),this.each(function(n){var r;1===this.nodeType&&(r=i?t.call(this,n,St(this).val()):t,null==r?r="":"number"==typeof r?r+="":Array.isArray(r)&&(r=St.map(r,function(t){return null==t?"":t+""})),e=St.valHooks[this.type]||St.valHooks[this.nodeName.toLowerCase()],e&&"set"in e&&void 0!==e.set(this,r,"value")||(this.value=r))});if(r)return e=St.valHooks[r.type]||St.valHooks[r.nodeName.toLowerCase()],e&&"get"in e&&void 0!==(n=e.get(r,"value"))?n:(n=r.value,"string"==typeof n?n.replace(Me,""):null==n?"":n)}}}),St.extend({valHooks:{option:{get:function(t){var e=St.find.attr(t,"value");return null!=e?e:J(St.text(t))}},select:{get:function(t){var e,n,i,r=t.options,a=t.selectedIndex,s="select-one"===t.type,l=s?null:[],u=s?a+1:r.length;for(i=a<0?u:s?a:0;i<u;i++)if(n=r[i],(n.selected||i===a)&&!n.disabled&&(!n.parentNode.disabled||!o(n.parentNode,"optgroup"))){if(e=St(n).val(),s)return e;l.push(e)}return l},set:function(t,e){for(var n,i,r=t.options,o=St.makeArray(e),a=r.length;a--;)i=r[a],(i.selected=St.inArray(St.valHooks.option.get(i),o)>-1)&&(n=!0);return n||(t.selectedIndex=-1),o}}}}),St.each(["radio","checkbox"],function(){St.valHooks[this]={set:function(t,e){if(Array.isArray(e))return t.checked=St.inArray(St(t).val(),e)>-1}},bt.checkOn||(St.valHooks[this].get=function(t){return null===t.getAttribute("value")?"on":t.value})}),bt.focusin="onfocusin"in t;var Pe=/^(?:focusinfocus|focusoutblur)$/,Ne=function(t){t.stopPropagation()};St.extend(St.event,{trigger:function(e,n,i,r){var o,a,s,l,u,c,h,d,f=[i||lt],p=mt.call(e,"type")?e.type:e,g=mt.call(e,"namespace")?e.namespace.split("."):[];if(a=d=s=i=i||lt,3!==i.nodeType&&8!==i.nodeType&&!Pe.test(p+St.event.triggered)&&(p.indexOf(".")>-1&&(g=p.split("."),p=g.shift(),g.sort()),u=p.indexOf(":")<0&&"on"+p,e=e[St.expando]?e:new St.Event(p,"object"==typeof e&&e),e.isTrigger=r?2:3,e.namespace=g.join("."),e.rnamespace=e.namespace?new RegExp("(^|\\.)"+g.join("\\.(?:.*\\.|)")+"(\\.|$)"):null,e.result=void 0,e.target||(e.target=i),n=null==n?[e]:St.makeArray(n,[e]),h=St.event.special[p]||{},r||!h.trigger||h.trigger.apply(i,n)!==!1)){if(!r&&!h.noBubble&&!xt(i)){for(l=h.delegateType||p,Pe.test(l+p)||(a=a.parentNode);a;a=a.parentNode)f.push(a),s=a;s===(i.ownerDocument||lt)&&f.push(s.defaultView||s.parentWindow||t)}for(o=0;(a=f[o++])&&!e.isPropagationStopped();)d=a,e.type=o>1?l:h.bindType||p,c=(Ut.get(a,"events")||{})[e.type]&&Ut.get(a,"handle"),c&&c.apply(a,n),c=u&&a[u],c&&c.apply&&Vt(a)&&(e.result=c.apply(a,n),e.result===!1&&e.preventDefault());return e.type=p,r||e.isDefaultPrevented()||h._default&&h._default.apply(f.pop(),n)!==!1||!Vt(i)||u&&_t(i[p])&&!xt(i)&&(s=i[u],s&&(i[u]=null),St.event.triggered=p,e.isPropagationStopped()&&d.addEventListener(p,Ne),i[p](),e.isPropagationStopped()&&d.removeEventListener(p,Ne),St.event.triggered=void 0,s&&(i[u]=s)),e.result}},simulate:function(t,e,n){var i=St.extend(new St.Event,n,{type:t,isSimulated:!0});St.event.trigger(i,null,e)}}),St.fn.extend({trigger:function(t,e){return this.each(function(){St.event.trigger(t,e,this)})},triggerHandler:function(t,e){var n=this[0];if(n)return St.event.trigger(t,e,n,!0)}}),bt.focusin||St.each({focus:"focusin",blur:"focusout"},function(t,e){var n=function(t){St.event.simulate(e,t.target,St.event.fix(t))};St.event.special[e]={setup:function(){var i=this.ownerDocument||this,r=Ut.access(i,e);r||i.addEventListener(t,n,!0),Ut.access(i,e,(r||0)+1)},teardown:function(){var i=this.ownerDocument||this,r=Ut.access(i,e)-1;r?Ut.access(i,e,r):(i.removeEventListener(t,n,!0),Ut.remove(i,e))}}});var Le=t.location,Fe=Date.now(),Re=/\?/;St.parseXML=function(e){var n;if(!e||"string"!=typeof e)return null;try{n=(new t.DOMParser).parseFromString(e,"text/xml")}catch(i){n=void 0}return n&&!n.getElementsByTagName("parsererror").length||St.error("Invalid XML: "+e),n};var je=/\[\]$/,He=/\r?\n/g,We=/^(?:submit|button|image|reset|file)$/i,Be=/^(?:input|select|textarea|keygen)/i;St.param=function(t,e){var n,i=[],r=function(t,e){var n=_t(e)?e():e;i[i.length]=encodeURIComponent(t)+"="+encodeURIComponent(null==n?"":n)};if(null==t)return"";if(Array.isArray(t)||t.jquery&&!St.isPlainObject(t))St.each(t,function(){r(this.name,this.value)});else for(n in t)et(n,t[n],e,r);return i.join("&")},St.fn.extend({serialize:function(){return St.param(this.serializeArray())},serializeArray:function(){return this.map(function(){var t=St.prop(this,"elements");return t?St.makeArray(t):this}).filter(function(){var t=this.type;return this.name&&!St(this).is(":disabled")&&Be.test(this.nodeName)&&!We.test(t)&&(this.checked||!ne.test(t))}).map(function(t,e){var n=St(this).val();return null==n?null:Array.isArray(n)?St.map(n,function(t){return{name:e.name,value:t.replace(He,"\r\n")}}):{name:e.name,value:n.replace(He,"\r\n")}}).get()}});var Ve=/%20/g,Ue=/#.*$/,qe=/([?&])_=[^&]*/,ze=/^(.*?):[ \t]*([^\r\n]*)$/gm,Ye=/^(?:about|app|app-storage|.+-extension|file|res|widget):$/,Ge=/^(?:GET|HEAD)$/,Xe=/^\/\//,Ke={},$e={},Qe="*/".concat("*"),Je=lt.createElement("a");Je.href=Le.href,St.extend({active:0,lastModified:{},etag:{},ajaxSettings:{url:Le.href,type:"GET",isLocal:Ye.test(Le.protocol),global:!0,processData:!0,async:!0,contentType:"application/x-www-form-urlencoded; charset=UTF-8",accepts:{"*":Qe,text:"text/plain",html:"text/html",xml:"application/xml, text/xml",json:"application/json, text/javascript"},contents:{xml:/\bxml\b/,html:/\bhtml/,json:/\bjson\b/},responseFields:{xml:"responseXML",text:"responseText",json:"responseJSON"},converters:{"* text":String,"text html":!0,"text json":JSON.parse,"text xml":St.parseXML},flatOptions:{url:!0,context:!0}},ajaxSetup:function(t,e){return e?rt(rt(t,St.ajaxSettings),e):rt(St.ajaxSettings,t)},ajaxPrefilter:nt(Ke),ajaxTransport:nt($e),ajax:function(e,n){function i(e,n,i,s){var u,d,f,_,x,w=n;c||(c=!0,l&&t.clearTimeout(l),r=void 0,a=s||"",D.readyState=e>0?4:0,u=e>=200&&e<300||304===e,i&&(_=ot(p,D,i)),_=at(p,_,D,u),u?(p.ifModified&&(x=D.getResponseHeader("Last-Modified"),x&&(St.lastModified[o]=x),x=D.getResponseHeader("etag"),x&&(St.etag[o]=x)),204===e||"HEAD"===p.type?w="nocontent":304===e?w="notmodified":(w=_.state,d=_.data,f=_.error,u=!f)):(f=w,!e&&w||(w="error",e<0&&(e=0))),D.status=e,D.statusText=(n||w)+"",u?v.resolveWith(g,[d,w,D]):v.rejectWith(g,[D,w,f]),D.statusCode(b),b=void 0,h&&m.trigger(u?"ajaxSuccess":"ajaxError",[D,p,u?d:f]),y.fireWith(g,[D,w]),h&&(m.trigger("ajaxComplete",[D,p]),--St.active||St.event.trigger("ajaxStop")))}"object"==typeof e&&(n=e,e=void 0),n=n||{};var r,o,a,s,l,u,c,h,d,f,p=St.ajaxSetup({},n),g=p.context||p,m=p.context&&(g.nodeType||g.jquery)?St(g):St.event,v=St.Deferred(),y=St.Callbacks("once memory"),b=p.statusCode||{},_={},x={},w="canceled",D={readyState:0,getResponseHeader:function(t){var e;if(c){if(!s)for(s={};e=ze.exec(a);)s[e[1].toLowerCase()+" "]=(s[e[1].toLowerCase()+" "]||[]).concat(e[2]);e=s[t.toLowerCase()+" "]}return null==e?null:e.join(", ")},getAllResponseHeaders:function(){return c?a:null},setRequestHeader:function(t,e){return null==c&&(t=x[t.toLowerCase()]=x[t.toLowerCase()]||t,_[t]=e),this},overrideMimeType:function(t){return null==c&&(p.mimeType=t),this},statusCode:function(t){var e;if(t)if(c)D.always(t[D.status]);else for(e in t)b[e]=[b[e],t[e]];return this},abort:function(t){var e=t||w;return r&&r.abort(e),i(0,e),this}};if(v.promise(D),p.url=((e||p.url||Le.href)+"").replace(Xe,Le.protocol+"//"),p.type=n.method||n.type||p.method||p.type,p.dataTypes=(p.dataType||"*").toLowerCase().match(Ft)||[""],null==p.crossDomain){u=lt.createElement("a");try{u.href=p.url,u.href=u.href,p.crossDomain=Je.protocol+"//"+Je.host!=u.protocol+"//"+u.host}catch(S){p.crossDomain=!0}}if(p.data&&p.processData&&"string"!=typeof p.data&&(p.data=St.param(p.data,p.traditional)),it(Ke,p,n,D),c)return D;h=St.event&&p.global,h&&0===St.active++&&St.event.trigger("ajaxStart"),p.type=p.type.toUpperCase(),p.hasContent=!Ge.test(p.type),o=p.url.replace(Ue,""),p.hasContent?p.data&&p.processData&&0===(p.contentType||"").indexOf("application/x-www-form-urlencoded")&&(p.data=p.data.replace(Ve,"+")):(f=p.url.slice(o.length),p.data&&(p.processData||"string"==typeof p.data)&&(o+=(Re.test(o)?"&":"?")+p.data,delete p.data),p.cache===!1&&(o=o.replace(qe,"$1"),f=(Re.test(o)?"&":"?")+"_="+Fe++ +f),p.url=o+f),p.ifModified&&(St.lastModified[o]&&D.setRequestHeader("If-Modified-Since",St.lastModified[o]),St.etag[o]&&D.setRequestHeader("If-None-Match",St.etag[o])),(p.data&&p.hasContent&&p.contentType!==!1||n.contentType)&&D.setRequestHeader("Content-Type",p.contentType),D.setRequestHeader("Accept",p.dataTypes[0]&&p.accepts[p.dataTypes[0]]?p.accepts[p.dataTypes[0]]+("*"!==p.dataTypes[0]?", "+Qe+"; q=0.01":""):p.accepts["*"]);for(d in p.headers)D.setRequestHeader(d,p.headers[d]);if(p.beforeSend&&(p.beforeSend.call(g,D,p)===!1||c))return D.abort();if(w="abort",y.add(p.complete),D.done(p.success),D.fail(p.error),r=it($e,p,n,D)){if(D.readyState=1,h&&m.trigger("ajaxSend",[D,p]),c)return D;p.async&&p.timeout>0&&(l=t.setTimeout(function(){D.abort("timeout")},p.timeout));try{c=!1,r.send(_,i)}catch(S){if(c)throw S;i(-1,S)}}else i(-1,"No Transport");return D},getJSON:function(t,e,n){return St.get(t,e,n,"json")},getScript:function(t,e){return St.get(t,void 0,e,"script")}}),St.each(["get","post"],function(t,e){St[e]=function(t,n,i,r){return _t(n)&&(r=r||i,i=n,n=void 0),St.ajax(St.extend({url:t,type:e,dataType:r,data:n,success:i},St.isPlainObject(t)&&t))}}),St._evalUrl=function(t,e){return St.ajax({url:t,type:"GET",dataType:"script",cache:!0,async:!1,global:!1,converters:{"text script":function(){}},dataFilter:function(t){St.globalEval(t,e)}})},St.fn.extend({wrapAll:function(t){var e;return this[0]&&(_t(t)&&(t=t.call(this[0])),e=St(t,this[0].ownerDocument).eq(0).clone(!0),this[0].parentNode&&e.insertBefore(this[0]),e.map(function(){for(var t=this;t.firstElementChild;)t=t.firstElementChild;return t}).append(this)),this},wrapInner:function(t){return _t(t)?this.each(function(e){St(this).wrapInner(t.call(this,e))}):this.each(function(){var e=St(this),n=e.contents();n.length?n.wrapAll(t):e.append(t)})},wrap:function(t){var e=_t(t);return this.each(function(n){St(this).wrapAll(e?t.call(this,n):t)})},unwrap:function(t){return this.parent(t).not("body").each(function(){St(this).replaceWith(this.childNodes)}),this}}),St.expr.pseudos.hidden=function(t){return!St.expr.pseudos.visible(t)},St.expr.pseudos.visible=function(t){return!!(t.offsetWidth||t.offsetHeight||t.getClientRects().length)},St.ajaxSettings.xhr=function(){try{return new t.XMLHttpRequest}catch(e){}};var Ze={0:200,1223:204},tn=St.ajaxSettings.xhr();bt.cors=!!tn&&"withCredentials"in tn,bt.ajax=tn=!!tn,St.ajaxTransport(function(e){var n,i;if(bt.cors||tn&&!e.crossDomain)return{send:function(r,o){var a,s=e.xhr();if(s.open(e.type,e.url,e.async,e.username,e.password),e.xhrFields)for(a in e.xhrFields)s[a]=e.xhrFields[a];e.mimeType&&s.overrideMimeType&&s.overrideMimeType(e.mimeType),e.crossDomain||r["X-Requested-With"]||(r["X-Requested-With"]="XMLHttpRequest");for(a in r)s.setRequestHeader(a,r[a]);n=function(t){return function(){n&&(n=i=s.onload=s.onerror=s.onabort=s.ontimeout=s.onreadystatechange=null,"abort"===t?s.abort():"error"===t?"number"!=typeof s.status?o(0,"error"):o(s.status,s.statusText):o(Ze[s.status]||s.status,s.statusText,"text"!==(s.responseType||"text")||"string"!=typeof s.responseText?{binary:s.response}:{text:s.responseText},s.getAllResponseHeaders()))}},s.onload=n(),i=s.onerror=s.ontimeout=n("error"),void 0!==s.onabort?s.onabort=i:s.onreadystatechange=function(){4===s.readyState&&t.setTimeout(function(){n&&i()})},n=n("abort");try{s.send(e.hasContent&&e.data||null)}catch(l){if(n)throw l}},abort:function(){n&&n()}}}),St.ajaxPrefilter(function(t){t.crossDomain&&(t.contents.script=!1)}),St.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/\b(?:java|ecma)script\b/},converters:{"text script":function(t){return St.globalEval(t),t}}}),St.ajaxPrefilter("script",function(t){void 0===t.cache&&(t.cache=!1),t.crossDomain&&(t.type="GET")}),St.ajaxTransport("script",function(t){if(t.crossDomain||t.scriptAttrs){var e,n;return{send:function(i,r){e=St("<script>").attr(t.scriptAttrs||{}).prop({charset:t.scriptCharset,src:t.url}).on("load error",n=function(t){e.remove(),n=null,t&&r("error"===t.type?404:200,t.type)}),lt.head.appendChild(e[0])},abort:function(){n&&n()}}}});var en=[],nn=/(=)\?(?=&|$)|\?\?/;St.ajaxSetup({jsonp:"callback",jsonpCallback:function(){var t=en.pop()||St.expando+"_"+Fe++;return this[t]=!0,t}}),St.ajaxPrefilter("json jsonp",function(e,n,i){var r,o,a,s=e.jsonp!==!1&&(nn.test(e.url)?"url":"string"==typeof e.data&&0===(e.contentType||"").indexOf("application/x-www-form-urlencoded")&&nn.test(e.data)&&"data");if(s||"jsonp"===e.dataTypes[0])return r=e.jsonpCallback=_t(e.jsonpCallback)?e.jsonpCallback():e.jsonpCallback,s?e[s]=e[s].replace(nn,"$1"+r):e.jsonp!==!1&&(e.url+=(Re.test(e.url)?"&":"?")+e.jsonp+"="+r),e.converters["script json"]=function(){return a||St.error(r+" was not called"),a[0]},e.dataTypes[0]="json",o=t[r],t[r]=function(){a=arguments},i.always(function(){void 0===o?St(t).removeProp(r):t[r]=o,e[r]&&(e.jsonpCallback=n.jsonpCallback,en.push(r)),a&&_t(o)&&o(a[0]),a=o=void 0}),"script"}),bt.createHTMLDocument=function(){var t=lt.implementation.createHTMLDocument("").body;return t.innerHTML="<form></form><form></form>",2===t.childNodes.length}(),St.parseHTML=function(t,e,n){if("string"!=typeof t)return[];"boolean"==typeof e&&(n=e,e=!1);var i,r,o;return e||(bt.createHTMLDocument?(e=lt.implementation.createHTMLDocument(""),i=e.createElement("base"),i.href=lt.location.href,e.head.appendChild(i)):e=lt),r=Et.exec(t),o=!n&&[],r?[e.createElement(r[1])]:(r=D([t],e,o),o&&o.length&&St(o).remove(),St.merge([],r.childNodes))},St.fn.load=function(t,e,n){var i,r,o,a=this,s=t.indexOf(" ");return s>-1&&(i=J(t.slice(s)),t=t.slice(0,s)),_t(e)?(n=e,e=void 0):e&&"object"==typeof e&&(r="POST"),a.length>0&&St.ajax({url:t,type:r||"GET",dataType:"html",data:e}).done(function(t){o=arguments,a.html(i?St("<div>").append(St.parseHTML(t)).find(i):t)}).always(n&&function(t,e){a.each(function(){n.apply(this,o||[t.responseText,e,t])})}),this},St.each(["ajaxStart","ajaxStop","ajaxComplete","ajaxError","ajaxSuccess","ajaxSend"],function(t,e){St.fn[e]=function(t){return this.on(e,t)}}),St.expr.pseudos.animated=function(t){return St.grep(St.timers,function(e){return t===e.elem}).length},St.offset={setOffset:function(t,e,n){var i,r,o,a,s,l,u,c=St.css(t,"position"),h=St(t),d={};"static"===c&&(t.style.position="relative"),s=h.offset(),o=St.css(t,"top"),l=St.css(t,"left"),u=("absolute"===c||"fixed"===c)&&(o+l).indexOf("auto")>-1,u?(i=h.position(),a=i.top,r=i.left):(a=parseFloat(o)||0,r=parseFloat(l)||0),_t(e)&&(e=e.call(t,n,St.extend({},s))),null!=e.top&&(d.top=e.top-s.top+a),null!=e.left&&(d.left=e.left-s.left+r),"using"in e?e.using.call(t,d):h.css(d)}},St.fn.extend({offset:function(t){if(arguments.length)return void 0===t?this:this.each(function(e){St.offset.setOffset(this,t,e)});var e,n,i=this[0];if(i)return i.getClientRects().length?(e=i.getBoundingClientRect(),n=i.ownerDocument.defaultView,{top:e.top+n.pageYOffset,left:e.left+n.pageXOffset}):{top:0,left:0}},position:function(){if(this[0]){var t,e,n,i=this[0],r={top:0,left:0};if("fixed"===St.css(i,"position"))e=i.getBoundingClientRect();else{for(e=this.offset(),n=i.ownerDocument,t=i.offsetParent||n.documentElement;t&&(t===n.body||t===n.documentElement)&&"static"===St.css(t,"position");)t=t.parentNode;t&&t!==i&&1===t.nodeType&&(r=St(t).offset(),r.top+=St.css(t,"borderTopWidth",!0),r.left+=St.css(t,"borderLeftWidth",!0))}return{top:e.top-r.top-St.css(i,"marginTop",!0),left:e.left-r.left-St.css(i,"marginLeft",!0)}}},offsetParent:function(){return this.map(function(){for(var t=this.offsetParent;t&&"static"===St.css(t,"position");)t=t.offsetParent;return t||$t})}}),St.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(t,e){var n="pageYOffset"===e;St.fn[t]=function(i){return Ht(this,function(t,i,r){var o;return xt(t)?o=t:9===t.nodeType&&(o=t.defaultView),void 0===r?o?o[e]:t[i]:void(o?o.scrollTo(n?o.pageXOffset:r,n?r:o.pageYOffset):t[i]=r)},t,i,arguments.length)}}),St.each(["top","left"],function(t,e){St.cssHooks[e]=j(bt.pixelPosition,function(t,n){if(n)return n=R(t,e),pe.test(n)?St(t).position()[e]+"px":n})}),St.each({Height:"height",Width:"width"},function(t,e){St.each({padding:"inner"+t,content:e,"":"outer"+t},function(n,i){St.fn[i]=function(r,o){var a=arguments.length&&(n||"boolean"!=typeof r),s=n||(r===!0||o===!0?"margin":"border");return Ht(this,function(e,n,r){var o;return xt(e)?0===i.indexOf("outer")?e["inner"+t]:e.document.documentElement["client"+t]:9===e.nodeType?(o=e.documentElement,Math.max(e.body["scroll"+t],o["scroll"+t],e.body["offset"+t],o["offset"+t],o["client"+t])):void 0===r?St.css(e,n,s):St.style(e,n,r,s)},e,a?r:void 0,a)}})}),St.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "),function(t,e){St.fn[e]=function(t,n){return arguments.length>0?this.on(e,null,t,n):this.trigger(e)}}),St.fn.extend({hover:function(t,e){return this.mouseenter(t).mouseleave(e||t)}}),St.fn.extend({bind:function(t,e,n){return this.on(t,null,e,n)},unbind:function(t,e){return this.off(t,null,e)},delegate:function(t,e,n,i){return this.on(e,t,n,i)},undelegate:function(t,e,n){return 1===arguments.length?this.off(t,"**"):this.off(e,t||"**",n)}}),St.proxy=function(t,e){var n,i,r;if("string"==typeof e&&(n=t[e],e=t,t=n),_t(t))return i=ct.call(arguments,2),r=function(){return t.apply(e||this,i.concat(ct.call(arguments)))},r.guid=t.guid=t.guid||St.guid++,r},St.holdReady=function(t){t?St.readyWait++:St.ready(!0)},St.isArray=Array.isArray,St.parseJSON=JSON.parse,St.nodeName=o,St.isFunction=_t,St.isWindow=xt,St.camelCase=p,St.type=i,St.now=Date.now,St.isNumeric=function(t){var e=St.type(t);return("number"===e||"string"===e)&&!isNaN(t-parseFloat(t))},"function"==typeof define&&define.amd&&define("jquery",[],function(){return St});var rn=t.jQuery,on=t.$;return St.noConflict=function(e){return t.$===St&&(t.$=on),e&&t.jQuery===St&&(t.jQuery=rn),St},e||(t.jQuery=t.$=St),St}),function(){var t,e,n,i,r=function(t,e){return function(){return t.apply(e,arguments)}},o=function(t,e){function n(){this.constructor=t}for(var i in e)a.call(e,i)&&(t[i]=e[i]);return n.prototype=e.prototype,t.prototype=new n,t.__super__=e.prototype,t},a={}.hasOwnProperty;i=function(){function t(){this.options_index=0,this.parsed=[]}return t.prototype.add_node=function(t){return"OPTGROUP"===t.nodeName.toUpperCase()?this.add_group(t):this.add_option(t)},t.prototype.add_group=function(t){var e,n,i,r,o,a;for(e=this.parsed.length,this.parsed.push({array_index:e,group:!0,label:t.label,title:t.title?t.title:void 0,children:0,disabled:t.disabled,classes:t.className}),o=t.childNodes,a=[],n=0,i=o.length;n<i;n++)r=o[n],a.push(this.add_option(r,e,t.disabled));return a},t.prototype.add_option=function(t,e,n){if("OPTION"===t.nodeName.toUpperCase())return""!==t.text?(null!=e&&(this.parsed[e].children+=1),this.parsed.push({array_index:this.parsed.length,options_index:this.options_index,value:t.value,text:t.text,html:t.innerHTML,title:t.title?t.title:void 0,selected:t.selected,disabled:n===!0?n:t.disabled,group_array_index:e,group_label:null!=e?this.parsed[e].label:null,classes:t.className,style:t.style.cssText})):this.parsed.push({array_index:this.parsed.length,options_index:this.options_index,empty:!0}),this.options_index+=1},t}(),i.select_to_array=function(t){var e,n,r,o,a;for(o=new i,a=t.childNodes,n=0,r=a.length;n<r;n++)e=a[n],o.add_node(e);return o.parsed},e=function(){function t(e,n){this.form_field=e,this.options=null!=n?n:{},this.label_click_handler=r(this.label_click_handler,this),t.browser_is_supported()&&(this.is_multiple=this.form_field.multiple,this.set_default_text(),this.set_default_values(),this.setup(),this.set_up_html(),this.register_observers(),this.on_ready())}return t.prototype.set_default_values=function(){return this.click_test_action=function(t){return function(e){return t.test_active_click(e)}}(this),this.activate_action=function(t){return function(e){return t.activate_field(e)}}(this),this.active_field=!1,this.mouse_on_container=!1,this.results_showing=!1,this.result_highlighted=null,this.is_rtl=this.options.rtl||/\bchosen-rtl\b/.test(this.form_field.className),this.allow_single_deselect=null!=this.options.allow_single_deselect&&null!=this.form_field.options[0]&&""===this.form_field.options[0].text&&this.options.allow_single_deselect,this.disable_search_threshold=this.options.disable_search_threshold||0,this.disable_search=this.options.disable_search||!1,this.enable_split_word_search=null==this.options.enable_split_word_search||this.options.enable_split_word_search,this.group_search=null==this.options.group_search||this.options.group_search,this.search_contains=this.options.search_contains||!1,this.single_backstroke_delete=null==this.options.single_backstroke_delete||this.options.single_backstroke_delete,this.max_selected_options=this.options.max_selected_options||1/0,this.inherit_select_classes=this.options.inherit_select_classes||!1,this.display_selected_options=null==this.options.display_selected_options||this.options.display_selected_options,this.display_disabled_options=null==this.options.display_disabled_options||this.options.display_disabled_options,this.include_group_label_in_selected=this.options.include_group_label_in_selected||!1,this.max_shown_results=this.options.max_shown_results||Number.POSITIVE_INFINITY,this.case_sensitive_search=this.options.case_sensitive_search||!1,this.hide_results_on_select=null==this.options.hide_results_on_select||this.options.hide_results_on_select},t.prototype.set_default_text=function(){return this.form_field.getAttribute("data-placeholder")?this.default_text=this.form_field.getAttribute("data-placeholder"):this.is_multiple?this.default_text=this.options.placeholder_text_multiple||this.options.placeholder_text||t.default_multiple_text:this.default_text=this.options.placeholder_text_single||this.options.placeholder_text||t.default_single_text,this.default_text=this.escape_html(this.default_text),this.results_none_found=this.form_field.getAttribute("data-no_results_text")||this.options.no_results_text||t.default_no_result_text},t.prototype.choice_label=function(t){return this.include_group_label_in_selected&&null!=t.group_label?"<b class='group-name'>"+this.escape_html(t.group_label)+"</b>"+t.html:t.html},t.prototype.mouse_enter=function(){return this.mouse_on_container=!0},t.prototype.mouse_leave=function(){return this.mouse_on_container=!1},t.prototype.input_focus=function(t){if(this.is_multiple){if(!this.active_field)return setTimeout(function(t){return function(){return t.container_mousedown()}}(this),50)}else if(!this.active_field)return this.activate_field()},t.prototype.input_blur=function(t){if(!this.mouse_on_container)return this.active_field=!1,setTimeout(function(t){return function(){return t.blur_test()}}(this),100)},t.prototype.label_click_handler=function(t){return this.is_multiple?this.container_mousedown(t):this.activate_field()},t.prototype.results_option_build=function(t){var e,n,i,r,o,a,s;for(e="",s=0,a=this.results_data,r=0,o=a.length;r<o&&(n=a[r],i="",i=n.group?this.result_add_group(n):this.result_add_option(n),""!==i&&(s++,e+=i),(null!=t?t.first:void 0)&&(n.selected&&this.is_multiple?this.choice_build(n):n.selected&&!this.is_multiple&&this.single_set_selected_text(this.choice_label(n))),!(s>=this.max_shown_results));r++);return e},t.prototype.result_add_option=function(t){var e,n;return t.search_match&&this.include_option_in_results(t)?(e=[],t.disabled||t.selected&&this.is_multiple||e.push("active-result"),!t.disabled||t.selected&&this.is_multiple||e.push("disabled-result"),t.selected&&e.push("result-selected"),null!=t.group_array_index&&e.push("group-option"),""!==t.classes&&e.push(t.classes),n=document.createElement("li"),n.className=e.join(" "),t.style&&(n.style.cssText=t.style),n.setAttribute("data-option-array-index",t.array_index),n.innerHTML=t.highlighted_html||t.html,t.title&&(n.title=t.title),this.outerHTML(n)):""},t.prototype.result_add_group=function(t){var e,n;return(t.search_match||t.group_match)&&t.active_options>0?(e=[],e.push("group-result"),t.classes&&e.push(t.classes),n=document.createElement("li"),n.className=e.join(" "),n.innerHTML=t.highlighted_html||this.escape_html(t.label),t.title&&(n.title=t.title),this.outerHTML(n)):""},t.prototype.results_update_field=function(){if(this.set_default_text(),this.is_multiple||this.results_reset_cleanup(),this.result_clear_highlight(),this.results_build(),this.results_showing)return this.winnow_results()},t.prototype.reset_single_select_options=function(){var t,e,n,i,r;for(n=this.results_data,r=[],t=0,e=n.length;t<e;t++)i=n[t],i.selected?r.push(i.selected=!1):r.push(void 0);return r},t.prototype.results_toggle=function(){return this.results_showing?this.results_hide():this.results_show()},t.prototype.results_search=function(t){
return this.results_showing?this.winnow_results():this.results_show()},t.prototype.winnow_results=function(t){var e,n,i,r,o,a,s,l,u,c,h,d,f,p,g;for(this.no_results_clear(),c=0,s=this.get_search_text(),e=s.replace(/[-[\]{}()*+?.,\\^$|#\s]/g,"\\$&"),u=this.get_search_regex(e),l=this.results_data,i=0,r=l.length;i<r;i++)o=l[i],o.search_match=!1,h=null,d=null,o.highlighted_html="",this.include_option_in_results(o)&&(o.group&&(o.group_match=!1,o.active_options=0),null!=o.group_array_index&&this.results_data[o.group_array_index]&&(h=this.results_data[o.group_array_index],0===h.active_options&&h.search_match&&(c+=1),h.active_options+=1),g=o.group?o.label:o.text,o.group&&!this.group_search||(d=this.search_string_match(g,u),o.search_match=null!=d,o.search_match&&!o.group&&(c+=1),o.search_match?(s.length&&(f=d.index,a=g.slice(0,f),n=g.slice(f,f+s.length),p=g.slice(f+s.length),o.highlighted_html=this.escape_html(a)+"<em>"+this.escape_html(n)+"</em>"+this.escape_html(p)),null!=h&&(h.group_match=!0)):null!=o.group_array_index&&this.results_data[o.group_array_index].search_match&&(o.search_match=!0)));return this.result_clear_highlight(),c<1&&s.length?(this.update_results_content(""),this.no_results(s)):(this.update_results_content(this.results_option_build()),(null!=t?t.skip_highlight:void 0)?void 0:this.winnow_results_set_highlight())},t.prototype.get_search_regex=function(t){var e,n;return n=this.search_contains?t:"(^|\\s|\\b)"+t+"[^\\s]*",this.enable_split_word_search||this.search_contains||(n="^"+n),e=this.case_sensitive_search?"":"i",new RegExp(n,e)},t.prototype.search_string_match=function(t,e){var n;return n=e.exec(t),!this.search_contains&&(null!=n?n[1]:void 0)&&(n.index+=1),n},t.prototype.choices_count=function(){var t,e,n,i;if(null!=this.selected_option_count)return this.selected_option_count;for(this.selected_option_count=0,i=this.form_field.options,t=0,e=i.length;t<e;t++)n=i[t],n.selected&&(this.selected_option_count+=1);return this.selected_option_count},t.prototype.choices_click=function(t){if(t.preventDefault(),this.activate_field(),!this.results_showing&&!this.is_disabled)return this.results_show()},t.prototype.keydown_checker=function(t){var e,n;switch(n=null!=(e=t.which)?e:t.keyCode,this.search_field_scale(),8!==n&&this.pending_backstroke&&this.clear_backstroke(),n){case 8:this.backstroke_length=this.get_search_field_value().length;break;case 9:this.results_showing&&!this.is_multiple&&this.result_select(t),this.mouse_on_container=!1;break;case 13:this.results_showing&&t.preventDefault();break;case 27:this.results_showing&&t.preventDefault();break;case 32:this.disable_search&&t.preventDefault();break;case 38:t.preventDefault(),this.keyup_arrow();break;case 40:t.preventDefault(),this.keydown_arrow()}},t.prototype.keyup_checker=function(t){var e,n;switch(n=null!=(e=t.which)?e:t.keyCode,this.search_field_scale(),n){case 8:this.is_multiple&&this.backstroke_length<1&&this.choices_count()>0?this.keydown_backstroke():this.pending_backstroke||(this.result_clear_highlight(),this.results_search());break;case 13:t.preventDefault(),this.results_showing&&this.result_select(t);break;case 27:this.results_showing&&this.results_hide();break;case 9:case 16:case 17:case 18:case 38:case 40:case 91:break;default:this.results_search()}},t.prototype.clipboard_event_checker=function(t){if(!this.is_disabled)return setTimeout(function(t){return function(){return t.results_search()}}(this),50)},t.prototype.container_width=function(){return null!=this.options.width?this.options.width:this.form_field.offsetWidth+"px"},t.prototype.include_option_in_results=function(t){return!(this.is_multiple&&!this.display_selected_options&&t.selected)&&(!(!this.display_disabled_options&&t.disabled)&&!t.empty)},t.prototype.search_results_touchstart=function(t){return this.touch_started=!0,this.search_results_mouseover(t)},t.prototype.search_results_touchmove=function(t){return this.touch_started=!1,this.search_results_mouseout(t)},t.prototype.search_results_touchend=function(t){if(this.touch_started)return this.search_results_mouseup(t)},t.prototype.outerHTML=function(t){var e;return t.outerHTML?t.outerHTML:(e=document.createElement("div"),e.appendChild(t),e.innerHTML)},t.prototype.get_single_html=function(){return'<a class="chosen-single chosen-default">\n  <span>'+this.default_text+'</span>\n  <div><b></b></div>\n</a>\n<div class="chosen-drop">\n  <div class="chosen-search">\n    <input class="chosen-search-input" type="text" autocomplete="off" />\n  </div>\n  <ul class="chosen-results"></ul>\n</div>'},t.prototype.get_multi_html=function(){return'<ul class="chosen-choices">\n  <li class="search-field">\n    <input class="chosen-search-input" type="text" autocomplete="off" value="'+this.default_text+'" />\n  </li>\n</ul>\n<div class="chosen-drop">\n  <ul class="chosen-results"></ul>\n</div>'},t.prototype.get_no_results_html=function(t){return'<li class="no-results">\n  '+this.results_none_found+" <span>"+this.escape_html(t)+"</span>\n</li>"},t.browser_is_supported=function(){return"Microsoft Internet Explorer"===window.navigator.appName?document.documentMode>=8:!(/iP(od|hone)/i.test(window.navigator.userAgent)||/IEMobile/i.test(window.navigator.userAgent)||/Windows Phone/i.test(window.navigator.userAgent)||/BlackBerry/i.test(window.navigator.userAgent)||/BB10/i.test(window.navigator.userAgent)||/Android.*Mobile/i.test(window.navigator.userAgent))},t.default_multiple_text="Select Some Options",t.default_single_text="Select an Option",t.default_no_result_text="No results match",t}(),t=jQuery,t.fn.extend({chosen:function(i){return e.browser_is_supported()?this.each(function(e){var r,o;return r=t(this),o=r.data("chosen"),"destroy"===i?void(o instanceof n&&o.destroy()):void(o instanceof n||r.data("chosen",new n(this,i)))}):this}}),n=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}return o(n,e),n.prototype.setup=function(){return this.form_field_jq=t(this.form_field),this.current_selectedIndex=this.form_field.selectedIndex},n.prototype.set_up_html=function(){var e,n;return e=["chosen-container"],e.push("chosen-container-"+(this.is_multiple?"multi":"single")),this.inherit_select_classes&&this.form_field.className&&e.push(this.form_field.className),this.is_rtl&&e.push("chosen-rtl"),n={"class":e.join(" "),title:this.form_field.title},this.form_field.id.length&&(n.id=this.form_field.id.replace(/[^\w]/g,"_")+"_chosen"),this.container=t("<div />",n),this.container.width(this.container_width()),this.is_multiple?this.container.html(this.get_multi_html()):this.container.html(this.get_single_html()),this.form_field_jq.hide().after(this.container),this.dropdown=this.container.find("div.chosen-drop").first(),this.search_field=this.container.find("input").first(),this.search_results=this.container.find("ul.chosen-results").first(),this.search_field_scale(),this.search_no_results=this.container.find("li.no-results").first(),this.is_multiple?(this.search_choices=this.container.find("ul.chosen-choices").first(),this.search_container=this.container.find("li.search-field").first()):(this.search_container=this.container.find("div.chosen-search").first(),this.selected_item=this.container.find(".chosen-single").first()),this.results_build(),this.set_tab_index(),this.set_label_behavior()},n.prototype.on_ready=function(){return this.form_field_jq.trigger("chosen:ready",{chosen:this})},n.prototype.register_observers=function(){return this.container.on("touchstart.chosen",function(t){return function(e){t.container_mousedown(e)}}(this)),this.container.on("touchend.chosen",function(t){return function(e){t.container_mouseup(e)}}(this)),this.container.on("mousedown.chosen",function(t){return function(e){t.container_mousedown(e)}}(this)),this.container.on("mouseup.chosen",function(t){return function(e){t.container_mouseup(e)}}(this)),this.container.on("mouseenter.chosen",function(t){return function(e){t.mouse_enter(e)}}(this)),this.container.on("mouseleave.chosen",function(t){return function(e){t.mouse_leave(e)}}(this)),this.search_results.on("mouseup.chosen",function(t){return function(e){t.search_results_mouseup(e)}}(this)),this.search_results.on("mouseover.chosen",function(t){return function(e){t.search_results_mouseover(e)}}(this)),this.search_results.on("mouseout.chosen",function(t){return function(e){t.search_results_mouseout(e)}}(this)),this.search_results.on("mousewheel.chosen DOMMouseScroll.chosen",function(t){return function(e){t.search_results_mousewheel(e)}}(this)),this.search_results.on("touchstart.chosen",function(t){return function(e){t.search_results_touchstart(e)}}(this)),this.search_results.on("touchmove.chosen",function(t){return function(e){t.search_results_touchmove(e)}}(this)),this.search_results.on("touchend.chosen",function(t){return function(e){t.search_results_touchend(e)}}(this)),this.form_field_jq.on("chosen:updated.chosen",function(t){return function(e){t.results_update_field(e)}}(this)),this.form_field_jq.on("chosen:activate.chosen",function(t){return function(e){t.activate_field(e)}}(this)),this.form_field_jq.on("chosen:open.chosen",function(t){return function(e){t.container_mousedown(e)}}(this)),this.form_field_jq.on("chosen:close.chosen",function(t){return function(e){t.close_field(e)}}(this)),this.search_field.on("blur.chosen",function(t){return function(e){t.input_blur(e)}}(this)),this.search_field.on("keyup.chosen",function(t){return function(e){t.keyup_checker(e)}}(this)),this.search_field.on("keydown.chosen",function(t){return function(e){t.keydown_checker(e)}}(this)),this.search_field.on("focus.chosen",function(t){return function(e){t.input_focus(e)}}(this)),this.search_field.on("cut.chosen",function(t){return function(e){t.clipboard_event_checker(e)}}(this)),this.search_field.on("paste.chosen",function(t){return function(e){t.clipboard_event_checker(e)}}(this)),this.is_multiple?this.search_choices.on("click.chosen",function(t){return function(e){t.choices_click(e)}}(this)):this.container.on("click.chosen",function(t){t.preventDefault()})},n.prototype.destroy=function(){return t(this.container[0].ownerDocument).off("click.chosen",this.click_test_action),this.form_field_label.length>0&&this.form_field_label.off("click.chosen"),this.search_field[0].tabIndex&&(this.form_field_jq[0].tabIndex=this.search_field[0].tabIndex),this.container.remove(),this.form_field_jq.removeData("chosen"),this.form_field_jq.show()},n.prototype.search_field_disabled=function(){return this.is_disabled=this.form_field.disabled||this.form_field_jq.parents("fieldset").is(":disabled"),this.container.toggleClass("chosen-disabled",this.is_disabled),this.search_field[0].disabled=this.is_disabled,this.is_multiple||this.selected_item.off("focus.chosen",this.activate_field),this.is_disabled?this.close_field():this.is_multiple?void 0:this.selected_item.on("focus.chosen",this.activate_field)},n.prototype.container_mousedown=function(e){var n;if(!this.is_disabled)return!e||"mousedown"!==(n=e.type)&&"touchstart"!==n||this.results_showing||e.preventDefault(),null!=e&&t(e.target).hasClass("search-choice-close")?void 0:(this.active_field?this.is_multiple||!e||t(e.target)[0]!==this.selected_item[0]&&!t(e.target).parents("a.chosen-single").length||(e.preventDefault(),this.results_toggle()):(this.is_multiple&&this.search_field.val(""),t(this.container[0].ownerDocument).on("click.chosen",this.click_test_action),this.results_show()),this.activate_field())},n.prototype.container_mouseup=function(t){if("ABBR"===t.target.nodeName&&!this.is_disabled)return this.results_reset(t)},n.prototype.search_results_mousewheel=function(t){var e;if(t.originalEvent&&(e=t.originalEvent.deltaY||-t.originalEvent.wheelDelta||t.originalEvent.detail),null!=e)return t.preventDefault(),"DOMMouseScroll"===t.type&&(e=40*e),this.search_results.scrollTop(e+this.search_results.scrollTop())},n.prototype.blur_test=function(t){if(!this.active_field&&this.container.hasClass("chosen-container-active"))return this.close_field()},n.prototype.close_field=function(){return t(this.container[0].ownerDocument).off("click.chosen",this.click_test_action),this.active_field=!1,this.results_hide(),this.container.removeClass("chosen-container-active"),this.clear_backstroke(),this.show_search_field_default(),this.search_field_scale(),this.search_field.blur()},n.prototype.activate_field=function(){if(!this.is_disabled)return this.container.addClass("chosen-container-active"),this.active_field=!0,this.search_field.val(this.search_field.val()),this.search_field.focus()},n.prototype.test_active_click=function(e){var n;return n=t(e.target).closest(".chosen-container"),n.length&&this.container[0]===n[0]?this.active_field=!0:this.close_field()},n.prototype.results_build=function(){return this.parsing=!0,this.selected_option_count=null,this.results_data=i.select_to_array(this.form_field),this.is_multiple?this.search_choices.find("li.search-choice").remove():(this.single_set_selected_text(),this.disable_search||this.form_field.options.length<=this.disable_search_threshold?(this.search_field[0].readOnly=!0,this.container.addClass("chosen-container-single-nosearch")):(this.search_field[0].readOnly=!1,this.container.removeClass("chosen-container-single-nosearch"))),this.update_results_content(this.results_option_build({first:!0})),this.search_field_disabled(),this.show_search_field_default(),this.search_field_scale(),this.parsing=!1},n.prototype.result_do_highlight=function(t){var e,n,i,r,o;if(t.length){if(this.result_clear_highlight(),this.result_highlight=t,this.result_highlight.addClass("highlighted"),i=parseInt(this.search_results.css("maxHeight"),10),o=this.search_results.scrollTop(),r=i+o,n=this.result_highlight.position().top+this.search_results.scrollTop(),e=n+this.result_highlight.outerHeight(),e>=r)return this.search_results.scrollTop(e-i>0?e-i:0);if(n<o)return this.search_results.scrollTop(n)}},n.prototype.result_clear_highlight=function(){return this.result_highlight&&this.result_highlight.removeClass("highlighted"),this.result_highlight=null},n.prototype.results_show=function(){return this.is_multiple&&this.max_selected_options<=this.choices_count()?(this.form_field_jq.trigger("chosen:maxselected",{chosen:this}),!1):(this.container.addClass("chosen-with-drop"),this.results_showing=!0,this.search_field.focus(),this.search_field.val(this.get_search_field_value()),this.winnow_results(),this.form_field_jq.trigger("chosen:showing_dropdown",{chosen:this}))},n.prototype.update_results_content=function(t){return this.search_results.html(t)},n.prototype.results_hide=function(){return this.results_showing&&(this.result_clear_highlight(),this.container.removeClass("chosen-with-drop"),this.form_field_jq.trigger("chosen:hiding_dropdown",{chosen:this})),this.results_showing=!1},n.prototype.set_tab_index=function(t){var e;if(this.form_field.tabIndex)return e=this.form_field.tabIndex,this.form_field.tabIndex=-1,this.search_field[0].tabIndex=e},n.prototype.set_label_behavior=function(){if(this.form_field_label=this.form_field_jq.parents("label"),!this.form_field_label.length&&this.form_field.id.length&&(this.form_field_label=t("label[for='"+this.form_field.id+"']")),this.form_field_label.length>0)return this.form_field_label.on("click.chosen",this.label_click_handler)},n.prototype.show_search_field_default=function(){return this.is_multiple&&this.choices_count()<1&&!this.active_field?(this.search_field.val(this.default_text),this.search_field.addClass("default")):(this.search_field.val(""),this.search_field.removeClass("default"))},n.prototype.search_results_mouseup=function(e){var n;if(n=t(e.target).hasClass("active-result")?t(e.target):t(e.target).parents(".active-result").first(),n.length)return this.result_highlight=n,this.result_select(e),this.search_field.focus()},n.prototype.search_results_mouseover=function(e){var n;if(n=t(e.target).hasClass("active-result")?t(e.target):t(e.target).parents(".active-result").first())return this.result_do_highlight(n)},n.prototype.search_results_mouseout=function(e){if(t(e.target).hasClass("active-result")||t(e.target).parents(".active-result").first())return this.result_clear_highlight()},n.prototype.choice_build=function(e){var n,i;return n=t("<li />",{"class":"search-choice"}).html("<span>"+this.choice_label(e)+"</span>"),e.disabled?n.addClass("search-choice-disabled"):(i=t("<a />",{"class":"search-choice-close","data-option-array-index":e.array_index}),i.on("click.chosen",function(t){return function(e){return t.choice_destroy_link_click(e)}}(this)),n.append(i)),this.search_container.before(n)},n.prototype.choice_destroy_link_click=function(e){if(e.preventDefault(),e.stopPropagation(),!this.is_disabled)return this.choice_destroy(t(e.target))},n.prototype.choice_destroy=function(t){if(this.result_deselect(t[0].getAttribute("data-option-array-index")))return this.active_field?this.search_field.focus():this.show_search_field_default(),this.is_multiple&&this.choices_count()>0&&this.get_search_field_value().length<1&&this.results_hide(),t.parents("li").first().remove(),this.search_field_scale()},n.prototype.results_reset=function(){if(this.reset_single_select_options(),this.form_field.options[0].selected=!0,this.single_set_selected_text(),this.show_search_field_default(),this.results_reset_cleanup(),this.trigger_form_field_change(),this.active_field)return this.results_hide()},n.prototype.results_reset_cleanup=function(){return this.current_selectedIndex=this.form_field.selectedIndex,this.selected_item.find("abbr").remove()},n.prototype.result_select=function(t){var e,n;if(this.result_highlight)return e=this.result_highlight,this.result_clear_highlight(),this.is_multiple&&this.max_selected_options<=this.choices_count()?(this.form_field_jq.trigger("chosen:maxselected",{chosen:this}),!1):(this.is_multiple?e.removeClass("active-result"):this.reset_single_select_options(),e.addClass("result-selected"),n=this.results_data[e[0].getAttribute("data-option-array-index")],n.selected=!0,this.form_field.options[n.options_index].selected=!0,this.selected_option_count=null,this.is_multiple?this.choice_build(n):this.single_set_selected_text(this.choice_label(n)),this.is_multiple&&(!this.hide_results_on_select||t.metaKey||t.ctrlKey)?t.metaKey||t.ctrlKey?this.winnow_results({skip_highlight:!0}):(this.search_field.val(""),this.winnow_results()):(this.results_hide(),this.show_search_field_default()),(this.is_multiple||this.form_field.selectedIndex!==this.current_selectedIndex)&&this.trigger_form_field_change({selected:this.form_field.options[n.options_index].value}),this.current_selectedIndex=this.form_field.selectedIndex,t.preventDefault(),this.search_field_scale())},n.prototype.single_set_selected_text=function(t){return null==t&&(t=this.default_text),t===this.default_text?this.selected_item.addClass("chosen-default"):(this.single_deselect_control_build(),this.selected_item.removeClass("chosen-default")),this.selected_item.find("span").html(t)},n.prototype.result_deselect=function(t){var e;return e=this.results_data[t],!this.form_field.options[e.options_index].disabled&&(e.selected=!1,this.form_field.options[e.options_index].selected=!1,this.selected_option_count=null,this.result_clear_highlight(),this.results_showing&&this.winnow_results(),this.trigger_form_field_change({deselected:this.form_field.options[e.options_index].value}),this.search_field_scale(),!0)},n.prototype.single_deselect_control_build=function(){if(this.allow_single_deselect)return this.selected_item.find("abbr").length||this.selected_item.find("span").first().after('<abbr class="search-choice-close"></abbr>'),this.selected_item.addClass("chosen-single-with-deselect")},n.prototype.get_search_field_value=function(){return this.search_field.val()},n.prototype.get_search_text=function(){return t.trim(this.get_search_field_value())},n.prototype.escape_html=function(e){return t("<div/>").text(e).html()},n.prototype.winnow_results_set_highlight=function(){var t,e;if(e=this.is_multiple?[]:this.search_results.find(".result-selected.active-result"),t=e.length?e.first():this.search_results.find(".active-result").first(),null!=t)return this.result_do_highlight(t)},n.prototype.no_results=function(t){var e;return e=this.get_no_results_html(t),this.search_results.append(e),this.form_field_jq.trigger("chosen:no_results",{chosen:this})},n.prototype.no_results_clear=function(){return this.search_results.find(".no-results").remove()},n.prototype.keydown_arrow=function(){var t;return this.results_showing&&this.result_highlight?(t=this.result_highlight.nextAll("li.active-result").first())?this.result_do_highlight(t):void 0:this.results_show()},n.prototype.keyup_arrow=function(){var t;return this.results_showing||this.is_multiple?this.result_highlight?(t=this.result_highlight.prevAll("li.active-result"),t.length?this.result_do_highlight(t.first()):(this.choices_count()>0&&this.results_hide(),this.result_clear_highlight())):void 0:this.results_show()},n.prototype.keydown_backstroke=function(){var t;return this.pending_backstroke?(this.choice_destroy(this.pending_backstroke.find("a").first()),this.clear_backstroke()):(t=this.search_container.siblings("li.search-choice").last(),t.length&&!t.hasClass("search-choice-disabled")?(this.pending_backstroke=t,this.single_backstroke_delete?this.keydown_backstroke():this.pending_backstroke.addClass("search-choice-focus")):void 0)},n.prototype.clear_backstroke=function(){return this.pending_backstroke&&this.pending_backstroke.removeClass("search-choice-focus"),this.pending_backstroke=null},n.prototype.search_field_scale=function(){var e,n,i,r,o,a,s;if(this.is_multiple){for(o={position:"absolute",left:"-1000px",top:"-1000px",display:"none",whiteSpace:"pre"},a=["fontSize","fontStyle","fontWeight","fontFamily","lineHeight","textTransform","letterSpacing"],n=0,i=a.length;n<i;n++)r=a[n],o[r]=this.search_field.css(r);return e=t("<div />").css(o),e.text(this.get_search_field_value()),t("body").append(e),s=e.width()+25,e.remove(),this.container.is(":visible")&&(s=Math.min(this.container.outerWidth()-10,s)),this.search_field.width(s)}},n.prototype.trigger_form_field_change=function(t){return this.form_field_jq.trigger("input",t),this.form_field_jq.trigger("change",t)},n}(e)}.call(this),function(t,e){"object"==typeof exports&&"undefined"!=typeof module?module.exports=e(require("jquery")):"function"==typeof define&&define.amd?define(["jquery"],e):(t=t||self,t.Util=e(t.jQuery))}(this,function(t){"use strict";function e(t){return{}.toString.call(t).match(/\s([a-z]+)/i)[1].toLowerCase()}function n(){return{bindType:o,delegateType:o,handle:function(e){if(t(e.target).is(this))return e.handleObj.handler.apply(this,arguments)}}}function i(e){var n=this,i=!1;return t(this).one(l.TRANSITION_END,function(){i=!0}),setTimeout(function(){i||l.triggerTransitionEnd(n)},e),this}function r(){t.fn.emulateTransitionEnd=i,t.event.special[l.TRANSITION_END]=n()}t=t&&t.hasOwnProperty("default")?t["default"]:t;var o="transitionend",a=1e6,s=1e3,l={TRANSITION_END:"bsTransitionEnd",getUID:function(t){do t+=~~(Math.random()*a);while(document.getElementById(t));return t},getSelectorFromElement:function(t){var e=t.getAttribute("data-target");if(!e||"#"===e){var n=t.getAttribute("href");e=n&&"#"!==n?n.trim():""}try{return document.querySelector(e)?e:null}catch(i){return null}},getTransitionDurationFromElement:function(e){if(!e)return 0;var n=t(e).css("transition-duration"),i=t(e).css("transition-delay"),r=parseFloat(n),o=parseFloat(i);return r||o?(n=n.split(",")[0],i=i.split(",")[0],(parseFloat(n)+parseFloat(i))*s):0},reflow:function(t){return t.offsetHeight},triggerTransitionEnd:function(e){t(e).trigger(o)},supportsTransitionEnd:function(){return Boolean(o)},isElement:function(t){return(t[0]||t).nodeType},typeCheckConfig:function(t,n,i){for(var r in i)if(Object.prototype.hasOwnProperty.call(i,r)){var o=i[r],a=n[r],s=a&&l.isElement(a)?"element":e(a);if(!new RegExp(o).test(s))throw new Error(t.toUpperCase()+": "+('Option "'+r+'" provided type "'+s+'" ')+('but expected type "'+o+'".'))}},findShadowRoot:function(t){if(!document.documentElement.attachShadow)return null;if("function"==typeof t.getRootNode){var e=t.getRootNode();return e instanceof ShadowRoot?e:null}return t instanceof ShadowRoot?t:t.parentNode?l.findShadowRoot(t.parentNode):null}};return r(),l}),function(t,e){"object"==typeof exports&&"undefined"!=typeof module?module.exports=e(require("jquery"),require("./util.js")):"function"==typeof define&&define.amd?define(["jquery","./util.js"],e):(t=t||self,t.Alert=e(t.jQuery,t.Util))}(this,function(t,e){"use strict";function n(t,e){for(var n=0;n<e.length;n++){var i=e[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}function i(t,e,i){return e&&n(t.prototype,e),i&&n(t,i),t}t=t&&t.hasOwnProperty("default")?t["default"]:t,e=e&&e.hasOwnProperty("default")?e["default"]:e;var r="alert",o="4.3.1",a="bs.alert",s="."+a,l=".data-api",u=t.fn[r],c={DISMISS:'[data-dismiss="alert"]'},h={CLOSE:"close"+s,CLOSED:"closed"+s,CLICK_DATA_API:"click"+s+l},d={ALERT:"alert",FADE:"fade",SHOW:"show"},f=function(){function n(t){this._element=t}var r=n.prototype;return r.close=function(t){var e=this._element;t&&(e=this._getRootElement(t));var n=this._triggerCloseEvent(e);n.isDefaultPrevented()||this._removeElement(e)},r.dispose=function(){t.removeData(this._element,a),this._element=null},r._getRootElement=function(n){var i=e.getSelectorFromElement(n),r=!1;return i&&(r=document.querySelector(i)),r||(r=t(n).closest("."+d.ALERT)[0]),r},r._triggerCloseEvent=function(e){var n=t.Event(h.CLOSE);return t(e).trigger(n),n},r._removeElement=function(n){var i=this;if(t(n).removeClass(d.SHOW),!t(n).hasClass(d.FADE))return void this._destroyElement(n);var r=e.getTransitionDurationFromElement(n);t(n).one(e.TRANSITION_END,function(t){return i._destroyElement(n,t)}).emulateTransitionEnd(r)},r._destroyElement=function(e){t(e).detach().trigger(h.CLOSED).remove()},n._jQueryInterface=function(e){return this.each(function(){var i=t(this),r=i.data(a);r||(r=new n(this),i.data(a,r)),"close"===e&&r[e](this)})},n._handleDismiss=function(t){return function(e){e&&e.preventDefault(),t.close(this)}},i(n,null,[{key:"VERSION",get:function(){return o}}]),n}();return t(document).on(h.CLICK_DATA_API,c.DISMISS,f._handleDismiss(new f)),t.fn[r]=f._jQueryInterface,t.fn[r].Constructor=f,t.fn[r].noConflict=function(){return t.fn[r]=u,f._jQueryInterface},f}),function(t,e){"object"==typeof exports&&"undefined"!=typeof module?module.exports=e(require("jquery")):"function"==typeof define&&define.amd?define(["jquery"],e):(t=t||self,t.Button=e(t.jQuery))}(this,function(t){"use strict";function e(t,e){for(var n=0;n<e.length;n++){var i=e[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}function n(t,n,i){return n&&e(t.prototype,n),i&&e(t,i),t}t=t&&t.hasOwnProperty("default")?t["default"]:t;var i="button",r="4.3.1",o="bs.button",a="."+o,s=".data-api",l=t.fn[i],u={ACTIVE:"active",BUTTON:"btn",FOCUS:"focus"},c={DATA_TOGGLE_CARROT:'[data-toggle^="button"]',DATA_TOGGLE:'[data-toggle="buttons"]',INPUT:'input:not([type="hidden"])',ACTIVE:".active",BUTTON:".btn"},h={CLICK_DATA_API:"click"+a+s,FOCUS_BLUR_DATA_API:"focus"+a+s+" "+("blur"+a+s)},d=function(){function e(t){this._element=t}var i=e.prototype;return i.toggle=function(){var e=!0,n=!0,i=t(this._element).closest(c.DATA_TOGGLE)[0];if(i){var r=this._element.querySelector(c.INPUT);if(r){if("radio"===r.type)if(r.checked&&this._element.classList.contains(u.ACTIVE))e=!1;else{var o=i.querySelector(c.ACTIVE);o&&t(o).removeClass(u.ACTIVE)}if(e){if(r.hasAttribute("disabled")||i.hasAttribute("disabled")||r.classList.contains("disabled")||i.classList.contains("disabled"))return;r.checked=!this._element.classList.contains(u.ACTIVE),t(r).trigger("change")}r.focus(),n=!1}}n&&this._element.setAttribute("aria-pressed",!this._element.classList.contains(u.ACTIVE)),e&&t(this._element).toggleClass(u.ACTIVE)},i.dispose=function(){t.removeData(this._element,o),this._element=null},e._jQueryInterface=function(n){return this.each(function(){var i=t(this).data(o);i||(i=new e(this),t(this).data(o,i)),"toggle"===n&&i[n]()})},n(e,null,[{key:"VERSION",get:function(){return r}}]),e}();return t(document).on(h.CLICK_DATA_API,c.DATA_TOGGLE_CARROT,function(e){e.preventDefault();var n=e.target;t(n).hasClass(u.BUTTON)||(n=t(n).closest(c.BUTTON)),d._jQueryInterface.call(t(n),"toggle")}).on(h.FOCUS_BLUR_DATA_API,c.DATA_TOGGLE_CARROT,function(e){var n=t(e.target).closest(c.BUTTON)[0];t(n).toggleClass(u.FOCUS,/^focus(in)?$/.test(e.type))}),t.fn[i]=d._jQueryInterface,t.fn[i].Constructor=d,t.fn[i].noConflict=function(){return t.fn[i]=l,d._jQueryInterface},d}),function(t,e){"object"==typeof exports&&"undefined"!=typeof module?module.exports=e(require("jquery"),require("./util.js")):"function"==typeof define&&define.amd?define(["jquery","./util.js"],e):(t=t||self,t.Carousel=e(t.jQuery,t.Util))}(this,function(t,e){"use strict";function n(t,e){for(var n=0;n<e.length;n++){var i=e[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}function i(t,e,i){return e&&n(t.prototype,e),i&&n(t,i),t}function r(t,e,n){return e in t?Object.defineProperty(t,e,{value:n,enumerable:!0,configurable:!0,writable:!0}):t[e]=n,t}function o(t){for(var e=1;e<arguments.length;e++){var n=null!=arguments[e]?arguments[e]:{},i=Object.keys(n);"function"==typeof Object.getOwnPropertySymbols&&(i=i.concat(Object.getOwnPropertySymbols(n).filter(function(t){return Object.getOwnPropertyDescriptor(n,t).enumerable}))),i.forEach(function(e){r(t,e,n[e])})}return t}t=t&&t.hasOwnProperty("default")?t["default"]:t,e=e&&e.hasOwnProperty("default")?e["default"]:e;var a="carousel",s="4.3.1",l="bs.carousel",u="."+l,c=".data-api",h=t.fn[a],d=37,f=39,p=500,g=40,m={interval:5e3,keyboard:!0,slide:!1,pause:"hover",wrap:!0,touch:!0},v={interval:"(number|boolean)",keyboard:"boolean",slide:"(boolean|string)",pause:"(string|boolean)",wrap:"boolean",touch:"boolean"},y={NEXT:"next",PREV:"prev",LEFT:"left",RIGHT:"right"},b={SLIDE:"slide"+u,SLID:"slid"+u,KEYDOWN:"keydown"+u,MOUSEENTER:"mouseenter"+u,MOUSELEAVE:"mouseleave"+u,TOUCHSTART:"touchstart"+u,TOUCHMOVE:"touchmove"+u,TOUCHEND:"touchend"+u,POINTERDOWN:"pointerdown"+u,POINTERUP:"pointerup"+u,DRAG_START:"dragstart"+u,LOAD_DATA_API:"load"+u+c,CLICK_DATA_API:"click"+u+c},_={CAROUSEL:"carousel",ACTIVE:"active",SLIDE:"slide",RIGHT:"carousel-item-right",LEFT:"carousel-item-left",NEXT:"carousel-item-next",PREV:"carousel-item-prev",ITEM:"carousel-item",POINTER_EVENT:"pointer-event"},x={ACTIVE:".active",ACTIVE_ITEM:".active.carousel-item",ITEM:".carousel-item",ITEM_IMG:".carousel-item img",NEXT_PREV:".carousel-item-next, .carousel-item-prev",INDICATORS:".carousel-indicators",DATA_SLIDE:"[data-slide], [data-slide-to]",DATA_RIDE:'[data-ride="carousel"]'},w={TOUCH:"touch",PEN:"pen"},D=function(){function n(t,e){this._items=null,this._interval=null,this._activeElement=null,this._isPaused=!1,this._isSliding=!1,this.touchTimeout=null,this.touchStartX=0,this.touchDeltaX=0,this._config=this._getConfig(e),this._element=t,this._indicatorsElement=this._element.querySelector(x.INDICATORS),this._touchSupported="ontouchstart"in document.documentElement||navigator.maxTouchPoints>0,this._pointerEvent=Boolean(window.PointerEvent||window.MSPointerEvent),this._addEventListeners()}var r=n.prototype;return r.next=function(){this._isSliding||this._slide(y.NEXT)},r.nextWhenVisible=function(){!document.hidden&&t(this._element).is(":visible")&&"hidden"!==t(this._element).css("visibility")&&this.next()},r.prev=function(){this._isSliding||this._slide(y.PREV)},r.pause=function(t){t||(this._isPaused=!0),this._element.querySelector(x.NEXT_PREV)&&(e.triggerTransitionEnd(this._element),this.cycle(!0)),clearInterval(this._interval),this._interval=null},r.cycle=function(t){t||(this._isPaused=!1),this._interval&&(clearInterval(this._interval),this._interval=null),this._config.interval&&!this._isPaused&&(this._interval=setInterval((document.visibilityState?this.nextWhenVisible:this.next).bind(this),this._config.interval));
},r.to=function(e){var n=this;this._activeElement=this._element.querySelector(x.ACTIVE_ITEM);var i=this._getItemIndex(this._activeElement);if(!(e>this._items.length-1||e<0)){if(this._isSliding)return void t(this._element).one(b.SLID,function(){return n.to(e)});if(i===e)return this.pause(),void this.cycle();var r=e>i?y.NEXT:y.PREV;this._slide(r,this._items[e])}},r.dispose=function(){t(this._element).off(u),t.removeData(this._element,l),this._items=null,this._config=null,this._element=null,this._interval=null,this._isPaused=null,this._isSliding=null,this._activeElement=null,this._indicatorsElement=null},r._getConfig=function(t){return t=o({},m,t),e.typeCheckConfig(a,t,v),t},r._handleSwipe=function(){var t=Math.abs(this.touchDeltaX);if(!(t<=g)){var e=t/this.touchDeltaX;e>0&&this.prev(),e<0&&this.next()}},r._addEventListeners=function(){var e=this;this._config.keyboard&&t(this._element).on(b.KEYDOWN,function(t){return e._keydown(t)}),"hover"===this._config.pause&&t(this._element).on(b.MOUSEENTER,function(t){return e.pause(t)}).on(b.MOUSELEAVE,function(t){return e.cycle(t)}),this._config.touch&&this._addTouchEventListeners()},r._addTouchEventListeners=function(){var e=this;if(this._touchSupported){var n=function(t){e._pointerEvent&&w[t.originalEvent.pointerType.toUpperCase()]?e.touchStartX=t.originalEvent.clientX:e._pointerEvent||(e.touchStartX=t.originalEvent.touches[0].clientX)},i=function(t){t.originalEvent.touches&&t.originalEvent.touches.length>1?e.touchDeltaX=0:e.touchDeltaX=t.originalEvent.touches[0].clientX-e.touchStartX},r=function(t){e._pointerEvent&&w[t.originalEvent.pointerType.toUpperCase()]&&(e.touchDeltaX=t.originalEvent.clientX-e.touchStartX),e._handleSwipe(),"hover"===e._config.pause&&(e.pause(),e.touchTimeout&&clearTimeout(e.touchTimeout),e.touchTimeout=setTimeout(function(t){return e.cycle(t)},p+e._config.interval))};t(this._element.querySelectorAll(x.ITEM_IMG)).on(b.DRAG_START,function(t){return t.preventDefault()}),this._pointerEvent?(t(this._element).on(b.POINTERDOWN,function(t){return n(t)}),t(this._element).on(b.POINTERUP,function(t){return r(t)}),this._element.classList.add(_.POINTER_EVENT)):(t(this._element).on(b.TOUCHSTART,function(t){return n(t)}),t(this._element).on(b.TOUCHMOVE,function(t){return i(t)}),t(this._element).on(b.TOUCHEND,function(t){return r(t)}))}},r._keydown=function(t){if(!/input|textarea/i.test(t.target.tagName))switch(t.which){case d:t.preventDefault(),this.prev();break;case f:t.preventDefault(),this.next()}},r._getItemIndex=function(t){return this._items=t&&t.parentNode?[].slice.call(t.parentNode.querySelectorAll(x.ITEM)):[],this._items.indexOf(t)},r._getItemByDirection=function(t,e){var n=t===y.NEXT,i=t===y.PREV,r=this._getItemIndex(e),o=this._items.length-1,a=i&&0===r||n&&r===o;if(a&&!this._config.wrap)return e;var s=t===y.PREV?-1:1,l=(r+s)%this._items.length;return l===-1?this._items[this._items.length-1]:this._items[l]},r._triggerSlideEvent=function(e,n){var i=this._getItemIndex(e),r=this._getItemIndex(this._element.querySelector(x.ACTIVE_ITEM)),o=t.Event(b.SLIDE,{relatedTarget:e,direction:n,from:r,to:i});return t(this._element).trigger(o),o},r._setActiveIndicatorElement=function(e){if(this._indicatorsElement){var n=[].slice.call(this._indicatorsElement.querySelectorAll(x.ACTIVE));t(n).removeClass(_.ACTIVE);var i=this._indicatorsElement.children[this._getItemIndex(e)];i&&t(i).addClass(_.ACTIVE)}},r._slide=function(n,i){var r,o,a,s=this,l=this._element.querySelector(x.ACTIVE_ITEM),u=this._getItemIndex(l),c=i||l&&this._getItemByDirection(n,l),h=this._getItemIndex(c),d=Boolean(this._interval);if(n===y.NEXT?(r=_.LEFT,o=_.NEXT,a=y.LEFT):(r=_.RIGHT,o=_.PREV,a=y.RIGHT),c&&t(c).hasClass(_.ACTIVE))return void(this._isSliding=!1);var f=this._triggerSlideEvent(c,a);if(!f.isDefaultPrevented()&&l&&c){this._isSliding=!0,d&&this.pause(),this._setActiveIndicatorElement(c);var p=t.Event(b.SLID,{relatedTarget:c,direction:a,from:u,to:h});if(t(this._element).hasClass(_.SLIDE)){t(c).addClass(o),e.reflow(c),t(l).addClass(r),t(c).addClass(r);var g=parseInt(c.getAttribute("data-interval"),10);g?(this._config.defaultInterval=this._config.defaultInterval||this._config.interval,this._config.interval=g):this._config.interval=this._config.defaultInterval||this._config.interval;var m=e.getTransitionDurationFromElement(l);t(l).one(e.TRANSITION_END,function(){t(c).removeClass(r+" "+o).addClass(_.ACTIVE),t(l).removeClass(_.ACTIVE+" "+o+" "+r),s._isSliding=!1,setTimeout(function(){return t(s._element).trigger(p)},0)}).emulateTransitionEnd(m)}else t(l).removeClass(_.ACTIVE),t(c).addClass(_.ACTIVE),this._isSliding=!1,t(this._element).trigger(p);d&&this.cycle()}},n._jQueryInterface=function(e){return this.each(function(){var i=t(this).data(l),r=o({},m,t(this).data());"object"==typeof e&&(r=o({},r,e));var a="string"==typeof e?e:r.slide;if(i||(i=new n(this,r),t(this).data(l,i)),"number"==typeof e)i.to(e);else if("string"==typeof a){if("undefined"==typeof i[a])throw new TypeError('No method named "'+a+'"');i[a]()}else r.interval&&r.ride&&(i.pause(),i.cycle())})},n._dataApiClickHandler=function(i){var r=e.getSelectorFromElement(this);if(r){var a=t(r)[0];if(a&&t(a).hasClass(_.CAROUSEL)){var s=o({},t(a).data(),t(this).data()),u=this.getAttribute("data-slide-to");u&&(s.interval=!1),n._jQueryInterface.call(t(a),s),u&&t(a).data(l).to(u),i.preventDefault()}}},i(n,null,[{key:"VERSION",get:function(){return s}},{key:"Default",get:function(){return m}}]),n}();return t(document).on(b.CLICK_DATA_API,x.DATA_SLIDE,D._dataApiClickHandler),t(window).on(b.LOAD_DATA_API,function(){for(var e=[].slice.call(document.querySelectorAll(x.DATA_RIDE)),n=0,i=e.length;n<i;n++){var r=t(e[n]);D._jQueryInterface.call(r,r.data())}}),t.fn[a]=D._jQueryInterface,t.fn[a].Constructor=D,t.fn[a].noConflict=function(){return t.fn[a]=h,D._jQueryInterface},D}),function(t,e){"object"==typeof exports&&"undefined"!=typeof module?module.exports=e(require("jquery"),require("./util.js")):"function"==typeof define&&define.amd?define(["jquery","./util.js"],e):(t=t||self,t.Collapse=e(t.jQuery,t.Util))}(this,function(t,e){"use strict";function n(t,e){for(var n=0;n<e.length;n++){var i=e[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}function i(t,e,i){return e&&n(t.prototype,e),i&&n(t,i),t}function r(t,e,n){return e in t?Object.defineProperty(t,e,{value:n,enumerable:!0,configurable:!0,writable:!0}):t[e]=n,t}function o(t){for(var e=1;e<arguments.length;e++){var n=null!=arguments[e]?arguments[e]:{},i=Object.keys(n);"function"==typeof Object.getOwnPropertySymbols&&(i=i.concat(Object.getOwnPropertySymbols(n).filter(function(t){return Object.getOwnPropertyDescriptor(n,t).enumerable}))),i.forEach(function(e){r(t,e,n[e])})}return t}t=t&&t.hasOwnProperty("default")?t["default"]:t,e=e&&e.hasOwnProperty("default")?e["default"]:e;var a="collapse",s="4.3.1",l="bs.collapse",u="."+l,c=".data-api",h=t.fn[a],d={toggle:!0,parent:""},f={toggle:"boolean",parent:"(string|element)"},p={SHOW:"show"+u,SHOWN:"shown"+u,HIDE:"hide"+u,HIDDEN:"hidden"+u,CLICK_DATA_API:"click"+u+c},g={SHOW:"show",COLLAPSE:"collapse",COLLAPSING:"collapsing",COLLAPSED:"collapsed"},m={WIDTH:"width",HEIGHT:"height"},v={ACTIVES:".show, .collapsing",DATA_TOGGLE:'[data-toggle="collapse"]'},y=function(){function n(t,n){this._isTransitioning=!1,this._element=t,this._config=this._getConfig(n),this._triggerArray=[].slice.call(document.querySelectorAll('[data-toggle="collapse"][href="#'+t.id+'"],'+('[data-toggle="collapse"][data-target="#'+t.id+'"]')));for(var i=[].slice.call(document.querySelectorAll(v.DATA_TOGGLE)),r=0,o=i.length;r<o;r++){var a=i[r],s=e.getSelectorFromElement(a),l=[].slice.call(document.querySelectorAll(s)).filter(function(e){return e===t});null!==s&&l.length>0&&(this._selector=s,this._triggerArray.push(a))}this._parent=this._config.parent?this._getParent():null,this._config.parent||this._addAriaAndCollapsedClass(this._element,this._triggerArray),this._config.toggle&&this.toggle()}var r=n.prototype;return r.toggle=function(){t(this._element).hasClass(g.SHOW)?this.hide():this.show()},r.show=function(){var i=this;if(!this._isTransitioning&&!t(this._element).hasClass(g.SHOW)){var r,o;if(this._parent&&(r=[].slice.call(this._parent.querySelectorAll(v.ACTIVES)).filter(function(t){return"string"==typeof i._config.parent?t.getAttribute("data-parent")===i._config.parent:t.classList.contains(g.COLLAPSE)}),0===r.length&&(r=null)),!(r&&(o=t(r).not(this._selector).data(l),o&&o._isTransitioning))){var a=t.Event(p.SHOW);if(t(this._element).trigger(a),!a.isDefaultPrevented()){r&&(n._jQueryInterface.call(t(r).not(this._selector),"hide"),o||t(r).data(l,null));var s=this._getDimension();t(this._element).removeClass(g.COLLAPSE).addClass(g.COLLAPSING),this._element.style[s]=0,this._triggerArray.length&&t(this._triggerArray).removeClass(g.COLLAPSED).attr("aria-expanded",!0),this.setTransitioning(!0);var u=function(){t(i._element).removeClass(g.COLLAPSING).addClass(g.COLLAPSE).addClass(g.SHOW),i._element.style[s]="",i.setTransitioning(!1),t(i._element).trigger(p.SHOWN)},c=s[0].toUpperCase()+s.slice(1),h="scroll"+c,d=e.getTransitionDurationFromElement(this._element);t(this._element).one(e.TRANSITION_END,u).emulateTransitionEnd(d),this._element.style[s]=this._element[h]+"px"}}}},r.hide=function(){var n=this;if(!this._isTransitioning&&t(this._element).hasClass(g.SHOW)){var i=t.Event(p.HIDE);if(t(this._element).trigger(i),!i.isDefaultPrevented()){var r=this._getDimension();this._element.style[r]=this._element.getBoundingClientRect()[r]+"px",e.reflow(this._element),t(this._element).addClass(g.COLLAPSING).removeClass(g.COLLAPSE).removeClass(g.SHOW);var o=this._triggerArray.length;if(o>0)for(var a=0;a<o;a++){var s=this._triggerArray[a],l=e.getSelectorFromElement(s);if(null!==l){var u=t([].slice.call(document.querySelectorAll(l)));u.hasClass(g.SHOW)||t(s).addClass(g.COLLAPSED).attr("aria-expanded",!1)}}this.setTransitioning(!0);var c=function(){n.setTransitioning(!1),t(n._element).removeClass(g.COLLAPSING).addClass(g.COLLAPSE).trigger(p.HIDDEN)};this._element.style[r]="";var h=e.getTransitionDurationFromElement(this._element);t(this._element).one(e.TRANSITION_END,c).emulateTransitionEnd(h)}}},r.setTransitioning=function(t){this._isTransitioning=t},r.dispose=function(){t.removeData(this._element,l),this._config=null,this._parent=null,this._element=null,this._triggerArray=null,this._isTransitioning=null},r._getConfig=function(t){return t=o({},d,t),t.toggle=Boolean(t.toggle),e.typeCheckConfig(a,t,f),t},r._getDimension=function(){var e=t(this._element).hasClass(m.WIDTH);return e?m.WIDTH:m.HEIGHT},r._getParent=function(){var i,r=this;e.isElement(this._config.parent)?(i=this._config.parent,"undefined"!=typeof this._config.parent.jquery&&(i=this._config.parent[0])):i=document.querySelector(this._config.parent);var o='[data-toggle="collapse"][data-parent="'+this._config.parent+'"]',a=[].slice.call(i.querySelectorAll(o));return t(a).each(function(t,e){r._addAriaAndCollapsedClass(n._getTargetFromElement(e),[e])}),i},r._addAriaAndCollapsedClass=function(e,n){var i=t(e).hasClass(g.SHOW);n.length&&t(n).toggleClass(g.COLLAPSED,!i).attr("aria-expanded",i)},n._getTargetFromElement=function(t){var n=e.getSelectorFromElement(t);return n?document.querySelector(n):null},n._jQueryInterface=function(e){return this.each(function(){var i=t(this),r=i.data(l),a=o({},d,i.data(),"object"==typeof e&&e?e:{});if(!r&&a.toggle&&/show|hide/.test(e)&&(a.toggle=!1),r||(r=new n(this,a),i.data(l,r)),"string"==typeof e){if("undefined"==typeof r[e])throw new TypeError('No method named "'+e+'"');r[e]()}})},i(n,null,[{key:"VERSION",get:function(){return s}},{key:"Default",get:function(){return d}}]),n}();return t(document).on(p.CLICK_DATA_API,v.DATA_TOGGLE,function(n){"A"===n.currentTarget.tagName&&n.preventDefault();var i=t(this),r=e.getSelectorFromElement(this),o=[].slice.call(document.querySelectorAll(r));t(o).each(function(){var e=t(this),n=e.data(l),r=n?"toggle":i.data();y._jQueryInterface.call(e,r)})}),t.fn[a]=y._jQueryInterface,t.fn[a].Constructor=y,t.fn[a].noConflict=function(){return t.fn[a]=h,y._jQueryInterface},y}),function(t,e){"object"==typeof exports&&"undefined"!=typeof module?module.exports=e(require("jquery"),require("popper.js"),require("./util.js")):"function"==typeof define&&define.amd?define(["jquery","popper.js","./util.js"],e):(t=t||self,t.Dropdown=e(t.jQuery,t.Popper,t.Util))}(this,function(t,e,n){"use strict";function i(t,e){for(var n=0;n<e.length;n++){var i=e[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}function r(t,e,n){return e&&i(t.prototype,e),n&&i(t,n),t}function o(t,e,n){return e in t?Object.defineProperty(t,e,{value:n,enumerable:!0,configurable:!0,writable:!0}):t[e]=n,t}function a(t){for(var e=1;e<arguments.length;e++){var n=null!=arguments[e]?arguments[e]:{},i=Object.keys(n);"function"==typeof Object.getOwnPropertySymbols&&(i=i.concat(Object.getOwnPropertySymbols(n).filter(function(t){return Object.getOwnPropertyDescriptor(n,t).enumerable}))),i.forEach(function(e){o(t,e,n[e])})}return t}t=t&&t.hasOwnProperty("default")?t["default"]:t,e=e&&e.hasOwnProperty("default")?e["default"]:e,n=n&&n.hasOwnProperty("default")?n["default"]:n;var s="dropdown",l="4.3.1",u="bs.dropdown",c="."+u,h=".data-api",d=t.fn[s],f=27,p=32,g=9,m=38,v=40,y=3,b=new RegExp(m+"|"+v+"|"+f),_={HIDE:"hide"+c,HIDDEN:"hidden"+c,SHOW:"show"+c,SHOWN:"shown"+c,CLICK:"click"+c,CLICK_DATA_API:"click"+c+h,KEYDOWN_DATA_API:"keydown"+c+h,KEYUP_DATA_API:"keyup"+c+h},x={DISABLED:"disabled",SHOW:"show",DROPUP:"dropup",DROPRIGHT:"dropright",DROPLEFT:"dropleft",MENURIGHT:"dropdown-menu-right",MENULEFT:"dropdown-menu-left",POSITION_STATIC:"position-static"},w={DATA_TOGGLE:'[data-toggle="dropdown"]',FORM_CHILD:".dropdown form",MENU:".dropdown-menu",NAVBAR_NAV:".navbar-nav",VISIBLE_ITEMS:".dropdown-menu .dropdown-item:not(.disabled):not(:disabled)"},D={TOP:"top-start",TOPEND:"top-end",BOTTOM:"bottom-start",BOTTOMEND:"bottom-end",RIGHT:"right-start",RIGHTEND:"right-end",LEFT:"left-start",LEFTEND:"left-end"},S={offset:0,flip:!0,boundary:"scrollParent",reference:"toggle",display:"dynamic"},C={offset:"(number|string|function)",flip:"boolean",boundary:"(string|element)",reference:"(string|element)",display:"string"},T=function(){function i(t,e){this._element=t,this._popper=null,this._config=this._getConfig(e),this._menu=this._getMenuElement(),this._inNavbar=this._detectNavbar(),this._addEventListeners()}var o=i.prototype;return o.toggle=function(){if(!this._element.disabled&&!t(this._element).hasClass(x.DISABLED)){var r=i._getParentFromElement(this._element),o=t(this._menu).hasClass(x.SHOW);if(i._clearMenus(),!o){var a={relatedTarget:this._element},s=t.Event(_.SHOW,a);if(t(r).trigger(s),!s.isDefaultPrevented()){if(!this._inNavbar){if("undefined"==typeof e)throw new TypeError("Bootstrap's dropdowns require Popper.js (https://popper.js.org/)");var l=this._element;"parent"===this._config.reference?l=r:n.isElement(this._config.reference)&&(l=this._config.reference,"undefined"!=typeof this._config.reference.jquery&&(l=this._config.reference[0])),"scrollParent"!==this._config.boundary&&t(r).addClass(x.POSITION_STATIC),this._popper=new e(l,this._menu,this._getPopperConfig())}"ontouchstart"in document.documentElement&&0===t(r).closest(w.NAVBAR_NAV).length&&t(document.body).children().on("mouseover",null,t.noop),this._element.focus(),this._element.setAttribute("aria-expanded",!0),t(this._menu).toggleClass(x.SHOW),t(r).toggleClass(x.SHOW).trigger(t.Event(_.SHOWN,a))}}}},o.show=function(){if(!(this._element.disabled||t(this._element).hasClass(x.DISABLED)||t(this._menu).hasClass(x.SHOW))){var e={relatedTarget:this._element},n=t.Event(_.SHOW,e),r=i._getParentFromElement(this._element);t(r).trigger(n),n.isDefaultPrevented()||(t(this._menu).toggleClass(x.SHOW),t(r).toggleClass(x.SHOW).trigger(t.Event(_.SHOWN,e)))}},o.hide=function(){if(!this._element.disabled&&!t(this._element).hasClass(x.DISABLED)&&t(this._menu).hasClass(x.SHOW)){var e={relatedTarget:this._element},n=t.Event(_.HIDE,e),r=i._getParentFromElement(this._element);t(r).trigger(n),n.isDefaultPrevented()||(t(this._menu).toggleClass(x.SHOW),t(r).toggleClass(x.SHOW).trigger(t.Event(_.HIDDEN,e)))}},o.dispose=function(){t.removeData(this._element,u),t(this._element).off(c),this._element=null,this._menu=null,null!==this._popper&&(this._popper.destroy(),this._popper=null)},o.update=function(){this._inNavbar=this._detectNavbar(),null!==this._popper&&this._popper.scheduleUpdate()},o._addEventListeners=function(){var e=this;t(this._element).on(_.CLICK,function(t){t.preventDefault(),t.stopPropagation(),e.toggle()})},o._getConfig=function(e){return e=a({},this.constructor.Default,t(this._element).data(),e),n.typeCheckConfig(s,e,this.constructor.DefaultType),e},o._getMenuElement=function(){if(!this._menu){var t=i._getParentFromElement(this._element);t&&(this._menu=t.querySelector(w.MENU))}return this._menu},o._getPlacement=function(){var e=t(this._element.parentNode),n=D.BOTTOM;return e.hasClass(x.DROPUP)?(n=D.TOP,t(this._menu).hasClass(x.MENURIGHT)&&(n=D.TOPEND)):e.hasClass(x.DROPRIGHT)?n=D.RIGHT:e.hasClass(x.DROPLEFT)?n=D.LEFT:t(this._menu).hasClass(x.MENURIGHT)&&(n=D.BOTTOMEND),n},o._detectNavbar=function(){return t(this._element).closest(".navbar").length>0},o._getOffset=function(){var t=this,e={};return"function"==typeof this._config.offset?e.fn=function(e){return e.offsets=a({},e.offsets,t._config.offset(e.offsets,t._element)||{}),e}:e.offset=this._config.offset,e},o._getPopperConfig=function(){var t={placement:this._getPlacement(),modifiers:{offset:this._getOffset(),flip:{enabled:this._config.flip},preventOverflow:{boundariesElement:this._config.boundary}}};return"static"===this._config.display&&(t.modifiers.applyStyle={enabled:!1}),t},i._jQueryInterface=function(e){return this.each(function(){var n=t(this).data(u),r="object"==typeof e?e:null;if(n||(n=new i(this,r),t(this).data(u,n)),"string"==typeof e){if("undefined"==typeof n[e])throw new TypeError('No method named "'+e+'"');n[e]()}})},i._clearMenus=function(e){if(!e||e.which!==y&&("keyup"!==e.type||e.which===g))for(var n=[].slice.call(document.querySelectorAll(w.DATA_TOGGLE)),r=0,o=n.length;r<o;r++){var a=i._getParentFromElement(n[r]),s=t(n[r]).data(u),l={relatedTarget:n[r]};if(e&&"click"===e.type&&(l.clickEvent=e),s){var c=s._menu;if(t(a).hasClass(x.SHOW)&&!(e&&("click"===e.type&&/input|textarea/i.test(e.target.tagName)||"keyup"===e.type&&e.which===g)&&t.contains(a,e.target))){var h=t.Event(_.HIDE,l);t(a).trigger(h),h.isDefaultPrevented()||("ontouchstart"in document.documentElement&&t(document.body).children().off("mouseover",null,t.noop),n[r].setAttribute("aria-expanded","false"),t(c).removeClass(x.SHOW),t(a).removeClass(x.SHOW).trigger(t.Event(_.HIDDEN,l)))}}}},i._getParentFromElement=function(t){var e,i=n.getSelectorFromElement(t);return i&&(e=document.querySelector(i)),e||t.parentNode},i._dataApiKeydownHandler=function(e){if((/input|textarea/i.test(e.target.tagName)?!(e.which===p||e.which!==f&&(e.which!==v&&e.which!==m||t(e.target).closest(w.MENU).length)):b.test(e.which))&&(e.preventDefault(),e.stopPropagation(),!this.disabled&&!t(this).hasClass(x.DISABLED))){var n=i._getParentFromElement(this),r=t(n).hasClass(x.SHOW);if(!r||r&&(e.which===f||e.which===p)){if(e.which===f){var o=n.querySelector(w.DATA_TOGGLE);t(o).trigger("focus")}return void t(this).trigger("click")}var a=[].slice.call(n.querySelectorAll(w.VISIBLE_ITEMS));if(0!==a.length){var s=a.indexOf(e.target);e.which===m&&s>0&&s--,e.which===v&&s<a.length-1&&s++,s<0&&(s=0),a[s].focus()}}},r(i,null,[{key:"VERSION",get:function(){return l}},{key:"Default",get:function(){return S}},{key:"DefaultType",get:function(){return C}}]),i}();return t(document).on(_.KEYDOWN_DATA_API,w.DATA_TOGGLE,T._dataApiKeydownHandler).on(_.KEYDOWN_DATA_API,w.MENU,T._dataApiKeydownHandler).on(_.CLICK_DATA_API+" "+_.KEYUP_DATA_API,T._clearMenus).on(_.CLICK_DATA_API,w.DATA_TOGGLE,function(e){e.preventDefault(),e.stopPropagation(),T._jQueryInterface.call(t(this),"toggle")}).on(_.CLICK_DATA_API,w.FORM_CHILD,function(t){t.stopPropagation()}),t.fn[s]=T._jQueryInterface,t.fn[s].Constructor=T,t.fn[s].noConflict=function(){return t.fn[s]=d,T._jQueryInterface},T}),function(t,e){"object"==typeof exports&&"undefined"!=typeof module?module.exports=e(require("jquery"),require("./util.js")):"function"==typeof define&&define.amd?define(["jquery","./util.js"],e):(t=t||self,t.Modal=e(t.jQuery,t.Util))}(this,function(t,e){"use strict";function n(t,e){for(var n=0;n<e.length;n++){var i=e[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}function i(t,e,i){return e&&n(t.prototype,e),i&&n(t,i),t}function r(t,e,n){return e in t?Object.defineProperty(t,e,{value:n,enumerable:!0,configurable:!0,writable:!0}):t[e]=n,t}function o(t){for(var e=1;e<arguments.length;e++){var n=null!=arguments[e]?arguments[e]:{},i=Object.keys(n);"function"==typeof Object.getOwnPropertySymbols&&(i=i.concat(Object.getOwnPropertySymbols(n).filter(function(t){return Object.getOwnPropertyDescriptor(n,t).enumerable}))),i.forEach(function(e){r(t,e,n[e])})}return t}t=t&&t.hasOwnProperty("default")?t["default"]:t,e=e&&e.hasOwnProperty("default")?e["default"]:e;var a="modal",s="4.3.1",l="bs.modal",u="."+l,c=".data-api",h=t.fn[a],d=27,f={backdrop:!0,keyboard:!0,focus:!0,show:!0},p={backdrop:"(boolean|string)",keyboard:"boolean",focus:"boolean",show:"boolean"},g={HIDE:"hide"+u,HIDDEN:"hidden"+u,SHOW:"show"+u,SHOWN:"shown"+u,FOCUSIN:"focusin"+u,RESIZE:"resize"+u,CLICK_DISMISS:"click.dismiss"+u,KEYDOWN_DISMISS:"keydown.dismiss"+u,MOUSEUP_DISMISS:"mouseup.dismiss"+u,MOUSEDOWN_DISMISS:"mousedown.dismiss"+u,CLICK_DATA_API:"click"+u+c},m={SCROLLABLE:"modal-dialog-scrollable",SCROLLBAR_MEASURER:"modal-scrollbar-measure",BACKDROP:"modal-backdrop",OPEN:"modal-open",FADE:"fade",SHOW:"show"},v={DIALOG:".modal-dialog",MODAL_BODY:".modal-body",DATA_TOGGLE:'[data-toggle="modal"]',DATA_DISMISS:'[data-dismiss="modal"]',FIXED_CONTENT:".fixed-top, .fixed-bottom, .is-fixed, .sticky-top",STICKY_CONTENT:".sticky-top"},y=function(){function n(t,e){this._config=this._getConfig(e),this._element=t,this._dialog=t.querySelector(v.DIALOG),this._backdrop=null,this._isShown=!1,this._isBodyOverflowing=!1,this._ignoreBackdropClick=!1,this._isTransitioning=!1,this._scrollbarWidth=0}var r=n.prototype;return r.toggle=function(t){return this._isShown?this.hide():this.show(t)},r.show=function(e){var n=this;if(!this._isShown&&!this._isTransitioning){t(this._element).hasClass(m.FADE)&&(this._isTransitioning=!0);var i=t.Event(g.SHOW,{relatedTarget:e});t(this._element).trigger(i),this._isShown||i.isDefaultPrevented()||(this._isShown=!0,this._checkScrollbar(),this._setScrollbar(),this._adjustDialog(),this._setEscapeEvent(),this._setResizeEvent(),t(this._element).on(g.CLICK_DISMISS,v.DATA_DISMISS,function(t){return n.hide(t)}),t(this._dialog).on(g.MOUSEDOWN_DISMISS,function(){t(n._element).one(g.MOUSEUP_DISMISS,function(e){t(e.target).is(n._element)&&(n._ignoreBackdropClick=!0)})}),this._showBackdrop(function(){return n._showElement(e)}))}},r.hide=function(n){var i=this;if(n&&n.preventDefault(),this._isShown&&!this._isTransitioning){var r=t.Event(g.HIDE);if(t(this._element).trigger(r),this._isShown&&!r.isDefaultPrevented()){this._isShown=!1;var o=t(this._element).hasClass(m.FADE);if(o&&(this._isTransitioning=!0),this._setEscapeEvent(),this._setResizeEvent(),t(document).off(g.FOCUSIN),t(this._element).removeClass(m.SHOW),t(this._element).off(g.CLICK_DISMISS),t(this._dialog).off(g.MOUSEDOWN_DISMISS),o){var a=e.getTransitionDurationFromElement(this._element);t(this._element).one(e.TRANSITION_END,function(t){return i._hideModal(t)}).emulateTransitionEnd(a)}else this._hideModal()}}},r.dispose=function(){[window,this._element,this._dialog].forEach(function(e){return t(e).off(u)}),t(document).off(g.FOCUSIN),t.removeData(this._element,l),this._config=null,this._element=null,this._dialog=null,this._backdrop=null,this._isShown=null,this._isBodyOverflowing=null,this._ignoreBackdropClick=null,this._isTransitioning=null,this._scrollbarWidth=null},r.handleUpdate=function(){this._adjustDialog()},r._getConfig=function(t){return t=o({},f,t),e.typeCheckConfig(a,t,p),t},r._showElement=function(n){var i=this,r=t(this._element).hasClass(m.FADE);this._element.parentNode&&this._element.parentNode.nodeType===Node.ELEMENT_NODE||document.body.appendChild(this._element),this._element.style.display="block",this._element.removeAttribute("aria-hidden"),this._element.setAttribute("aria-modal",!0),t(this._dialog).hasClass(m.SCROLLABLE)?this._dialog.querySelector(v.MODAL_BODY).scrollTop=0:this._element.scrollTop=0,r&&e.reflow(this._element),t(this._element).addClass(m.SHOW),this._config.focus&&this._enforceFocus();var o=t.Event(g.SHOWN,{relatedTarget:n}),a=function(){i._config.focus&&i._element.focus(),i._isTransitioning=!1,t(i._element).trigger(o)};if(r){var s=e.getTransitionDurationFromElement(this._dialog);t(this._dialog).one(e.TRANSITION_END,a).emulateTransitionEnd(s)}else a()},r._enforceFocus=function(){var e=this;t(document).off(g.FOCUSIN).on(g.FOCUSIN,function(n){document!==n.target&&e._element!==n.target&&0===t(e._element).has(n.target).length&&e._element.focus()})},r._setEscapeEvent=function(){var e=this;this._isShown&&this._config.keyboard?t(this._element).on(g.KEYDOWN_DISMISS,function(t){t.which===d&&(t.preventDefault(),e.hide())}):this._isShown||t(this._element).off(g.KEYDOWN_DISMISS)},r._setResizeEvent=function(){var e=this;this._isShown?t(window).on(g.RESIZE,function(t){return e.handleUpdate(t)}):t(window).off(g.RESIZE)},r._hideModal=function(){var e=this;this._element.style.display="none",this._element.setAttribute("aria-hidden",!0),this._element.removeAttribute("aria-modal"),this._isTransitioning=!1,this._showBackdrop(function(){t(document.body).removeClass(m.OPEN),e._resetAdjustments(),e._resetScrollbar(),t(e._element).trigger(g.HIDDEN)})},r._removeBackdrop=function(){this._backdrop&&(t(this._backdrop).remove(),this._backdrop=null)},r._showBackdrop=function(n){var i=this,r=t(this._element).hasClass(m.FADE)?m.FADE:"";if(this._isShown&&this._config.backdrop){if(this._backdrop=document.createElement("div"),this._backdrop.className=m.BACKDROP,r&&this._backdrop.classList.add(r),t(this._backdrop).appendTo(document.body),t(this._element).on(g.CLICK_DISMISS,function(t){return i._ignoreBackdropClick?void(i._ignoreBackdropClick=!1):void(t.target===t.currentTarget&&("static"===i._config.backdrop?i._element.focus():i.hide()))}),r&&e.reflow(this._backdrop),t(this._backdrop).addClass(m.SHOW),!n)return;if(!r)return void n();var o=e.getTransitionDurationFromElement(this._backdrop);t(this._backdrop).one(e.TRANSITION_END,n).emulateTransitionEnd(o)}else if(!this._isShown&&this._backdrop){t(this._backdrop).removeClass(m.SHOW);var a=function(){i._removeBackdrop(),n&&n()};if(t(this._element).hasClass(m.FADE)){var s=e.getTransitionDurationFromElement(this._backdrop);t(this._backdrop).one(e.TRANSITION_END,a).emulateTransitionEnd(s)}else a()}else n&&n()},r._adjustDialog=function(){var t=this._element.scrollHeight>document.documentElement.clientHeight;!this._isBodyOverflowing&&t&&(this._element.style.paddingLeft=this._scrollbarWidth+"px"),this._isBodyOverflowing&&!t&&(this._element.style.paddingRight=this._scrollbarWidth+"px")},r._resetAdjustments=function(){this._element.style.paddingLeft="",this._element.style.paddingRight=""},r._checkScrollbar=function(){var t=document.body.getBoundingClientRect();this._isBodyOverflowing=t.left+t.right<window.innerWidth,this._scrollbarWidth=this._getScrollbarWidth()},r._setScrollbar=function(){var e=this;if(this._isBodyOverflowing){var n=[].slice.call(document.querySelectorAll(v.FIXED_CONTENT)),i=[].slice.call(document.querySelectorAll(v.STICKY_CONTENT));t(n).each(function(n,i){var r=i.style.paddingRight,o=t(i).css("padding-right");t(i).data("padding-right",r).css("padding-right",parseFloat(o)+e._scrollbarWidth+"px")}),t(i).each(function(n,i){var r=i.style.marginRight,o=t(i).css("margin-right");t(i).data("margin-right",r).css("margin-right",parseFloat(o)-e._scrollbarWidth+"px")});var r=document.body.style.paddingRight,o=t(document.body).css("padding-right");t(document.body).data("padding-right",r).css("padding-right",parseFloat(o)+this._scrollbarWidth+"px")}t(document.body).addClass(m.OPEN)},r._resetScrollbar=function(){var e=[].slice.call(document.querySelectorAll(v.FIXED_CONTENT));t(e).each(function(e,n){var i=t(n).data("padding-right");t(n).removeData("padding-right"),n.style.paddingRight=i?i:""});var n=[].slice.call(document.querySelectorAll(""+v.STICKY_CONTENT));t(n).each(function(e,n){var i=t(n).data("margin-right");"undefined"!=typeof i&&t(n).css("margin-right",i).removeData("margin-right")});var i=t(document.body).data("padding-right");t(document.body).removeData("padding-right"),document.body.style.paddingRight=i?i:""},r._getScrollbarWidth=function(){var t=document.createElement("div");t.className=m.SCROLLBAR_MEASURER,document.body.appendChild(t);var e=t.getBoundingClientRect().width-t.clientWidth;return document.body.removeChild(t),e},n._jQueryInterface=function(e,i){return this.each(function(){var r=t(this).data(l),a=o({},f,t(this).data(),"object"==typeof e&&e?e:{});if(r||(r=new n(this,a),t(this).data(l,r)),"string"==typeof e){if("undefined"==typeof r[e])throw new TypeError('No method named "'+e+'"');r[e](i)}else a.show&&r.show(i)})},i(n,null,[{key:"VERSION",get:function(){return s}},{key:"Default",get:function(){return f}}]),n}();return t(document).on(g.CLICK_DATA_API,v.DATA_TOGGLE,function(n){var i,r=this,a=e.getSelectorFromElement(this);a&&(i=document.querySelector(a));var s=t(i).data(l)?"toggle":o({},t(i).data(),t(this).data());"A"!==this.tagName&&"AREA"!==this.tagName||n.preventDefault();var u=t(i).one(g.SHOW,function(e){e.isDefaultPrevented()||u.one(g.HIDDEN,function(){t(r).is(":visible")&&r.focus()})});y._jQueryInterface.call(t(i),s,this)}),t.fn[a]=y._jQueryInterface,t.fn[a].Constructor=y,t.fn[a].noConflict=function(){return t.fn[a]=h,y._jQueryInterface},y}),function(t,e){"object"==typeof exports&&"undefined"!=typeof module?module.exports=e(require("jquery"),require("popper.js"),require("./util.js")):"function"==typeof define&&define.amd?define(["jquery","popper.js","./util.js"],e):(t=t||self,t.Tooltip=e(t.jQuery,t.Popper,t.Util))}(this,function(t,e,n){"use strict";function i(t,e){for(var n=0;n<e.length;n++){var i=e[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}function r(t,e,n){return e&&i(t.prototype,e),n&&i(t,n),t}function o(t,e,n){return e in t?Object.defineProperty(t,e,{value:n,enumerable:!0,configurable:!0,writable:!0}):t[e]=n,t}function a(t){for(var e=1;e<arguments.length;e++){var n=null!=arguments[e]?arguments[e]:{},i=Object.keys(n);"function"==typeof Object.getOwnPropertySymbols&&(i=i.concat(Object.getOwnPropertySymbols(n).filter(function(t){return Object.getOwnPropertyDescriptor(n,t).enumerable}))),i.forEach(function(e){o(t,e,n[e])})}return t}function s(t,e){var n=t.nodeName.toLowerCase();if(e.indexOf(n)!==-1)return u.indexOf(n)===-1||Boolean(t.nodeValue.match(d)||t.nodeValue.match(f));for(var i=e.filter(function(t){return t instanceof RegExp}),r=0,o=i.length;r<o;r++)if(n.match(i[r]))return!0;return!1}function l(t,e,n){if(0===t.length)return t;if(n&&"function"==typeof n)return n(t);for(var i=new window.DOMParser,r=i.parseFromString(t,"text/html"),o=Object.keys(e),a=[].slice.call(r.body.querySelectorAll("*")),l=function(t,n){var i=a[t],r=i.nodeName.toLowerCase();if(o.indexOf(i.nodeName.toLowerCase())===-1)return i.parentNode.removeChild(i),"continue";var l=[].slice.call(i.attributes),u=[].concat(e["*"]||[],e[r]||[]);l.forEach(function(t){s(t,u)||i.removeAttribute(t.nodeName)})},u=0,c=a.length;u<c;u++){l(u,c)}return r.body.innerHTML}t=t&&t.hasOwnProperty("default")?t["default"]:t,
e=e&&e.hasOwnProperty("default")?e["default"]:e,n=n&&n.hasOwnProperty("default")?n["default"]:n;var u=["background","cite","href","itemtype","longdesc","poster","src","xlink:href"],c=/^aria-[\w-]*$/i,h={"*":["class","dir","id","lang","role",c],a:["target","href","title","rel"],area:[],b:[],br:[],col:[],code:[],div:[],em:[],hr:[],h1:[],h2:[],h3:[],h4:[],h5:[],h6:[],i:[],img:["src","alt","title","width","height"],li:[],ol:[],p:[],pre:[],s:[],small:[],span:[],sub:[],sup:[],strong:[],u:[],ul:[]},d=/^(?:(?:https?|mailto|ftp|tel|file):|[^&:\/?#]*(?:[\/?#]|$))/gi,f=/^data:(?:image\/(?:bmp|gif|jpeg|jpg|png|tiff|webp)|video\/(?:mpeg|mp4|ogg|webm)|audio\/(?:mp3|oga|ogg|opus));base64,[a-z0-9+\/]+=*$/i,p="tooltip",g="4.3.1",m="bs.tooltip",v="."+m,y=t.fn[p],b="bs-tooltip",_=new RegExp("(^|\\s)"+b+"\\S+","g"),x=["sanitize","whiteList","sanitizeFn"],w={animation:"boolean",template:"string",title:"(string|element|function)",trigger:"string",delay:"(number|object)",html:"boolean",selector:"(string|boolean)",placement:"(string|function)",offset:"(number|string|function)",container:"(string|element|boolean)",fallbackPlacement:"(string|array)",boundary:"(string|element)",sanitize:"boolean",sanitizeFn:"(null|function)",whiteList:"object"},D={AUTO:"auto",TOP:"top",RIGHT:"right",BOTTOM:"bottom",LEFT:"left"},S={animation:!0,template:'<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>',trigger:"hover focus",title:"",delay:0,html:!1,selector:!1,placement:"top",offset:0,container:!1,fallbackPlacement:"flip",boundary:"scrollParent",sanitize:!0,sanitizeFn:null,whiteList:h},C={SHOW:"show",OUT:"out"},T={HIDE:"hide"+v,HIDDEN:"hidden"+v,SHOW:"show"+v,SHOWN:"shown"+v,INSERTED:"inserted"+v,CLICK:"click"+v,FOCUSIN:"focusin"+v,FOCUSOUT:"focusout"+v,MOUSEENTER:"mouseenter"+v,MOUSELEAVE:"mouseleave"+v},k={FADE:"fade",SHOW:"show"},A={TOOLTIP:".tooltip",TOOLTIP_INNER:".tooltip-inner",ARROW:".arrow"},I={HOVER:"hover",FOCUS:"focus",CLICK:"click",MANUAL:"manual"},E=function(){function i(t,n){if("undefined"==typeof e)throw new TypeError("Bootstrap's tooltips require Popper.js (https://popper.js.org/)");this._isEnabled=!0,this._timeout=0,this._hoverState="",this._activeTrigger={},this._popper=null,this.element=t,this.config=this._getConfig(n),this.tip=null,this._setListeners()}var o=i.prototype;return o.enable=function(){this._isEnabled=!0},o.disable=function(){this._isEnabled=!1},o.toggleEnabled=function(){this._isEnabled=!this._isEnabled},o.toggle=function(e){if(this._isEnabled)if(e){var n=this.constructor.DATA_KEY,i=t(e.currentTarget).data(n);i||(i=new this.constructor(e.currentTarget,this._getDelegateConfig()),t(e.currentTarget).data(n,i)),i._activeTrigger.click=!i._activeTrigger.click,i._isWithActiveTrigger()?i._enter(null,i):i._leave(null,i)}else{if(t(this.getTipElement()).hasClass(k.SHOW))return void this._leave(null,this);this._enter(null,this)}},o.dispose=function(){clearTimeout(this._timeout),t.removeData(this.element,this.constructor.DATA_KEY),t(this.element).off(this.constructor.EVENT_KEY),t(this.element).closest(".modal").off("hide.bs.modal"),this.tip&&t(this.tip).remove(),this._isEnabled=null,this._timeout=null,this._hoverState=null,this._activeTrigger=null,null!==this._popper&&this._popper.destroy(),this._popper=null,this.element=null,this.config=null,this.tip=null},o.show=function(){var i=this;if("none"===t(this.element).css("display"))throw new Error("Please use show on visible elements");var r=t.Event(this.constructor.Event.SHOW);if(this.isWithContent()&&this._isEnabled){t(this.element).trigger(r);var o=n.findShadowRoot(this.element),a=t.contains(null!==o?o:this.element.ownerDocument.documentElement,this.element);if(r.isDefaultPrevented()||!a)return;var s=this.getTipElement(),l=n.getUID(this.constructor.NAME);s.setAttribute("id",l),this.element.setAttribute("aria-describedby",l),this.setContent(),this.config.animation&&t(s).addClass(k.FADE);var u="function"==typeof this.config.placement?this.config.placement.call(this,s,this.element):this.config.placement,c=this._getAttachment(u);this.addAttachmentClass(c);var h=this._getContainer();t(s).data(this.constructor.DATA_KEY,this),t.contains(this.element.ownerDocument.documentElement,this.tip)||t(s).appendTo(h),t(this.element).trigger(this.constructor.Event.INSERTED),this._popper=new e(this.element,s,{placement:c,modifiers:{offset:this._getOffset(),flip:{behavior:this.config.fallbackPlacement},arrow:{element:A.ARROW},preventOverflow:{boundariesElement:this.config.boundary}},onCreate:function(t){t.originalPlacement!==t.placement&&i._handlePopperPlacementChange(t)},onUpdate:function(t){return i._handlePopperPlacementChange(t)}}),t(s).addClass(k.SHOW),"ontouchstart"in document.documentElement&&t(document.body).children().on("mouseover",null,t.noop);var d=function(){i.config.animation&&i._fixTransition();var e=i._hoverState;i._hoverState=null,t(i.element).trigger(i.constructor.Event.SHOWN),e===C.OUT&&i._leave(null,i)};if(t(this.tip).hasClass(k.FADE)){var f=n.getTransitionDurationFromElement(this.tip);t(this.tip).one(n.TRANSITION_END,d).emulateTransitionEnd(f)}else d()}},o.hide=function(e){var i=this,r=this.getTipElement(),o=t.Event(this.constructor.Event.HIDE),a=function(){i._hoverState!==C.SHOW&&r.parentNode&&r.parentNode.removeChild(r),i._cleanTipClass(),i.element.removeAttribute("aria-describedby"),t(i.element).trigger(i.constructor.Event.HIDDEN),null!==i._popper&&i._popper.destroy(),e&&e()};if(t(this.element).trigger(o),!o.isDefaultPrevented()){if(t(r).removeClass(k.SHOW),"ontouchstart"in document.documentElement&&t(document.body).children().off("mouseover",null,t.noop),this._activeTrigger[I.CLICK]=!1,this._activeTrigger[I.FOCUS]=!1,this._activeTrigger[I.HOVER]=!1,t(this.tip).hasClass(k.FADE)){var s=n.getTransitionDurationFromElement(r);t(r).one(n.TRANSITION_END,a).emulateTransitionEnd(s)}else a();this._hoverState=""}},o.update=function(){null!==this._popper&&this._popper.scheduleUpdate()},o.isWithContent=function(){return Boolean(this.getTitle())},o.addAttachmentClass=function(e){t(this.getTipElement()).addClass(b+"-"+e)},o.getTipElement=function(){return this.tip=this.tip||t(this.config.template)[0],this.tip},o.setContent=function(){var e=this.getTipElement();this.setElementContent(t(e.querySelectorAll(A.TOOLTIP_INNER)),this.getTitle()),t(e).removeClass(k.FADE+" "+k.SHOW)},o.setElementContent=function(e,n){return"object"==typeof n&&(n.nodeType||n.jquery)?void(this.config.html?t(n).parent().is(e)||e.empty().append(n):e.text(t(n).text())):void(this.config.html?(this.config.sanitize&&(n=l(n,this.config.whiteList,this.config.sanitizeFn)),e.html(n)):e.text(n))},o.getTitle=function(){var t=this.element.getAttribute("data-original-title");return t||(t="function"==typeof this.config.title?this.config.title.call(this.element):this.config.title),t},o._getOffset=function(){var t=this,e={};return"function"==typeof this.config.offset?e.fn=function(e){return e.offsets=a({},e.offsets,t.config.offset(e.offsets,t.element)||{}),e}:e.offset=this.config.offset,e},o._getContainer=function(){return this.config.container===!1?document.body:n.isElement(this.config.container)?t(this.config.container):t(document).find(this.config.container)},o._getAttachment=function(t){return D[t.toUpperCase()]},o._setListeners=function(){var e=this,n=this.config.trigger.split(" ");n.forEach(function(n){if("click"===n)t(e.element).on(e.constructor.Event.CLICK,e.config.selector,function(t){return e.toggle(t)});else if(n!==I.MANUAL){var i=n===I.HOVER?e.constructor.Event.MOUSEENTER:e.constructor.Event.FOCUSIN,r=n===I.HOVER?e.constructor.Event.MOUSELEAVE:e.constructor.Event.FOCUSOUT;t(e.element).on(i,e.config.selector,function(t){return e._enter(t)}).on(r,e.config.selector,function(t){return e._leave(t)})}}),t(this.element).closest(".modal").on("hide.bs.modal",function(){e.element&&e.hide()}),this.config.selector?this.config=a({},this.config,{trigger:"manual",selector:""}):this._fixTitle()},o._fixTitle=function(){var t=typeof this.element.getAttribute("data-original-title");(this.element.getAttribute("title")||"string"!==t)&&(this.element.setAttribute("data-original-title",this.element.getAttribute("title")||""),this.element.setAttribute("title",""))},o._enter=function(e,n){var i=this.constructor.DATA_KEY;return n=n||t(e.currentTarget).data(i),n||(n=new this.constructor(e.currentTarget,this._getDelegateConfig()),t(e.currentTarget).data(i,n)),e&&(n._activeTrigger["focusin"===e.type?I.FOCUS:I.HOVER]=!0),t(n.getTipElement()).hasClass(k.SHOW)||n._hoverState===C.SHOW?void(n._hoverState=C.SHOW):(clearTimeout(n._timeout),n._hoverState=C.SHOW,n.config.delay&&n.config.delay.show?void(n._timeout=setTimeout(function(){n._hoverState===C.SHOW&&n.show()},n.config.delay.show)):void n.show())},o._leave=function(e,n){var i=this.constructor.DATA_KEY;if(n=n||t(e.currentTarget).data(i),n||(n=new this.constructor(e.currentTarget,this._getDelegateConfig()),t(e.currentTarget).data(i,n)),e&&(n._activeTrigger["focusout"===e.type?I.FOCUS:I.HOVER]=!1),!n._isWithActiveTrigger())return clearTimeout(n._timeout),n._hoverState=C.OUT,n.config.delay&&n.config.delay.hide?void(n._timeout=setTimeout(function(){n._hoverState===C.OUT&&n.hide()},n.config.delay.hide)):void n.hide()},o._isWithActiveTrigger=function(){for(var t in this._activeTrigger)if(this._activeTrigger[t])return!0;return!1},o._getConfig=function(e){var i=t(this.element).data();return Object.keys(i).forEach(function(t){x.indexOf(t)!==-1&&delete i[t]}),e=a({},this.constructor.Default,i,"object"==typeof e&&e?e:{}),"number"==typeof e.delay&&(e.delay={show:e.delay,hide:e.delay}),"number"==typeof e.title&&(e.title=e.title.toString()),"number"==typeof e.content&&(e.content=e.content.toString()),n.typeCheckConfig(p,e,this.constructor.DefaultType),e.sanitize&&(e.template=l(e.template,e.whiteList,e.sanitizeFn)),e},o._getDelegateConfig=function(){var t={};if(this.config)for(var e in this.config)this.constructor.Default[e]!==this.config[e]&&(t[e]=this.config[e]);return t},o._cleanTipClass=function(){var e=t(this.getTipElement()),n=e.attr("class").match(_);null!==n&&n.length&&e.removeClass(n.join(""))},o._handlePopperPlacementChange=function(t){var e=t.instance;this.tip=e.popper,this._cleanTipClass(),this.addAttachmentClass(this._getAttachment(t.placement))},o._fixTransition=function(){var e=this.getTipElement(),n=this.config.animation;null===e.getAttribute("x-placement")&&(t(e).removeClass(k.FADE),this.config.animation=!1,this.hide(),this.show(),this.config.animation=n)},i._jQueryInterface=function(e){return this.each(function(){var n=t(this).data(m),r="object"==typeof e&&e;if((n||!/dispose|hide/.test(e))&&(n||(n=new i(this,r),t(this).data(m,n)),"string"==typeof e)){if("undefined"==typeof n[e])throw new TypeError('No method named "'+e+'"');n[e]()}})},r(i,null,[{key:"VERSION",get:function(){return g}},{key:"Default",get:function(){return S}},{key:"NAME",get:function(){return p}},{key:"DATA_KEY",get:function(){return m}},{key:"Event",get:function(){return T}},{key:"EVENT_KEY",get:function(){return v}},{key:"DefaultType",get:function(){return w}}]),i}();return t.fn[p]=E._jQueryInterface,t.fn[p].Constructor=E,t.fn[p].noConflict=function(){return t.fn[p]=y,E._jQueryInterface},E}),function(t,e){"object"==typeof exports&&"undefined"!=typeof module?module.exports=e(require("jquery"),require("./tooltip.js")):"function"==typeof define&&define.amd?define(["jquery","./tooltip.js"],e):(t=t||self,t.Popover=e(t.jQuery,t.Tooltip))}(this,function(t,e){"use strict";function n(t,e){for(var n=0;n<e.length;n++){var i=e[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}function i(t,e,i){return e&&n(t.prototype,e),i&&n(t,i),t}function r(t,e,n){return e in t?Object.defineProperty(t,e,{value:n,enumerable:!0,configurable:!0,writable:!0}):t[e]=n,t}function o(t){for(var e=1;e<arguments.length;e++){var n=null!=arguments[e]?arguments[e]:{},i=Object.keys(n);"function"==typeof Object.getOwnPropertySymbols&&(i=i.concat(Object.getOwnPropertySymbols(n).filter(function(t){return Object.getOwnPropertyDescriptor(n,t).enumerable}))),i.forEach(function(e){r(t,e,n[e])})}return t}function a(t,e){t.prototype=Object.create(e.prototype),t.prototype.constructor=t,t.__proto__=e}t=t&&t.hasOwnProperty("default")?t["default"]:t,e=e&&e.hasOwnProperty("default")?e["default"]:e;var s="popover",l="4.3.1",u="bs.popover",c="."+u,h=t.fn[s],d="bs-popover",f=new RegExp("(^|\\s)"+d+"\\S+","g"),p=o({},e.Default,{placement:"right",trigger:"click",content:"",template:'<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'}),g=o({},e.DefaultType,{content:"(string|element|function)"}),m={FADE:"fade",SHOW:"show"},v={TITLE:".popover-header",CONTENT:".popover-body"},y={HIDE:"hide"+c,HIDDEN:"hidden"+c,SHOW:"show"+c,SHOWN:"shown"+c,INSERTED:"inserted"+c,CLICK:"click"+c,FOCUSIN:"focusin"+c,FOCUSOUT:"focusout"+c,MOUSEENTER:"mouseenter"+c,MOUSELEAVE:"mouseleave"+c},b=function(e){function n(){return e.apply(this,arguments)||this}a(n,e);var r=n.prototype;return r.isWithContent=function(){return this.getTitle()||this._getContent()},r.addAttachmentClass=function(e){t(this.getTipElement()).addClass(d+"-"+e)},r.getTipElement=function(){return this.tip=this.tip||t(this.config.template)[0],this.tip},r.setContent=function(){var e=t(this.getTipElement());this.setElementContent(e.find(v.TITLE),this.getTitle());var n=this._getContent();"function"==typeof n&&(n=n.call(this.element)),this.setElementContent(e.find(v.CONTENT),n),e.removeClass(m.FADE+" "+m.SHOW)},r._getContent=function(){return this.element.getAttribute("data-content")||this.config.content},r._cleanTipClass=function(){var e=t(this.getTipElement()),n=e.attr("class").match(f);null!==n&&n.length>0&&e.removeClass(n.join(""))},n._jQueryInterface=function(e){return this.each(function(){var i=t(this).data(u),r="object"==typeof e?e:null;if((i||!/dispose|hide/.test(e))&&(i||(i=new n(this,r),t(this).data(u,i)),"string"==typeof e)){if("undefined"==typeof i[e])throw new TypeError('No method named "'+e+'"');i[e]()}})},i(n,null,[{key:"VERSION",get:function(){return l}},{key:"Default",get:function(){return p}},{key:"NAME",get:function(){return s}},{key:"DATA_KEY",get:function(){return u}},{key:"Event",get:function(){return y}},{key:"EVENT_KEY",get:function(){return c}},{key:"DefaultType",get:function(){return g}}]),n}(e);return t.fn[s]=b._jQueryInterface,t.fn[s].Constructor=b,t.fn[s].noConflict=function(){return t.fn[s]=h,b._jQueryInterface},b}),function(t,e){"object"==typeof exports&&"undefined"!=typeof module?module.exports=e(require("jquery"),require("./util.js")):"function"==typeof define&&define.amd?define(["jquery","./util.js"],e):(t=t||self,t.ScrollSpy=e(t.jQuery,t.Util))}(this,function(t,e){"use strict";function n(t,e){for(var n=0;n<e.length;n++){var i=e[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}function i(t,e,i){return e&&n(t.prototype,e),i&&n(t,i),t}function r(t,e,n){return e in t?Object.defineProperty(t,e,{value:n,enumerable:!0,configurable:!0,writable:!0}):t[e]=n,t}function o(t){for(var e=1;e<arguments.length;e++){var n=null!=arguments[e]?arguments[e]:{},i=Object.keys(n);"function"==typeof Object.getOwnPropertySymbols&&(i=i.concat(Object.getOwnPropertySymbols(n).filter(function(t){return Object.getOwnPropertyDescriptor(n,t).enumerable}))),i.forEach(function(e){r(t,e,n[e])})}return t}t=t&&t.hasOwnProperty("default")?t["default"]:t,e=e&&e.hasOwnProperty("default")?e["default"]:e;var a="scrollspy",s="4.3.1",l="bs.scrollspy",u="."+l,c=".data-api",h=t.fn[a],d={offset:10,method:"auto",target:""},f={offset:"number",method:"string",target:"(string|element)"},p={ACTIVATE:"activate"+u,SCROLL:"scroll"+u,LOAD_DATA_API:"load"+u+c},g={DROPDOWN_ITEM:"dropdown-item",DROPDOWN_MENU:"dropdown-menu",ACTIVE:"active"},m={DATA_SPY:'[data-spy="scroll"]',ACTIVE:".active",NAV_LIST_GROUP:".nav, .list-group",NAV_LINKS:".nav-link",NAV_ITEMS:".nav-item",LIST_ITEMS:".list-group-item",DROPDOWN:".dropdown",DROPDOWN_ITEMS:".dropdown-item",DROPDOWN_TOGGLE:".dropdown-toggle"},v={OFFSET:"offset",POSITION:"position"},y=function(){function n(e,n){var i=this;this._element=e,this._scrollElement="BODY"===e.tagName?window:e,this._config=this._getConfig(n),this._selector=this._config.target+" "+m.NAV_LINKS+","+(this._config.target+" "+m.LIST_ITEMS+",")+(this._config.target+" "+m.DROPDOWN_ITEMS),this._offsets=[],this._targets=[],this._activeTarget=null,this._scrollHeight=0,t(this._scrollElement).on(p.SCROLL,function(t){return i._process(t)}),this.refresh(),this._process()}var r=n.prototype;return r.refresh=function(){var n=this,i=this._scrollElement===this._scrollElement.window?v.OFFSET:v.POSITION,r="auto"===this._config.method?i:this._config.method,o=r===v.POSITION?this._getScrollTop():0;this._offsets=[],this._targets=[],this._scrollHeight=this._getScrollHeight();var a=[].slice.call(document.querySelectorAll(this._selector));a.map(function(n){var i,a=e.getSelectorFromElement(n);if(a&&(i=document.querySelector(a)),i){var s=i.getBoundingClientRect();if(s.width||s.height)return[t(i)[r]().top+o,a]}return null}).filter(function(t){return t}).sort(function(t,e){return t[0]-e[0]}).forEach(function(t){n._offsets.push(t[0]),n._targets.push(t[1])})},r.dispose=function(){t.removeData(this._element,l),t(this._scrollElement).off(u),this._element=null,this._scrollElement=null,this._config=null,this._selector=null,this._offsets=null,this._targets=null,this._activeTarget=null,this._scrollHeight=null},r._getConfig=function(n){if(n=o({},d,"object"==typeof n&&n?n:{}),"string"!=typeof n.target){var i=t(n.target).attr("id");i||(i=e.getUID(a),t(n.target).attr("id",i)),n.target="#"+i}return e.typeCheckConfig(a,n,f),n},r._getScrollTop=function(){return this._scrollElement===window?this._scrollElement.pageYOffset:this._scrollElement.scrollTop},r._getScrollHeight=function(){return this._scrollElement.scrollHeight||Math.max(document.body.scrollHeight,document.documentElement.scrollHeight)},r._getOffsetHeight=function(){return this._scrollElement===window?window.innerHeight:this._scrollElement.getBoundingClientRect().height},r._process=function(){var t=this._getScrollTop()+this._config.offset,e=this._getScrollHeight(),n=this._config.offset+e-this._getOffsetHeight();if(this._scrollHeight!==e&&this.refresh(),t>=n){var i=this._targets[this._targets.length-1];return void(this._activeTarget!==i&&this._activate(i))}if(this._activeTarget&&t<this._offsets[0]&&this._offsets[0]>0)return this._activeTarget=null,void this._clear();for(var r=this._offsets.length,o=r;o--;){var a=this._activeTarget!==this._targets[o]&&t>=this._offsets[o]&&("undefined"==typeof this._offsets[o+1]||t<this._offsets[o+1]);a&&this._activate(this._targets[o])}},r._activate=function(e){this._activeTarget=e,this._clear();var n=this._selector.split(",").map(function(t){return t+'[data-target="'+e+'"],'+t+'[href="'+e+'"]'}),i=t([].slice.call(document.querySelectorAll(n.join(","))));i.hasClass(g.DROPDOWN_ITEM)?(i.closest(m.DROPDOWN).find(m.DROPDOWN_TOGGLE).addClass(g.ACTIVE),i.addClass(g.ACTIVE)):(i.addClass(g.ACTIVE),i.parents(m.NAV_LIST_GROUP).prev(m.NAV_LINKS+", "+m.LIST_ITEMS).addClass(g.ACTIVE),i.parents(m.NAV_LIST_GROUP).prev(m.NAV_ITEMS).children(m.NAV_LINKS).addClass(g.ACTIVE)),t(this._scrollElement).trigger(p.ACTIVATE,{relatedTarget:e})},r._clear=function(){[].slice.call(document.querySelectorAll(this._selector)).filter(function(t){return t.classList.contains(g.ACTIVE)}).forEach(function(t){return t.classList.remove(g.ACTIVE)})},n._jQueryInterface=function(e){return this.each(function(){var i=t(this).data(l),r="object"==typeof e&&e;if(i||(i=new n(this,r),t(this).data(l,i)),"string"==typeof e){if("undefined"==typeof i[e])throw new TypeError('No method named "'+e+'"');i[e]()}})},i(n,null,[{key:"VERSION",get:function(){return s}},{key:"Default",get:function(){return d}}]),n}();return t(window).on(p.LOAD_DATA_API,function(){for(var e=[].slice.call(document.querySelectorAll(m.DATA_SPY)),n=e.length,i=n;i--;){var r=t(e[i]);y._jQueryInterface.call(r,r.data())}}),t.fn[a]=y._jQueryInterface,t.fn[a].Constructor=y,t.fn[a].noConflict=function(){return t.fn[a]=h,y._jQueryInterface},y}),function(t,e){"object"==typeof exports&&"undefined"!=typeof module?module.exports=e(require("jquery"),require("./util.js")):"function"==typeof define&&define.amd?define(["jquery","./util.js"],e):(t=t||self,t.Tab=e(t.jQuery,t.Util))}(this,function(t,e){"use strict";function n(t,e){for(var n=0;n<e.length;n++){var i=e[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}function i(t,e,i){return e&&n(t.prototype,e),i&&n(t,i),t}t=t&&t.hasOwnProperty("default")?t["default"]:t,e=e&&e.hasOwnProperty("default")?e["default"]:e;var r="tab",o="4.3.1",a="bs.tab",s="."+a,l=".data-api",u=t.fn[r],c={HIDE:"hide"+s,HIDDEN:"hidden"+s,SHOW:"show"+s,SHOWN:"shown"+s,CLICK_DATA_API:"click"+s+l},h={DROPDOWN_MENU:"dropdown-menu",ACTIVE:"active",DISABLED:"disabled",FADE:"fade",SHOW:"show"},d={DROPDOWN:".dropdown",NAV_LIST_GROUP:".nav, .list-group",ACTIVE:".active",ACTIVE_UL:"> li > .active",DATA_TOGGLE:'[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]',DROPDOWN_TOGGLE:".dropdown-toggle",DROPDOWN_ACTIVE_CHILD:"> .dropdown-menu .active"},f=function(){function n(t){this._element=t}var r=n.prototype;return r.show=function(){var n=this;if(!(this._element.parentNode&&this._element.parentNode.nodeType===Node.ELEMENT_NODE&&t(this._element).hasClass(h.ACTIVE)||t(this._element).hasClass(h.DISABLED))){var i,r,o=t(this._element).closest(d.NAV_LIST_GROUP)[0],a=e.getSelectorFromElement(this._element);if(o){var s="UL"===o.nodeName||"OL"===o.nodeName?d.ACTIVE_UL:d.ACTIVE;r=t.makeArray(t(o).find(s)),r=r[r.length-1]}var l=t.Event(c.HIDE,{relatedTarget:this._element}),u=t.Event(c.SHOW,{relatedTarget:r});if(r&&t(r).trigger(l),t(this._element).trigger(u),!u.isDefaultPrevented()&&!l.isDefaultPrevented()){a&&(i=document.querySelector(a)),this._activate(this._element,o);var f=function(){var e=t.Event(c.HIDDEN,{relatedTarget:n._element}),i=t.Event(c.SHOWN,{relatedTarget:r});t(r).trigger(e),t(n._element).trigger(i)};i?this._activate(i,i.parentNode,f):f()}}},r.dispose=function(){t.removeData(this._element,a),this._element=null},r._activate=function(n,i,r){var o=this,a=!i||"UL"!==i.nodeName&&"OL"!==i.nodeName?t(i).children(d.ACTIVE):t(i).find(d.ACTIVE_UL),s=a[0],l=r&&s&&t(s).hasClass(h.FADE),u=function(){return o._transitionComplete(n,s,r)};if(s&&l){var c=e.getTransitionDurationFromElement(s);t(s).removeClass(h.SHOW).one(e.TRANSITION_END,u).emulateTransitionEnd(c)}else u()},r._transitionComplete=function(n,i,r){if(i){t(i).removeClass(h.ACTIVE);var o=t(i.parentNode).find(d.DROPDOWN_ACTIVE_CHILD)[0];o&&t(o).removeClass(h.ACTIVE),"tab"===i.getAttribute("role")&&i.setAttribute("aria-selected",!1)}if(t(n).addClass(h.ACTIVE),"tab"===n.getAttribute("role")&&n.setAttribute("aria-selected",!0),e.reflow(n),n.classList.contains(h.FADE)&&n.classList.add(h.SHOW),n.parentNode&&t(n.parentNode).hasClass(h.DROPDOWN_MENU)){var a=t(n).closest(d.DROPDOWN)[0];if(a){var s=[].slice.call(a.querySelectorAll(d.DROPDOWN_TOGGLE));t(s).addClass(h.ACTIVE)}n.setAttribute("aria-expanded",!0)}r&&r()},n._jQueryInterface=function(e){return this.each(function(){var i=t(this),r=i.data(a);if(r||(r=new n(this),i.data(a,r)),"string"==typeof e){if("undefined"==typeof r[e])throw new TypeError('No method named "'+e+'"');r[e]()}})},i(n,null,[{key:"VERSION",get:function(){return o}}]),n}();return t(document).on(c.CLICK_DATA_API,d.DATA_TOGGLE,function(e){e.preventDefault(),f._jQueryInterface.call(t(this),"show")}),t.fn[r]=f._jQueryInterface,t.fn[r].Constructor=f,t.fn[r].noConflict=function(){return t.fn[r]=u,f._jQueryInterface},f}),function(t,e){"object"==typeof exports&&"undefined"!=typeof module?module.exports=e(require("jquery"),require("./util.js")):"function"==typeof define&&define.amd?define(["jquery","./util.js"],e):(t=t||self,t.Toast=e(t.jQuery,t.Util))}(this,function(t,e){"use strict";function n(t,e){for(var n=0;n<e.length;n++){var i=e[n];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}function i(t,e,i){return e&&n(t.prototype,e),i&&n(t,i),t}function r(t,e,n){return e in t?Object.defineProperty(t,e,{value:n,enumerable:!0,configurable:!0,writable:!0}):t[e]=n,t}function o(t){for(var e=1;e<arguments.length;e++){var n=null!=arguments[e]?arguments[e]:{},i=Object.keys(n);"function"==typeof Object.getOwnPropertySymbols&&(i=i.concat(Object.getOwnPropertySymbols(n).filter(function(t){return Object.getOwnPropertyDescriptor(n,t).enumerable}))),i.forEach(function(e){r(t,e,n[e])})}return t}t=t&&t.hasOwnProperty("default")?t["default"]:t,e=e&&e.hasOwnProperty("default")?e["default"]:e;var a="toast",s="4.3.1",l="bs.toast",u="."+l,c=t.fn[a],h={CLICK_DISMISS:"click.dismiss"+u,HIDE:"hide"+u,HIDDEN:"hidden"+u,SHOW:"show"+u,SHOWN:"shown"+u},d={FADE:"fade",HIDE:"hide",SHOW:"show",SHOWING:"showing"},f={animation:"boolean",autohide:"boolean",delay:"number"},p={animation:!0,autohide:!0,delay:500},g={DATA_DISMISS:'[data-dismiss="toast"]'},m=function(){function n(t,e){this._element=t,this._config=this._getConfig(e),this._timeout=null,this._setListeners()}var r=n.prototype;return r.show=function(){var n=this;t(this._element).trigger(h.SHOW),this._config.animation&&this._element.classList.add(d.FADE);var i=function(){n._element.classList.remove(d.SHOWING),n._element.classList.add(d.SHOW),t(n._element).trigger(h.SHOWN),n._config.autohide&&n.hide()};if(this._element.classList.remove(d.HIDE),this._element.classList.add(d.SHOWING),this._config.animation){var r=e.getTransitionDurationFromElement(this._element);t(this._element).one(e.TRANSITION_END,i).emulateTransitionEnd(r)}else i()},r.hide=function(e){var n=this;this._element.classList.contains(d.SHOW)&&(t(this._element).trigger(h.HIDE),e?this._close():this._timeout=setTimeout(function(){n._close()},this._config.delay))},r.dispose=function(){clearTimeout(this._timeout),this._timeout=null,this._element.classList.contains(d.SHOW)&&this._element.classList.remove(d.SHOW),t(this._element).off(h.CLICK_DISMISS),t.removeData(this._element,l),this._element=null,this._config=null},r._getConfig=function(n){return n=o({},p,t(this._element).data(),"object"==typeof n&&n?n:{}),e.typeCheckConfig(a,n,this.constructor.DefaultType),n},r._setListeners=function(){var e=this;t(this._element).on(h.CLICK_DISMISS,g.DATA_DISMISS,function(){return e.hide(!0)})},r._close=function(){var n=this,i=function(){n._element.classList.add(d.HIDE),t(n._element).trigger(h.HIDDEN)};if(this._element.classList.remove(d.SHOW),this._config.animation){var r=e.getTransitionDurationFromElement(this._element);t(this._element).one(e.TRANSITION_END,i).emulateTransitionEnd(r)}else i()},n._jQueryInterface=function(e){return this.each(function(){var i=t(this),r=i.data(l),o="object"==typeof e&&e;if(r||(r=new n(this,o),i.data(l,r)),"string"==typeof e){if("undefined"==typeof r[e])throw new TypeError('No method named "'+e+'"');r[e](this)}})},i(n,null,[{key:"VERSION",get:function(){return s}},{key:"DefaultType",get:function(){return f}},{key:"Default",get:function(){return p}}]),n}();return t.fn[a]=m._jQueryInterface,t.fn[a].Constructor=m,t.fn[a].noConflict=function(){return t.fn[a]=c,m._jQueryInterface},m}),function(t){"function"==typeof define&&define.amd?define(["jquery"],t):t("object"==typeof exports?require("jquery"):jQuery)}(function(t,e){function n(){return new Date(Date.UTC.apply(Date,arguments))}function i(){var t=new Date;return n(t.getFullYear(),t.getMonth(),t.getDate())}function r(t,e){return t.getUTCFullYear()===e.getUTCFullYear()&&t.getUTCMonth()===e.getUTCMonth()&&t.getUTCDate()===e.getUTCDate()}function o(n,i){return function(){return i!==e&&t.fn.datepicker.deprecated(i),this[n].apply(this,arguments)}}function a(t){return t&&!isNaN(t.getTime())}function s(e,n){function i(t,e){return e.toLowerCase()}var r,o=t(e).data(),a={},s=new RegExp("^"+n.toLowerCase()+"([A-Z])");n=new RegExp("^"+n.toLowerCase());for(var l in o)n.test(l)&&(r=l.replace(s,i),a[r]=o[l]);return a}function l(e){var n={};if(m[e]||(e=e.split("-")[0],m[e])){var i=m[e];return t.each(g,function(t,e){e in i&&(n[e]=i[e])}),n}}var u=function(){var e={get:function(t){return this.slice(t)[0]},contains:function(t){for(var e=t&&t.valueOf(),n=0,i=this.length;n<i;n++)if(0<=this[n].valueOf()-e&&this[n].valueOf()-e<864e5)return n;return-1},remove:function(t){this.splice(t,1)},replace:function(e){e&&(t.isArray(e)||(e=[e]),this.clear(),this.push.apply(this,e))},clear:function(){this.length=0},copy:function(){var t=new u;return t.replace(this),t}};return function(){var n=[];return n.push.apply(n,arguments),t.extend(n,e),n}}(),c=function(e,n){t.data(e,"datepicker",this),this._events=[],this._secondaryEvents=[],this._process_options(n),this.dates=new u,this.viewDate=this.o.defaultViewDate,this.focusDate=null,this.element=t(e),this.isInput=this.element.is("input"),this.inputField=this.isInput?this.element:this.element.find("input"),this.component=!!this.element.hasClass("date")&&this.element.find(".add-on, .input-group-addon, .input-group-append, .input-group-prepend, .btn"),this.component&&0===this.component.length&&(this.component=!1),this.isInline=!this.component&&this.element.is("div"),this.picker=t(v.template),this._check_template(this.o.templates.leftArrow)&&this.picker.find(".prev").html(this.o.templates.leftArrow),this._check_template(this.o.templates.rightArrow)&&this.picker.find(".next").html(this.o.templates.rightArrow),this._buildEvents(),this._attachEvents(),this.isInline?this.picker.addClass("datepicker-inline").appendTo(this.element):this.picker.addClass("datepicker-dropdown dropdown-menu"),this.o.rtl&&this.picker.addClass("datepicker-rtl"),this.o.calendarWeeks&&this.picker.find(".datepicker-days .datepicker-switch, thead .datepicker-title, tfoot .today, tfoot .clear").attr("colspan",function(t,e){return Number(e)+1}),this._process_options({startDate:this._o.startDate,endDate:this._o.endDate,daysOfWeekDisabled:this.o.daysOfWeekDisabled,daysOfWeekHighlighted:this.o.daysOfWeekHighlighted,datesDisabled:this.o.datesDisabled}),this._allow_update=!1,this.setViewMode(this.o.startView),this._allow_update=!0,this.fillDow(),this.fillMonths(),this.update(),this.isInline&&this.show()};c.prototype={constructor:c,_resolveViewName:function(e){return t.each(v.viewModes,function(n,i){if(e===n||t.inArray(e,i.names)!==-1)return e=n,!1}),e},_resolveDaysOfWeek:function(e){return t.isArray(e)||(e=e.split(/[,\s]*/)),t.map(e,Number)},_check_template:function(n){try{if(n===e||""===n)return!1;if((n.match(/[<>]/g)||[]).length<=0)return!0;var i=t(n);return i.length>0}catch(r){return!1}},_process_options:function(e){this._o=t.extend({},this._o,e);var r=this.o=t.extend({},this._o),o=r.language;m[o]||(o=o.split("-")[0],m[o]||(o=p.language)),r.language=o,r.startView=this._resolveViewName(r.startView),r.minViewMode=this._resolveViewName(r.minViewMode),r.maxViewMode=this._resolveViewName(r.maxViewMode),r.startView=Math.max(this.o.minViewMode,Math.min(this.o.maxViewMode,r.startView)),r.multidate!==!0&&(r.multidate=Number(r.multidate)||!1,r.multidate!==!1&&(r.multidate=Math.max(0,r.multidate))),r.multidateSeparator=String(r.multidateSeparator),r.weekStart%=7,r.weekEnd=(r.weekStart+6)%7;var a=v.parseFormat(r.format);r.startDate!==-(1/0)&&(r.startDate?r.startDate instanceof Date?r.startDate=this._local_to_utc(this._zero_time(r.startDate)):r.startDate=v.parseDate(r.startDate,a,r.language,r.assumeNearbyYear):r.startDate=-(1/0)),r.endDate!==1/0&&(r.endDate?r.endDate instanceof Date?r.endDate=this._local_to_utc(this._zero_time(r.endDate)):r.endDate=v.parseDate(r.endDate,a,r.language,r.assumeNearbyYear):r.endDate=1/0),r.daysOfWeekDisabled=this._resolveDaysOfWeek(r.daysOfWeekDisabled||[]),r.daysOfWeekHighlighted=this._resolveDaysOfWeek(r.daysOfWeekHighlighted||[]),
r.datesDisabled=r.datesDisabled||[],t.isArray(r.datesDisabled)||(r.datesDisabled=r.datesDisabled.split(",")),r.datesDisabled=t.map(r.datesDisabled,function(t){return v.parseDate(t,a,r.language,r.assumeNearbyYear)});var s=String(r.orientation).toLowerCase().split(/\s+/g),l=r.orientation.toLowerCase();if(s=t.grep(s,function(t){return/^auto|left|right|top|bottom$/.test(t)}),r.orientation={x:"auto",y:"auto"},l&&"auto"!==l)if(1===s.length)switch(s[0]){case"top":case"bottom":r.orientation.y=s[0];break;case"left":case"right":r.orientation.x=s[0]}else l=t.grep(s,function(t){return/^left|right$/.test(t)}),r.orientation.x=l[0]||"auto",l=t.grep(s,function(t){return/^top|bottom$/.test(t)}),r.orientation.y=l[0]||"auto";else;if(r.defaultViewDate instanceof Date||"string"==typeof r.defaultViewDate)r.defaultViewDate=v.parseDate(r.defaultViewDate,a,r.language,r.assumeNearbyYear);else if(r.defaultViewDate){var u=r.defaultViewDate.year||(new Date).getFullYear(),c=r.defaultViewDate.month||0,h=r.defaultViewDate.day||1;r.defaultViewDate=n(u,c,h)}else r.defaultViewDate=i()},_applyEvents:function(t){for(var n,i,r,o=0;o<t.length;o++)n=t[o][0],2===t[o].length?(i=e,r=t[o][1]):3===t[o].length&&(i=t[o][1],r=t[o][2]),n.on(r,i)},_unapplyEvents:function(t){for(var n,i,r,o=0;o<t.length;o++)n=t[o][0],2===t[o].length?(r=e,i=t[o][1]):3===t[o].length&&(r=t[o][1],i=t[o][2]),n.off(i,r)},_buildEvents:function(){var e={keyup:t.proxy(function(e){t.inArray(e.keyCode,[27,37,39,38,40,32,13,9])===-1&&this.update()},this),keydown:t.proxy(this.keydown,this),paste:t.proxy(this.paste,this)};this.o.showOnFocus===!0&&(e.focus=t.proxy(this.show,this)),this.isInput?this._events=[[this.element,e]]:this.component&&this.inputField.length?this._events=[[this.inputField,e],[this.component,{click:t.proxy(this.show,this)}]]:this._events=[[this.element,{click:t.proxy(this.show,this),keydown:t.proxy(this.keydown,this)}]],this._events.push([this.element,"*",{blur:t.proxy(function(t){this._focused_from=t.target},this)}],[this.element,{blur:t.proxy(function(t){this._focused_from=t.target},this)}]),this.o.immediateUpdates&&this._events.push([this.element,{"changeYear changeMonth":t.proxy(function(t){this.update(t.date)},this)}]),this._secondaryEvents=[[this.picker,{click:t.proxy(this.click,this)}],[this.picker,".prev, .next",{click:t.proxy(this.navArrowsClick,this)}],[this.picker,".day:not(.disabled)",{click:t.proxy(this.dayCellClick,this)}],[t(window),{resize:t.proxy(this.place,this)}],[t(document),{"mousedown touchstart":t.proxy(function(t){this.element.is(t.target)||this.element.find(t.target).length||this.picker.is(t.target)||this.picker.find(t.target).length||this.isInline||this.hide()},this)}]]},_attachEvents:function(){this._detachEvents(),this._applyEvents(this._events)},_detachEvents:function(){this._unapplyEvents(this._events)},_attachSecondaryEvents:function(){this._detachSecondaryEvents(),this._applyEvents(this._secondaryEvents)},_detachSecondaryEvents:function(){this._unapplyEvents(this._secondaryEvents)},_trigger:function(e,n){var i=n||this.dates.get(-1),r=this._utc_to_local(i);this.element.trigger({type:e,date:r,viewMode:this.viewMode,dates:t.map(this.dates,this._utc_to_local),format:t.proxy(function(t,e){0===arguments.length?(t=this.dates.length-1,e=this.o.format):"string"==typeof t&&(e=t,t=this.dates.length-1),e=e||this.o.format;var n=this.dates.get(t);return v.formatDate(n,e,this.o.language)},this)})},show:function(){if(!(this.inputField.is(":disabled")||this.inputField.prop("readonly")&&this.o.enableOnReadonly===!1))return this.isInline||this.picker.appendTo(this.o.container),this.place(),this.picker.show(),this._attachSecondaryEvents(),this._trigger("show"),(window.navigator.msMaxTouchPoints||"ontouchstart"in document)&&this.o.disableTouchKeyboard&&t(this.element).blur(),this},hide:function(){return this.isInline||!this.picker.is(":visible")?this:(this.focusDate=null,this.picker.hide().detach(),this._detachSecondaryEvents(),this.setViewMode(this.o.startView),this.o.forceParse&&this.inputField.val()&&this.setValue(),this._trigger("hide"),this)},destroy:function(){return this.hide(),this._detachEvents(),this._detachSecondaryEvents(),this.picker.remove(),delete this.element.data().datepicker,this.isInput||delete this.element.data().date,this},paste:function(e){var n;if(e.originalEvent.clipboardData&&e.originalEvent.clipboardData.types&&t.inArray("text/plain",e.originalEvent.clipboardData.types)!==-1)n=e.originalEvent.clipboardData.getData("text/plain");else{if(!window.clipboardData)return;n=window.clipboardData.getData("Text")}this.setDate(n),this.update(),e.preventDefault()},_utc_to_local:function(t){if(!t)return t;var e=new Date(t.getTime()+6e4*t.getTimezoneOffset());return e.getTimezoneOffset()!==t.getTimezoneOffset()&&(e=new Date(t.getTime()+6e4*e.getTimezoneOffset())),e},_local_to_utc:function(t){return t&&new Date(t.getTime()-6e4*t.getTimezoneOffset())},_zero_time:function(t){return t&&new Date(t.getFullYear(),t.getMonth(),t.getDate())},_zero_utc_time:function(t){return t&&n(t.getUTCFullYear(),t.getUTCMonth(),t.getUTCDate())},getDates:function(){return t.map(this.dates,this._utc_to_local)},getUTCDates:function(){return t.map(this.dates,function(t){return new Date(t)})},getDate:function(){return this._utc_to_local(this.getUTCDate())},getUTCDate:function(){var t=this.dates.get(-1);return t!==e?new Date(t):null},clearDates:function(){this.inputField.val(""),this.update(),this._trigger("changeDate"),this.o.autoclose&&this.hide()},setDates:function(){var e=t.isArray(arguments[0])?arguments[0]:arguments;return this.update.apply(this,e),this._trigger("changeDate"),this.setValue(),this},setUTCDates:function(){var e=t.isArray(arguments[0])?arguments[0]:arguments;return this.setDates.apply(this,t.map(e,this._utc_to_local)),this},setDate:o("setDates"),setUTCDate:o("setUTCDates"),remove:o("destroy","Method `remove` is deprecated and will be removed in version 2.0. Use `destroy` instead"),setValue:function(){var t=this.getFormattedDate();return this.inputField.val(t),this},getFormattedDate:function(n){n===e&&(n=this.o.format);var i=this.o.language;return t.map(this.dates,function(t){return v.formatDate(t,n,i)}).join(this.o.multidateSeparator)},getStartDate:function(){return this.o.startDate},setStartDate:function(t){return this._process_options({startDate:t}),this.update(),this.updateNavArrows(),this},getEndDate:function(){return this.o.endDate},setEndDate:function(t){return this._process_options({endDate:t}),this.update(),this.updateNavArrows(),this},setDaysOfWeekDisabled:function(t){return this._process_options({daysOfWeekDisabled:t}),this.update(),this},setDaysOfWeekHighlighted:function(t){return this._process_options({daysOfWeekHighlighted:t}),this.update(),this},setDatesDisabled:function(t){return this._process_options({datesDisabled:t}),this.update(),this},place:function(){if(this.isInline)return this;var e=this.picker.outerWidth(),n=this.picker.outerHeight(),i=10,r=t(this.o.container),o=r.width(),a="body"===this.o.container?t(document).scrollTop():r.scrollTop(),s=r.offset(),l=[0];this.element.parents().each(function(){var e=t(this).css("z-index");"auto"!==e&&0!==Number(e)&&l.push(Number(e))});var u=Math.max.apply(Math,l)+this.o.zIndexOffset,c=this.component?this.component.parent().offset():this.element.offset(),h=this.component?this.component.outerHeight(!0):this.element.outerHeight(!1),d=this.component?this.component.outerWidth(!0):this.element.outerWidth(!1),f=c.left-s.left,p=c.top-s.top;"body"!==this.o.container&&(p+=a),this.picker.removeClass("datepicker-orient-top datepicker-orient-bottom datepicker-orient-right datepicker-orient-left"),"auto"!==this.o.orientation.x?(this.picker.addClass("datepicker-orient-"+this.o.orientation.x),"right"===this.o.orientation.x&&(f-=e-d)):c.left<0?(this.picker.addClass("datepicker-orient-left"),f-=c.left-i):f+e>o?(this.picker.addClass("datepicker-orient-right"),f+=d-e):this.o.rtl?this.picker.addClass("datepicker-orient-right"):this.picker.addClass("datepicker-orient-left");var g,m=this.o.orientation.y;if("auto"===m&&(g=-a+p-n,m=g<0?"bottom":"top"),this.picker.addClass("datepicker-orient-"+m),"top"===m?p-=n+parseInt(this.picker.css("padding-top")):p+=h,this.o.rtl){var v=o-(f+d);this.picker.css({top:p,right:v,zIndex:u})}else this.picker.css({top:p,left:f,zIndex:u});return this},_allow_update:!0,update:function(){if(!this._allow_update)return this;var e=this.dates.copy(),n=[],i=!1;return arguments.length?(t.each(arguments,t.proxy(function(t,e){e instanceof Date&&(e=this._local_to_utc(e)),n.push(e)},this)),i=!0):(n=this.isInput?this.element.val():this.element.data("date")||this.inputField.val(),n=n&&this.o.multidate?n.split(this.o.multidateSeparator):[n],delete this.element.data().date),n=t.map(n,t.proxy(function(t){return v.parseDate(t,this.o.format,this.o.language,this.o.assumeNearbyYear)},this)),n=t.grep(n,t.proxy(function(t){return!this.dateWithinRange(t)||!t},this),!0),this.dates.replace(n),this.o.updateViewDate&&(this.dates.length?this.viewDate=new Date(this.dates.get(-1)):this.viewDate<this.o.startDate?this.viewDate=new Date(this.o.startDate):this.viewDate>this.o.endDate?this.viewDate=new Date(this.o.endDate):this.viewDate=this.o.defaultViewDate),i?(this.setValue(),this.element.change()):this.dates.length&&String(e)!==String(this.dates)&&i&&(this._trigger("changeDate"),this.element.change()),!this.dates.length&&e.length&&(this._trigger("clearDate"),this.element.change()),this.fill(),this},fillDow:function(){if(this.o.showWeekDays){var e=this.o.weekStart,n="<tr>";for(this.o.calendarWeeks&&(n+='<th class="cw">&#160;</th>');e<this.o.weekStart+7;)n+='<th class="dow',t.inArray(e,this.o.daysOfWeekDisabled)!==-1&&(n+=" disabled"),n+='">'+m[this.o.language].daysMin[e++%7]+"</th>";n+="</tr>",this.picker.find(".datepicker-days thead").append(n)}},fillMonths:function(){for(var t,e=this._utc_to_local(this.viewDate),n="",i=0;i<12;i++)t=e&&e.getMonth()===i?" focused":"",n+='<span class="month'+t+'">'+m[this.o.language].monthsShort[i]+"</span>";this.picker.find(".datepicker-months td").html(n)},setRange:function(e){e&&e.length?this.range=t.map(e,function(t){return t.valueOf()}):delete this.range,this.fill()},getClassNames:function(e){var n=[],o=this.viewDate.getUTCFullYear(),a=this.viewDate.getUTCMonth(),s=i();return e.getUTCFullYear()<o||e.getUTCFullYear()===o&&e.getUTCMonth()<a?n.push("old"):(e.getUTCFullYear()>o||e.getUTCFullYear()===o&&e.getUTCMonth()>a)&&n.push("new"),this.focusDate&&e.valueOf()===this.focusDate.valueOf()&&n.push("focused"),this.o.todayHighlight&&r(e,s)&&n.push("today"),this.dates.contains(e)!==-1&&n.push("active"),this.dateWithinRange(e)||n.push("disabled"),this.dateIsDisabled(e)&&n.push("disabled","disabled-date"),t.inArray(e.getUTCDay(),this.o.daysOfWeekHighlighted)!==-1&&n.push("highlighted"),this.range&&(e>this.range[0]&&e<this.range[this.range.length-1]&&n.push("range"),t.inArray(e.valueOf(),this.range)!==-1&&n.push("selected"),e.valueOf()===this.range[0]&&n.push("range-start"),e.valueOf()===this.range[this.range.length-1]&&n.push("range-end")),n},_fill_yearsView:function(n,i,r,o,a,s,l){for(var u,c,h,d="",f=r/10,p=this.picker.find(n),g=Math.floor(o/r)*r,m=g+9*f,v=Math.floor(this.viewDate.getFullYear()/f)*f,y=t.map(this.dates,function(t){return Math.floor(t.getUTCFullYear()/f)*f}),b=g-f;b<=m+f;b+=f)u=[i],c=null,b===g-f?u.push("old"):b===m+f&&u.push("new"),t.inArray(b,y)!==-1&&u.push("active"),(b<a||b>s)&&u.push("disabled"),b===v&&u.push("focused"),l!==t.noop&&(h=l(new Date(b,0,1)),h===e?h={}:"boolean"==typeof h?h={enabled:h}:"string"==typeof h&&(h={classes:h}),h.enabled===!1&&u.push("disabled"),h.classes&&(u=u.concat(h.classes.split(/\s+/))),h.tooltip&&(c=h.tooltip)),d+='<span class="'+u.join(" ")+'"'+(c?' title="'+c+'"':"")+">"+b+"</span>";p.find(".datepicker-switch").text(g+"-"+m),p.find("td").html(d)},fill:function(){var r,o,a=new Date(this.viewDate),s=a.getUTCFullYear(),l=a.getUTCMonth(),u=this.o.startDate!==-(1/0)?this.o.startDate.getUTCFullYear():-(1/0),c=this.o.startDate!==-(1/0)?this.o.startDate.getUTCMonth():-(1/0),h=this.o.endDate!==1/0?this.o.endDate.getUTCFullYear():1/0,d=this.o.endDate!==1/0?this.o.endDate.getUTCMonth():1/0,f=m[this.o.language].today||m.en.today||"",p=m[this.o.language].clear||m.en.clear||"",g=m[this.o.language].titleFormat||m.en.titleFormat,y=i(),b=(this.o.todayBtn===!0||"linked"===this.o.todayBtn)&&y>=this.o.startDate&&y<=this.o.endDate&&!this.weekOfDateIsDisabled(y);if(!isNaN(s)&&!isNaN(l)){this.picker.find(".datepicker-days .datepicker-switch").text(v.formatDate(a,g,this.o.language)),this.picker.find("tfoot .today").text(f).css("display",b?"table-cell":"none"),this.picker.find("tfoot .clear").text(p).css("display",this.o.clearBtn===!0?"table-cell":"none"),this.picker.find("thead .datepicker-title").text(this.o.title).css("display","string"==typeof this.o.title&&""!==this.o.title?"table-cell":"none"),this.updateNavArrows(),this.fillMonths();var _=n(s,l,0),x=_.getUTCDate();_.setUTCDate(x-(_.getUTCDay()-this.o.weekStart+7)%7);var w=new Date(_);_.getUTCFullYear()<100&&w.setUTCFullYear(_.getUTCFullYear()),w.setUTCDate(w.getUTCDate()+42),w=w.valueOf();for(var D,S,C=[];_.valueOf()<w;){if(D=_.getUTCDay(),D===this.o.weekStart&&(C.push("<tr>"),this.o.calendarWeeks)){var T=new Date(+_+(this.o.weekStart-D-7)%7*864e5),k=new Date(Number(T)+(11-T.getUTCDay())%7*864e5),A=new Date(Number(A=n(k.getUTCFullYear(),0,1))+(11-A.getUTCDay())%7*864e5),I=(k-A)/864e5/7+1;C.push('<td class="cw">'+I+"</td>")}S=this.getClassNames(_),S.push("day");var E=_.getUTCDate();this.o.beforeShowDay!==t.noop&&(o=this.o.beforeShowDay(this._utc_to_local(_)),o===e?o={}:"boolean"==typeof o?o={enabled:o}:"string"==typeof o&&(o={classes:o}),o.enabled===!1&&S.push("disabled"),o.classes&&(S=S.concat(o.classes.split(/\s+/))),o.tooltip&&(r=o.tooltip),o.content&&(E=o.content)),S=t.isFunction(t.uniqueSort)?t.uniqueSort(S):t.unique(S),C.push('<td class="'+S.join(" ")+'"'+(r?' title="'+r+'"':"")+' data-date="'+_.getTime().toString()+'">'+E+"</td>"),r=null,D===this.o.weekEnd&&C.push("</tr>"),_.setUTCDate(_.getUTCDate()+1)}this.picker.find(".datepicker-days tbody").html(C.join(""));var O=m[this.o.language].monthsTitle||m.en.monthsTitle||"Months",M=this.picker.find(".datepicker-months").find(".datepicker-switch").text(this.o.maxViewMode<2?O:s).end().find("tbody span").removeClass("active");if(t.each(this.dates,function(t,e){e.getUTCFullYear()===s&&M.eq(e.getUTCMonth()).addClass("active")}),(s<u||s>h)&&M.addClass("disabled"),s===u&&M.slice(0,c).addClass("disabled"),s===h&&M.slice(d+1).addClass("disabled"),this.o.beforeShowMonth!==t.noop){var P=this;t.each(M,function(n,i){var r=new Date(s,n,1),o=P.o.beforeShowMonth(r);o===e?o={}:"boolean"==typeof o?o={enabled:o}:"string"==typeof o&&(o={classes:o}),o.enabled!==!1||t(i).hasClass("disabled")||t(i).addClass("disabled"),o.classes&&t(i).addClass(o.classes),o.tooltip&&t(i).prop("title",o.tooltip)})}this._fill_yearsView(".datepicker-years","year",10,s,u,h,this.o.beforeShowYear),this._fill_yearsView(".datepicker-decades","decade",100,s,u,h,this.o.beforeShowDecade),this._fill_yearsView(".datepicker-centuries","century",1e3,s,u,h,this.o.beforeShowCentury)}},updateNavArrows:function(){if(this._allow_update){var t,e,n=new Date(this.viewDate),i=n.getUTCFullYear(),r=n.getUTCMonth(),o=this.o.startDate!==-(1/0)?this.o.startDate.getUTCFullYear():-(1/0),a=this.o.startDate!==-(1/0)?this.o.startDate.getUTCMonth():-(1/0),s=this.o.endDate!==1/0?this.o.endDate.getUTCFullYear():1/0,l=this.o.endDate!==1/0?this.o.endDate.getUTCMonth():1/0,u=1;switch(this.viewMode){case 4:u*=10;case 3:u*=10;case 2:u*=10;case 1:t=Math.floor(i/u)*u<=o,e=Math.floor(i/u)*u+u>s;break;case 0:t=i<=o&&r<=a,e=i>=s&&r>=l}this.picker.find(".prev").toggleClass("disabled",t),this.picker.find(".next").toggleClass("disabled",e)}},click:function(e){e.preventDefault(),e.stopPropagation();var r,o,a,s;r=t(e.target),r.hasClass("datepicker-switch")&&this.viewMode!==this.o.maxViewMode&&this.setViewMode(this.viewMode+1),r.hasClass("today")&&!r.hasClass("day")&&(this.setViewMode(0),this._setDate(i(),"linked"===this.o.todayBtn?null:"view")),r.hasClass("clear")&&this.clearDates(),r.hasClass("disabled")||(r.hasClass("month")||r.hasClass("year")||r.hasClass("decade")||r.hasClass("century"))&&(this.viewDate.setUTCDate(1),o=1,1===this.viewMode?(s=r.parent().find("span").index(r),a=this.viewDate.getUTCFullYear(),this.viewDate.setUTCMonth(s)):(s=0,a=Number(r.text()),this.viewDate.setUTCFullYear(a)),this._trigger(v.viewModes[this.viewMode-1].e,this.viewDate),this.viewMode===this.o.minViewMode?this._setDate(n(a,s,o)):(this.setViewMode(this.viewMode-1),this.fill())),this.picker.is(":visible")&&this._focused_from&&this._focused_from.focus(),delete this._focused_from},dayCellClick:function(e){var n=t(e.currentTarget),i=n.data("date"),r=new Date(i);this.o.updateViewDate&&(r.getUTCFullYear()!==this.viewDate.getUTCFullYear()&&this._trigger("changeYear",this.viewDate),r.getUTCMonth()!==this.viewDate.getUTCMonth()&&this._trigger("changeMonth",this.viewDate)),this._setDate(r)},navArrowsClick:function(e){var n=t(e.currentTarget),i=n.hasClass("prev")?-1:1;0!==this.viewMode&&(i*=12*v.viewModes[this.viewMode].navStep),this.viewDate=this.moveMonth(this.viewDate,i),this._trigger(v.viewModes[this.viewMode].e,this.viewDate),this.fill()},_toggle_multidate:function(t){var e=this.dates.contains(t);if(t||this.dates.clear(),e!==-1?(this.o.multidate===!0||this.o.multidate>1||this.o.toggleActive)&&this.dates.remove(e):this.o.multidate===!1?(this.dates.clear(),this.dates.push(t)):this.dates.push(t),"number"==typeof this.o.multidate)for(;this.dates.length>this.o.multidate;)this.dates.remove(0)},_setDate:function(t,e){e&&"date"!==e||this._toggle_multidate(t&&new Date(t)),(!e&&this.o.updateViewDate||"view"===e)&&(this.viewDate=t&&new Date(t)),this.fill(),this.setValue(),e&&"view"===e||this._trigger("changeDate"),this.inputField.trigger("change"),!this.o.autoclose||e&&"date"!==e||this.hide()},moveDay:function(t,e){var n=new Date(t);return n.setUTCDate(t.getUTCDate()+e),n},moveWeek:function(t,e){return this.moveDay(t,7*e)},moveMonth:function(t,e){if(!a(t))return this.o.defaultViewDate;if(!e)return t;var n,i,r=new Date(t.valueOf()),o=r.getUTCDate(),s=r.getUTCMonth(),l=Math.abs(e);if(e=e>0?1:-1,1===l)i=e===-1?function(){return r.getUTCMonth()===s}:function(){return r.getUTCMonth()!==n},n=s+e,r.setUTCMonth(n),n=(n+12)%12;else{for(var u=0;u<l;u++)r=this.moveMonth(r,e);n=r.getUTCMonth(),r.setUTCDate(o),i=function(){return n!==r.getUTCMonth()}}for(;i();)r.setUTCDate(--o),r.setUTCMonth(n);return r},moveYear:function(t,e){return this.moveMonth(t,12*e)},moveAvailableDate:function(t,e,n){do{if(t=this[n](t,e),!this.dateWithinRange(t))return!1;n="moveDay"}while(this.dateIsDisabled(t));return t},weekOfDateIsDisabled:function(e){return t.inArray(e.getUTCDay(),this.o.daysOfWeekDisabled)!==-1},dateIsDisabled:function(e){return this.weekOfDateIsDisabled(e)||t.grep(this.o.datesDisabled,function(t){return r(e,t)}).length>0},dateWithinRange:function(t){return t>=this.o.startDate&&t<=this.o.endDate},keydown:function(t){if(!this.picker.is(":visible"))return void(40!==t.keyCode&&27!==t.keyCode||(this.show(),t.stopPropagation()));var e,n,i=!1,r=this.focusDate||this.viewDate;switch(t.keyCode){case 27:this.focusDate?(this.focusDate=null,this.viewDate=this.dates.get(-1)||this.viewDate,this.fill()):this.hide(),t.preventDefault(),t.stopPropagation();break;case 37:case 38:case 39:case 40:if(!this.o.keyboardNavigation||7===this.o.daysOfWeekDisabled.length)break;e=37===t.keyCode||38===t.keyCode?-1:1,0===this.viewMode?t.ctrlKey?(n=this.moveAvailableDate(r,e,"moveYear"),n&&this._trigger("changeYear",this.viewDate)):t.shiftKey?(n=this.moveAvailableDate(r,e,"moveMonth"),n&&this._trigger("changeMonth",this.viewDate)):37===t.keyCode||39===t.keyCode?n=this.moveAvailableDate(r,e,"moveDay"):this.weekOfDateIsDisabled(r)||(n=this.moveAvailableDate(r,e,"moveWeek")):1===this.viewMode?(38!==t.keyCode&&40!==t.keyCode||(e=4*e),n=this.moveAvailableDate(r,e,"moveMonth")):2===this.viewMode&&(38!==t.keyCode&&40!==t.keyCode||(e=4*e),n=this.moveAvailableDate(r,e,"moveYear")),n&&(this.focusDate=this.viewDate=n,this.setValue(),this.fill(),t.preventDefault());break;case 13:if(!this.o.forceParse)break;r=this.focusDate||this.dates.get(-1)||this.viewDate,this.o.keyboardNavigation&&(this._toggle_multidate(r),i=!0),this.focusDate=null,this.viewDate=this.dates.get(-1)||this.viewDate,this.setValue(),this.fill(),this.picker.is(":visible")&&(t.preventDefault(),t.stopPropagation(),this.o.autoclose&&this.hide());break;case 9:this.focusDate=null,this.viewDate=this.dates.get(-1)||this.viewDate,this.fill(),this.hide()}i&&(this.dates.length?this._trigger("changeDate"):this._trigger("clearDate"),this.inputField.trigger("change"))},setViewMode:function(t){this.viewMode=t,this.picker.children("div").hide().filter(".datepicker-"+v.viewModes[this.viewMode].clsName).show(),this.updateNavArrows(),this._trigger("changeViewMode",new Date(this.viewDate))}};var h=function(e,n){t.data(e,"datepicker",this),this.element=t(e),this.inputs=t.map(n.inputs,function(t){return t.jquery?t[0]:t}),delete n.inputs,this.keepEmptyValues=n.keepEmptyValues,delete n.keepEmptyValues,f.call(t(this.inputs),n).on("changeDate",t.proxy(this.dateUpdated,this)),this.pickers=t.map(this.inputs,function(e){return t.data(e,"datepicker")}),this.updateDates()};h.prototype={updateDates:function(){this.dates=t.map(this.pickers,function(t){return t.getUTCDate()}),this.updateRanges()},updateRanges:function(){var e=t.map(this.dates,function(t){return t.valueOf()});t.each(this.pickers,function(t,n){n.setRange(e)})},clearDates:function(){t.each(this.pickers,function(t,e){e.clearDates()})},dateUpdated:function(n){if(!this.updating){this.updating=!0;var i=t.data(n.target,"datepicker");if(i!==e){var r=i.getUTCDate(),o=this.keepEmptyValues,a=t.inArray(n.target,this.inputs),s=a-1,l=a+1,u=this.inputs.length;if(a!==-1){if(t.each(this.pickers,function(t,e){e.getUTCDate()||e!==i&&o||e.setUTCDate(r)}),r<this.dates[s])for(;s>=0&&r<this.dates[s];)this.pickers[s--].setUTCDate(r);else if(r>this.dates[l])for(;l<u&&r>this.dates[l];)this.pickers[l++].setUTCDate(r);this.updateDates(),delete this.updating}}}},destroy:function(){t.map(this.pickers,function(t){t.destroy()}),t(this.inputs).off("changeDate",this.dateUpdated),delete this.element.data().datepicker},remove:o("destroy","Method `remove` is deprecated and will be removed in version 2.0. Use `destroy` instead")};var d=t.fn.datepicker,f=function(n){var i=Array.apply(null,arguments);i.shift();var r;if(this.each(function(){var e=t(this),o=e.data("datepicker"),a="object"==typeof n&&n;if(!o){var u=s(this,"date"),d=t.extend({},p,u,a),f=l(d.language),g=t.extend({},p,f,u,a);e.hasClass("input-daterange")||g.inputs?(t.extend(g,{inputs:g.inputs||e.find("input").toArray()}),o=new h(this,g)):o=new c(this,g),e.data("datepicker",o)}"string"==typeof n&&"function"==typeof o[n]&&(r=o[n].apply(o,i))}),r===e||r instanceof c||r instanceof h)return this;if(this.length>1)throw new Error("Using only allowed for the collection of a single element ("+n+" function)");return r};t.fn.datepicker=f;var p=t.fn.datepicker.defaults={assumeNearbyYear:!1,autoclose:!1,beforeShowDay:t.noop,beforeShowMonth:t.noop,beforeShowYear:t.noop,beforeShowDecade:t.noop,beforeShowCentury:t.noop,calendarWeeks:!1,clearBtn:!1,toggleActive:!1,daysOfWeekDisabled:[],daysOfWeekHighlighted:[],datesDisabled:[],endDate:1/0,forceParse:!0,format:"mm/dd/yyyy",keepEmptyValues:!1,keyboardNavigation:!0,language:"en",minViewMode:0,maxViewMode:4,multidate:!1,multidateSeparator:",",orientation:"auto",rtl:!1,startDate:-(1/0),startView:0,todayBtn:!1,todayHighlight:!1,updateViewDate:!0,weekStart:0,disableTouchKeyboard:!1,enableOnReadonly:!0,showOnFocus:!0,zIndexOffset:10,container:"body",immediateUpdates:!1,title:"",templates:{leftArrow:"&#x00AB;",rightArrow:"&#x00BB;"},showWeekDays:!0},g=t.fn.datepicker.locale_opts=["format","rtl","weekStart"];t.fn.datepicker.Constructor=c;var m=t.fn.datepicker.dates={en:{days:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],daysShort:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],daysMin:["Su","Mo","Tu","We","Th","Fr","Sa"],months:["January","February","March","April","May","June","July","August","September","October","November","December"],monthsShort:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],today:"Today",clear:"Clear",titleFormat:"MM yyyy"}},v={viewModes:[{names:["days","month"],clsName:"days",e:"changeMonth"},{names:["months","year"],clsName:"months",e:"changeYear",navStep:1},{names:["years","decade"],clsName:"years",e:"changeDecade",navStep:10},{names:["decades","century"],clsName:"decades",e:"changeCentury",navStep:100},{names:["centuries","millennium"],clsName:"centuries",e:"changeMillennium",navStep:1e3}],validParts:/dd?|DD?|mm?|MM?|yy(?:yy)?/g,nonpunctuation:/[^ -\/:-@\u5e74\u6708\u65e5\[-`{-~\t\n\r]+/g,parseFormat:function(t){if("function"==typeof t.toValue&&"function"==typeof t.toDisplay)return t;var e=t.replace(this.validParts,"\0").split("\0"),n=t.match(this.validParts);if(!e||!e.length||!n||0===n.length)throw new Error("Invalid date format.");return{separators:e,parts:n}},parseDate:function(n,r,o,a){function s(t,e){return e===!0&&(e=10),t<100&&(t+=2e3,t>(new Date).getFullYear()+e&&(t-=100)),t}function l(){var t=this.slice(0,u[f].length),e=u[f].slice(0,t.length);return t.toLowerCase()===e.toLowerCase()}if(!n)return e;if(n instanceof Date)return n;if("string"==typeof r&&(r=v.parseFormat(r)),r.toValue)return r.toValue(n,r,o);var u,h,d,f,p,g={d:"moveDay",m:"moveMonth",w:"moveWeek",y:"moveYear"},y={yesterday:"-1d",today:"+0d",tomorrow:"+1d"};if(n in y&&(n=y[n]),/^[\-+]\d+[dmwy]([\s,]+[\-+]\d+[dmwy])*$/i.test(n)){for(u=n.match(/([\-+]\d+)([dmwy])/gi),n=new Date,f=0;f<u.length;f++)h=u[f].match(/([\-+]\d+)([dmwy])/i),d=Number(h[1]),p=g[h[2].toLowerCase()],n=c.prototype[p](n,d);return c.prototype._zero_utc_time(n)}u=n&&n.match(this.nonpunctuation)||[];var b,_,x={},w=["yyyy","yy","M","MM","m","mm","d","dd"],D={yyyy:function(t,e){return t.setUTCFullYear(a?s(e,a):e)},m:function(t,e){if(isNaN(t))return t;for(e-=1;e<0;)e+=12;for(e%=12,t.setUTCMonth(e);t.getUTCMonth()!==e;)t.setUTCDate(t.getUTCDate()-1);return t},d:function(t,e){return t.setUTCDate(e)}};D.yy=D.yyyy,D.M=D.MM=D.mm=D.m,D.dd=D.d,n=i();var S=r.parts.slice();if(u.length!==S.length&&(S=t(S).filter(function(e,n){return t.inArray(n,w)!==-1}).toArray()),u.length===S.length){var C;for(f=0,C=S.length;f<C;f++){if(b=parseInt(u[f],10),h=S[f],isNaN(b))switch(h){case"MM":_=t(m[o].months).filter(l),b=t.inArray(_[0],m[o].months)+1;break;case"M":_=t(m[o].monthsShort).filter(l),b=t.inArray(_[0],m[o].monthsShort)+1}x[h]=b}var T,k;for(f=0;f<w.length;f++)k=w[f],k in x&&!isNaN(x[k])&&(T=new Date(n),D[k](T,x[k]),isNaN(T)||(n=T))}return n},formatDate:function(e,n,i){if(!e)return"";if("string"==typeof n&&(n=v.parseFormat(n)),n.toDisplay)return n.toDisplay(e,n,i);var r={d:e.getUTCDate(),D:m[i].daysShort[e.getUTCDay()],DD:m[i].days[e.getUTCDay()],m:e.getUTCMonth()+1,M:m[i].monthsShort[e.getUTCMonth()],MM:m[i].months[e.getUTCMonth()],yy:e.getUTCFullYear().toString().substring(2),yyyy:e.getUTCFullYear()};r.dd=(r.d<10?"0":"")+r.d,r.mm=(r.m<10?"0":"")+r.m,e=[];for(var o=t.extend([],n.separators),a=0,s=n.parts.length;a<=s;a++)o.length&&e.push(o.shift()),e.push(r[n.parts[a]]);return e.join("")},headTemplate:'<thead><tr><th colspan="7" class="datepicker-title"></th></tr><tr><th class="prev">'+p.templates.leftArrow+'</th><th colspan="5" class="datepicker-switch"></th><th class="next">'+p.templates.rightArrow+"</th></tr></thead>",contTemplate:'<tbody><tr><td colspan="7"></td></tr></tbody>',footTemplate:'<tfoot><tr><th colspan="7" class="today"></th></tr><tr><th colspan="7" class="clear"></th></tr></tfoot>'};v.template='<div class="datepicker"><div class="datepicker-days"><table class="table-condensed">'+v.headTemplate+"<tbody></tbody>"+v.footTemplate+'</table></div><div class="datepicker-months"><table class="table-condensed">'+v.headTemplate+v.contTemplate+v.footTemplate+'</table></div><div class="datepicker-years"><table class="table-condensed">'+v.headTemplate+v.contTemplate+v.footTemplate+'</table></div><div class="datepicker-decades"><table class="table-condensed">'+v.headTemplate+v.contTemplate+v.footTemplate+'</table></div><div class="datepicker-centuries"><table class="table-condensed">'+v.headTemplate+v.contTemplate+v.footTemplate+"</table></div></div>",t.fn.datepicker.DPGlobal=v,t.fn.datepicker.noConflict=function(){return t.fn.datepicker=d,this},t.fn.datepicker.version="1.9.0",t.fn.datepicker.deprecated=function(t){var e=window.console;e&&e.warn&&e.warn("DEPRECATED: "+t)},t(document).on("focus.datepicker.data-api click.datepicker.data-api",'[data-provide="datepicker"]',function(e){var n=t(this);n.data("datepicker")||(e.preventDefault(),f.call(n,"show"))}),t(function(){f.call(t('[data-provide="datepicker-inline"]'))})}),!function(t){t.fn.datepicker.dates.ru={days:["Воскресенье","Понедельник","Вторник","Среда","Четверг","Пятница","Суббота"],daysShort:["Вск","Пнд","Втр","Срд","Чтв","Птн","Суб"],daysMin:["Вс","Пн","Вт","Ср","Чт","Пт","Сб"],months:["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"],monthsShort:["Янв","Фев","Мар","Апр","Май","Июн","Июл","Авг","Сен","Окт","Ноя","Дек"],today:"Сегодня",clear:"Очистить",format:"dd.mm.yyyy",weekStart:1,monthsTitle:"Месяцы"}}(jQuery),function(t){if("object"==typeof exports&&"undefined"!=typeof module)module.exports=t();else if("function"==typeof define&&define.amd)define([],t);else{var e;e="undefined"!=typeof window?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:this,e.Chart=t()}}(function(){return function(){function t(e,n,i){function r(a,s){if(!n[a]){if(!e[a]){var l="function"==typeof require&&require;if(!s&&l)return l(a,!0);if(o)return o(a,!0);var u=new Error("Cannot find module '"+a+"'");throw u.code="MODULE_NOT_FOUND",u}var c=n[a]={exports:{}};e[a][0].call(c.exports,function(t){var n=e[a][1][t];return r(n?n:t)},c,c.exports,t,e,n,i)}return n[a].exports}for(var o="function"==typeof require&&require,a=0;a<i.length;a++)r(i[a]);return r}return t}()({1:[function(t,e,n){},{}],2:[function(t,e,n){function i(t){if(t){var e=/^#([a-fA-F0-9]{3})$/i,n=/^#([a-fA-F0-9]{6})$/i,i=/^rgba?\(\s*([+-]?\d+)\s*,\s*([+-]?\d+)\s*,\s*([+-]?\d+)\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)$/i,r=/^rgba?\(\s*([+-]?[\d\.]+)\%\s*,\s*([+-]?[\d\.]+)\%\s*,\s*([+-]?[\d\.]+)\%\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)$/i,o=/(\w+)/,a=[0,0,0],s=1,l=t.match(e);if(l){l=l[1];for(var u=0;u<a.length;u++)a[u]=parseInt(l[u]+l[u],16)}else if(l=t.match(n)){l=l[1];for(var u=0;u<a.length;u++)a[u]=parseInt(l.slice(2*u,2*u+2),16)}else if(l=t.match(i)){for(var u=0;u<a.length;u++)a[u]=parseInt(l[u+1]);s=parseFloat(l[4])}else if(l=t.match(r)){for(var u=0;u<a.length;u++)a[u]=Math.round(2.55*parseFloat(l[u+1]));s=parseFloat(l[4])}else if(l=t.match(o)){if("transparent"==l[1])return[0,0,0,0];if(a=_[l[1]],!a)return}for(var u=0;u<a.length;u++)a[u]=y(a[u],0,255);return s=s||0==s?y(s,0,1):1,a[3]=s,a}}function r(t){if(t){var e=/^hsla?\(\s*([+-]?\d+)(?:deg)?\s*,\s*([+-]?[\d\.]+)%\s*,\s*([+-]?[\d\.]+)%\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)/,n=t.match(e);if(n){var i=parseFloat(n[4]),r=y(parseInt(n[1]),0,360),o=y(parseFloat(n[2]),0,100),a=y(parseFloat(n[3]),0,100),s=y(isNaN(i)?1:i,0,1);return[r,o,a,s]}}}function o(t){if(t){var e=/^hwb\(\s*([+-]?\d+)(?:deg)?\s*,\s*([+-]?[\d\.]+)%\s*,\s*([+-]?[\d\.]+)%\s*(?:,\s*([+-]?[\d\.]+)\s*)?\)/,n=t.match(e);if(n){var i=parseFloat(n[4]),r=y(parseInt(n[1]),0,360),o=y(parseFloat(n[2]),0,100),a=y(parseFloat(n[3]),0,100),s=y(isNaN(i)?1:i,0,1);return[r,o,a,s]}}}function a(t){var e=i(t);return e&&e.slice(0,3)}function s(t){var e=r(t);return e&&e.slice(0,3)}function l(t){var e=i(t);return e?e[3]:(e=r(t))?e[3]:(e=o(t))?e[3]:void 0}function u(t){return"#"+b(t[0])+b(t[1])+b(t[2])}function c(t,e){return e<1||t[3]&&t[3]<1?h(t,e):"rgb("+t[0]+", "+t[1]+", "+t[2]+")";
}function h(t,e){return void 0===e&&(e=void 0!==t[3]?t[3]:1),"rgba("+t[0]+", "+t[1]+", "+t[2]+", "+e+")"}function d(t,e){if(e<1||t[3]&&t[3]<1)return f(t,e);var n=Math.round(t[0]/255*100),i=Math.round(t[1]/255*100),r=Math.round(t[2]/255*100);return"rgb("+n+"%, "+i+"%, "+r+"%)"}function f(t,e){var n=Math.round(t[0]/255*100),i=Math.round(t[1]/255*100),r=Math.round(t[2]/255*100);return"rgba("+n+"%, "+i+"%, "+r+"%, "+(e||t[3]||1)+")"}function p(t,e){return e<1||t[3]&&t[3]<1?g(t,e):"hsl("+t[0]+", "+t[1]+"%, "+t[2]+"%)"}function g(t,e){return void 0===e&&(e=void 0!==t[3]?t[3]:1),"hsla("+t[0]+", "+t[1]+"%, "+t[2]+"%, "+e+")"}function m(t,e){return void 0===e&&(e=void 0!==t[3]?t[3]:1),"hwb("+t[0]+", "+t[1]+"%, "+t[2]+"%"+(void 0!==e&&1!==e?", "+e:"")+")"}function v(t){return x[t.slice(0,3)]}function y(t,e,n){return Math.min(Math.max(e,t),n)}function b(t){var e=t.toString(16).toUpperCase();return e.length<2?"0"+e:e}var _=t(6);e.exports={getRgba:i,getHsla:r,getRgb:a,getHsl:s,getHwb:o,getAlpha:l,hexString:u,rgbString:c,rgbaString:h,percentString:d,percentaString:f,hslString:p,hslaString:g,hwbString:m,keyword:v};var x={};for(var w in _)x[_[w]]=w},{6:6}],3:[function(t,e,n){var i=t(5),r=t(2),o=function(t){if(t instanceof o)return t;if(!(this instanceof o))return new o(t);this.valid=!1,this.values={rgb:[0,0,0],hsl:[0,0,0],hsv:[0,0,0],hwb:[0,0,0],cmyk:[0,0,0,0],alpha:1};var e;"string"==typeof t?(e=r.getRgba(t),e?this.setValues("rgb",e):(e=r.getHsla(t))?this.setValues("hsl",e):(e=r.getHwb(t))&&this.setValues("hwb",e)):"object"==typeof t&&(e=t,void 0!==e.r||void 0!==e.red?this.setValues("rgb",e):void 0!==e.l||void 0!==e.lightness?this.setValues("hsl",e):void 0!==e.v||void 0!==e.value?this.setValues("hsv",e):void 0!==e.w||void 0!==e.whiteness?this.setValues("hwb",e):void 0===e.c&&void 0===e.cyan||this.setValues("cmyk",e))};o.prototype={isValid:function(){return this.valid},rgb:function(){return this.setSpace("rgb",arguments)},hsl:function(){return this.setSpace("hsl",arguments)},hsv:function(){return this.setSpace("hsv",arguments)},hwb:function(){return this.setSpace("hwb",arguments)},cmyk:function(){return this.setSpace("cmyk",arguments)},rgbArray:function(){return this.values.rgb},hslArray:function(){return this.values.hsl},hsvArray:function(){return this.values.hsv},hwbArray:function(){var t=this.values;return 1!==t.alpha?t.hwb.concat([t.alpha]):t.hwb},cmykArray:function(){return this.values.cmyk},rgbaArray:function(){var t=this.values;return t.rgb.concat([t.alpha])},hslaArray:function(){var t=this.values;return t.hsl.concat([t.alpha])},alpha:function(t){return void 0===t?this.values.alpha:(this.setValues("alpha",t),this)},red:function(t){return this.setChannel("rgb",0,t)},green:function(t){return this.setChannel("rgb",1,t)},blue:function(t){return this.setChannel("rgb",2,t)},hue:function(t){return t&&(t%=360,t=t<0?360+t:t),this.setChannel("hsl",0,t)},saturation:function(t){return this.setChannel("hsl",1,t)},lightness:function(t){return this.setChannel("hsl",2,t)},saturationv:function(t){return this.setChannel("hsv",1,t)},whiteness:function(t){return this.setChannel("hwb",1,t)},blackness:function(t){return this.setChannel("hwb",2,t)},value:function(t){return this.setChannel("hsv",2,t)},cyan:function(t){return this.setChannel("cmyk",0,t)},magenta:function(t){return this.setChannel("cmyk",1,t)},yellow:function(t){return this.setChannel("cmyk",2,t)},black:function(t){return this.setChannel("cmyk",3,t)},hexString:function(){return r.hexString(this.values.rgb)},rgbString:function(){return r.rgbString(this.values.rgb,this.values.alpha)},rgbaString:function(){return r.rgbaString(this.values.rgb,this.values.alpha)},percentString:function(){return r.percentString(this.values.rgb,this.values.alpha)},hslString:function(){return r.hslString(this.values.hsl,this.values.alpha)},hslaString:function(){return r.hslaString(this.values.hsl,this.values.alpha)},hwbString:function(){return r.hwbString(this.values.hwb,this.values.alpha)},keyword:function(){return r.keyword(this.values.rgb,this.values.alpha)},rgbNumber:function(){var t=this.values.rgb;return t[0]<<16|t[1]<<8|t[2]},luminosity:function(){for(var t=this.values.rgb,e=[],n=0;n<t.length;n++){var i=t[n]/255;e[n]=i<=.03928?i/12.92:Math.pow((i+.055)/1.055,2.4)}return.2126*e[0]+.7152*e[1]+.0722*e[2]},contrast:function(t){var e=this.luminosity(),n=t.luminosity();return e>n?(e+.05)/(n+.05):(n+.05)/(e+.05)},level:function(t){var e=this.contrast(t);return e>=7.1?"AAA":e>=4.5?"AA":""},dark:function(){var t=this.values.rgb,e=(299*t[0]+587*t[1]+114*t[2])/1e3;return e<128},light:function(){return!this.dark()},negate:function(){for(var t=[],e=0;e<3;e++)t[e]=255-this.values.rgb[e];return this.setValues("rgb",t),this},lighten:function(t){var e=this.values.hsl;return e[2]+=e[2]*t,this.setValues("hsl",e),this},darken:function(t){var e=this.values.hsl;return e[2]-=e[2]*t,this.setValues("hsl",e),this},saturate:function(t){var e=this.values.hsl;return e[1]+=e[1]*t,this.setValues("hsl",e),this},desaturate:function(t){var e=this.values.hsl;return e[1]-=e[1]*t,this.setValues("hsl",e),this},whiten:function(t){var e=this.values.hwb;return e[1]+=e[1]*t,this.setValues("hwb",e),this},blacken:function(t){var e=this.values.hwb;return e[2]+=e[2]*t,this.setValues("hwb",e),this},greyscale:function(){var t=this.values.rgb,e=.3*t[0]+.59*t[1]+.11*t[2];return this.setValues("rgb",[e,e,e]),this},clearer:function(t){var e=this.values.alpha;return this.setValues("alpha",e-e*t),this},opaquer:function(t){var e=this.values.alpha;return this.setValues("alpha",e+e*t),this},rotate:function(t){var e=this.values.hsl,n=(e[0]+t)%360;return e[0]=n<0?360+n:n,this.setValues("hsl",e),this},mix:function(t,e){var n=this,i=t,r=void 0===e?.5:e,o=2*r-1,a=n.alpha()-i.alpha(),s=((o*a===-1?o:(o+a)/(1+o*a))+1)/2,l=1-s;return this.rgb(s*n.red()+l*i.red(),s*n.green()+l*i.green(),s*n.blue()+l*i.blue()).alpha(n.alpha()*r+i.alpha()*(1-r))},toJSON:function(){return this.rgb()},clone:function(){var t,e,n=new o,i=this.values,r=n.values;for(var a in i)i.hasOwnProperty(a)&&(t=i[a],e={}.toString.call(t),"[object Array]"===e?r[a]=t.slice(0):"[object Number]"===e?r[a]=t:console.error("unexpected color value:",t));return n}},o.prototype.spaces={rgb:["red","green","blue"],hsl:["hue","saturation","lightness"],hsv:["hue","saturation","value"],hwb:["hue","whiteness","blackness"],cmyk:["cyan","magenta","yellow","black"]},o.prototype.maxes={rgb:[255,255,255],hsl:[360,100,100],hsv:[360,100,100],hwb:[360,100,100],cmyk:[100,100,100,100]},o.prototype.getValues=function(t){for(var e=this.values,n={},i=0;i<t.length;i++)n[t.charAt(i)]=e[t][i];return 1!==e.alpha&&(n.a=e.alpha),n},o.prototype.setValues=function(t,e){var n,r=this.values,o=this.spaces,a=this.maxes,s=1;if(this.valid=!0,"alpha"===t)s=e;else if(e.length)r[t]=e.slice(0,t.length),s=e[t.length];else if(void 0!==e[t.charAt(0)]){for(n=0;n<t.length;n++)r[t][n]=e[t.charAt(n)];s=e.a}else if(void 0!==e[o[t][0]]){var l=o[t];for(n=0;n<t.length;n++)r[t][n]=e[l[n]];s=e.alpha}if(r.alpha=Math.max(0,Math.min(1,void 0===s?r.alpha:s)),"alpha"===t)return!1;var u;for(n=0;n<t.length;n++)u=Math.max(0,Math.min(a[t][n],r[t][n])),r[t][n]=Math.round(u);for(var c in o)c!==t&&(r[c]=i[t][c](r[t]));return!0},o.prototype.setSpace=function(t,e){var n=e[0];return void 0===n?this.getValues(t):("number"==typeof n&&(n=Array.prototype.slice.call(e)),this.setValues(t,n),this)},o.prototype.setChannel=function(t,e,n){var i=this.values[t];return void 0===n?i[e]:n===i[e]?this:(i[e]=n,this.setValues(t,i),this)},"undefined"!=typeof window&&(window.Color=o),e.exports=o},{2:2,5:5}],4:[function(t,e,n){function i(t){var e,n,i,r=t[0]/255,o=t[1]/255,a=t[2]/255,s=Math.min(r,o,a),l=Math.max(r,o,a),u=l-s;return l==s?e=0:r==l?e=(o-a)/u:o==l?e=2+(a-r)/u:a==l&&(e=4+(r-o)/u),e=Math.min(60*e,360),e<0&&(e+=360),i=(s+l)/2,n=l==s?0:i<=.5?u/(l+s):u/(2-l-s),[e,100*n,100*i]}function o(t){var e,n,i,r=t[0],o=t[1],a=t[2],s=Math.min(r,o,a),l=Math.max(r,o,a),u=l-s;return n=0==l?0:u/l*1e3/10,l==s?e=0:r==l?e=(o-a)/u:o==l?e=2+(a-r)/u:a==l&&(e=4+(r-o)/u),e=Math.min(60*e,360),e<0&&(e+=360),i=l/255*1e3/10,[e,n,i]}function a(t){var e=t[0],n=t[1],r=t[2],o=i(t)[0],a=1/255*Math.min(e,Math.min(n,r)),r=1-1/255*Math.max(e,Math.max(n,r));return[o,100*a,100*r]}function s(t){var e,n,i,r,o=t[0]/255,a=t[1]/255,s=t[2]/255;return r=Math.min(1-o,1-a,1-s),e=(1-o-r)/(1-r)||0,n=(1-a-r)/(1-r)||0,i=(1-s-r)/(1-r)||0,[100*e,100*n,100*i,100*r]}function l(t){return Q[JSON.stringify(t)]}function u(t){var e=t[0]/255,n=t[1]/255,i=t[2]/255;e=e>.04045?Math.pow((e+.055)/1.055,2.4):e/12.92,n=n>.04045?Math.pow((n+.055)/1.055,2.4):n/12.92,i=i>.04045?Math.pow((i+.055)/1.055,2.4):i/12.92;var r=.4124*e+.3576*n+.1805*i,o=.2126*e+.7152*n+.0722*i,a=.0193*e+.1192*n+.9505*i;return[100*r,100*o,100*a]}function c(t){var e,n,i,r=u(t),o=r[0],a=r[1],s=r[2];return o/=95.047,a/=100,s/=108.883,o=o>.008856?Math.pow(o,1/3):7.787*o+16/116,a=a>.008856?Math.pow(a,1/3):7.787*a+16/116,s=s>.008856?Math.pow(s,1/3):7.787*s+16/116,e=116*a-16,n=500*(o-a),i=200*(a-s),[e,n,i]}function h(t){return j(c(t))}function d(t){var e,n,i,r,o,a=t[0]/360,s=t[1]/100,l=t[2]/100;if(0==s)return o=255*l,[o,o,o];n=l<.5?l*(1+s):l+s-l*s,e=2*l-n,r=[0,0,0];for(var u=0;u<3;u++)i=a+1/3*-(u-1),i<0&&i++,i>1&&i--,o=6*i<1?e+6*(n-e)*i:2*i<1?n:3*i<2?e+(n-e)*(2/3-i)*6:e,r[u]=255*o;return r}function f(t){var e,n,i=t[0],r=t[1]/100,o=t[2]/100;return 0===o?[0,0,0]:(o*=2,r*=o<=1?o:2-o,n=(o+r)/2,e=2*r/(o+r),[i,100*e,100*n])}function p(t){return a(d(t))}function m(t){return s(d(t))}function v(t){return l(d(t))}function y(t){var e=t[0]/60,n=t[1]/100,i=t[2]/100,r=Math.floor(e)%6,o=e-Math.floor(e),a=255*i*(1-n),s=255*i*(1-n*o),l=255*i*(1-n*(1-o)),i=255*i;switch(r){case 0:return[i,l,a];case 1:return[s,i,a];case 2:return[a,i,l];case 3:return[a,s,i];case 4:return[l,a,i];case 5:return[i,a,s]}}function _(t){var e,n,i=t[0],r=t[1]/100,o=t[2]/100;return n=(2-r)*o,e=r*o,e/=n<=1?n:2-n,e=e||0,n/=2,[i,100*e,100*n]}function x(t){return a(y(t))}function w(t){return s(y(t))}function D(t){return l(y(t))}function S(t){var e,n,i,o,a=t[0]/360,s=t[1]/100,l=t[2]/100,u=s+l;switch(u>1&&(s/=u,l/=u),e=Math.floor(6*a),n=1-l,i=6*a-e,0!=(1&e)&&(i=1-i),o=s+i*(n-s),e){default:case 6:case 0:r=n,g=o,b=s;break;case 1:r=o,g=n,b=s;break;case 2:r=s,g=n,b=o;break;case 3:r=s,g=o,b=n;break;case 4:r=o,g=s,b=n;break;case 5:r=n,g=s,b=o}return[255*r,255*g,255*b]}function C(t){return i(S(t))}function T(t){return o(S(t))}function k(t){return s(S(t))}function A(t){return l(S(t))}function I(t){var e,n,i,r=t[0]/100,o=t[1]/100,a=t[2]/100,s=t[3]/100;return e=1-Math.min(1,r*(1-s)+s),n=1-Math.min(1,o*(1-s)+s),i=1-Math.min(1,a*(1-s)+s),[255*e,255*n,255*i]}function E(t){return i(I(t))}function O(t){return o(I(t))}function M(t){return a(I(t))}function P(t){return l(I(t))}function N(t){var e,n,i,r=t[0]/100,o=t[1]/100,a=t[2]/100;return e=3.2406*r+o*-1.5372+a*-.4986,n=r*-.9689+1.8758*o+.0415*a,i=.0557*r+o*-.204+1.057*a,e=e>.0031308?1.055*Math.pow(e,1/2.4)-.055:e=12.92*e,n=n>.0031308?1.055*Math.pow(n,1/2.4)-.055:n=12.92*n,i=i>.0031308?1.055*Math.pow(i,1/2.4)-.055:i=12.92*i,e=Math.min(Math.max(0,e),1),n=Math.min(Math.max(0,n),1),i=Math.min(Math.max(0,i),1),[255*e,255*n,255*i]}function L(t){var e,n,i,r=t[0],o=t[1],a=t[2];return r/=95.047,o/=100,a/=108.883,r=r>.008856?Math.pow(r,1/3):7.787*r+16/116,o=o>.008856?Math.pow(o,1/3):7.787*o+16/116,a=a>.008856?Math.pow(a,1/3):7.787*a+16/116,e=116*o-16,n=500*(r-o),i=200*(o-a),[e,n,i]}function F(t){return j(L(t))}function R(t){var e,n,i,r,o=t[0],a=t[1],s=t[2];return o<=8?(n=100*o/903.3,r=7.787*(n/100)+16/116):(n=100*Math.pow((o+16)/116,3),r=Math.pow(n/100,1/3)),e=e/95.047<=.008856?e=95.047*(a/500+r-16/116)/7.787:95.047*Math.pow(a/500+r,3),i=i/108.883<=.008859?i=108.883*(r-s/200-16/116)/7.787:108.883*Math.pow(r-s/200,3),[e,n,i]}function j(t){var e,n,i,r=t[0],o=t[1],a=t[2];return e=Math.atan2(a,o),n=360*e/2/Math.PI,n<0&&(n+=360),i=Math.sqrt(o*o+a*a),[r,i,n]}function H(t){return N(R(t))}function W(t){var e,n,i,r=t[0],o=t[1],a=t[2];return i=a/360*2*Math.PI,e=o*Math.cos(i),n=o*Math.sin(i),[r,e,n]}function B(t){return R(W(t))}function V(t){return H(W(t))}function U(t){return $[t]}function q(t){return i(U(t))}function z(t){return o(U(t))}function Y(t){return a(U(t))}function G(t){return s(U(t))}function X(t){return c(U(t))}function K(t){return u(U(t))}e.exports={rgb2hsl:i,rgb2hsv:o,rgb2hwb:a,rgb2cmyk:s,rgb2keyword:l,rgb2xyz:u,rgb2lab:c,rgb2lch:h,hsl2rgb:d,hsl2hsv:f,hsl2hwb:p,hsl2cmyk:m,hsl2keyword:v,hsv2rgb:y,hsv2hsl:_,hsv2hwb:x,hsv2cmyk:w,hsv2keyword:D,hwb2rgb:S,hwb2hsl:C,hwb2hsv:T,hwb2cmyk:k,hwb2keyword:A,cmyk2rgb:I,cmyk2hsl:E,cmyk2hsv:O,cmyk2hwb:M,cmyk2keyword:P,keyword2rgb:U,keyword2hsl:q,keyword2hsv:z,keyword2hwb:Y,keyword2cmyk:G,keyword2lab:X,keyword2xyz:K,xyz2rgb:N,xyz2lab:L,xyz2lch:F,lab2xyz:R,lab2rgb:H,lab2lch:j,lch2lab:W,lch2xyz:B,lch2rgb:V};var $={aliceblue:[240,248,255],antiquewhite:[250,235,215],aqua:[0,255,255],aquamarine:[127,255,212],azure:[240,255,255],beige:[245,245,220],bisque:[255,228,196],black:[0,0,0],blanchedalmond:[255,235,205],blue:[0,0,255],blueviolet:[138,43,226],brown:[165,42,42],burlywood:[222,184,135],cadetblue:[95,158,160],chartreuse:[127,255,0],chocolate:[210,105,30],coral:[255,127,80],cornflowerblue:[100,149,237],cornsilk:[255,248,220],crimson:[220,20,60],cyan:[0,255,255],darkblue:[0,0,139],darkcyan:[0,139,139],darkgoldenrod:[184,134,11],darkgray:[169,169,169],darkgreen:[0,100,0],darkgrey:[169,169,169],darkkhaki:[189,183,107],darkmagenta:[139,0,139],darkolivegreen:[85,107,47],darkorange:[255,140,0],darkorchid:[153,50,204],darkred:[139,0,0],darksalmon:[233,150,122],darkseagreen:[143,188,143],darkslateblue:[72,61,139],darkslategray:[47,79,79],darkslategrey:[47,79,79],darkturquoise:[0,206,209],darkviolet:[148,0,211],deeppink:[255,20,147],deepskyblue:[0,191,255],dimgray:[105,105,105],dimgrey:[105,105,105],dodgerblue:[30,144,255],firebrick:[178,34,34],floralwhite:[255,250,240],forestgreen:[34,139,34],fuchsia:[255,0,255],gainsboro:[220,220,220],ghostwhite:[248,248,255],gold:[255,215,0],goldenrod:[218,165,32],gray:[128,128,128],green:[0,128,0],greenyellow:[173,255,47],grey:[128,128,128],honeydew:[240,255,240],hotpink:[255,105,180],indianred:[205,92,92],indigo:[75,0,130],ivory:[255,255,240],khaki:[240,230,140],lavender:[230,230,250],lavenderblush:[255,240,245],lawngreen:[124,252,0],lemonchiffon:[255,250,205],lightblue:[173,216,230],lightcoral:[240,128,128],lightcyan:[224,255,255],lightgoldenrodyellow:[250,250,210],lightgray:[211,211,211],lightgreen:[144,238,144],lightgrey:[211,211,211],lightpink:[255,182,193],lightsalmon:[255,160,122],lightseagreen:[32,178,170],lightskyblue:[135,206,250],lightslategray:[119,136,153],lightslategrey:[119,136,153],lightsteelblue:[176,196,222],lightyellow:[255,255,224],lime:[0,255,0],limegreen:[50,205,50],linen:[250,240,230],magenta:[255,0,255],maroon:[128,0,0],mediumaquamarine:[102,205,170],mediumblue:[0,0,205],mediumorchid:[186,85,211],mediumpurple:[147,112,219],mediumseagreen:[60,179,113],mediumslateblue:[123,104,238],mediumspringgreen:[0,250,154],mediumturquoise:[72,209,204],mediumvioletred:[199,21,133],midnightblue:[25,25,112],mintcream:[245,255,250],mistyrose:[255,228,225],moccasin:[255,228,181],navajowhite:[255,222,173],navy:[0,0,128],oldlace:[253,245,230],olive:[128,128,0],olivedrab:[107,142,35],orange:[255,165,0],orangered:[255,69,0],orchid:[218,112,214],palegoldenrod:[238,232,170],palegreen:[152,251,152],paleturquoise:[175,238,238],palevioletred:[219,112,147],papayawhip:[255,239,213],peachpuff:[255,218,185],peru:[205,133,63],pink:[255,192,203],plum:[221,160,221],powderblue:[176,224,230],purple:[128,0,128],rebeccapurple:[102,51,153],red:[255,0,0],rosybrown:[188,143,143],royalblue:[65,105,225],saddlebrown:[139,69,19],salmon:[250,128,114],sandybrown:[244,164,96],seagreen:[46,139,87],seashell:[255,245,238],sienna:[160,82,45],silver:[192,192,192],skyblue:[135,206,235],slateblue:[106,90,205],slategray:[112,128,144],slategrey:[112,128,144],snow:[255,250,250],springgreen:[0,255,127],steelblue:[70,130,180],tan:[210,180,140],teal:[0,128,128],thistle:[216,191,216],tomato:[255,99,71],turquoise:[64,224,208],violet:[238,130,238],wheat:[245,222,179],white:[255,255,255],whitesmoke:[245,245,245],yellow:[255,255,0],yellowgreen:[154,205,50]},Q={};for(var J in $)Q[JSON.stringify($[J])]=J},{}],5:[function(t,e,n){var i=t(4),r=function(){return new u};for(var o in i){r[o+"Raw"]=function(t){return function(e){return"number"==typeof e&&(e=Array.prototype.slice.call(arguments)),i[t](e)}}(o);var a=/(\w+)2(\w+)/.exec(o),s=a[1],l=a[2];r[s]=r[s]||{},r[s][l]=r[o]=function(t){return function(e){"number"==typeof e&&(e=Array.prototype.slice.call(arguments));var n=i[t](e);if("string"==typeof n||void 0===n)return n;for(var r=0;r<n.length;r++)n[r]=Math.round(n[r]);return n}}(o)}var u=function(){this.convs={}};u.prototype.routeSpace=function(t,e){var n=e[0];return void 0===n?this.getValues(t):("number"==typeof n&&(n=Array.prototype.slice.call(e)),this.setValues(t,n))},u.prototype.setValues=function(t,e){return this.space=t,this.convs={},this.convs[t]=e,this},u.prototype.getValues=function(t){var e=this.convs[t];if(!e){var n=this.space,i=this.convs[n];e=r[n][t](i),this.convs[t]=e}return e},["rgb","hsl","hsv","cmyk","keyword"].forEach(function(t){u.prototype[t]=function(e){return this.routeSpace(t,arguments)}}),e.exports=r},{4:4}],6:[function(t,e,n){"use strict";e.exports={aliceblue:[240,248,255],antiquewhite:[250,235,215],aqua:[0,255,255],aquamarine:[127,255,212],azure:[240,255,255],beige:[245,245,220],bisque:[255,228,196],black:[0,0,0],blanchedalmond:[255,235,205],blue:[0,0,255],blueviolet:[138,43,226],brown:[165,42,42],burlywood:[222,184,135],cadetblue:[95,158,160],chartreuse:[127,255,0],chocolate:[210,105,30],coral:[255,127,80],cornflowerblue:[100,149,237],cornsilk:[255,248,220],crimson:[220,20,60],cyan:[0,255,255],darkblue:[0,0,139],darkcyan:[0,139,139],darkgoldenrod:[184,134,11],darkgray:[169,169,169],darkgreen:[0,100,0],darkgrey:[169,169,169],darkkhaki:[189,183,107],darkmagenta:[139,0,139],darkolivegreen:[85,107,47],darkorange:[255,140,0],darkorchid:[153,50,204],darkred:[139,0,0],darksalmon:[233,150,122],darkseagreen:[143,188,143],darkslateblue:[72,61,139],darkslategray:[47,79,79],darkslategrey:[47,79,79],darkturquoise:[0,206,209],darkviolet:[148,0,211],deeppink:[255,20,147],deepskyblue:[0,191,255],dimgray:[105,105,105],dimgrey:[105,105,105],dodgerblue:[30,144,255],firebrick:[178,34,34],floralwhite:[255,250,240],forestgreen:[34,139,34],fuchsia:[255,0,255],gainsboro:[220,220,220],ghostwhite:[248,248,255],gold:[255,215,0],goldenrod:[218,165,32],gray:[128,128,128],green:[0,128,0],greenyellow:[173,255,47],grey:[128,128,128],honeydew:[240,255,240],hotpink:[255,105,180],indianred:[205,92,92],indigo:[75,0,130],ivory:[255,255,240],khaki:[240,230,140],lavender:[230,230,250],lavenderblush:[255,240,245],lawngreen:[124,252,0],lemonchiffon:[255,250,205],lightblue:[173,216,230],lightcoral:[240,128,128],lightcyan:[224,255,255],lightgoldenrodyellow:[250,250,210],lightgray:[211,211,211],lightgreen:[144,238,144],lightgrey:[211,211,211],lightpink:[255,182,193],lightsalmon:[255,160,122],lightseagreen:[32,178,170],lightskyblue:[135,206,250],lightslategray:[119,136,153],lightslategrey:[119,136,153],lightsteelblue:[176,196,222],lightyellow:[255,255,224],lime:[0,255,0],limegreen:[50,205,50],linen:[250,240,230],magenta:[255,0,255],maroon:[128,0,0],mediumaquamarine:[102,205,170],mediumblue:[0,0,205],mediumorchid:[186,85,211],mediumpurple:[147,112,219],mediumseagreen:[60,179,113],mediumslateblue:[123,104,238],mediumspringgreen:[0,250,154],mediumturquoise:[72,209,204],mediumvioletred:[199,21,133],midnightblue:[25,25,112],mintcream:[245,255,250],mistyrose:[255,228,225],moccasin:[255,228,181],navajowhite:[255,222,173],navy:[0,0,128],oldlace:[253,245,230],olive:[128,128,0],olivedrab:[107,142,35],orange:[255,165,0],orangered:[255,69,0],orchid:[218,112,214],palegoldenrod:[238,232,170],palegreen:[152,251,152],paleturquoise:[175,238,238],palevioletred:[219,112,147],papayawhip:[255,239,213],peachpuff:[255,218,185],peru:[205,133,63],pink:[255,192,203],plum:[221,160,221],powderblue:[176,224,230],purple:[128,0,128],rebeccapurple:[102,51,153],red:[255,0,0],rosybrown:[188,143,143],royalblue:[65,105,225],saddlebrown:[139,69,19],salmon:[250,128,114],sandybrown:[244,164,96],seagreen:[46,139,87],seashell:[255,245,238],sienna:[160,82,45],silver:[192,192,192],skyblue:[135,206,235],slateblue:[106,90,205],slategray:[112,128,144],slategrey:[112,128,144],snow:[255,250,250],springgreen:[0,255,127],steelblue:[70,130,180],tan:[210,180,140],teal:[0,128,128],thistle:[216,191,216],tomato:[255,99,71],turquoise:[64,224,208],violet:[238,130,238],wheat:[245,222,179],white:[255,255,255],whitesmoke:[245,245,245],yellow:[255,255,0],yellowgreen:[154,205,50]}},{}],7:[function(t,e,n){var i=t(29)();i.helpers=t(45),t(27)(i),i.defaults=t(25),i.Element=t(26),i.elements=t(40),i.Interaction=t(28),i.layouts=t(30),i.platform=t(48),i.plugins=t(31),i.Ticks=t(34),t(22)(i),t(23)(i),t(24)(i),t(33)(i),t(32)(i),t(35)(i),t(55)(i),t(53)(i),t(54)(i),t(56)(i),t(57)(i),t(58)(i),t(15)(i),t(16)(i),t(17)(i),t(18)(i),t(19)(i),t(20)(i),t(21)(i),t(8)(i),t(9)(i),t(10)(i),t(11)(i),t(12)(i),t(13)(i),t(14)(i);var r=t(49);for(var o in r)r.hasOwnProperty(o)&&i.plugins.register(r[o]);i.platform.initialize(),e.exports=i,"undefined"!=typeof window&&(window.Chart=i),i.Legend=r.legend._element,i.Title=r.title._element,i.pluginService=i.plugins,i.PluginBase=i.Element.extend({}),i.canvasHelpers=i.helpers.canvas,i.layoutService=i.layouts},{10:10,11:11,12:12,13:13,14:14,15:15,16:16,17:17,18:18,19:19,20:20,21:21,22:22,23:23,24:24,25:25,26:26,27:27,28:28,29:29,30:30,31:31,32:32,33:33,34:34,35:35,40:40,45:45,48:48,49:49,53:53,54:54,55:55,56:56,57:57,58:58,8:8,9:9}],8:[function(t,e,n){"use strict";e.exports=function(t){t.Bar=function(e,n){return n.type="bar",new t(e,n)}}},{}],9:[function(t,e,n){"use strict";e.exports=function(t){t.Bubble=function(e,n){return n.type="bubble",new t(e,n)}}},{}],10:[function(t,e,n){"use strict";e.exports=function(t){t.Doughnut=function(e,n){return n.type="doughnut",new t(e,n)}}},{}],11:[function(t,e,n){"use strict";e.exports=function(t){t.Line=function(e,n){return n.type="line",new t(e,n)}}},{}],12:[function(t,e,n){"use strict";e.exports=function(t){t.PolarArea=function(e,n){return n.type="polarArea",new t(e,n)}}},{}],13:[function(t,e,n){"use strict";e.exports=function(t){t.Radar=function(e,n){return n.type="radar",new t(e,n)}}},{}],14:[function(t,e,n){"use strict";e.exports=function(t){t.Scatter=function(e,n){return n.type="scatter",new t(e,n)}}},{}],15:[function(t,e,n){"use strict";function i(t,e){var n,i,r,o,a=t.isHorizontal()?t.width:t.height,s=t.getTicks();for(r=1,o=e.length;r<o;++r)a=Math.min(a,e[r]-e[r-1]);for(r=0,o=s.length;r<o;++r)i=t.getPixelForTick(r),a=r>0?Math.min(a,i-n):a,n=i;return a}function r(t,e,n){var i,r,o=n.barThickness,a=e.stackCount,s=e.pixels[t];return l.isNullOrUndef(o)?(i=e.min*n.categoryPercentage,r=n.barPercentage):(i=o*a,r=1),{chunk:i/a,ratio:r,start:s-i/2}}function o(t,e,n){var i,r,o=e.pixels,a=o[t],s=t>0?o[t-1]:null,l=t<o.length-1?o[t+1]:null,u=n.categoryPercentage;return null===s&&(s=a-(null===l?e.end-a:l-a)),null===l&&(l=a+a-s),i=a-(a-s)/2*u,r=(l-s)/2*u,{chunk:r/e.stackCount,ratio:n.barPercentage,start:i}}var a=t(25),s=t(40),l=t(45);a._set("bar",{hover:{mode:"label"},scales:{xAxes:[{type:"category",categoryPercentage:.8,barPercentage:.9,offset:!0,gridLines:{offsetGridLines:!0}}],yAxes:[{type:"linear"}]}}),a._set("horizontalBar",{hover:{mode:"index",axis:"y"},scales:{xAxes:[{type:"linear",position:"bottom"}],yAxes:[{position:"left",type:"category",categoryPercentage:.8,barPercentage:.9,offset:!0,gridLines:{offsetGridLines:!0}}]},elements:{rectangle:{borderSkipped:"left"}},tooltips:{callbacks:{title:function(t,e){var n="";return t.length>0&&(t[0].yLabel?n=t[0].yLabel:e.labels.length>0&&t[0].index<e.labels.length&&(n=e.labels[t[0].index])),n},label:function(t,e){var n=e.datasets[t.datasetIndex].label||"";return n+": "+t.xLabel}},mode:"index",axis:"y"}}),e.exports=function(t){t.controllers.bar=t.DatasetController.extend({dataElementType:s.Rectangle,initialize:function(){var e,n=this;t.DatasetController.prototype.initialize.apply(n,arguments),e=n.getMeta(),e.stack=n.getDataset().stack,e.bar=!0},update:function(t){var e,n,i=this,r=i.getMeta().data;for(i._ruler=i.getRuler(),e=0,n=r.length;e<n;++e)i.updateElement(r[e],e,t)},updateElement:function(t,e,n){var i=this,r=i.chart,o=i.getMeta(),a=i.getDataset(),s=t.custom||{},u=r.options.elements.rectangle;t._xScale=i.getScaleForId(o.xAxisID),t._yScale=i.getScaleForId(o.yAxisID),t._datasetIndex=i.index,t._index=e,t._model={datasetLabel:a.label,label:r.data.labels[e],borderSkipped:s.borderSkipped?s.borderSkipped:u.borderSkipped,backgroundColor:s.backgroundColor?s.backgroundColor:l.valueAtIndexOrDefault(a.backgroundColor,e,u.backgroundColor),borderColor:s.borderColor?s.borderColor:l.valueAtIndexOrDefault(a.borderColor,e,u.borderColor),borderWidth:s.borderWidth?s.borderWidth:l.valueAtIndexOrDefault(a.borderWidth,e,u.borderWidth)},i.updateElementGeometry(t,e,n),t.pivot()},updateElementGeometry:function(t,e,n){var i=this,r=t._model,o=i.getValueScale(),a=o.getBasePixel(),s=o.isHorizontal(),l=i._ruler||i.getRuler(),u=i.calculateBarValuePixels(i.index,e),c=i.calculateBarIndexPixels(i.index,e,l);r.horizontal=s,r.base=n?a:u.base,r.x=s?n?a:u.head:c.center,r.y=s?c.center:n?a:u.head,r.height=s?c.size:void 0,r.width=s?void 0:c.size},getValueScaleId:function(){return this.getMeta().yAxisID},getIndexScaleId:function(){return this.getMeta().xAxisID},getValueScale:function(){return this.getScaleForId(this.getValueScaleId())},getIndexScale:function(){return this.getScaleForId(this.getIndexScaleId())},_getStacks:function(t){var e,n,i=this,r=i.chart,o=i.getIndexScale(),a=o.options.stacked,s=void 0===t?r.data.datasets.length:t+1,l=[];for(e=0;e<s;++e)n=r.getDatasetMeta(e),n.bar&&r.isDatasetVisible(e)&&(a===!1||a===!0&&l.indexOf(n.stack)===-1||void 0===a&&(void 0===n.stack||l.indexOf(n.stack)===-1))&&l.push(n.stack);return l},getStackCount:function(){return this._getStacks().length},getStackIndex:function(t,e){var n=this._getStacks(t),i=void 0!==e?n.indexOf(e):-1;return i===-1?n.length-1:i},getRuler:function(){var t,e,n,r=this,o=r.getIndexScale(),a=r.getStackCount(),s=r.index,u=o.isHorizontal(),c=u?o.left:o.top,h=c+(u?o.width:o.height),d=[];for(t=0,e=r.getMeta().data.length;t<e;++t)d.push(o.getPixelForValue(null,t,s));return n=l.isNullOrUndef(o.options.barThickness)?i(o,d):-1,{min:n,pixels:d,start:c,end:h,stackCount:a,scale:o}},calculateBarValuePixels:function(t,e){var n,i,r,o,a,s,l=this,u=l.chart,c=l.getMeta(),h=l.getValueScale(),d=u.data.datasets,f=h.getRightValue(d[t].data[e]),p=h.options.stacked,g=c.stack,m=0;if(p||void 0===p&&void 0!==g)for(n=0;n<t;++n)i=u.getDatasetMeta(n),i.bar&&i.stack===g&&i.controller.getValueScaleId()===h.id&&u.isDatasetVisible(n)&&(r=h.getRightValue(d[n].data[e]),(f<0&&r<0||f>=0&&r>0)&&(m+=r));return o=h.getPixelForValue(m),a=h.getPixelForValue(m+f),s=(a-o)/2,{size:s,base:o,head:a,center:a+s/2}},calculateBarIndexPixels:function(t,e,n){var i=this,a=n.scale.options,s="flex"===a.barThickness?o(e,n,a):r(e,n,a),u=i.getStackIndex(t,i.getMeta().stack),c=s.start+s.chunk*u+s.chunk/2,h=Math.min(l.valueOrDefault(a.maxBarThickness,1/0),s.chunk*s.ratio);return{base:c-h/2,head:c+h/2,center:c,size:h}},draw:function(){var t=this,e=t.chart,n=t.getValueScale(),i=t.getMeta().data,r=t.getDataset(),o=i.length,a=0;for(l.canvas.clipArea(e.ctx,e.chartArea);a<o;++a)isNaN(n.getRightValue(r.data[a]))||i[a].draw();l.canvas.unclipArea(e.ctx)},setHoverStyle:function(t){var e=this.chart.data.datasets[t._datasetIndex],n=t._index,i=t.custom||{},r=t._model;r.backgroundColor=i.hoverBackgroundColor?i.hoverBackgroundColor:l.valueAtIndexOrDefault(e.hoverBackgroundColor,n,l.getHoverColor(r.backgroundColor)),r.borderColor=i.hoverBorderColor?i.hoverBorderColor:l.valueAtIndexOrDefault(e.hoverBorderColor,n,l.getHoverColor(r.borderColor)),r.borderWidth=i.hoverBorderWidth?i.hoverBorderWidth:l.valueAtIndexOrDefault(e.hoverBorderWidth,n,r.borderWidth)},removeHoverStyle:function(t){var e=this.chart.data.datasets[t._datasetIndex],n=t._index,i=t.custom||{},r=t._model,o=this.chart.options.elements.rectangle;r.backgroundColor=i.backgroundColor?i.backgroundColor:l.valueAtIndexOrDefault(e.backgroundColor,n,o.backgroundColor),r.borderColor=i.borderColor?i.borderColor:l.valueAtIndexOrDefault(e.borderColor,n,o.borderColor),r.borderWidth=i.borderWidth?i.borderWidth:l.valueAtIndexOrDefault(e.borderWidth,n,o.borderWidth)}}),t.controllers.horizontalBar=t.controllers.bar.extend({getValueScaleId:function(){return this.getMeta().xAxisID},getIndexScaleId:function(){return this.getMeta().yAxisID}})}},{25:25,40:40,45:45}],16:[function(t,e,n){"use strict";var i=t(25),r=t(40),o=t(45);i._set("bubble",{hover:{mode:"single"},scales:{xAxes:[{type:"linear",position:"bottom",id:"x-axis-0"}],yAxes:[{type:"linear",position:"left",id:"y-axis-0"}]},tooltips:{callbacks:{title:function(){return""},label:function(t,e){var n=e.datasets[t.datasetIndex].label||"",i=e.datasets[t.datasetIndex].data[t.index];return n+": ("+t.xLabel+", "+t.yLabel+", "+i.r+")"}}}}),e.exports=function(t){t.controllers.bubble=t.DatasetController.extend({dataElementType:r.Point,update:function(t){var e=this,n=e.getMeta(),i=n.data;o.each(i,function(n,i){e.updateElement(n,i,t)})},updateElement:function(t,e,n){var i=this,r=i.getMeta(),o=t.custom||{},a=i.getScaleForId(r.xAxisID),s=i.getScaleForId(r.yAxisID),l=i._resolveElementOptions(t,e),u=i.getDataset().data[e],c=i.index,h=n?a.getPixelForDecimal(.5):a.getPixelForValue("object"==typeof u?u:NaN,e,c),d=n?s.getBasePixel():s.getPixelForValue(u,e,c);t._xScale=a,t._yScale=s,t._options=l,t._datasetIndex=c,t._index=e,t._model={backgroundColor:l.backgroundColor,borderColor:l.borderColor,borderWidth:l.borderWidth,hitRadius:l.hitRadius,pointStyle:l.pointStyle,radius:n?0:l.radius,skip:o.skip||isNaN(h)||isNaN(d),x:h,y:d},t.pivot()},setHoverStyle:function(t){var e=t._model,n=t._options;e.backgroundColor=o.valueOrDefault(n.hoverBackgroundColor,o.getHoverColor(n.backgroundColor)),e.borderColor=o.valueOrDefault(n.hoverBorderColor,o.getHoverColor(n.borderColor)),e.borderWidth=o.valueOrDefault(n.hoverBorderWidth,n.borderWidth),e.radius=n.radius+n.hoverRadius},removeHoverStyle:function(t){var e=t._model,n=t._options;e.backgroundColor=n.backgroundColor,e.borderColor=n.borderColor,e.borderWidth=n.borderWidth,e.radius=n.radius},_resolveElementOptions:function(t,e){var n,i,r,a=this,s=a.chart,l=s.data.datasets,u=l[a.index],c=t.custom||{},h=s.options.elements.point,d=o.options.resolve,f=u.data[e],p={},g={chart:s,dataIndex:e,dataset:u,datasetIndex:a.index},m=["backgroundColor","borderColor","borderWidth","hoverBackgroundColor","hoverBorderColor","hoverBorderWidth","hoverRadius","hitRadius","pointStyle"];for(n=0,i=m.length;n<i;++n)r=m[n],p[r]=d([c[r],u[r],h[r]],g,e);return p.radius=d([c.radius,f?f.r:void 0,u.radius,h.radius],g,e),p}})}},{25:25,40:40,45:45}],17:[function(t,e,n){"use strict";var i=t(25),r=t(40),o=t(45);i._set("doughnut",{animation:{animateRotate:!0,animateScale:!1},hover:{mode:"single"},legendCallback:function(t){var e=[];e.push('<ul class="'+t.id+'-legend">');var n=t.data,i=n.datasets,r=n.labels;if(i.length)for(var o=0;o<i[0].data.length;++o)e.push('<li><span style="background-color:'+i[0].backgroundColor[o]+'"></span>'),r[o]&&e.push(r[o]),e.push("</li>");return e.push("</ul>"),e.join("")},legend:{labels:{generateLabels:function(t){var e=t.data;return e.labels.length&&e.datasets.length?e.labels.map(function(n,i){var r=t.getDatasetMeta(0),a=e.datasets[0],s=r.data[i],l=s&&s.custom||{},u=o.valueAtIndexOrDefault,c=t.options.elements.arc,h=l.backgroundColor?l.backgroundColor:u(a.backgroundColor,i,c.backgroundColor),d=l.borderColor?l.borderColor:u(a.borderColor,i,c.borderColor),f=l.borderWidth?l.borderWidth:u(a.borderWidth,i,c.borderWidth);return{text:n,fillStyle:h,strokeStyle:d,lineWidth:f,hidden:isNaN(a.data[i])||r.data[i].hidden,index:i}}):[];
}},onClick:function(t,e){var n,i,r,o=e.index,a=this.chart;for(n=0,i=(a.data.datasets||[]).length;n<i;++n)r=a.getDatasetMeta(n),r.data[o]&&(r.data[o].hidden=!r.data[o].hidden);a.update()}},cutoutPercentage:50,rotation:Math.PI*-.5,circumference:2*Math.PI,tooltips:{callbacks:{title:function(){return""},label:function(t,e){var n=e.labels[t.index],i=": "+e.datasets[t.datasetIndex].data[t.index];return o.isArray(n)?(n=n.slice(),n[0]+=i):n+=i,n}}}}),i._set("pie",o.clone(i.doughnut)),i._set("pie",{cutoutPercentage:0}),e.exports=function(t){t.controllers.doughnut=t.controllers.pie=t.DatasetController.extend({dataElementType:r.Arc,linkScales:o.noop,getRingIndex:function(t){for(var e=0,n=0;n<t;++n)this.chart.isDatasetVisible(n)&&++e;return e},update:function(t){var e=this,n=e.chart,i=n.chartArea,r=n.options,a=r.elements.arc,s=i.right-i.left-a.borderWidth,l=i.bottom-i.top-a.borderWidth,u=Math.min(s,l),c={x:0,y:0},h=e.getMeta(),d=r.cutoutPercentage,f=r.circumference;if(f<2*Math.PI){var p=r.rotation%(2*Math.PI);p+=2*Math.PI*(p>=Math.PI?-1:p<-Math.PI?1:0);var g=p+f,m={x:Math.cos(p),y:Math.sin(p)},v={x:Math.cos(g),y:Math.sin(g)},y=p<=0&&g>=0||p<=2*Math.PI&&2*Math.PI<=g,b=p<=.5*Math.PI&&.5*Math.PI<=g||p<=2.5*Math.PI&&2.5*Math.PI<=g,_=p<=-Math.PI&&-Math.PI<=g||p<=Math.PI&&Math.PI<=g,x=p<=.5*-Math.PI&&.5*-Math.PI<=g||p<=1.5*Math.PI&&1.5*Math.PI<=g,w=d/100,D={x:_?-1:Math.min(m.x*(m.x<0?1:w),v.x*(v.x<0?1:w)),y:x?-1:Math.min(m.y*(m.y<0?1:w),v.y*(v.y<0?1:w))},S={x:y?1:Math.max(m.x*(m.x>0?1:w),v.x*(v.x>0?1:w)),y:b?1:Math.max(m.y*(m.y>0?1:w),v.y*(v.y>0?1:w))},C={width:.5*(S.x-D.x),height:.5*(S.y-D.y)};u=Math.min(s/C.width,l/C.height),c={x:(S.x+D.x)*-.5,y:(S.y+D.y)*-.5}}n.borderWidth=e.getMaxBorderWidth(h.data),n.outerRadius=Math.max((u-n.borderWidth)/2,0),n.innerRadius=Math.max(d?n.outerRadius/100*d:0,0),n.radiusLength=(n.outerRadius-n.innerRadius)/n.getVisibleDatasetCount(),n.offsetX=c.x*n.outerRadius,n.offsetY=c.y*n.outerRadius,h.total=e.calculateTotal(),e.outerRadius=n.outerRadius-n.radiusLength*e.getRingIndex(e.index),e.innerRadius=Math.max(e.outerRadius-n.radiusLength,0),o.each(h.data,function(n,i){e.updateElement(n,i,t)})},updateElement:function(t,e,n){var i=this,r=i.chart,a=r.chartArea,s=r.options,l=s.animation,u=(a.left+a.right)/2,c=(a.top+a.bottom)/2,h=s.rotation,d=s.rotation,f=i.getDataset(),p=n&&l.animateRotate?0:t.hidden?0:i.calculateCircumference(f.data[e])*(s.circumference/(2*Math.PI)),g=n&&l.animateScale?0:i.innerRadius,m=n&&l.animateScale?0:i.outerRadius,v=o.valueAtIndexOrDefault;o.extend(t,{_datasetIndex:i.index,_index:e,_model:{x:u+r.offsetX,y:c+r.offsetY,startAngle:h,endAngle:d,circumference:p,outerRadius:m,innerRadius:g,label:v(f.label,e,r.data.labels[e])}});var y=t._model;this.removeHoverStyle(t),n&&l.animateRotate||(0===e?y.startAngle=s.rotation:y.startAngle=i.getMeta().data[e-1]._model.endAngle,y.endAngle=y.startAngle+y.circumference),t.pivot()},removeHoverStyle:function(e){t.DatasetController.prototype.removeHoverStyle.call(this,e,this.chart.options.elements.arc)},calculateTotal:function(){var t,e=this.getDataset(),n=this.getMeta(),i=0;return o.each(n.data,function(n,r){t=e.data[r],isNaN(t)||n.hidden||(i+=Math.abs(t))}),i},calculateCircumference:function(t){var e=this.getMeta().total;return e>0&&!isNaN(t)?2*Math.PI*(Math.abs(t)/e):0},getMaxBorderWidth:function(t){for(var e,n,i=0,r=this.index,o=t.length,a=0;a<o;a++)e=t[a]._model?t[a]._model.borderWidth:0,n=t[a]._chart?t[a]._chart.config.data.datasets[r].hoverBorderWidth:0,i=e>i?e:i,i=n>i?n:i;return i}})}},{25:25,40:40,45:45}],18:[function(t,e,n){"use strict";var i=t(25),r=t(40),o=t(45);i._set("line",{showLines:!0,spanGaps:!1,hover:{mode:"label"},scales:{xAxes:[{type:"category",id:"x-axis-0"}],yAxes:[{type:"linear",id:"y-axis-0"}]}}),e.exports=function(t){function e(t,e){return o.valueOrDefault(t.showLine,e.showLines)}t.controllers.line=t.DatasetController.extend({datasetElementType:r.Line,dataElementType:r.Point,update:function(t){var n,i,r,a=this,s=a.getMeta(),l=s.dataset,u=s.data||[],c=a.chart.options,h=c.elements.line,d=a.getScaleForId(s.yAxisID),f=a.getDataset(),p=e(f,c);for(p&&(r=l.custom||{},void 0!==f.tension&&void 0===f.lineTension&&(f.lineTension=f.tension),l._scale=d,l._datasetIndex=a.index,l._children=u,l._model={spanGaps:f.spanGaps?f.spanGaps:c.spanGaps,tension:r.tension?r.tension:o.valueOrDefault(f.lineTension,h.tension),backgroundColor:r.backgroundColor?r.backgroundColor:f.backgroundColor||h.backgroundColor,borderWidth:r.borderWidth?r.borderWidth:f.borderWidth||h.borderWidth,borderColor:r.borderColor?r.borderColor:f.borderColor||h.borderColor,borderCapStyle:r.borderCapStyle?r.borderCapStyle:f.borderCapStyle||h.borderCapStyle,borderDash:r.borderDash?r.borderDash:f.borderDash||h.borderDash,borderDashOffset:r.borderDashOffset?r.borderDashOffset:f.borderDashOffset||h.borderDashOffset,borderJoinStyle:r.borderJoinStyle?r.borderJoinStyle:f.borderJoinStyle||h.borderJoinStyle,fill:r.fill?r.fill:void 0!==f.fill?f.fill:h.fill,steppedLine:r.steppedLine?r.steppedLine:o.valueOrDefault(f.steppedLine,h.stepped),cubicInterpolationMode:r.cubicInterpolationMode?r.cubicInterpolationMode:o.valueOrDefault(f.cubicInterpolationMode,h.cubicInterpolationMode)},l.pivot()),n=0,i=u.length;n<i;++n)a.updateElement(u[n],n,t);for(p&&0!==l._model.tension&&a.updateBezierControlPoints(),n=0,i=u.length;n<i;++n)u[n].pivot()},getPointBackgroundColor:function(t,e){var n=this.chart.options.elements.point.backgroundColor,i=this.getDataset(),r=t.custom||{};return r.backgroundColor?n=r.backgroundColor:i.pointBackgroundColor?n=o.valueAtIndexOrDefault(i.pointBackgroundColor,e,n):i.backgroundColor&&(n=i.backgroundColor),n},getPointBorderColor:function(t,e){var n=this.chart.options.elements.point.borderColor,i=this.getDataset(),r=t.custom||{};return r.borderColor?n=r.borderColor:i.pointBorderColor?n=o.valueAtIndexOrDefault(i.pointBorderColor,e,n):i.borderColor&&(n=i.borderColor),n},getPointBorderWidth:function(t,e){var n=this.chart.options.elements.point.borderWidth,i=this.getDataset(),r=t.custom||{};return isNaN(r.borderWidth)?!isNaN(i.pointBorderWidth)||o.isArray(i.pointBorderWidth)?n=o.valueAtIndexOrDefault(i.pointBorderWidth,e,n):isNaN(i.borderWidth)||(n=i.borderWidth):n=r.borderWidth,n},updateElement:function(t,e,n){var i,r,a=this,s=a.getMeta(),l=t.custom||{},u=a.getDataset(),c=a.index,h=u.data[e],d=a.getScaleForId(s.yAxisID),f=a.getScaleForId(s.xAxisID),p=a.chart.options.elements.point;void 0!==u.radius&&void 0===u.pointRadius&&(u.pointRadius=u.radius),void 0!==u.hitRadius&&void 0===u.pointHitRadius&&(u.pointHitRadius=u.hitRadius),i=f.getPixelForValue("object"==typeof h?h:NaN,e,c),r=n?d.getBasePixel():a.calculatePointY(h,e,c),t._xScale=f,t._yScale=d,t._datasetIndex=c,t._index=e,t._model={x:i,y:r,skip:l.skip||isNaN(i)||isNaN(r),radius:l.radius||o.valueAtIndexOrDefault(u.pointRadius,e,p.radius),pointStyle:l.pointStyle||o.valueAtIndexOrDefault(u.pointStyle,e,p.pointStyle),backgroundColor:a.getPointBackgroundColor(t,e),borderColor:a.getPointBorderColor(t,e),borderWidth:a.getPointBorderWidth(t,e),tension:s.dataset._model?s.dataset._model.tension:0,steppedLine:!!s.dataset._model&&s.dataset._model.steppedLine,hitRadius:l.hitRadius||o.valueAtIndexOrDefault(u.pointHitRadius,e,p.hitRadius)}},calculatePointY:function(t,e,n){var i,r,o,a=this,s=a.chart,l=a.getMeta(),u=a.getScaleForId(l.yAxisID),c=0,h=0;if(u.options.stacked){for(i=0;i<n;i++)if(r=s.data.datasets[i],o=s.getDatasetMeta(i),"line"===o.type&&o.yAxisID===u.id&&s.isDatasetVisible(i)){var d=Number(u.getRightValue(r.data[e]));d<0?h+=d||0:c+=d||0}var f=Number(u.getRightValue(t));return f<0?u.getPixelForValue(h+f):u.getPixelForValue(c+f)}return u.getPixelForValue(t)},updateBezierControlPoints:function(){function t(t,e,n){return Math.max(Math.min(t,n),e)}var e,n,i,r,a,s=this,l=s.getMeta(),u=s.chart.chartArea,c=l.data||[];if(l.dataset._model.spanGaps&&(c=c.filter(function(t){return!t._model.skip})),"monotone"===l.dataset._model.cubicInterpolationMode)o.splineCurveMonotone(c);else for(e=0,n=c.length;e<n;++e)i=c[e],r=i._model,a=o.splineCurve(o.previousItem(c,e)._model,r,o.nextItem(c,e)._model,l.dataset._model.tension),r.controlPointPreviousX=a.previous.x,r.controlPointPreviousY=a.previous.y,r.controlPointNextX=a.next.x,r.controlPointNextY=a.next.y;if(s.chart.options.elements.line.capBezierPoints)for(e=0,n=c.length;e<n;++e)r=c[e]._model,r.controlPointPreviousX=t(r.controlPointPreviousX,u.left,u.right),r.controlPointPreviousY=t(r.controlPointPreviousY,u.top,u.bottom),r.controlPointNextX=t(r.controlPointNextX,u.left,u.right),r.controlPointNextY=t(r.controlPointNextY,u.top,u.bottom)},draw:function(){var t=this,n=t.chart,i=t.getMeta(),r=i.data||[],a=n.chartArea,s=r.length,l=0;for(o.canvas.clipArea(n.ctx,a),e(t.getDataset(),n.options)&&i.dataset.draw(),o.canvas.unclipArea(n.ctx);l<s;++l)r[l].draw(a)},setHoverStyle:function(t){var e=this.chart.data.datasets[t._datasetIndex],n=t._index,i=t.custom||{},r=t._model;r.radius=i.hoverRadius||o.valueAtIndexOrDefault(e.pointHoverRadius,n,this.chart.options.elements.point.hoverRadius),r.backgroundColor=i.hoverBackgroundColor||o.valueAtIndexOrDefault(e.pointHoverBackgroundColor,n,o.getHoverColor(r.backgroundColor)),r.borderColor=i.hoverBorderColor||o.valueAtIndexOrDefault(e.pointHoverBorderColor,n,o.getHoverColor(r.borderColor)),r.borderWidth=i.hoverBorderWidth||o.valueAtIndexOrDefault(e.pointHoverBorderWidth,n,r.borderWidth)},removeHoverStyle:function(t){var e=this,n=e.chart.data.datasets[t._datasetIndex],i=t._index,r=t.custom||{},a=t._model;void 0!==n.radius&&void 0===n.pointRadius&&(n.pointRadius=n.radius),a.radius=r.radius||o.valueAtIndexOrDefault(n.pointRadius,i,e.chart.options.elements.point.radius),a.backgroundColor=e.getPointBackgroundColor(t,i),a.borderColor=e.getPointBorderColor(t,i),a.borderWidth=e.getPointBorderWidth(t,i)}})}},{25:25,40:40,45:45}],19:[function(t,e,n){"use strict";var i=t(25),r=t(40),o=t(45);i._set("polarArea",{scale:{type:"radialLinear",angleLines:{display:!1},gridLines:{circular:!0},pointLabels:{display:!1},ticks:{beginAtZero:!0}},animation:{animateRotate:!0,animateScale:!0},startAngle:-.5*Math.PI,legendCallback:function(t){var e=[];e.push('<ul class="'+t.id+'-legend">');var n=t.data,i=n.datasets,r=n.labels;if(i.length)for(var o=0;o<i[0].data.length;++o)e.push('<li><span style="background-color:'+i[0].backgroundColor[o]+'"></span>'),r[o]&&e.push(r[o]),e.push("</li>");return e.push("</ul>"),e.join("")},legend:{labels:{generateLabels:function(t){var e=t.data;return e.labels.length&&e.datasets.length?e.labels.map(function(n,i){var r=t.getDatasetMeta(0),a=e.datasets[0],s=r.data[i],l=s.custom||{},u=o.valueAtIndexOrDefault,c=t.options.elements.arc,h=l.backgroundColor?l.backgroundColor:u(a.backgroundColor,i,c.backgroundColor),d=l.borderColor?l.borderColor:u(a.borderColor,i,c.borderColor),f=l.borderWidth?l.borderWidth:u(a.borderWidth,i,c.borderWidth);return{text:n,fillStyle:h,strokeStyle:d,lineWidth:f,hidden:isNaN(a.data[i])||r.data[i].hidden,index:i}}):[]}},onClick:function(t,e){var n,i,r,o=e.index,a=this.chart;for(n=0,i=(a.data.datasets||[]).length;n<i;++n)r=a.getDatasetMeta(n),r.data[o].hidden=!r.data[o].hidden;a.update()}},tooltips:{callbacks:{title:function(){return""},label:function(t,e){return e.labels[t.index]+": "+t.yLabel}}}}),e.exports=function(t){t.controllers.polarArea=t.DatasetController.extend({dataElementType:r.Arc,linkScales:o.noop,update:function(t){var e=this,n=e.chart,i=n.chartArea,r=e.getMeta(),a=n.options,s=a.elements.arc,l=Math.min(i.right-i.left,i.bottom-i.top);n.outerRadius=Math.max((l-s.borderWidth/2)/2,0),n.innerRadius=Math.max(a.cutoutPercentage?n.outerRadius/100*a.cutoutPercentage:1,0),n.radiusLength=(n.outerRadius-n.innerRadius)/n.getVisibleDatasetCount(),e.outerRadius=n.outerRadius-n.radiusLength*e.index,e.innerRadius=e.outerRadius-n.radiusLength,r.count=e.countVisibleElements(),o.each(r.data,function(n,i){e.updateElement(n,i,t)})},updateElement:function(t,e,n){for(var i=this,r=i.chart,a=i.getDataset(),s=r.options,l=s.animation,u=r.scale,c=r.data.labels,h=i.calculateCircumference(a.data[e]),d=u.xCenter,f=u.yCenter,p=0,g=i.getMeta(),m=0;m<e;++m)isNaN(a.data[m])||g.data[m].hidden||++p;var v=s.startAngle,y=t.hidden?0:u.getDistanceFromCenterForValue(a.data[e]),b=v+h*p,_=b+(t.hidden?0:h),x=l.animateScale?0:u.getDistanceFromCenterForValue(a.data[e]);o.extend(t,{_datasetIndex:i.index,_index:e,_scale:u,_model:{x:d,y:f,innerRadius:0,outerRadius:n?x:y,startAngle:n&&l.animateRotate?v:b,endAngle:n&&l.animateRotate?v:_,label:o.valueAtIndexOrDefault(c,e,c[e])}}),i.removeHoverStyle(t),t.pivot()},removeHoverStyle:function(e){t.DatasetController.prototype.removeHoverStyle.call(this,e,this.chart.options.elements.arc)},countVisibleElements:function(){var t=this.getDataset(),e=this.getMeta(),n=0;return o.each(e.data,function(e,i){isNaN(t.data[i])||e.hidden||n++}),n},calculateCircumference:function(t){var e=this.getMeta().count;return e>0&&!isNaN(t)?2*Math.PI/e:0}})}},{25:25,40:40,45:45}],20:[function(t,e,n){"use strict";var i=t(25),r=t(40),o=t(45);i._set("radar",{scale:{type:"radialLinear"},elements:{line:{tension:0}}}),e.exports=function(t){t.controllers.radar=t.DatasetController.extend({datasetElementType:r.Line,dataElementType:r.Point,linkScales:o.noop,update:function(t){var e=this,n=e.getMeta(),i=n.dataset,r=n.data,a=i.custom||{},s=e.getDataset(),l=e.chart.options.elements.line,u=e.chart.scale;void 0!==s.tension&&void 0===s.lineTension&&(s.lineTension=s.tension),o.extend(n.dataset,{_datasetIndex:e.index,_scale:u,_children:r,_loop:!0,_model:{tension:a.tension?a.tension:o.valueOrDefault(s.lineTension,l.tension),backgroundColor:a.backgroundColor?a.backgroundColor:s.backgroundColor||l.backgroundColor,borderWidth:a.borderWidth?a.borderWidth:s.borderWidth||l.borderWidth,borderColor:a.borderColor?a.borderColor:s.borderColor||l.borderColor,fill:a.fill?a.fill:void 0!==s.fill?s.fill:l.fill,borderCapStyle:a.borderCapStyle?a.borderCapStyle:s.borderCapStyle||l.borderCapStyle,borderDash:a.borderDash?a.borderDash:s.borderDash||l.borderDash,borderDashOffset:a.borderDashOffset?a.borderDashOffset:s.borderDashOffset||l.borderDashOffset,borderJoinStyle:a.borderJoinStyle?a.borderJoinStyle:s.borderJoinStyle||l.borderJoinStyle}}),n.dataset.pivot(),o.each(r,function(n,i){e.updateElement(n,i,t)},e),e.updateBezierControlPoints()},updateElement:function(t,e,n){var i=this,r=t.custom||{},a=i.getDataset(),s=i.chart.scale,l=i.chart.options.elements.point,u=s.getPointPositionForValue(e,a.data[e]);void 0!==a.radius&&void 0===a.pointRadius&&(a.pointRadius=a.radius),void 0!==a.hitRadius&&void 0===a.pointHitRadius&&(a.pointHitRadius=a.hitRadius),o.extend(t,{_datasetIndex:i.index,_index:e,_scale:s,_model:{x:n?s.xCenter:u.x,y:n?s.yCenter:u.y,tension:r.tension?r.tension:o.valueOrDefault(a.lineTension,i.chart.options.elements.line.tension),radius:r.radius?r.radius:o.valueAtIndexOrDefault(a.pointRadius,e,l.radius),backgroundColor:r.backgroundColor?r.backgroundColor:o.valueAtIndexOrDefault(a.pointBackgroundColor,e,l.backgroundColor),borderColor:r.borderColor?r.borderColor:o.valueAtIndexOrDefault(a.pointBorderColor,e,l.borderColor),borderWidth:r.borderWidth?r.borderWidth:o.valueAtIndexOrDefault(a.pointBorderWidth,e,l.borderWidth),pointStyle:r.pointStyle?r.pointStyle:o.valueAtIndexOrDefault(a.pointStyle,e,l.pointStyle),hitRadius:r.hitRadius?r.hitRadius:o.valueAtIndexOrDefault(a.pointHitRadius,e,l.hitRadius)}}),t._model.skip=r.skip?r.skip:isNaN(t._model.x)||isNaN(t._model.y)},updateBezierControlPoints:function(){var t=this.chart.chartArea,e=this.getMeta();o.each(e.data,function(n,i){var r=n._model,a=o.splineCurve(o.previousItem(e.data,i,!0)._model,r,o.nextItem(e.data,i,!0)._model,r.tension);r.controlPointPreviousX=Math.max(Math.min(a.previous.x,t.right),t.left),r.controlPointPreviousY=Math.max(Math.min(a.previous.y,t.bottom),t.top),r.controlPointNextX=Math.max(Math.min(a.next.x,t.right),t.left),r.controlPointNextY=Math.max(Math.min(a.next.y,t.bottom),t.top),n.pivot()})},setHoverStyle:function(t){var e=this.chart.data.datasets[t._datasetIndex],n=t.custom||{},i=t._index,r=t._model;r.radius=n.hoverRadius?n.hoverRadius:o.valueAtIndexOrDefault(e.pointHoverRadius,i,this.chart.options.elements.point.hoverRadius),r.backgroundColor=n.hoverBackgroundColor?n.hoverBackgroundColor:o.valueAtIndexOrDefault(e.pointHoverBackgroundColor,i,o.getHoverColor(r.backgroundColor)),r.borderColor=n.hoverBorderColor?n.hoverBorderColor:o.valueAtIndexOrDefault(e.pointHoverBorderColor,i,o.getHoverColor(r.borderColor)),r.borderWidth=n.hoverBorderWidth?n.hoverBorderWidth:o.valueAtIndexOrDefault(e.pointHoverBorderWidth,i,r.borderWidth)},removeHoverStyle:function(t){var e=this.chart.data.datasets[t._datasetIndex],n=t.custom||{},i=t._index,r=t._model,a=this.chart.options.elements.point;r.radius=n.radius?n.radius:o.valueAtIndexOrDefault(e.pointRadius,i,a.radius),r.backgroundColor=n.backgroundColor?n.backgroundColor:o.valueAtIndexOrDefault(e.pointBackgroundColor,i,a.backgroundColor),r.borderColor=n.borderColor?n.borderColor:o.valueAtIndexOrDefault(e.pointBorderColor,i,a.borderColor),r.borderWidth=n.borderWidth?n.borderWidth:o.valueAtIndexOrDefault(e.pointBorderWidth,i,a.borderWidth)}})}},{25:25,40:40,45:45}],21:[function(t,e,n){"use strict";var i=t(25);i._set("scatter",{hover:{mode:"single"},scales:{xAxes:[{id:"x-axis-1",type:"linear",position:"bottom"}],yAxes:[{id:"y-axis-1",type:"linear",position:"left"}]},showLines:!1,tooltips:{callbacks:{title:function(){return""},label:function(t){return"("+t.xLabel+", "+t.yLabel+")"}}}}),e.exports=function(t){t.controllers.scatter=t.controllers.line}},{25:25}],22:[function(t,e,n){"use strict";var i=t(25),r=t(26),o=t(45);i._set("global",{animation:{duration:1e3,easing:"easeOutQuart",onProgress:o.noop,onComplete:o.noop}}),e.exports=function(t){t.Animation=r.extend({chart:null,currentStep:0,numSteps:60,easing:"",render:null,onAnimationProgress:null,onAnimationComplete:null}),t.animationService={frameDuration:17,animations:[],dropFrames:0,request:null,addAnimation:function(t,e,n,i){var r,o,a=this.animations;for(e.chart=t,i||(t.animating=!0),r=0,o=a.length;r<o;++r)if(a[r].chart===t)return void(a[r]=e);a.push(e),1===a.length&&this.requestAnimationFrame()},cancelAnimation:function(t){var e=o.findIndex(this.animations,function(e){return e.chart===t});e!==-1&&(this.animations.splice(e,1),t.animating=!1)},requestAnimationFrame:function(){var t=this;null===t.request&&(t.request=o.requestAnimFrame.call(window,function(){t.request=null,t.startDigest()}))},startDigest:function(){var t=this,e=Date.now(),n=0;t.dropFrames>1&&(n=Math.floor(t.dropFrames),t.dropFrames=t.dropFrames%1),t.advance(1+n);var i=Date.now();t.dropFrames+=(i-e)/t.frameDuration,t.animations.length>0&&t.requestAnimationFrame()},advance:function(t){for(var e,n,i=this.animations,r=0;r<i.length;)e=i[r],n=e.chart,e.currentStep=(e.currentStep||0)+t,e.currentStep=Math.min(e.currentStep,e.numSteps),o.callback(e.render,[n,e],n),o.callback(e.onAnimationProgress,[e],n),e.currentStep>=e.numSteps?(o.callback(e.onAnimationComplete,[e],n),n.animating=!1,i.splice(r,1)):++r}},Object.defineProperty(t.Animation.prototype,"animationObject",{get:function(){return this}}),Object.defineProperty(t.Animation.prototype,"chartInstance",{get:function(){return this.chart},set:function(t){this.chart=t}})}},{25:25,26:26,45:45}],23:[function(t,e,n){"use strict";var i=t(25),r=t(45),o=t(28),a=t(30),s=t(48),l=t(31);e.exports=function(t){function e(t){t=t||{};var e=t.data=t.data||{};return e.datasets=e.datasets||[],e.labels=e.labels||[],t.options=r.configMerge(i.global,i[t.type],t.options||{}),t}function n(e){var n=e.options;r.each(e.scales,function(t){a.removeBox(e,t)}),n=r.configMerge(t.defaults.global,t.defaults[e.config.type],n),e.options=e.config.options=n,e.ensureScalesHaveIDs(),e.buildOrUpdateScales(),e.tooltip._options=n.tooltips,e.tooltip.initialize()}function u(t){return"top"===t||"bottom"===t}t.types={},t.instances={},t.controllers={},r.extend(t.prototype,{construct:function(n,i){var o=this;i=e(i);var a=s.acquireContext(n,i),l=a&&a.canvas,u=l&&l.height,c=l&&l.width;return o.id=r.uid(),o.ctx=a,o.canvas=l,o.config=i,o.width=c,o.height=u,o.aspectRatio=u?c/u:null,o.options=i.options,o._bufferedRender=!1,o.chart=o,o.controller=o,t.instances[o.id]=o,Object.defineProperty(o,"data",{get:function(){return o.config.data},set:function(t){o.config.data=t}}),a&&l?(o.initialize(),void o.update()):void console.error("Failed to create chart: can't acquire context from the given item")},initialize:function(){var t=this;return l.notify(t,"beforeInit"),r.retinaScale(t,t.options.devicePixelRatio),t.bindEvents(),t.options.responsive&&t.resize(!0),t.ensureScalesHaveIDs(),t.buildOrUpdateScales(),t.initToolTip(),l.notify(t,"afterInit"),t},clear:function(){return r.canvas.clear(this),this},stop:function(){return t.animationService.cancelAnimation(this),this},resize:function(t){var e=this,n=e.options,i=e.canvas,o=n.maintainAspectRatio&&e.aspectRatio||null,a=Math.max(0,Math.floor(r.getMaximumWidth(i))),s=Math.max(0,Math.floor(o?a/o:r.getMaximumHeight(i)));if((e.width!==a||e.height!==s)&&(i.width=e.width=a,i.height=e.height=s,i.style.width=a+"px",i.style.height=s+"px",r.retinaScale(e,n.devicePixelRatio),!t)){var u={width:a,height:s};l.notify(e,"resize",[u]),e.options.onResize&&e.options.onResize(e,u),e.stop(),e.update(e.options.responsiveAnimationDuration)}},ensureScalesHaveIDs:function(){var t=this.options,e=t.scales||{},n=t.scale;r.each(e.xAxes,function(t,e){t.id=t.id||"x-axis-"+e}),r.each(e.yAxes,function(t,e){t.id=t.id||"y-axis-"+e}),n&&(n.id=n.id||"scale")},buildOrUpdateScales:function(){var e=this,n=e.options,i=e.scales||{},o=[],a=Object.keys(i).reduce(function(t,e){return t[e]=!1,t},{});n.scales&&(o=o.concat((n.scales.xAxes||[]).map(function(t){return{options:t,dtype:"category",dposition:"bottom"}}),(n.scales.yAxes||[]).map(function(t){return{options:t,dtype:"linear",dposition:"left"}}))),n.scale&&o.push({options:n.scale,dtype:"radialLinear",isDefault:!0,dposition:"chartArea"}),r.each(o,function(n){var o=n.options,s=o.id,l=r.valueOrDefault(o.type,n.dtype);u(o.position)!==u(n.dposition)&&(o.position=n.dposition),a[s]=!0;var c=null;if(s in i&&i[s].type===l)c=i[s],c.options=o,c.ctx=e.ctx,c.chart=e;else{var h=t.scaleService.getScaleConstructor(l);if(!h)return;c=new h({id:s,type:l,options:o,ctx:e.ctx,chart:e}),i[c.id]=c}c.mergeTicksOptions(),n.isDefault&&(e.scale=c)}),r.each(a,function(t,e){t||delete i[e]}),e.scales=i,t.scaleService.addScalesToLayout(this)},buildOrUpdateControllers:function(){var e=this,n=[],i=[];return r.each(e.data.datasets,function(r,o){var a=e.getDatasetMeta(o),s=r.type||e.config.type;if(a.type&&a.type!==s&&(e.destroyDatasetMeta(o),a=e.getDatasetMeta(o)),a.type=s,n.push(a.type),a.controller)a.controller.updateIndex(o),a.controller.linkScales();else{var l=t.controllers[a.type];if(void 0===l)throw new Error('"'+a.type+'" is not a chart type.');a.controller=new l(e,o),i.push(a.controller)}},e),i},resetElements:function(){var t=this;r.each(t.data.datasets,function(e,n){t.getDatasetMeta(n).controller.reset()},t)},reset:function(){this.resetElements(),this.tooltip.initialize()},update:function(t){var e=this;if(t&&"object"==typeof t||(t={duration:t,lazy:arguments[1]}),n(e),l._invalidate(e),l.notify(e,"beforeUpdate")!==!1){e.tooltip._data=e.data;var i=e.buildOrUpdateControllers();r.each(e.data.datasets,function(t,n){e.getDatasetMeta(n).controller.buildOrUpdateElements()},e),e.updateLayout(),e.options.animation&&e.options.animation.duration&&r.each(i,function(t){t.reset()}),e.updateDatasets(),e.tooltip.initialize(),e.lastActive=[],l.notify(e,"afterUpdate"),e._bufferedRender?e._bufferedRequest={duration:t.duration,easing:t.easing,lazy:t.lazy}:e.render(t)}},updateLayout:function(){var t=this;l.notify(t,"beforeLayout")!==!1&&(a.update(this,this.width,this.height),l.notify(t,"afterScaleUpdate"),l.notify(t,"afterLayout"))},updateDatasets:function(){var t=this;if(l.notify(t,"beforeDatasetsUpdate")!==!1){for(var e=0,n=t.data.datasets.length;e<n;++e)t.updateDataset(e);l.notify(t,"afterDatasetsUpdate")}},updateDataset:function(t){var e=this,n=e.getDatasetMeta(t),i={meta:n,index:t};l.notify(e,"beforeDatasetUpdate",[i])!==!1&&(n.controller.update(),l.notify(e,"afterDatasetUpdate",[i]))},render:function(e){var n=this;e&&"object"==typeof e||(e={duration:e,lazy:arguments[1]});var i=e.duration,o=e.lazy;if(l.notify(n,"beforeRender")!==!1){var a=n.options.animation,s=function(t){l.notify(n,"afterRender"),r.callback(a&&a.onComplete,[t],n)};if(a&&("undefined"!=typeof i&&0!==i||"undefined"==typeof i&&0!==a.duration)){var u=new t.Animation({numSteps:(i||a.duration)/16.66,easing:e.easing||a.easing,render:function(t,e){var n=r.easing.effects[e.easing],i=e.currentStep,o=i/e.numSteps;t.draw(n(o),o,i)},onAnimationProgress:a.onProgress,onAnimationComplete:s});t.animationService.addAnimation(n,u,i,o)}else n.draw(),s(new t.Animation({numSteps:0,chart:n}));return n}},draw:function(t){var e=this;e.clear(),r.isNullOrUndef(t)&&(t=1),e.transition(t),l.notify(e,"beforeDraw",[t])!==!1&&(r.each(e.boxes,function(t){t.draw(e.chartArea)},e),e.scale&&e.scale.draw(),e.drawDatasets(t),e._drawTooltip(t),l.notify(e,"afterDraw",[t]))},transition:function(t){for(var e=this,n=0,i=(e.data.datasets||[]).length;n<i;++n)e.isDatasetVisible(n)&&e.getDatasetMeta(n).controller.transition(t);e.tooltip.transition(t)},drawDatasets:function(t){var e=this;if(l.notify(e,"beforeDatasetsDraw",[t])!==!1){for(var n=(e.data.datasets||[]).length-1;n>=0;--n)e.isDatasetVisible(n)&&e.drawDataset(n,t);l.notify(e,"afterDatasetsDraw",[t])}},drawDataset:function(t,e){var n=this,i=n.getDatasetMeta(t),r={meta:i,index:t,easingValue:e};l.notify(n,"beforeDatasetDraw",[r])!==!1&&(i.controller.draw(e),l.notify(n,"afterDatasetDraw",[r]))},_drawTooltip:function(t){var e=this,n=e.tooltip,i={tooltip:n,easingValue:t};l.notify(e,"beforeTooltipDraw",[i])!==!1&&(n.draw(),l.notify(e,"afterTooltipDraw",[i]))},getElementAtEvent:function(t){return o.modes.single(this,t)},getElementsAtEvent:function(t){return o.modes.label(this,t,{intersect:!0})},getElementsAtXAxis:function(t){return o.modes["x-axis"](this,t,{intersect:!0})},getElementsAtEventForMode:function(t,e,n){var i=o.modes[e];return"function"==typeof i?i(this,t,n):[]},getDatasetAtEvent:function(t){return o.modes.dataset(this,t,{intersect:!0})},getDatasetMeta:function(t){var e=this,n=e.data.datasets[t];n._meta||(n._meta={});var i=n._meta[e.id];return i||(i=n._meta[e.id]={type:null,data:[],dataset:null,controller:null,hidden:null,xAxisID:null,yAxisID:null}),i},getVisibleDatasetCount:function(){for(var t=0,e=0,n=this.data.datasets.length;e<n;++e)this.isDatasetVisible(e)&&t++;return t},isDatasetVisible:function(t){var e=this.getDatasetMeta(t);return"boolean"==typeof e.hidden?!e.hidden:!this.data.datasets[t].hidden},generateLegend:function(){return this.options.legendCallback(this)},destroyDatasetMeta:function(t){var e=this.id,n=this.data.datasets[t],i=n._meta&&n._meta[e];i&&(i.controller.destroy(),delete n._meta[e])},destroy:function(){var e,n,i=this,o=i.canvas;for(i.stop(),e=0,n=i.data.datasets.length;e<n;++e)i.destroyDatasetMeta(e);o&&(i.unbindEvents(),r.canvas.clear(i),s.releaseContext(i.ctx),i.canvas=null,i.ctx=null),l.notify(i,"destroy"),delete t.instances[i.id]},toBase64Image:function(){return this.canvas.toDataURL.apply(this.canvas,arguments)},initToolTip:function(){var e=this;e.tooltip=new t.Tooltip({_chart:e,_chartInstance:e,_data:e.data,_options:e.options.tooltips},e)},bindEvents:function(){var t=this,e=t._listeners={},n=function(){t.eventHandler.apply(t,arguments)};r.each(t.options.events,function(i){s.addEventListener(t,i,n),e[i]=n}),t.options.responsive&&(n=function(){t.resize()},s.addEventListener(t,"resize",n),e.resize=n)},unbindEvents:function(){var t=this,e=t._listeners;e&&(delete t._listeners,r.each(e,function(e,n){s.removeEventListener(t,n,e)}))},updateHoverStyle:function(t,e,n){var i,r,o,a=n?"setHoverStyle":"removeHoverStyle";for(r=0,o=t.length;r<o;++r)i=t[r],i&&this.getDatasetMeta(i._datasetIndex).controller[a](i)},eventHandler:function(t){var e=this,n=e.tooltip;if(l.notify(e,"beforeEvent",[t])!==!1){e._bufferedRender=!0,e._bufferedRequest=null;var i=e.handleEvent(t);n&&(i=n._start?n.handleEvent(t):i|n.handleEvent(t)),l.notify(e,"afterEvent",[t]);var r=e._bufferedRequest;return r?e.render(r):i&&!e.animating&&(e.stop(),e.render(e.options.hover.animationDuration,!0)),e._bufferedRender=!1,e._bufferedRequest=null,e}},handleEvent:function(t){var e=this,n=e.options||{},i=n.hover,o=!1;return e.lastActive=e.lastActive||[],"mouseout"===t.type?e.active=[]:e.active=e.getElementsAtEventForMode(t,i.mode,i),r.callback(n.onHover||n.hover.onHover,[t["native"],e.active],e),"mouseup"!==t.type&&"click"!==t.type||n.onClick&&n.onClick.call(e,t["native"],e.active),e.lastActive.length&&e.updateHoverStyle(e.lastActive,i.mode,!1),e.active.length&&i.mode&&e.updateHoverStyle(e.active,i.mode,!0),o=!r.arrayEquals(e.active,e.lastActive),e.lastActive=e.active,o}}),t.Controller=t}},{25:25,28:28,30:30,31:31,45:45,48:48}],24:[function(t,e,n){"use strict";var i=t(45);e.exports=function(t){function e(t,e){return t._chartjs?void t._chartjs.listeners.push(e):(Object.defineProperty(t,"_chartjs",{configurable:!0,enumerable:!1,value:{listeners:[e]}}),void r.forEach(function(e){var n="onData"+e.charAt(0).toUpperCase()+e.slice(1),r=t[e];Object.defineProperty(t,e,{configurable:!0,enumerable:!1,value:function(){var e=Array.prototype.slice.call(arguments),o=r.apply(this,e);return i.each(t._chartjs.listeners,function(t){"function"==typeof t[n]&&t[n].apply(t,e)}),o}})}))}function n(t,e){var n=t._chartjs;if(n){var i=n.listeners,o=i.indexOf(e);o!==-1&&i.splice(o,1),i.length>0||(r.forEach(function(e){delete t[e]}),delete t._chartjs)}}var r=["push","pop","shift","splice","unshift"];t.DatasetController=function(t,e){this.initialize(t,e)},i.extend(t.DatasetController.prototype,{datasetElementType:null,dataElementType:null,initialize:function(t,e){var n=this;n.chart=t,n.index=e,n.linkScales(),n.addElements()},updateIndex:function(t){this.index=t},linkScales:function(){var t=this,e=t.getMeta(),n=t.getDataset();null!==e.xAxisID&&e.xAxisID in t.chart.scales||(e.xAxisID=n.xAxisID||t.chart.options.scales.xAxes[0].id),null!==e.yAxisID&&e.yAxisID in t.chart.scales||(e.yAxisID=n.yAxisID||t.chart.options.scales.yAxes[0].id)},getDataset:function(){return this.chart.data.datasets[this.index]},getMeta:function(){return this.chart.getDatasetMeta(this.index)},getScaleForId:function(t){return this.chart.scales[t]},reset:function(){this.update(!0)},destroy:function(){this._data&&n(this._data,this)},createMetaDataset:function(){var t=this,e=t.datasetElementType;return e&&new e({_chart:t.chart,_datasetIndex:t.index})},createMetaData:function(t){var e=this,n=e.dataElementType;return n&&new n({_chart:e.chart,_datasetIndex:e.index,_index:t})},addElements:function(){var t,e,n=this,i=n.getMeta(),r=n.getDataset().data||[],o=i.data;for(t=0,e=r.length;t<e;++t)o[t]=o[t]||n.createMetaData(t);i.dataset=i.dataset||n.createMetaDataset()},addElementAndReset:function(t){var e=this.createMetaData(t);this.getMeta().data.splice(t,0,e),this.updateElement(e,t,!0)},buildOrUpdateElements:function(){var t=this,i=t.getDataset(),r=i.data||(i.data=[]);t._data!==r&&(t._data&&n(t._data,t),e(r,t),t._data=r),t.resyncElements()},update:i.noop,transition:function(t){for(var e=this.getMeta(),n=e.data||[],i=n.length,r=0;r<i;++r)n[r].transition(t);e.dataset&&e.dataset.transition(t)},draw:function(){var t=this.getMeta(),e=t.data||[],n=e.length,i=0;for(t.dataset&&t.dataset.draw();i<n;++i)e[i].draw()},removeHoverStyle:function(t,e){var n=this.chart.data.datasets[t._datasetIndex],r=t._index,o=t.custom||{},a=i.valueAtIndexOrDefault,s=t._model;s.backgroundColor=o.backgroundColor?o.backgroundColor:a(n.backgroundColor,r,e.backgroundColor),s.borderColor=o.borderColor?o.borderColor:a(n.borderColor,r,e.borderColor),s.borderWidth=o.borderWidth?o.borderWidth:a(n.borderWidth,r,e.borderWidth)},setHoverStyle:function(t){var e=this.chart.data.datasets[t._datasetIndex],n=t._index,r=t.custom||{},o=i.valueAtIndexOrDefault,a=i.getHoverColor,s=t._model;
s.backgroundColor=r.hoverBackgroundColor?r.hoverBackgroundColor:o(e.hoverBackgroundColor,n,a(s.backgroundColor)),s.borderColor=r.hoverBorderColor?r.hoverBorderColor:o(e.hoverBorderColor,n,a(s.borderColor)),s.borderWidth=r.hoverBorderWidth?r.hoverBorderWidth:o(e.hoverBorderWidth,n,s.borderWidth)},resyncElements:function(){var t=this,e=t.getMeta(),n=t.getDataset().data,i=e.data.length,r=n.length;r<i?e.data.splice(r,i-r):r>i&&t.insertElements(i,r-i)},insertElements:function(t,e){for(var n=0;n<e;++n)this.addElementAndReset(t+n)},onDataPush:function(){this.insertElements(this.getDataset().data.length-1,arguments.length)},onDataPop:function(){this.getMeta().data.pop()},onDataShift:function(){this.getMeta().data.shift()},onDataSplice:function(t,e){this.getMeta().data.splice(t,e),this.insertElements(t,arguments.length-2)},onDataUnshift:function(){this.insertElements(0,arguments.length)}}),t.DatasetController.extend=i.inherits}},{45:45}],25:[function(t,e,n){"use strict";var i=t(45);e.exports={_set:function(t,e){return i.merge(this[t]||(this[t]={}),e)}}},{45:45}],26:[function(t,e,n){"use strict";function i(t,e,n,i){var o,a,s,l,u,c,h,d,f,p=Object.keys(n);for(o=0,a=p.length;o<a;++o)if(s=p[o],c=n[s],e.hasOwnProperty(s)||(e[s]=c),l=e[s],l!==c&&"_"!==s[0]){if(t.hasOwnProperty(s)||(t[s]=l),u=t[s],h=typeof c,h===typeof u)if("string"===h){if(d=r(u),d.valid&&(f=r(c),f.valid)){e[s]=f.mix(d,i).rgbString();continue}}else if("number"===h&&isFinite(u)&&isFinite(c)){e[s]=u+(c-u)*i;continue}e[s]=c}}var r=t(3),o=t(45),a=function(t){o.extend(this,t),this.initialize.apply(this,arguments)};o.extend(a.prototype,{initialize:function(){this.hidden=!1},pivot:function(){var t=this;return t._view||(t._view=o.clone(t._model)),t._start={},t},transition:function(t){var e=this,n=e._model,r=e._start,o=e._view;return n&&1!==t?(o||(o=e._view={}),r||(r=e._start={}),i(r,o,n,t),e):(e._view=n,e._start=null,e)},tooltipPosition:function(){return{x:this._model.x,y:this._model.y}},hasValue:function(){return o.isNumber(this._model.x)&&o.isNumber(this._model.y)}}),a.extend=o.inherits,e.exports=a},{3:3,45:45}],27:[function(t,e,n){"use strict";var i=t(3),r=t(25),o=t(45);e.exports=function(t){function e(t,e,n){var i;return"string"==typeof t?(i=parseInt(t,10),t.indexOf("%")!==-1&&(i=i/100*e.parentNode[n])):i=t,i}function n(t){return void 0!==t&&null!==t&&"none"!==t}function a(t,i,r){var o=document.defaultView,a=t.parentNode,s=o.getComputedStyle(t)[i],l=o.getComputedStyle(a)[i],u=n(s),c=n(l),h=Number.POSITIVE_INFINITY;return u||c?Math.min(u?e(s,t,r):h,c?e(l,a,r):h):"none"}o.configMerge=function(){return o.merge(o.clone(arguments[0]),[].slice.call(arguments,1),{merger:function(e,n,i,r){var a=n[e]||{},s=i[e];"scales"===e?n[e]=o.scaleMerge(a,s):"scale"===e?n[e]=o.merge(a,[t.scaleService.getScaleDefaults(s.type),s]):o._merger(e,n,i,r)}})},o.scaleMerge=function(){return o.merge(o.clone(arguments[0]),[].slice.call(arguments,1),{merger:function(e,n,i,r){if("xAxes"===e||"yAxes"===e){var a,s,l,u=i[e].length;for(n[e]||(n[e]=[]),a=0;a<u;++a)l=i[e][a],s=o.valueOrDefault(l.type,"xAxes"===e?"category":"linear"),a>=n[e].length&&n[e].push({}),!n[e][a].type||l.type&&l.type!==n[e][a].type?o.merge(n[e][a],[t.scaleService.getScaleDefaults(s),l]):o.merge(n[e][a],l)}else o._merger(e,n,i,r)}})},o.where=function(t,e){if(o.isArray(t)&&Array.prototype.filter)return t.filter(e);var n=[];return o.each(t,function(t){e(t)&&n.push(t)}),n},o.findIndex=Array.prototype.findIndex?function(t,e,n){return t.findIndex(e,n)}:function(t,e,n){n=void 0===n?t:n;for(var i=0,r=t.length;i<r;++i)if(e.call(n,t[i],i,t))return i;return-1},o.findNextWhere=function(t,e,n){o.isNullOrUndef(n)&&(n=-1);for(var i=n+1;i<t.length;i++){var r=t[i];if(e(r))return r}},o.findPreviousWhere=function(t,e,n){o.isNullOrUndef(n)&&(n=t.length);for(var i=n-1;i>=0;i--){var r=t[i];if(e(r))return r}},o.isNumber=function(t){return!isNaN(parseFloat(t))&&isFinite(t)},o.almostEquals=function(t,e,n){return Math.abs(t-e)<n},o.almostWhole=function(t,e){var n=Math.round(t);return n-e<t&&n+e>t},o.max=function(t){return t.reduce(function(t,e){return isNaN(e)?t:Math.max(t,e)},Number.NEGATIVE_INFINITY)},o.min=function(t){return t.reduce(function(t,e){return isNaN(e)?t:Math.min(t,e)},Number.POSITIVE_INFINITY)},o.sign=Math.sign?function(t){return Math.sign(t)}:function(t){return t=+t,0===t||isNaN(t)?t:t>0?1:-1},o.log10=Math.log10?function(t){return Math.log10(t)}:function(t){var e=Math.log(t)*Math.LOG10E,n=Math.round(e),i=t===Math.pow(10,n);return i?n:e},o.toRadians=function(t){return t*(Math.PI/180)},o.toDegrees=function(t){return t*(180/Math.PI)},o.getAngleFromPoint=function(t,e){var n=e.x-t.x,i=e.y-t.y,r=Math.sqrt(n*n+i*i),o=Math.atan2(i,n);return o<-.5*Math.PI&&(o+=2*Math.PI),{angle:o,distance:r}},o.distanceBetweenPoints=function(t,e){return Math.sqrt(Math.pow(e.x-t.x,2)+Math.pow(e.y-t.y,2))},o.aliasPixel=function(t){return t%2===0?0:.5},o.splineCurve=function(t,e,n,i){var r=t.skip?e:t,o=e,a=n.skip?e:n,s=Math.sqrt(Math.pow(o.x-r.x,2)+Math.pow(o.y-r.y,2)),l=Math.sqrt(Math.pow(a.x-o.x,2)+Math.pow(a.y-o.y,2)),u=s/(s+l),c=l/(s+l);u=isNaN(u)?0:u,c=isNaN(c)?0:c;var h=i*u,d=i*c;return{previous:{x:o.x-h*(a.x-r.x),y:o.y-h*(a.y-r.y)},next:{x:o.x+d*(a.x-r.x),y:o.y+d*(a.y-r.y)}}},o.EPSILON=Number.EPSILON||1e-14,o.splineCurveMonotone=function(t){var e,n,i,r,a=(t||[]).map(function(t){return{model:t._model,deltaK:0,mK:0}}),s=a.length;for(e=0;e<s;++e)if(i=a[e],!i.model.skip){if(n=e>0?a[e-1]:null,r=e<s-1?a[e+1]:null,r&&!r.model.skip){var l=r.model.x-i.model.x;i.deltaK=0!==l?(r.model.y-i.model.y)/l:0}!n||n.model.skip?i.mK=i.deltaK:!r||r.model.skip?i.mK=n.deltaK:this.sign(n.deltaK)!==this.sign(i.deltaK)?i.mK=0:i.mK=(n.deltaK+i.deltaK)/2}var u,c,h,d;for(e=0;e<s-1;++e)i=a[e],r=a[e+1],i.model.skip||r.model.skip||(o.almostEquals(i.deltaK,0,this.EPSILON)?i.mK=r.mK=0:(u=i.mK/i.deltaK,c=r.mK/i.deltaK,d=Math.pow(u,2)+Math.pow(c,2),d<=9||(h=3/Math.sqrt(d),i.mK=u*h*i.deltaK,r.mK=c*h*i.deltaK)));var f;for(e=0;e<s;++e)i=a[e],i.model.skip||(n=e>0?a[e-1]:null,r=e<s-1?a[e+1]:null,n&&!n.model.skip&&(f=(i.model.x-n.model.x)/3,i.model.controlPointPreviousX=i.model.x-f,i.model.controlPointPreviousY=i.model.y-f*i.mK),r&&!r.model.skip&&(f=(r.model.x-i.model.x)/3,i.model.controlPointNextX=i.model.x+f,i.model.controlPointNextY=i.model.y+f*i.mK))},o.nextItem=function(t,e,n){return n?e>=t.length-1?t[0]:t[e+1]:e>=t.length-1?t[t.length-1]:t[e+1]},o.previousItem=function(t,e,n){return n?e<=0?t[t.length-1]:t[e-1]:e<=0?t[0]:t[e-1]},o.niceNum=function(t,e){var n,i=Math.floor(o.log10(t)),r=t/Math.pow(10,i);return n=e?r<1.5?1:r<3?2:r<7?5:10:r<=1?1:r<=2?2:r<=5?5:10,n*Math.pow(10,i)},o.requestAnimFrame=function(){return"undefined"==typeof window?function(t){t()}:window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.oRequestAnimationFrame||window.msRequestAnimationFrame||function(t){return window.setTimeout(t,1e3/60)}}(),o.getRelativePosition=function(t,e){var n,i,r=t.originalEvent||t,a=t.currentTarget||t.srcElement,s=a.getBoundingClientRect(),l=r.touches;l&&l.length>0?(n=l[0].clientX,i=l[0].clientY):(n=r.clientX,i=r.clientY);var u=parseFloat(o.getStyle(a,"padding-left")),c=parseFloat(o.getStyle(a,"padding-top")),h=parseFloat(o.getStyle(a,"padding-right")),d=parseFloat(o.getStyle(a,"padding-bottom")),f=s.right-s.left-u-h,p=s.bottom-s.top-c-d;return n=Math.round((n-s.left-u)/f*a.width/e.currentDevicePixelRatio),i=Math.round((i-s.top-c)/p*a.height/e.currentDevicePixelRatio),{x:n,y:i}},o.getConstraintWidth=function(t){return a(t,"max-width","clientWidth")},o.getConstraintHeight=function(t){return a(t,"max-height","clientHeight")},o.getMaximumWidth=function(t){var e=t.parentNode;if(!e)return t.clientWidth;var n=parseInt(o.getStyle(e,"padding-left"),10),i=parseInt(o.getStyle(e,"padding-right"),10),r=e.clientWidth-n-i,a=o.getConstraintWidth(t);return isNaN(a)?r:Math.min(r,a)},o.getMaximumHeight=function(t){var e=t.parentNode;if(!e)return t.clientHeight;var n=parseInt(o.getStyle(e,"padding-top"),10),i=parseInt(o.getStyle(e,"padding-bottom"),10),r=e.clientHeight-n-i,a=o.getConstraintHeight(t);return isNaN(a)?r:Math.min(r,a)},o.getStyle=function(t,e){return t.currentStyle?t.currentStyle[e]:document.defaultView.getComputedStyle(t,null).getPropertyValue(e)},o.retinaScale=function(t,e){var n=t.currentDevicePixelRatio=e||window.devicePixelRatio||1;if(1!==n){var i=t.canvas,r=t.height,o=t.width;i.height=r*n,i.width=o*n,t.ctx.scale(n,n),i.style.height||i.style.width||(i.style.height=r+"px",i.style.width=o+"px")}},o.fontString=function(t,e,n){return e+" "+t+"px "+n},o.longestText=function(t,e,n,i){i=i||{};var r=i.data=i.data||{},a=i.garbageCollect=i.garbageCollect||[];i.font!==e&&(r=i.data={},a=i.garbageCollect=[],i.font=e),t.font=e;var s=0;o.each(n,function(e){void 0!==e&&null!==e&&o.isArray(e)!==!0?s=o.measureText(t,r,a,s,e):o.isArray(e)&&o.each(e,function(e){void 0===e||null===e||o.isArray(e)||(s=o.measureText(t,r,a,s,e))})});var l=a.length/2;if(l>n.length){for(var u=0;u<l;u++)delete r[a[u]];a.splice(0,l)}return s},o.measureText=function(t,e,n,i,r){var o=e[r];return o||(o=e[r]=t.measureText(r).width,n.push(r)),o>i&&(i=o),i},o.numberOfLabelLines=function(t){var e=1;return o.each(t,function(t){o.isArray(t)&&t.length>e&&(e=t.length)}),e},o.color=i?function(t){return t instanceof CanvasGradient&&(t=r.global.defaultColor),i(t)}:function(t){return console.error("Color.js not found!"),t},o.getHoverColor=function(t){return t instanceof CanvasPattern?t:o.color(t).saturate(.5).darken(.1).rgbString()}}},{25:25,3:3,45:45}],28:[function(t,e,n){"use strict";function i(t,e){return t["native"]?{x:t.x,y:t.y}:u.getRelativePosition(t,e)}function r(t,e){var n,i,r,o,a,s=t.data.datasets;for(i=0,o=s.length;i<o;++i)if(t.isDatasetVisible(i))for(n=t.getDatasetMeta(i),r=0,a=n.data.length;r<a;++r){var l=n.data[r];l._view.skip||e(l)}}function o(t,e){var n=[];return r(t,function(t){t.inRange(e.x,e.y)&&n.push(t)}),n}function a(t,e,n,i){var o=Number.POSITIVE_INFINITY,a=[];return r(t,function(t){if(!n||t.inRange(e.x,e.y)){var r=t.getCenterPoint(),s=i(e,r);s<o?(a=[t],o=s):s===o&&a.push(t)}}),a}function s(t){var e=t.indexOf("x")!==-1,n=t.indexOf("y")!==-1;return function(t,i){var r=e?Math.abs(t.x-i.x):0,o=n?Math.abs(t.y-i.y):0;return Math.sqrt(Math.pow(r,2)+Math.pow(o,2))}}function l(t,e,n){var r=i(e,t);n.axis=n.axis||"x";var l=s(n.axis),u=n.intersect?o(t,r):a(t,r,!1,l),c=[];return u.length?(t.data.datasets.forEach(function(e,n){if(t.isDatasetVisible(n)){var i=t.getDatasetMeta(n),r=i.data[u[0]._index];r&&!r._view.skip&&c.push(r)}}),c):[]}var u=t(45);e.exports={modes:{single:function(t,e){var n=i(e,t),o=[];return r(t,function(t){if(t.inRange(n.x,n.y))return o.push(t),o}),o.slice(0,1)},label:l,index:l,dataset:function(t,e,n){var r=i(e,t);n.axis=n.axis||"xy";var l=s(n.axis),u=n.intersect?o(t,r):a(t,r,!1,l);return u.length>0&&(u=t.getDatasetMeta(u[0]._datasetIndex).data),u},"x-axis":function(t,e){return l(t,e,{intersect:!1})},point:function(t,e){var n=i(e,t);return o(t,n)},nearest:function(t,e,n){var r=i(e,t);n.axis=n.axis||"xy";var o=s(n.axis),l=a(t,r,n.intersect,o);return l.length>1&&l.sort(function(t,e){var n=t.getArea(),i=e.getArea(),r=n-i;return 0===r&&(r=t._datasetIndex-e._datasetIndex),r}),l.slice(0,1)},x:function(t,e,n){var o=i(e,t),a=[],s=!1;return r(t,function(t){t.inXRange(o.x)&&a.push(t),t.inRange(o.x,o.y)&&(s=!0)}),n.intersect&&!s&&(a=[]),a},y:function(t,e,n){var o=i(e,t),a=[],s=!1;return r(t,function(t){t.inYRange(o.y)&&a.push(t),t.inRange(o.x,o.y)&&(s=!0)}),n.intersect&&!s&&(a=[]),a}}}},{45:45}],29:[function(t,e,n){"use strict";var i=t(25);i._set("global",{responsive:!0,responsiveAnimationDuration:0,maintainAspectRatio:!0,events:["mousemove","mouseout","click","touchstart","touchmove"],hover:{onHover:null,mode:"nearest",intersect:!0,animationDuration:400},onClick:null,defaultColor:"rgba(0,0,0,0.1)",defaultFontColor:"#666",defaultFontFamily:"'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",defaultFontSize:12,defaultFontStyle:"normal",showLines:!0,elements:{},layout:{padding:{top:0,right:0,bottom:0,left:0}}}),e.exports=function(){var t=function(t,e){return this.construct(t,e),this};return t.Chart=t,t}},{25:25}],30:[function(t,e,n){"use strict";function i(t,e){return o.where(t,function(t){return t.position===e})}function r(t,e){t.forEach(function(t,e){return t._tmpIndex_=e,t}),t.sort(function(t,n){var i=e?n:t,r=e?t:n;return i.weight===r.weight?i._tmpIndex_-r._tmpIndex_:i.weight-r.weight}),t.forEach(function(t){delete t._tmpIndex_})}var o=t(45);e.exports={defaults:{},addBox:function(t,e){t.boxes||(t.boxes=[]),e.fullWidth=e.fullWidth||!1,e.position=e.position||"top",e.weight=e.weight||0,t.boxes.push(e)},removeBox:function(t,e){var n=t.boxes?t.boxes.indexOf(e):-1;n!==-1&&t.boxes.splice(n,1)},configure:function(t,e,n){for(var i,r=["fullWidth","position","weight"],o=r.length,a=0;a<o;++a)i=r[a],n.hasOwnProperty(i)&&(e[i]=n[i])},update:function(t,e,n){function a(t){var e,n=t.isHorizontal();n?(e=t.update(t.fullWidth?x:k,T),A-=e.height):(e=t.update(C,A),k-=e.width),I.push({horizontal:n,minSize:e,box:t})}function s(t){var e=o.findNextWhere(I,function(e){return e.box===t});if(e)if(t.isHorizontal()){var n={left:Math.max(N,E),right:Math.max(L,O),top:0,bottom:0};t.update(t.fullWidth?x:k,w/2,n)}else t.update(e.minSize.width,A)}function l(t){var e=o.findNextWhere(I,function(e){return e.box===t}),n={left:0,right:0,top:F,bottom:R};e&&t.update(e.minSize.width,A,n)}function u(t){t.isHorizontal()?(t.left=t.fullWidth?d:N,t.right=t.fullWidth?e-f:N+k,t.top=U,t.bottom=U+t.height,U=t.bottom):(t.left=V,t.right=V+t.width,t.top=F,t.bottom=F+A,V=t.right)}if(t){var c=t.options.layout||{},h=o.options.toPadding(c.padding),d=h.left,f=h.right,p=h.top,g=h.bottom,m=i(t.boxes,"left"),v=i(t.boxes,"right"),y=i(t.boxes,"top"),b=i(t.boxes,"bottom"),_=i(t.boxes,"chartArea");r(m,!0),r(v,!1),r(y,!0),r(b,!1);var x=e-d-f,w=n-p-g,D=x/2,S=w/2,C=(e-D)/(m.length+v.length),T=(n-S)/(y.length+b.length),k=x,A=w,I=[];o.each(m.concat(v,y,b),a);var E=0,O=0,M=0,P=0;o.each(y.concat(b),function(t){if(t.getPadding){var e=t.getPadding();E=Math.max(E,e.left),O=Math.max(O,e.right)}}),o.each(m.concat(v),function(t){if(t.getPadding){var e=t.getPadding();M=Math.max(M,e.top),P=Math.max(P,e.bottom)}});var N=d,L=f,F=p,R=g;o.each(m.concat(v),s),o.each(m,function(t){N+=t.width}),o.each(v,function(t){L+=t.width}),o.each(y.concat(b),s),o.each(y,function(t){F+=t.height}),o.each(b,function(t){R+=t.height}),o.each(m.concat(v),l),N=d,L=f,F=p,R=g,o.each(m,function(t){N+=t.width}),o.each(v,function(t){L+=t.width}),o.each(y,function(t){F+=t.height}),o.each(b,function(t){R+=t.height});var j=Math.max(E-N,0);N+=j,L+=Math.max(O-L,0);var H=Math.max(M-F,0);F+=H,R+=Math.max(P-R,0);var W=n-F-R,B=e-N-L;B===k&&W===A||(o.each(m,function(t){t.height=W}),o.each(v,function(t){t.height=W}),o.each(y,function(t){t.fullWidth||(t.width=B)}),o.each(b,function(t){t.fullWidth||(t.width=B)}),A=W,k=B);var V=d+j,U=p+H;o.each(m.concat(y),u),V+=k,U+=A,o.each(v,u),o.each(b,u),t.chartArea={left:N,top:F,right:N+k,bottom:F+A},o.each(_,function(e){e.left=t.chartArea.left,e.top=t.chartArea.top,e.right=t.chartArea.right,e.bottom=t.chartArea.bottom,e.update(k,A)})}}}},{45:45}],31:[function(t,e,n){"use strict";var i=t(25),r=t(45);i._set("global",{plugins:{}}),e.exports={_plugins:[],_cacheId:0,register:function(t){var e=this._plugins;[].concat(t).forEach(function(t){e.indexOf(t)===-1&&e.push(t)}),this._cacheId++},unregister:function(t){var e=this._plugins;[].concat(t).forEach(function(t){var n=e.indexOf(t);n!==-1&&e.splice(n,1)}),this._cacheId++},clear:function(){this._plugins=[],this._cacheId++},count:function(){return this._plugins.length},getAll:function(){return this._plugins},notify:function(t,e,n){var i,r,o,a,s,l=this.descriptors(t),u=l.length;for(i=0;i<u;++i)if(r=l[i],o=r.plugin,s=o[e],"function"==typeof s&&(a=[t].concat(n||[]),a.push(r.options),s.apply(o,a)===!1))return!1;return!0},descriptors:function(t){var e=t.$plugins||(t.$plugins={});if(e.id===this._cacheId)return e.descriptors;var n=[],o=[],a=t&&t.config||{},s=a.options&&a.options.plugins||{};return this._plugins.concat(a.plugins||[]).forEach(function(t){var e=n.indexOf(t);if(e===-1){var a=t.id,l=s[a];l!==!1&&(l===!0&&(l=r.clone(i.global.plugins[a])),n.push(t),o.push({plugin:t,options:l||{}}))}}),e.descriptors=o,e.id=this._cacheId,o},_invalidate:function(t){delete t.$plugins}}},{25:25,45:45}],32:[function(t,e,n){"use strict";function i(t){var e,n,i=[];for(e=0,n=t.length;e<n;++e)i.push(t[e].label);return i}function r(t,e,n){var i=t.getPixelForTick(e);return n&&(i-=0===e?(t.getPixelForTick(1)-i)/2:(i-t.getPixelForTick(e-1))/2),i}var o=t(25),a=t(26),s=t(45),l=t(34);o._set("scale",{display:!0,position:"left",offset:!1,gridLines:{display:!0,color:"rgba(0, 0, 0, 0.1)",lineWidth:1,drawBorder:!0,drawOnChartArea:!0,drawTicks:!0,tickMarkLength:10,zeroLineWidth:1,zeroLineColor:"rgba(0,0,0,0.25)",zeroLineBorderDash:[],zeroLineBorderDashOffset:0,offsetGridLines:!1,borderDash:[],borderDashOffset:0},scaleLabel:{display:!1,labelString:"",lineHeight:1.2,padding:{top:4,bottom:4}},ticks:{beginAtZero:!1,minRotation:0,maxRotation:50,mirror:!1,padding:0,reverse:!1,display:!0,autoSkip:!0,autoSkipPadding:0,labelOffset:0,callback:l.formatters.values,minor:{},major:{}}}),e.exports=function(t){function e(t,e,n){return s.isArray(e)?s.longestText(t,n,e):t.measureText(e).width}function n(t){var e=s.valueOrDefault,n=o.global,i=e(t.fontSize,n.defaultFontSize),r=e(t.fontStyle,n.defaultFontStyle),a=e(t.fontFamily,n.defaultFontFamily);return{size:i,style:r,family:a,font:s.fontString(i,r,a)}}function l(t){return s.options.toLineHeight(s.valueOrDefault(t.lineHeight,1.2),s.valueOrDefault(t.fontSize,o.global.defaultFontSize))}t.Scale=a.extend({getPadding:function(){var t=this;return{left:t.paddingLeft||0,top:t.paddingTop||0,right:t.paddingRight||0,bottom:t.paddingBottom||0}},getTicks:function(){return this._ticks},mergeTicksOptions:function(){var t=this.options.ticks;t.minor===!1&&(t.minor={display:!1}),t.major===!1&&(t.major={display:!1});for(var e in t)"major"!==e&&"minor"!==e&&("undefined"==typeof t.minor[e]&&(t.minor[e]=t[e]),"undefined"==typeof t.major[e]&&(t.major[e]=t[e]))},beforeUpdate:function(){s.callback(this.options.beforeUpdate,[this])},update:function(t,e,n){var i,r,o,a,l,u,c=this;for(c.beforeUpdate(),c.maxWidth=t,c.maxHeight=e,c.margins=s.extend({left:0,right:0,top:0,bottom:0},n),c.longestTextCache=c.longestTextCache||{},c.beforeSetDimensions(),c.setDimensions(),c.afterSetDimensions(),c.beforeDataLimits(),c.determineDataLimits(),c.afterDataLimits(),c.beforeBuildTicks(),l=c.buildTicks()||[],c.afterBuildTicks(),c.beforeTickToLabelConversion(),o=c.convertTicksToLabels(l)||c.ticks,c.afterTickToLabelConversion(),c.ticks=o,i=0,r=o.length;i<r;++i)a=o[i],u=l[i],u?u.label=a:l.push(u={label:a,major:!1});return c._ticks=l,c.beforeCalculateTickRotation(),c.calculateTickRotation(),c.afterCalculateTickRotation(),c.beforeFit(),c.fit(),c.afterFit(),c.afterUpdate(),c.minSize},afterUpdate:function(){s.callback(this.options.afterUpdate,[this])},beforeSetDimensions:function(){s.callback(this.options.beforeSetDimensions,[this])},setDimensions:function(){var t=this;t.isHorizontal()?(t.width=t.maxWidth,t.left=0,t.right=t.width):(t.height=t.maxHeight,t.top=0,t.bottom=t.height),t.paddingLeft=0,t.paddingTop=0,t.paddingRight=0,t.paddingBottom=0},afterSetDimensions:function(){s.callback(this.options.afterSetDimensions,[this])},beforeDataLimits:function(){s.callback(this.options.beforeDataLimits,[this])},determineDataLimits:s.noop,afterDataLimits:function(){s.callback(this.options.afterDataLimits,[this])},beforeBuildTicks:function(){s.callback(this.options.beforeBuildTicks,[this])},buildTicks:s.noop,afterBuildTicks:function(){s.callback(this.options.afterBuildTicks,[this])},beforeTickToLabelConversion:function(){s.callback(this.options.beforeTickToLabelConversion,[this])},convertTicksToLabels:function(){var t=this,e=t.options.ticks;t.ticks=t.ticks.map(e.userCallback||e.callback,this)},afterTickToLabelConversion:function(){s.callback(this.options.afterTickToLabelConversion,[this])},beforeCalculateTickRotation:function(){s.callback(this.options.beforeCalculateTickRotation,[this])},calculateTickRotation:function(){var t=this,e=t.ctx,r=t.options.ticks,o=i(t._ticks),a=n(r);e.font=a.font;var l=r.minRotation||0;if(o.length&&t.options.display&&t.isHorizontal())for(var u,c,h=s.longestText(e,a.font,o,t.longestTextCache),d=h,f=t.getPixelForTick(1)-t.getPixelForTick(0)-6;d>f&&l<r.maxRotation;){var p=s.toRadians(l);if(u=Math.cos(p),c=Math.sin(p),c*h>t.maxHeight){l--;break}l++,d=u*h}t.labelRotation=l},afterCalculateTickRotation:function(){s.callback(this.options.afterCalculateTickRotation,[this])},beforeFit:function(){s.callback(this.options.beforeFit,[this])},fit:function(){var t=this,r=t.minSize={width:0,height:0},o=i(t._ticks),a=t.options,u=a.ticks,c=a.scaleLabel,h=a.gridLines,d=a.display,f=t.isHorizontal(),p=n(u),g=a.gridLines.tickMarkLength;if(f?r.width=t.isFullWidth()?t.maxWidth-t.margins.left-t.margins.right:t.maxWidth:r.width=d&&h.drawTicks?g:0,f?r.height=d&&h.drawTicks?g:0:r.height=t.maxHeight,c.display&&d){var m=l(c),v=s.options.toPadding(c.padding),y=m+v.height;f?r.height+=y:r.width+=y}if(u.display&&d){var b=s.longestText(t.ctx,p.font,o,t.longestTextCache),_=s.numberOfLabelLines(o),x=.5*p.size,w=t.options.ticks.padding;if(f){t.longestLabelWidth=b;var D=s.toRadians(t.labelRotation),S=Math.cos(D),C=Math.sin(D),T=C*b+p.size*_+x*(_-1)+x;r.height=Math.min(t.maxHeight,r.height+T+w),t.ctx.font=p.font;var k=e(t.ctx,o[0],p.font),A=e(t.ctx,o[o.length-1],p.font);0!==t.labelRotation?(t.paddingLeft="bottom"===a.position?S*k+3:S*x+3,t.paddingRight="bottom"===a.position?S*x+3:S*A+3):(t.paddingLeft=k/2+3,t.paddingRight=A/2+3)}else u.mirror?b=0:b+=w+x,r.width=Math.min(t.maxWidth,r.width+b),t.paddingTop=p.size/2,t.paddingBottom=p.size/2}t.handleMargins(),t.width=r.width,t.height=r.height},handleMargins:function(){var t=this;t.margins&&(t.paddingLeft=Math.max(t.paddingLeft-t.margins.left,0),t.paddingTop=Math.max(t.paddingTop-t.margins.top,0),t.paddingRight=Math.max(t.paddingRight-t.margins.right,0),t.paddingBottom=Math.max(t.paddingBottom-t.margins.bottom,0))},afterFit:function(){s.callback(this.options.afterFit,[this])},isHorizontal:function(){return"top"===this.options.position||"bottom"===this.options.position},isFullWidth:function(){return this.options.fullWidth},getRightValue:function(t){if(s.isNullOrUndef(t))return NaN;if("number"==typeof t&&!isFinite(t))return NaN;if(t)if(this.isHorizontal()){if(void 0!==t.x)return this.getRightValue(t.x)}else if(void 0!==t.y)return this.getRightValue(t.y);return t},getLabelForIndex:s.noop,getPixelForValue:s.noop,getValueForPixel:s.noop,getPixelForTick:function(t){var e=this,n=e.options.offset;if(e.isHorizontal()){var i=e.width-(e.paddingLeft+e.paddingRight),r=i/Math.max(e._ticks.length-(n?0:1),1),o=r*t+e.paddingLeft;n&&(o+=r/2);var a=e.left+Math.round(o);return a+=e.isFullWidth()?e.margins.left:0}var s=e.height-(e.paddingTop+e.paddingBottom);return e.top+t*(s/(e._ticks.length-1))},getPixelForDecimal:function(t){var e=this;if(e.isHorizontal()){var n=e.width-(e.paddingLeft+e.paddingRight),i=n*t+e.paddingLeft,r=e.left+Math.round(i);return r+=e.isFullWidth()?e.margins.left:0}return e.top+t*e.height},getBasePixel:function(){return this.getPixelForValue(this.getBaseValue())},getBaseValue:function(){var t=this,e=t.min,n=t.max;return t.beginAtZero?0:e<0&&n<0?n:e>0&&n>0?e:0},_autoSkip:function(t){var e,n,i,r,o,a=this,l=a.isHorizontal(),u=a.options.ticks.minor,c=t.length,h=s.toRadians(a.labelRotation),d=Math.cos(h),f=a.longestLabelWidth*d,p=[];for(u.maxTicksLimit&&(o=u.maxTicksLimit),l&&(e=!1,(f+u.autoSkipPadding)*c>a.width-(a.paddingLeft+a.paddingRight)&&(e=1+Math.floor((f+u.autoSkipPadding)*c/(a.width-(a.paddingLeft+a.paddingRight)))),o&&c>o&&(e=Math.max(e,Math.floor(c/o)))),n=0;n<c;n++)i=t[n],r=e>1&&n%e>0||n%e===0&&n+e>=c,r&&n!==c-1&&delete i.label,p.push(i);return p},draw:function(t){var e=this,i=e.options;if(i.display){var a=e.ctx,u=o.global,c=i.ticks.minor,h=i.ticks.major||c,d=i.gridLines,f=i.scaleLabel,p=0!==e.labelRotation,g=e.isHorizontal(),m=c.autoSkip?e._autoSkip(e.getTicks()):e.getTicks(),v=s.valueOrDefault(c.fontColor,u.defaultFontColor),y=n(c),b=s.valueOrDefault(h.fontColor,u.defaultFontColor),_=n(h),x=d.drawTicks?d.tickMarkLength:0,w=s.valueOrDefault(f.fontColor,u.defaultFontColor),D=n(f),S=s.options.toPadding(f.padding),C=s.toRadians(e.labelRotation),T=[],k=e.options.gridLines.lineWidth,A="right"===i.position?e.right:e.right-k-x,I="right"===i.position?e.right+x:e.right,E="bottom"===i.position?e.top+k:e.bottom-x-k,O="bottom"===i.position?e.top+k+x:e.bottom+k;if(s.each(m,function(n,o){if(!s.isNullOrUndef(n.label)){var a,l,h,f,v=n.label;o===e.zeroLineIndex&&i.offset===d.offsetGridLines?(a=d.zeroLineWidth,l=d.zeroLineColor,h=d.zeroLineBorderDash,f=d.zeroLineBorderDashOffset):(a=s.valueAtIndexOrDefault(d.lineWidth,o),l=s.valueAtIndexOrDefault(d.color,o),h=s.valueOrDefault(d.borderDash,u.borderDash),f=s.valueOrDefault(d.borderDashOffset,u.borderDashOffset));var y,b,_,w,D,S,M,P,N,L,F="middle",R="middle",j=c.padding;if(g){var H=x+j;"bottom"===i.position?(R=p?"middle":"top",F=p?"right":"center",L=e.top+H):(R=p?"middle":"bottom",F=p?"left":"center",L=e.bottom-H);var W=r(e,o,d.offsetGridLines&&m.length>1);W<e.left&&(l="rgba(0,0,0,0)"),W+=s.aliasPixel(a),N=e.getPixelForTick(o)+c.labelOffset,y=_=D=M=W,b=E,w=O,S=t.top,P=t.bottom+k}else{var B,V="left"===i.position;c.mirror?(F=V?"left":"right",B=j):(F=V?"right":"left",B=x+j),N=V?e.right-B:e.left+B;var U=r(e,o,d.offsetGridLines&&m.length>1);U<e.top&&(l="rgba(0,0,0,0)"),U+=s.aliasPixel(a),L=e.getPixelForTick(o)+c.labelOffset,y=A,_=I,D=t.left,M=t.right+k,b=w=S=P=U}T.push({tx1:y,ty1:b,tx2:_,ty2:w,x1:D,y1:S,x2:M,y2:P,labelX:N,labelY:L,glWidth:a,glColor:l,glBorderDash:h,glBorderDashOffset:f,rotation:-1*C,label:v,major:n.major,textBaseline:R,textAlign:F})}}),s.each(T,function(t){if(d.display&&(a.save(),a.lineWidth=t.glWidth,a.strokeStyle=t.glColor,a.setLineDash&&(a.setLineDash(t.glBorderDash),a.lineDashOffset=t.glBorderDashOffset),a.beginPath(),d.drawTicks&&(a.moveTo(t.tx1,t.ty1),a.lineTo(t.tx2,t.ty2)),d.drawOnChartArea&&(a.moveTo(t.x1,t.y1),a.lineTo(t.x2,t.y2)),a.stroke(),a.restore()),c.display){a.save(),a.translate(t.labelX,t.labelY),a.rotate(t.rotation),a.font=t.major?_.font:y.font,a.fillStyle=t.major?b:v,a.textBaseline=t.textBaseline,a.textAlign=t.textAlign;var n=t.label;if(s.isArray(n))for(var i=n.length,r=1.5*y.size,o=e.isHorizontal()?0:-r*(i-1)/2,l=0;l<i;++l)a.fillText(""+n[l],0,o),o+=r;else a.fillText(n,0,0);a.restore()}}),f.display){var M,P,N=0,L=l(f)/2;if(g)M=e.left+(e.right-e.left)/2,P="bottom"===i.position?e.bottom-L-S.bottom:e.top+L+S.top;else{var F="left"===i.position;M=F?e.left+L+S.top:e.right-L-S.top,P=e.top+(e.bottom-e.top)/2,N=F?-.5*Math.PI:.5*Math.PI}a.save(),a.translate(M,P),a.rotate(N),a.textAlign="center",a.textBaseline="middle",a.fillStyle=w,a.font=D.font,a.fillText(f.labelString,0,0),a.restore()}if(d.drawBorder){a.lineWidth=s.valueAtIndexOrDefault(d.lineWidth,0),a.strokeStyle=s.valueAtIndexOrDefault(d.color,0);var R=e.left,j=e.right+k,H=e.top,W=e.bottom+k,B=s.aliasPixel(a.lineWidth);g?(H=W="top"===i.position?e.bottom:e.top,H+=B,W+=B):(R=j="left"===i.position?e.right:e.left,R+=B,j+=B),a.beginPath(),a.moveTo(R,H),a.lineTo(j,W),a.stroke()}}}})}},{25:25,26:26,34:34,45:45}],33:[function(t,e,n){"use strict";var i=t(25),r=t(45),o=t(30);e.exports=function(t){t.scaleService={constructors:{},defaults:{},registerScaleType:function(t,e,n){this.constructors[t]=e,this.defaults[t]=r.clone(n)},getScaleConstructor:function(t){return this.constructors.hasOwnProperty(t)?this.constructors[t]:void 0},getScaleDefaults:function(t){return this.defaults.hasOwnProperty(t)?r.merge({},[i.scale,this.defaults[t]]):{}},updateScaleDefaults:function(t,e){var n=this;n.defaults.hasOwnProperty(t)&&(n.defaults[t]=r.extend(n.defaults[t],e))},addScalesToLayout:function(t){r.each(t.scales,function(e){e.fullWidth=e.options.fullWidth,e.position=e.options.position,e.weight=e.options.weight,o.addBox(t,e)})}}}},{25:25,30:30,45:45}],34:[function(t,e,n){"use strict";var i=t(45);e.exports={formatters:{values:function(t){return i.isArray(t)?t:""+t},linear:function(t,e,n){var r=n.length>3?n[2]-n[1]:n[1]-n[0];Math.abs(r)>1&&t!==Math.floor(t)&&(r=t-Math.floor(t));var o=i.log10(Math.abs(r)),a="";if(0!==t){var s=-1*Math.floor(o);s=Math.max(Math.min(s,20),0),a=t.toFixed(s)}else a="0";return a},logarithmic:function(t,e,n){var r=t/Math.pow(10,Math.floor(i.log10(t)));return 0===t?"0":1===r||2===r||5===r||0===e||e===n.length-1?t.toExponential():""}}}},{45:45}],35:[function(t,e,n){"use strict";var i=t(25),r=t(26),o=t(45);i._set("global",{tooltips:{enabled:!0,custom:null,mode:"nearest",position:"average",intersect:!0,backgroundColor:"rgba(0,0,0,0.8)",titleFontStyle:"bold",titleSpacing:2,titleMarginBottom:6,titleFontColor:"#fff",titleAlign:"left",bodySpacing:2,bodyFontColor:"#fff",bodyAlign:"left",footerFontStyle:"bold",footerSpacing:2,footerMarginTop:6,footerFontColor:"#fff",footerAlign:"left",yPadding:6,xPadding:6,caretPadding:2,caretSize:5,cornerRadius:6,multiKeyBackground:"#fff",displayColors:!0,borderColor:"rgba(0,0,0,0)",borderWidth:0,callbacks:{beforeTitle:o.noop,title:function(t,e){var n="",i=e.labels,r=i?i.length:0;if(t.length>0){var o=t[0];o.xLabel?n=o.xLabel:r>0&&o.index<r&&(n=i[o.index])}return n},afterTitle:o.noop,beforeBody:o.noop,beforeLabel:o.noop,label:function(t,e){var n=e.datasets[t.datasetIndex].label||"";return n&&(n+=": "),n+=t.yLabel},labelColor:function(t,e){var n=e.getDatasetMeta(t.datasetIndex),i=n.data[t.index],r=i._view;return{borderColor:r.borderColor,backgroundColor:r.backgroundColor}},labelTextColor:function(){return this._options.bodyFontColor},afterLabel:o.noop,afterBody:o.noop,beforeFooter:o.noop,footer:o.noop,afterFooter:o.noop}}}),e.exports=function(t){function e(t,e){var n=o.color(t);return n.alpha(e*n.alpha()).rgbaString()}function n(t,e){return e&&(o.isArray(e)?Array.prototype.push.apply(t,e):t.push(e)),t}function a(t){var e=t._xScale,n=t._yScale||t._scale,i=t._index,r=t._datasetIndex;return{xLabel:e?e.getLabelForIndex(i,r):"",yLabel:n?n.getLabelForIndex(i,r):"",index:i,datasetIndex:r,x:t._model.x,y:t._model.y}}function s(t){var e=i.global,n=o.valueOrDefault;return{xPadding:t.xPadding,yPadding:t.yPadding,xAlign:t.xAlign,yAlign:t.yAlign,bodyFontColor:t.bodyFontColor,_bodyFontFamily:n(t.bodyFontFamily,e.defaultFontFamily),_bodyFontStyle:n(t.bodyFontStyle,e.defaultFontStyle),_bodyAlign:t.bodyAlign,bodyFontSize:n(t.bodyFontSize,e.defaultFontSize),bodySpacing:t.bodySpacing,titleFontColor:t.titleFontColor,_titleFontFamily:n(t.titleFontFamily,e.defaultFontFamily),_titleFontStyle:n(t.titleFontStyle,e.defaultFontStyle),titleFontSize:n(t.titleFontSize,e.defaultFontSize),_titleAlign:t.titleAlign,titleSpacing:t.titleSpacing,titleMarginBottom:t.titleMarginBottom,footerFontColor:t.footerFontColor,_footerFontFamily:n(t.footerFontFamily,e.defaultFontFamily),_footerFontStyle:n(t.footerFontStyle,e.defaultFontStyle),footerFontSize:n(t.footerFontSize,e.defaultFontSize),_footerAlign:t.footerAlign,footerSpacing:t.footerSpacing,footerMarginTop:t.footerMarginTop,caretSize:t.caretSize,cornerRadius:t.cornerRadius,backgroundColor:t.backgroundColor,opacity:0,legendColorBackground:t.multiKeyBackground,displayColors:t.displayColors,borderColor:t.borderColor,borderWidth:t.borderWidth}}function l(t,e){var n=t._chart.ctx,i=2*e.yPadding,r=0,a=e.body,s=a.reduce(function(t,e){return t+e.before.length+e.lines.length+e.after.length},0);s+=e.beforeBody.length+e.afterBody.length;var l=e.title.length,u=e.footer.length,c=e.titleFontSize,h=e.bodyFontSize,d=e.footerFontSize;i+=l*c,i+=l?(l-1)*e.titleSpacing:0,i+=l?e.titleMarginBottom:0,i+=s*h,i+=s?(s-1)*e.bodySpacing:0,i+=u?e.footerMarginTop:0,i+=u*d,i+=u?(u-1)*e.footerSpacing:0;var f=0,p=function(t){r=Math.max(r,n.measureText(t).width+f)};return n.font=o.fontString(c,e._titleFontStyle,e._titleFontFamily),
o.each(e.title,p),n.font=o.fontString(h,e._bodyFontStyle,e._bodyFontFamily),o.each(e.beforeBody.concat(e.afterBody),p),f=e.displayColors?h+2:0,o.each(a,function(t){o.each(t.before,p),o.each(t.lines,p),o.each(t.after,p)}),f=0,n.font=o.fontString(d,e._footerFontStyle,e._footerFontFamily),o.each(e.footer,p),r+=2*e.xPadding,{width:r,height:i}}function u(t,e){var n=t._model,i=t._chart,r=t._chart.chartArea,o="center",a="center";n.y<e.height?a="top":n.y>i.height-e.height&&(a="bottom");var s,l,u,c,h,d=(r.left+r.right)/2,f=(r.top+r.bottom)/2;"center"===a?(s=function(t){return t<=d},l=function(t){return t>d}):(s=function(t){return t<=e.width/2},l=function(t){return t>=i.width-e.width/2}),u=function(t){return t+e.width+n.caretSize+n.caretPadding>i.width},c=function(t){return t-e.width-n.caretSize-n.caretPadding<0},h=function(t){return t<=f?"top":"bottom"},s(n.x)?(o="left",u(n.x)&&(o="center",a=h(n.y))):l(n.x)&&(o="right",c(n.x)&&(o="center",a=h(n.y)));var p=t._options;return{xAlign:p.xAlign?p.xAlign:o,yAlign:p.yAlign?p.yAlign:a}}function c(t,e,n,i){var r=t.x,o=t.y,a=t.caretSize,s=t.caretPadding,l=t.cornerRadius,u=n.xAlign,c=n.yAlign,h=a+s,d=l+s;return"right"===u?r-=e.width:"center"===u&&(r-=e.width/2,r+e.width>i.width&&(r=i.width-e.width),r<0&&(r=0)),"top"===c?o+=h:o-="bottom"===c?e.height+h:e.height/2,"center"===c?"left"===u?r+=h:"right"===u&&(r-=h):"left"===u?r-=d:"right"===u&&(r+=d),{x:r,y:o}}t.Tooltip=r.extend({initialize:function(){this._model=s(this._options),this._lastActive=[]},getTitle:function(){var t=this,e=t._options,i=e.callbacks,r=i.beforeTitle.apply(t,arguments),o=i.title.apply(t,arguments),a=i.afterTitle.apply(t,arguments),s=[];return s=n(s,r),s=n(s,o),s=n(s,a)},getBeforeBody:function(){var t=this._options.callbacks.beforeBody.apply(this,arguments);return o.isArray(t)?t:void 0!==t?[t]:[]},getBody:function(t,e){var i=this,r=i._options.callbacks,a=[];return o.each(t,function(t){var o={before:[],lines:[],after:[]};n(o.before,r.beforeLabel.call(i,t,e)),n(o.lines,r.label.call(i,t,e)),n(o.after,r.afterLabel.call(i,t,e)),a.push(o)}),a},getAfterBody:function(){var t=this._options.callbacks.afterBody.apply(this,arguments);return o.isArray(t)?t:void 0!==t?[t]:[]},getFooter:function(){var t=this,e=t._options.callbacks,i=e.beforeFooter.apply(t,arguments),r=e.footer.apply(t,arguments),o=e.afterFooter.apply(t,arguments),a=[];return a=n(a,i),a=n(a,r),a=n(a,o)},update:function(e){var n,i,r=this,h=r._options,d=r._model,f=r._model=s(h),p=r._active,g=r._data,m={xAlign:d.xAlign,yAlign:d.yAlign},v={x:d.x,y:d.y},y={width:d.width,height:d.height},b={x:d.caretX,y:d.caretY};if(p.length){f.opacity=1;var _=[],x=[];b=t.Tooltip.positioners[h.position].call(r,p,r._eventPosition);var w=[];for(n=0,i=p.length;n<i;++n)w.push(a(p[n]));h.filter&&(w=w.filter(function(t){return h.filter(t,g)})),h.itemSort&&(w=w.sort(function(t,e){return h.itemSort(t,e,g)})),o.each(w,function(t){_.push(h.callbacks.labelColor.call(r,t,r._chart)),x.push(h.callbacks.labelTextColor.call(r,t,r._chart))}),f.title=r.getTitle(w,g),f.beforeBody=r.getBeforeBody(w,g),f.body=r.getBody(w,g),f.afterBody=r.getAfterBody(w,g),f.footer=r.getFooter(w,g),f.x=Math.round(b.x),f.y=Math.round(b.y),f.caretPadding=h.caretPadding,f.labelColors=_,f.labelTextColors=x,f.dataPoints=w,y=l(this,f),m=u(this,y),v=c(f,y,m,r._chart)}else f.opacity=0;return f.xAlign=m.xAlign,f.yAlign=m.yAlign,f.x=v.x,f.y=v.y,f.width=y.width,f.height=y.height,f.caretX=b.x,f.caretY=b.y,r._model=f,e&&h.custom&&h.custom.call(r,f),r},drawCaret:function(t,e){var n=this._chart.ctx,i=this._view,r=this.getCaretPosition(t,e,i);n.lineTo(r.x1,r.y1),n.lineTo(r.x2,r.y2),n.lineTo(r.x3,r.y3)},getCaretPosition:function(t,e,n){var i,r,o,a,s,l,u=n.caretSize,c=n.cornerRadius,h=n.xAlign,d=n.yAlign,f=t.x,p=t.y,g=e.width,m=e.height;if("center"===d)s=p+m/2,"left"===h?(i=f,r=i-u,o=i,a=s+u,l=s-u):(i=f+g,r=i+u,o=i,a=s-u,l=s+u);else if("left"===h?(r=f+c+u,i=r-u,o=r+u):"right"===h?(r=f+g-c-u,i=r-u,o=r+u):(r=n.caretX,i=r-u,o=r+u),"top"===d)a=p,s=a-u,l=a;else{a=p+m,s=a+u,l=a;var v=o;o=i,i=v}return{x1:i,x2:r,x3:o,y1:a,y2:s,y3:l}},drawTitle:function(t,n,i,r){var a=n.title;if(a.length){i.textAlign=n._titleAlign,i.textBaseline="top";var s=n.titleFontSize,l=n.titleSpacing;i.fillStyle=e(n.titleFontColor,r),i.font=o.fontString(s,n._titleFontStyle,n._titleFontFamily);var u,c;for(u=0,c=a.length;u<c;++u)i.fillText(a[u],t.x,t.y),t.y+=s+l,u+1===a.length&&(t.y+=n.titleMarginBottom-l)}},drawBody:function(t,n,i,r){var a=n.bodyFontSize,s=n.bodySpacing,l=n.body;i.textAlign=n._bodyAlign,i.textBaseline="top",i.font=o.fontString(a,n._bodyFontStyle,n._bodyFontFamily);var u=0,c=function(e){i.fillText(e,t.x+u,t.y),t.y+=a+s};i.fillStyle=e(n.bodyFontColor,r),o.each(n.beforeBody,c);var h=n.displayColors;u=h?a+2:0,o.each(l,function(s,l){var u=e(n.labelTextColors[l],r);i.fillStyle=u,o.each(s.before,c),o.each(s.lines,function(o){h&&(i.fillStyle=e(n.legendColorBackground,r),i.fillRect(t.x,t.y,a,a),i.lineWidth=1,i.strokeStyle=e(n.labelColors[l].borderColor,r),i.strokeRect(t.x,t.y,a,a),i.fillStyle=e(n.labelColors[l].backgroundColor,r),i.fillRect(t.x+1,t.y+1,a-2,a-2),i.fillStyle=u),c(o)}),o.each(s.after,c)}),u=0,o.each(n.afterBody,c),t.y-=s},drawFooter:function(t,n,i,r){var a=n.footer;a.length&&(t.y+=n.footerMarginTop,i.textAlign=n._footerAlign,i.textBaseline="top",i.fillStyle=e(n.footerFontColor,r),i.font=o.fontString(n.footerFontSize,n._footerFontStyle,n._footerFontFamily),o.each(a,function(e){i.fillText(e,t.x,t.y),t.y+=n.footerFontSize+n.footerSpacing}))},drawBackground:function(t,n,i,r,o){i.fillStyle=e(n.backgroundColor,o),i.strokeStyle=e(n.borderColor,o),i.lineWidth=n.borderWidth;var a=n.xAlign,s=n.yAlign,l=t.x,u=t.y,c=r.width,h=r.height,d=n.cornerRadius;i.beginPath(),i.moveTo(l+d,u),"top"===s&&this.drawCaret(t,r),i.lineTo(l+c-d,u),i.quadraticCurveTo(l+c,u,l+c,u+d),"center"===s&&"right"===a&&this.drawCaret(t,r),i.lineTo(l+c,u+h-d),i.quadraticCurveTo(l+c,u+h,l+c-d,u+h),"bottom"===s&&this.drawCaret(t,r),i.lineTo(l+d,u+h),i.quadraticCurveTo(l,u+h,l,u+h-d),"center"===s&&"left"===a&&this.drawCaret(t,r),i.lineTo(l,u+d),i.quadraticCurveTo(l,u,l+d,u),i.closePath(),i.fill(),n.borderWidth>0&&i.stroke()},draw:function(){var t=this._chart.ctx,e=this._view;if(0!==e.opacity){var n={width:e.width,height:e.height},i={x:e.x,y:e.y},r=Math.abs(e.opacity<.001)?0:e.opacity,o=e.title.length||e.beforeBody.length||e.body.length||e.afterBody.length||e.footer.length;this._options.enabled&&o&&(this.drawBackground(i,e,t,n,r),i.x+=e.xPadding,i.y+=e.yPadding,this.drawTitle(i,e,t,r),this.drawBody(i,e,t,r),this.drawFooter(i,e,t,r))}},handleEvent:function(t){var e=this,n=e._options,i=!1;return e._lastActive=e._lastActive||[],"mouseout"===t.type?e._active=[]:e._active=e._chart.getElementsAtEventForMode(t,n.mode,n),i=!o.arrayEquals(e._active,e._lastActive),i&&(e._lastActive=e._active,(n.enabled||n.custom)&&(e._eventPosition={x:t.x,y:t.y},e.update(!0),e.pivot())),i}}),t.Tooltip.positioners={average:function(t){if(!t.length)return!1;var e,n,i=0,r=0,o=0;for(e=0,n=t.length;e<n;++e){var a=t[e];if(a&&a.hasValue()){var s=a.tooltipPosition();i+=s.x,r+=s.y,++o}}return{x:Math.round(i/o),y:Math.round(r/o)}},nearest:function(t,e){var n,i,r,a=e.x,s=e.y,l=Number.POSITIVE_INFINITY;for(n=0,i=t.length;n<i;++n){var u=t[n];if(u&&u.hasValue()){var c=u.getCenterPoint(),h=o.distanceBetweenPoints(e,c);h<l&&(l=h,r=u)}}if(r){var d=r.tooltipPosition();a=d.x,s=d.y}return{x:a,y:s}}}}},{25:25,26:26,45:45}],36:[function(t,e,n){"use strict";var i=t(25),r=t(26),o=t(45);i._set("global",{elements:{arc:{backgroundColor:i.global.defaultColor,borderColor:"#fff",borderWidth:2}}}),e.exports=r.extend({inLabelRange:function(t){var e=this._view;return!!e&&Math.pow(t-e.x,2)<Math.pow(e.radius+e.hoverRadius,2)},inRange:function(t,e){var n=this._view;if(n){for(var i=o.getAngleFromPoint(n,{x:t,y:e}),r=i.angle,a=i.distance,s=n.startAngle,l=n.endAngle;l<s;)l+=2*Math.PI;for(;r>l;)r-=2*Math.PI;for(;r<s;)r+=2*Math.PI;var u=r>=s&&r<=l,c=a>=n.innerRadius&&a<=n.outerRadius;return u&&c}return!1},getCenterPoint:function(){var t=this._view,e=(t.startAngle+t.endAngle)/2,n=(t.innerRadius+t.outerRadius)/2;return{x:t.x+Math.cos(e)*n,y:t.y+Math.sin(e)*n}},getArea:function(){var t=this._view;return Math.PI*((t.endAngle-t.startAngle)/(2*Math.PI))*(Math.pow(t.outerRadius,2)-Math.pow(t.innerRadius,2))},tooltipPosition:function(){var t=this._view,e=t.startAngle+(t.endAngle-t.startAngle)/2,n=(t.outerRadius-t.innerRadius)/2+t.innerRadius;return{x:t.x+Math.cos(e)*n,y:t.y+Math.sin(e)*n}},draw:function(){var t=this._chart.ctx,e=this._view,n=e.startAngle,i=e.endAngle;t.beginPath(),t.arc(e.x,e.y,e.outerRadius,n,i),t.arc(e.x,e.y,e.innerRadius,i,n,!0),t.closePath(),t.strokeStyle=e.borderColor,t.lineWidth=e.borderWidth,t.fillStyle=e.backgroundColor,t.fill(),t.lineJoin="bevel",e.borderWidth&&t.stroke()}})},{25:25,26:26,45:45}],37:[function(t,e,n){"use strict";var i=t(25),r=t(26),o=t(45),a=i.global;i._set("global",{elements:{line:{tension:.4,backgroundColor:a.defaultColor,borderWidth:3,borderColor:a.defaultColor,borderCapStyle:"butt",borderDash:[],borderDashOffset:0,borderJoinStyle:"miter",capBezierPoints:!0,fill:!0}}}),e.exports=r.extend({draw:function(){var t,e,n,i,r=this,s=r._view,l=r._chart.ctx,u=s.spanGaps,c=r._children.slice(),h=a.elements.line,d=-1;for(r._loop&&c.length&&c.push(c[0]),l.save(),l.lineCap=s.borderCapStyle||h.borderCapStyle,l.setLineDash&&l.setLineDash(s.borderDash||h.borderDash),l.lineDashOffset=s.borderDashOffset||h.borderDashOffset,l.lineJoin=s.borderJoinStyle||h.borderJoinStyle,l.lineWidth=s.borderWidth||h.borderWidth,l.strokeStyle=s.borderColor||a.defaultColor,l.beginPath(),d=-1,t=0;t<c.length;++t)e=c[t],n=o.previousItem(c,t),i=e._view,0===t?i.skip||(l.moveTo(i.x,i.y),d=t):(n=d===-1?n:c[d],i.skip||(d!==t-1&&!u||d===-1?l.moveTo(i.x,i.y):o.canvas.lineTo(l,n._view,e._view),d=t));l.stroke(),l.restore()}})},{25:25,26:26,45:45}],38:[function(t,e,n){"use strict";function i(t){var e=this._view;return!!e&&Math.abs(t-e.x)<e.radius+e.hitRadius}function r(t){var e=this._view;return!!e&&Math.abs(t-e.y)<e.radius+e.hitRadius}var o=t(25),a=t(26),s=t(45),l=o.global.defaultColor;o._set("global",{elements:{point:{radius:3,pointStyle:"circle",backgroundColor:l,borderColor:l,borderWidth:1,hitRadius:1,hoverRadius:4,hoverBorderWidth:1}}}),e.exports=a.extend({inRange:function(t,e){var n=this._view;return!!n&&Math.pow(t-n.x,2)+Math.pow(e-n.y,2)<Math.pow(n.hitRadius+n.radius,2)},inLabelRange:i,inXRange:i,inYRange:r,getCenterPoint:function(){var t=this._view;return{x:t.x,y:t.y}},getArea:function(){return Math.PI*Math.pow(this._view.radius,2)},tooltipPosition:function(){var t=this._view;return{x:t.x,y:t.y,padding:t.radius+t.borderWidth}},draw:function(t){var e=this._view,n=this._model,i=this._chart.ctx,r=e.pointStyle,a=e.radius,u=e.x,c=e.y,h=s.color,d=1.01,f=0;e.skip||(i.strokeStyle=e.borderColor||l,i.lineWidth=s.valueOrDefault(e.borderWidth,o.global.elements.point.borderWidth),i.fillStyle=e.backgroundColor||l,void 0!==t&&(n.x<t.left||t.right*d<n.x||n.y<t.top||t.bottom*d<n.y)&&(n.x<t.left?f=(u-n.x)/(t.left-n.x):t.right*d<n.x?f=(n.x-u)/(n.x-t.right):n.y<t.top?f=(c-n.y)/(t.top-n.y):t.bottom*d<n.y&&(f=(n.y-c)/(n.y-t.bottom)),f=Math.round(100*f)/100,i.strokeStyle=h(i.strokeStyle).alpha(f).rgbString(),i.fillStyle=h(i.fillStyle).alpha(f).rgbString()),s.canvas.drawPoint(i,r,a,u,c))}})},{25:25,26:26,45:45}],39:[function(t,e,n){"use strict";function i(t){return void 0!==t._view.width}function r(t){var e,n,r,o,a=t._view;if(i(t)){var s=a.width/2;e=a.x-s,n=a.x+s,r=Math.min(a.y,a.base),o=Math.max(a.y,a.base)}else{var l=a.height/2;e=Math.min(a.x,a.base),n=Math.max(a.x,a.base),r=a.y-l,o=a.y+l}return{left:e,top:r,right:n,bottom:o}}var o=t(25),a=t(26);o._set("global",{elements:{rectangle:{backgroundColor:o.global.defaultColor,borderColor:o.global.defaultColor,borderSkipped:"bottom",borderWidth:0}}}),e.exports=a.extend({draw:function(){function t(t){return v[(b+t)%4]}var e,n,i,r,o,a,s,l=this._chart.ctx,u=this._view,c=u.borderWidth;if(u.horizontal?(e=u.base,n=u.x,i=u.y-u.height/2,r=u.y+u.height/2,o=n>e?1:-1,a=1,s=u.borderSkipped||"left"):(e=u.x-u.width/2,n=u.x+u.width/2,i=u.y,r=u.base,o=1,a=r>i?1:-1,s=u.borderSkipped||"bottom"),c){var h=Math.min(Math.abs(e-n),Math.abs(i-r));c=c>h?h:c;var d=c/2,f=e+("left"!==s?d*o:0),p=n+("right"!==s?-d*o:0),g=i+("top"!==s?d*a:0),m=r+("bottom"!==s?-d*a:0);f!==p&&(i=g,r=m),g!==m&&(e=f,n=p)}l.beginPath(),l.fillStyle=u.backgroundColor,l.strokeStyle=u.borderColor,l.lineWidth=c;var v=[[e,r],[e,i],[n,i],[n,r]],y=["bottom","left","top","right"],b=y.indexOf(s,0);b===-1&&(b=0);var _=t(0);l.moveTo(_[0],_[1]);for(var x=1;x<4;x++)_=t(x),l.lineTo(_[0],_[1]);l.fill(),c&&l.stroke()},height:function(){var t=this._view;return t.base-t.y},inRange:function(t,e){var n=!1;if(this._view){var i=r(this);n=t>=i.left&&t<=i.right&&e>=i.top&&e<=i.bottom}return n},inLabelRange:function(t,e){var n=this;if(!n._view)return!1;var o=!1,a=r(n);return o=i(n)?t>=a.left&&t<=a.right:e>=a.top&&e<=a.bottom},inXRange:function(t){var e=r(this);return t>=e.left&&t<=e.right},inYRange:function(t){var e=r(this);return t>=e.top&&t<=e.bottom},getCenterPoint:function(){var t,e,n=this._view;return i(this)?(t=n.x,e=(n.y+n.base)/2):(t=(n.x+n.base)/2,e=n.y),{x:t,y:e}},getArea:function(){var t=this._view;return t.width*Math.abs(t.y-t.base)},tooltipPosition:function(){var t=this._view;return{x:t.x,y:t.y}}})},{25:25,26:26}],40:[function(t,e,n){"use strict";e.exports={},e.exports.Arc=t(36),e.exports.Line=t(37),e.exports.Point=t(38),e.exports.Rectangle=t(39)},{36:36,37:37,38:38,39:39}],41:[function(t,e,n){"use strict";var i=t(42),n=e.exports={clear:function(t){t.ctx.clearRect(0,0,t.width,t.height)},roundedRect:function(t,e,n,i,r,o){if(o){var a=Math.min(o,i/2),s=Math.min(o,r/2);t.moveTo(e+a,n),t.lineTo(e+i-a,n),t.quadraticCurveTo(e+i,n,e+i,n+s),t.lineTo(e+i,n+r-s),t.quadraticCurveTo(e+i,n+r,e+i-a,n+r),t.lineTo(e+a,n+r),t.quadraticCurveTo(e,n+r,e,n+r-s),t.lineTo(e,n+s),t.quadraticCurveTo(e,n,e+a,n)}else t.rect(e,n,i,r)},drawPoint:function(t,e,n,i,r){var o,a,s,l,u,c;if(e&&"object"==typeof e&&(o=e.toString(),"[object HTMLImageElement]"===o||"[object HTMLCanvasElement]"===o))return void t.drawImage(e,i-e.width/2,r-e.height/2,e.width,e.height);if(!(isNaN(n)||n<=0)){switch(e){default:t.beginPath(),t.arc(i,r,n,0,2*Math.PI),t.closePath(),t.fill();break;case"triangle":t.beginPath(),a=3*n/Math.sqrt(3),u=a*Math.sqrt(3)/2,t.moveTo(i-a/2,r+u/3),t.lineTo(i+a/2,r+u/3),t.lineTo(i,r-2*u/3),t.closePath(),t.fill();break;case"rect":c=1/Math.SQRT2*n,t.beginPath(),t.fillRect(i-c,r-c,2*c,2*c),t.strokeRect(i-c,r-c,2*c,2*c);break;case"rectRounded":var h=n/Math.SQRT2,d=i-h,f=r-h,p=Math.SQRT2*n;t.beginPath(),this.roundedRect(t,d,f,p,p,n/2),t.closePath(),t.fill();break;case"rectRot":c=1/Math.SQRT2*n,t.beginPath(),t.moveTo(i-c,r),t.lineTo(i,r+c),t.lineTo(i+c,r),t.lineTo(i,r-c),t.closePath(),t.fill();break;case"cross":t.beginPath(),t.moveTo(i,r+n),t.lineTo(i,r-n),t.moveTo(i-n,r),t.lineTo(i+n,r),t.closePath();break;case"crossRot":t.beginPath(),s=Math.cos(Math.PI/4)*n,l=Math.sin(Math.PI/4)*n,t.moveTo(i-s,r-l),t.lineTo(i+s,r+l),t.moveTo(i-s,r+l),t.lineTo(i+s,r-l),t.closePath();break;case"star":t.beginPath(),t.moveTo(i,r+n),t.lineTo(i,r-n),t.moveTo(i-n,r),t.lineTo(i+n,r),s=Math.cos(Math.PI/4)*n,l=Math.sin(Math.PI/4)*n,t.moveTo(i-s,r-l),t.lineTo(i+s,r+l),t.moveTo(i-s,r+l),t.lineTo(i+s,r-l),t.closePath();break;case"line":t.beginPath(),t.moveTo(i-n,r),t.lineTo(i+n,r),t.closePath();break;case"dash":t.beginPath(),t.moveTo(i,r),t.lineTo(i+n,r),t.closePath()}t.stroke()}},clipArea:function(t,e){t.save(),t.beginPath(),t.rect(e.left,e.top,e.right-e.left,e.bottom-e.top),t.clip()},unclipArea:function(t){t.restore()},lineTo:function(t,e,n,i){return n.steppedLine?("after"===n.steppedLine&&!i||"after"!==n.steppedLine&&i?t.lineTo(e.x,n.y):t.lineTo(n.x,e.y),void t.lineTo(n.x,n.y)):n.tension?void t.bezierCurveTo(i?e.controlPointPreviousX:e.controlPointNextX,i?e.controlPointPreviousY:e.controlPointNextY,i?n.controlPointNextX:n.controlPointPreviousX,i?n.controlPointNextY:n.controlPointPreviousY,n.x,n.y):void t.lineTo(n.x,n.y)}};i.clear=n.clear,i.drawRoundedRectangle=function(t){t.beginPath(),n.roundedRect.apply(n,arguments),t.closePath()}},{42:42}],42:[function(t,e,n){"use strict";var i={noop:function(){},uid:function(){var t=0;return function(){return t++}}(),isNullOrUndef:function(t){return null===t||"undefined"==typeof t},isArray:Array.isArray?Array.isArray:function(t){return"[object Array]"===Object.prototype.toString.call(t)},isObject:function(t){return null!==t&&"[object Object]"===Object.prototype.toString.call(t)},valueOrDefault:function(t,e){return"undefined"==typeof t?e:t},valueAtIndexOrDefault:function(t,e,n){return i.valueOrDefault(i.isArray(t)?t[e]:t,n)},callback:function(t,e,n){if(t&&"function"==typeof t.call)return t.apply(n,e)},each:function(t,e,n,r){var o,a,s;if(i.isArray(t))if(a=t.length,r)for(o=a-1;o>=0;o--)e.call(n,t[o],o);else for(o=0;o<a;o++)e.call(n,t[o],o);else if(i.isObject(t))for(s=Object.keys(t),a=s.length,o=0;o<a;o++)e.call(n,t[s[o]],s[o])},arrayEquals:function(t,e){var n,r,o,a;if(!t||!e||t.length!==e.length)return!1;for(n=0,r=t.length;n<r;++n)if(o=t[n],a=e[n],o instanceof Array&&a instanceof Array){if(!i.arrayEquals(o,a))return!1}else if(o!==a)return!1;return!0},clone:function(t){if(i.isArray(t))return t.map(i.clone);if(i.isObject(t)){for(var e={},n=Object.keys(t),r=n.length,o=0;o<r;++o)e[n[o]]=i.clone(t[n[o]]);return e}return t},_merger:function(t,e,n,r){var o=e[t],a=n[t];i.isObject(o)&&i.isObject(a)?i.merge(o,a,r):e[t]=i.clone(a)},_mergerIf:function(t,e,n){var r=e[t],o=n[t];i.isObject(r)&&i.isObject(o)?i.mergeIf(r,o):e.hasOwnProperty(t)||(e[t]=i.clone(o))},merge:function(t,e,n){var r,o,a,s,l,u=i.isArray(e)?e:[e],c=u.length;if(!i.isObject(t))return t;for(n=n||{},r=n.merger||i._merger,o=0;o<c;++o)if(e=u[o],i.isObject(e))for(a=Object.keys(e),l=0,s=a.length;l<s;++l)r(a[l],t,e,n);return t},mergeIf:function(t,e){return i.merge(t,e,{merger:i._mergerIf})},extend:function(t){for(var e=function(e,n){t[n]=e},n=1,r=arguments.length;n<r;++n)i.each(arguments[n],e);return t},inherits:function(t){var e=this,n=t&&t.hasOwnProperty("constructor")?t.constructor:function(){return e.apply(this,arguments)},r=function(){this.constructor=n};return r.prototype=e.prototype,n.prototype=new r,n.extend=i.inherits,t&&i.extend(n.prototype,t),n.__super__=e.prototype,n}};e.exports=i,i.callCallback=i.callback,i.indexOf=function(t,e,n){return Array.prototype.indexOf.call(t,e,n)},i.getValueOrDefault=i.valueOrDefault,i.getValueAtIndexOrDefault=i.valueAtIndexOrDefault},{}],43:[function(t,e,n){"use strict";var i=t(42),r={linear:function(t){return t},easeInQuad:function(t){return t*t},easeOutQuad:function(t){return-t*(t-2)},easeInOutQuad:function(t){return(t/=.5)<1?.5*t*t:-.5*(--t*(t-2)-1)},easeInCubic:function(t){return t*t*t},easeOutCubic:function(t){return(t-=1)*t*t+1},easeInOutCubic:function(t){return(t/=.5)<1?.5*t*t*t:.5*((t-=2)*t*t+2)},easeInQuart:function(t){return t*t*t*t},easeOutQuart:function(t){return-((t-=1)*t*t*t-1)},easeInOutQuart:function(t){return(t/=.5)<1?.5*t*t*t*t:-.5*((t-=2)*t*t*t-2)},easeInQuint:function(t){return t*t*t*t*t},easeOutQuint:function(t){return(t-=1)*t*t*t*t+1},easeInOutQuint:function(t){return(t/=.5)<1?.5*t*t*t*t*t:.5*((t-=2)*t*t*t*t+2)},easeInSine:function(t){return-Math.cos(t*(Math.PI/2))+1},easeOutSine:function(t){return Math.sin(t*(Math.PI/2))},easeInOutSine:function(t){return-.5*(Math.cos(Math.PI*t)-1)},easeInExpo:function(t){return 0===t?0:Math.pow(2,10*(t-1))},easeOutExpo:function(t){return 1===t?1:-Math.pow(2,-10*t)+1},easeInOutExpo:function(t){return 0===t?0:1===t?1:(t/=.5)<1?.5*Math.pow(2,10*(t-1)):.5*(-Math.pow(2,-10*--t)+2)},easeInCirc:function(t){return t>=1?t:-(Math.sqrt(1-t*t)-1)},easeOutCirc:function(t){return Math.sqrt(1-(t-=1)*t)},easeInOutCirc:function(t){return(t/=.5)<1?-.5*(Math.sqrt(1-t*t)-1):.5*(Math.sqrt(1-(t-=2)*t)+1)},easeInElastic:function(t){var e=1.70158,n=0,i=1;return 0===t?0:1===t?1:(n||(n=.3),i<1?(i=1,e=n/4):e=n/(2*Math.PI)*Math.asin(1/i),-(i*Math.pow(2,10*(t-=1))*Math.sin((t-e)*(2*Math.PI)/n)))},easeOutElastic:function(t){var e=1.70158,n=0,i=1;return 0===t?0:1===t?1:(n||(n=.3),i<1?(i=1,e=n/4):e=n/(2*Math.PI)*Math.asin(1/i),i*Math.pow(2,-10*t)*Math.sin((t-e)*(2*Math.PI)/n)+1)},easeInOutElastic:function(t){var e=1.70158,n=0,i=1;return 0===t?0:2===(t/=.5)?1:(n||(n=.45),i<1?(i=1,e=n/4):e=n/(2*Math.PI)*Math.asin(1/i),t<1?-.5*(i*Math.pow(2,10*(t-=1))*Math.sin((t-e)*(2*Math.PI)/n)):i*Math.pow(2,-10*(t-=1))*Math.sin((t-e)*(2*Math.PI)/n)*.5+1)},easeInBack:function(t){var e=1.70158;return t*t*((e+1)*t-e)},easeOutBack:function(t){var e=1.70158;return(t-=1)*t*((e+1)*t+e)+1},easeInOutBack:function(t){var e=1.70158;return(t/=.5)<1?.5*(t*t*(((e*=1.525)+1)*t-e)):.5*((t-=2)*t*(((e*=1.525)+1)*t+e)+2)},easeInBounce:function(t){return 1-r.easeOutBounce(1-t)},easeOutBounce:function(t){return t<1/2.75?7.5625*t*t:t<2/2.75?7.5625*(t-=1.5/2.75)*t+.75:t<2.5/2.75?7.5625*(t-=2.25/2.75)*t+.9375:7.5625*(t-=2.625/2.75)*t+.984375},easeInOutBounce:function(t){return t<.5?.5*r.easeInBounce(2*t):.5*r.easeOutBounce(2*t-1)+.5}};e.exports={effects:r},i.easingEffects=r},{42:42}],44:[function(t,e,n){"use strict";var i=t(42);e.exports={toLineHeight:function(t,e){var n=(""+t).match(/^(normal|(\d+(?:\.\d+)?)(px|em|%)?)$/);if(!n||"normal"===n[1])return 1.2*e;switch(t=+n[2],n[3]){case"px":return t;case"%":t/=100}return e*t},toPadding:function(t){var e,n,r,o;return i.isObject(t)?(e=+t.top||0,n=+t.right||0,r=+t.bottom||0,o=+t.left||0):e=n=r=o=+t||0,{top:e,right:n,bottom:r,left:o,height:e+r,width:o+n}},resolve:function(t,e,n){var r,o,a;for(r=0,o=t.length;r<o;++r)if(a=t[r],void 0!==a&&(void 0!==e&&"function"==typeof a&&(a=a(e)),void 0!==n&&i.isArray(a)&&(a=a[n]),void 0!==a))return a}}},{42:42}],45:[function(t,e,n){"use strict";e.exports=t(42),e.exports.easing=t(43),e.exports.canvas=t(41),e.exports.options=t(44)},{41:41,42:42,43:43,44:44}],46:[function(t,e,n){e.exports={acquireContext:function(t){return t&&t.canvas&&(t=t.canvas),t&&t.getContext("2d")||null}}},{}],47:[function(t,e,n){"use strict";function i(t,e){var n=m.getStyle(t,e),i=n&&n.match(/^(\d+)(\.\d+)?px$/);return i?Number(i[1]):void 0}function r(t,e){var n=t.style,r=t.getAttribute("height"),o=t.getAttribute("width");if(t[v]={initial:{height:r,width:o,style:{display:n.display,height:n.height,width:n.width}}},n.display=n.display||"block",null===o||""===o){var a=i(t,"width");void 0!==a&&(t.width=a)}if(null===r||""===r)if(""===t.style.height)t.height=t.width/(e.options.aspectRatio||2);else{var s=i(t,"height");void 0!==a&&(t.height=s)}return t}function o(t,e,n){t.addEventListener(e,n,S)}function a(t,e,n){t.removeEventListener(e,n,S)}function s(t,e,n,i,r){return{type:t,chart:e,"native":r||null,x:void 0!==n?n:null,y:void 0!==i?i:null}}function l(t,e){var n=w[t.type]||t.type,i=m.getRelativePosition(t,e);return s(n,e,i.x,i.y,t)}function u(t,e){var n=!1,i=[];return function(){i=Array.prototype.slice.call(arguments),e=e||this,n||(n=!0,m.requestAnimFrame.call(window,function(){n=!1,t.apply(e,i)}))}}function c(t){var e=document.createElement("div"),n=y+"size-monitor",i=1e6,r="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;";e.style.cssText=r,e.className=n,e.innerHTML='<div class="'+n+'-expand" style="'+r+'"><div style="position:absolute;width:'+i+"px;height:"+i+'px;left:0;top:0"></div></div><div class="'+n+'-shrink" style="'+r+'"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div>';var a=e.childNodes[0],s=e.childNodes[1];e._reset=function(){a.scrollLeft=i,a.scrollTop=i,s.scrollLeft=i,s.scrollTop=i};var l=function(){e._reset(),t()};return o(a,"scroll",l.bind(a,"expand")),o(s,"scroll",l.bind(s,"shrink")),e}function h(t,e){var n=t[v]||(t[v]={}),i=n.renderProxy=function(t){t.animationName===_&&e()};m.each(x,function(e){o(t,e,i)}),n.reflow=!!t.offsetParent,t.classList.add(b)}function d(t){var e=t[v]||{},n=e.renderProxy;n&&(m.each(x,function(e){a(t,e,n)}),delete e.renderProxy),t.classList.remove(b)}function f(t,e,n){var i=t[v]||(t[v]={}),r=i.resizer=c(u(function(){if(i.resizer)return e(s("resize",n))}));h(t,function(){if(i.resizer){var e=t.parentNode;e&&e!==r.parentNode&&e.insertBefore(r,e.firstChild),r._reset()}})}function p(t){var e=t[v]||{},n=e.resizer;delete e.resizer,d(t),n&&n.parentNode&&n.parentNode.removeChild(n)}function g(t,e){var n=t._style||document.createElement("style");t._style||(t._style=n,e="/* Chart.js */\n"+e,n.setAttribute("type","text/css"),document.getElementsByTagName("head")[0].appendChild(n)),n.appendChild(document.createTextNode(e))}var m=t(45),v="$chartjs",y="chartjs-",b=y+"render-monitor",_=y+"render-animation",x=["animationstart","webkitAnimationStart"],w={touchstart:"mousedown",touchmove:"mousemove",touchend:"mouseup",pointerenter:"mouseenter",pointerdown:"mousedown",pointermove:"mousemove",pointerup:"mouseup",pointerleave:"mouseout",pointerout:"mouseout"},D=function(){var t=!1;try{var e=Object.defineProperty({},"passive",{get:function(){t=!0}});window.addEventListener("e",null,e)}catch(n){}return t}(),S=!!D&&{passive:!0};e.exports={_enabled:"undefined"!=typeof window&&"undefined"!=typeof document,initialize:function(){var t="from{opacity:0.99}to{opacity:1}";g(this,"@-webkit-keyframes "+_+"{"+t+"}@keyframes "+_+"{"+t+"}."+b+"{-webkit-animation:"+_+" 0.001s;animation:"+_+" 0.001s;}")},acquireContext:function(t,e){"string"==typeof t?t=document.getElementById(t):t.length&&(t=t[0]),t&&t.canvas&&(t=t.canvas);var n=t&&t.getContext&&t.getContext("2d");return n&&n.canvas===t?(r(t,e),n):null},releaseContext:function(t){var e=t.canvas;if(e[v]){var n=e[v].initial;["height","width"].forEach(function(t){var i=n[t];m.isNullOrUndef(i)?e.removeAttribute(t):e.setAttribute(t,i)}),m.each(n.style||{},function(t,n){e.style[n]=t}),e.width=e.width,delete e[v]}},addEventListener:function(t,e,n){var i=t.canvas;if("resize"===e)return void f(i,n,t);var r=n[v]||(n[v]={}),a=r.proxies||(r.proxies={}),s=a[t.id+"_"+e]=function(e){n(l(e,t))};o(i,e,s)},removeEventListener:function(t,e,n){var i=t.canvas;if("resize"===e)return void p(i,n);var r=n[v]||{},o=r.proxies||{},s=o[t.id+"_"+e];s&&a(i,e,s)}},m.addEvent=o,m.removeEvent=a},{45:45}],48:[function(t,e,n){"use strict";var i=t(45),r=t(46),o=t(47),a=o._enabled?o:r;e.exports=i.extend({initialize:function(){},acquireContext:function(){},releaseContext:function(){},addEventListener:function(){},removeEventListener:function(){}},a)},{45:45,46:46,47:47}],49:[function(t,e,n){"use strict";e.exports={},e.exports.filler=t(50),e.exports.legend=t(51),e.exports.title=t(52)},{50:50,51:51,52:52}],50:[function(t,e,n){"use strict";function i(t,e,n){var i,r=t._model||{},o=r.fill;if(void 0===o&&(o=!!r.backgroundColor),o===!1||null===o)return!1;if(o===!0)return"origin";if(i=parseFloat(o,10),isFinite(i)&&Math.floor(i)===i)return"-"!==o[0]&&"+"!==o[0]||(i=e+i),!(i===e||i<0||i>=n)&&i;switch(o){case"bottom":return"start";case"top":return"end";case"zero":return"origin";case"origin":case"start":case"end":return o;default:return!1}}function r(t){var e,n=t.el._model||{},i=t.el._scale||{},r=t.fill,o=null;if(isFinite(r))return null;if("start"===r?o=void 0===n.scaleBottom?i.bottom:n.scaleBottom:"end"===r?o=void 0===n.scaleTop?i.top:n.scaleTop:void 0!==n.scaleZero?o=n.scaleZero:i.getBasePosition?o=i.getBasePosition():i.getBasePixel&&(o=i.getBasePixel()),void 0!==o&&null!==o){if(void 0!==o.x&&void 0!==o.y)return o;if("number"==typeof o&&isFinite(o))return e=i.isHorizontal(),{x:e?o:null,y:e?null:o}}return null}function o(t,e,n){var i,r=t[e],o=r.fill,a=[e];if(!n)return o;for(;o!==!1&&a.indexOf(o)===-1;){if(!isFinite(o))return o;if(i=t[o],!i)return!1;if(i.visible)return o;a.push(o),o=i.fill}return!1}function a(t){var e=t.fill,n="dataset";return e===!1?null:(isFinite(e)||(n="boundary"),f[n](t))}function s(t){return t&&!t.skip}function l(t,e,n,i,r){var o;if(i&&r){for(t.moveTo(e[0].x,e[0].y),o=1;o<i;++o)d.canvas.lineTo(t,e[o-1],e[o]);for(t.lineTo(n[r-1].x,n[r-1].y),o=r-1;o>0;--o)d.canvas.lineTo(t,n[o],n[o-1],!0)}}function u(t,e,n,i,r,o){var a,u,c,h,d,f,p,g=e.length,m=i.spanGaps,v=[],y=[],b=0,_=0;for(t.beginPath(),a=0,u=g+!!o;a<u;++a)c=a%g,h=e[c]._view,d=n(h,c,i),f=s(h),p=s(d),f&&p?(b=v.push(h),_=y.push(d)):b&&_&&(m?(f&&v.push(h),p&&y.push(d)):(l(t,v,y,b,_),b=_=0,v=[],y=[]));l(t,v,y,b,_),t.closePath(),t.fillStyle=r,t.fill()}var c=t(25),h=t(40),d=t(45);c._set("global",{plugins:{filler:{propagate:!0}}});var f={dataset:function(t){var e=t.fill,n=t.chart,i=n.getDatasetMeta(e),r=i&&n.isDatasetVisible(e),o=r&&i.dataset._children||[],a=o.length||0;return a?function(t,e){return e<a&&o[e]._view||null}:null},boundary:function(t){var e=t.boundary,n=e?e.x:null,i=e?e.y:null;return function(t){return{x:null===n?t.x:n,y:null===i?t.y:i}}}};e.exports={id:"filler",afterDatasetsUpdate:function(t,e){var n,s,l,u,c=(t.data.datasets||[]).length,d=e.propagate,f=[];for(s=0;s<c;++s)n=t.getDatasetMeta(s),l=n.dataset,u=null,l&&l._model&&l instanceof h.Line&&(u={visible:t.isDatasetVisible(s),fill:i(l,s,c),chart:t,el:l}),n.$filler=u,f.push(u);for(s=0;s<c;++s)u=f[s],u&&(u.fill=o(f,s,d),u.boundary=r(u),u.mapper=a(u))},beforeDatasetDraw:function(t,e){var n=e.meta.$filler;if(n){var i=t.ctx,r=n.el,o=r._view,a=r._children||[],s=n.mapper,l=o.backgroundColor||c.global.defaultColor;s&&l&&a.length&&(d.canvas.clipArea(i,t.chartArea),u(i,a,s,o,l,r._loop),d.canvas.unclipArea(i))}}}},{25:25,40:40,45:45}],51:[function(t,e,n){"use strict";function i(t,e){return t.usePointStyle?e*Math.SQRT2:t.boxWidth}function r(t,e){var n=new c({ctx:t.ctx,options:e,chart:t});l.configure(t,n,e),l.addBox(t,n),t.legend=n}var o=t(25),a=t(26),s=t(45),l=t(30),u=s.noop;o._set("global",{legend:{display:!0,position:"top",fullWidth:!0,reverse:!1,weight:1e3,onClick:function(t,e){var n=e.datasetIndex,i=this.chart,r=i.getDatasetMeta(n);r.hidden=null===r.hidden?!i.data.datasets[n].hidden:null,i.update()},onHover:null,labels:{boxWidth:40,padding:10,generateLabels:function(t){var e=t.data;return s.isArray(e.datasets)?e.datasets.map(function(e,n){return{text:e.label,fillStyle:s.isArray(e.backgroundColor)?e.backgroundColor[0]:e.backgroundColor,hidden:!t.isDatasetVisible(n),lineCap:e.borderCapStyle,lineDash:e.borderDash,lineDashOffset:e.borderDashOffset,lineJoin:e.borderJoinStyle,lineWidth:e.borderWidth,strokeStyle:e.borderColor,pointStyle:e.pointStyle,datasetIndex:n}},this):[]}}},legendCallback:function(t){var e=[];e.push('<ul class="'+t.id+'-legend">');for(var n=0;n<t.data.datasets.length;n++)e.push('<li><span style="background-color:'+t.data.datasets[n].backgroundColor+'"></span>'),t.data.datasets[n].label&&e.push(t.data.datasets[n].label),e.push("</li>");return e.push("</ul>"),e.join("")}});var c=a.extend({initialize:function(t){s.extend(this,t),this.legendHitBoxes=[],this.doughnutMode=!1},beforeUpdate:u,update:function(t,e,n){var i=this;return i.beforeUpdate(),i.maxWidth=t,i.maxHeight=e,i.margins=n,i.beforeSetDimensions(),i.setDimensions(),i.afterSetDimensions(),i.beforeBuildLabels(),i.buildLabels(),i.afterBuildLabels(),i.beforeFit(),i.fit(),i.afterFit(),i.afterUpdate(),i.minSize},afterUpdate:u,beforeSetDimensions:u,setDimensions:function(){var t=this;t.isHorizontal()?(t.width=t.maxWidth,t.left=0,t.right=t.width):(t.height=t.maxHeight,t.top=0,t.bottom=t.height),t.paddingLeft=0,t.paddingTop=0,t.paddingRight=0,t.paddingBottom=0,t.minSize={width:0,height:0}},afterSetDimensions:u,beforeBuildLabels:u,buildLabels:function(){var t=this,e=t.options.labels||{},n=s.callback(e.generateLabels,[t.chart],t)||[];e.filter&&(n=n.filter(function(n){return e.filter(n,t.chart.data)})),t.options.reverse&&n.reverse(),t.legendItems=n},afterBuildLabels:u,beforeFit:u,fit:function(){var t=this,e=t.options,n=e.labels,r=e.display,a=t.ctx,l=o.global,u=s.valueOrDefault,c=u(n.fontSize,l.defaultFontSize),h=u(n.fontStyle,l.defaultFontStyle),d=u(n.fontFamily,l.defaultFontFamily),f=s.fontString(c,h,d),p=t.legendHitBoxes=[],g=t.minSize,m=t.isHorizontal();if(m?(g.width=t.maxWidth,g.height=r?10:0):(g.width=r?10:0,g.height=t.maxHeight),
r)if(a.font=f,m){var v=t.lineWidths=[0],y=t.legendItems.length?c+n.padding:0;a.textAlign="left",a.textBaseline="top",s.each(t.legendItems,function(e,r){var o=i(n,c),s=o+c/2+a.measureText(e.text).width;v[v.length-1]+s+n.padding>=t.width&&(y+=c+n.padding,v[v.length]=t.left),p[r]={left:0,top:0,width:s,height:c},v[v.length-1]+=s+n.padding}),g.height+=y}else{var b=n.padding,_=t.columnWidths=[],x=n.padding,w=0,D=0,S=c+b;s.each(t.legendItems,function(t,e){var r=i(n,c),o=r+c/2+a.measureText(t.text).width;D+S>g.height&&(x+=w+n.padding,_.push(w),w=0,D=0),w=Math.max(w,o),D+=S,p[e]={left:0,top:0,width:o,height:c}}),x+=w,_.push(w),g.width+=x}t.width=g.width,t.height=g.height},afterFit:u,isHorizontal:function(){return"top"===this.options.position||"bottom"===this.options.position},draw:function(){var t=this,e=t.options,n=e.labels,r=o.global,a=r.elements.line,l=t.width,u=t.lineWidths;if(e.display){var c,h=t.ctx,d=s.valueOrDefault,f=d(n.fontColor,r.defaultFontColor),p=d(n.fontSize,r.defaultFontSize),g=d(n.fontStyle,r.defaultFontStyle),m=d(n.fontFamily,r.defaultFontFamily),v=s.fontString(p,g,m);h.textAlign="left",h.textBaseline="middle",h.lineWidth=.5,h.strokeStyle=f,h.fillStyle=f,h.font=v;var y=i(n,p),b=t.legendHitBoxes,_=function(t,n,i){if(!(isNaN(y)||y<=0)){h.save(),h.fillStyle=d(i.fillStyle,r.defaultColor),h.lineCap=d(i.lineCap,a.borderCapStyle),h.lineDashOffset=d(i.lineDashOffset,a.borderDashOffset),h.lineJoin=d(i.lineJoin,a.borderJoinStyle),h.lineWidth=d(i.lineWidth,a.borderWidth),h.strokeStyle=d(i.strokeStyle,r.defaultColor);var o=0===d(i.lineWidth,a.borderWidth);if(h.setLineDash&&h.setLineDash(d(i.lineDash,a.borderDash)),e.labels&&e.labels.usePointStyle){var l=p*Math.SQRT2/2,u=l/Math.SQRT2,c=t+u,f=n+u;s.canvas.drawPoint(h,i.pointStyle,l,c,f)}else o||h.strokeRect(t,n,y,p),h.fillRect(t,n,y,p);h.restore()}},x=function(t,e,n,i){var r=p/2,o=y+r+t,a=e+r;h.fillText(n.text,o,a),n.hidden&&(h.beginPath(),h.lineWidth=2,h.moveTo(o,a),h.lineTo(o+i,a),h.stroke())},w=t.isHorizontal();c=w?{x:t.left+(l-u[0])/2,y:t.top+n.padding,line:0}:{x:t.left+n.padding,y:t.top+n.padding,line:0};var D=p+n.padding;s.each(t.legendItems,function(e,i){var r=h.measureText(e.text).width,o=y+p/2+r,a=c.x,s=c.y;w?a+o>=l&&(s=c.y+=D,c.line++,a=c.x=t.left+(l-u[c.line])/2):s+D>t.bottom&&(a=c.x=a+t.columnWidths[c.line]+n.padding,s=c.y=t.top+n.padding,c.line++),_(a,s,e),b[i].left=a,b[i].top=s,x(a,s,e,r),w?c.x+=o+n.padding:c.y+=D})}},handleEvent:function(t){var e=this,n=e.options,i="mouseup"===t.type?"click":t.type,r=!1;if("mousemove"===i){if(!n.onHover)return}else{if("click"!==i)return;if(!n.onClick)return}var o=t.x,a=t.y;if(o>=e.left&&o<=e.right&&a>=e.top&&a<=e.bottom)for(var s=e.legendHitBoxes,l=0;l<s.length;++l){var u=s[l];if(o>=u.left&&o<=u.left+u.width&&a>=u.top&&a<=u.top+u.height){if("click"===i){n.onClick.call(e,t["native"],e.legendItems[l]),r=!0;break}if("mousemove"===i){n.onHover.call(e,t["native"],e.legendItems[l]),r=!0;break}}}return r}});e.exports={id:"legend",_element:c,beforeInit:function(t){var e=t.options.legend;e&&r(t,e)},beforeUpdate:function(t){var e=t.options.legend,n=t.legend;e?(s.mergeIf(e,o.global.legend),n?(l.configure(t,n,e),n.options=e):r(t,e)):n&&(l.removeBox(t,n),delete t.legend)},afterEvent:function(t,e){var n=t.legend;n&&n.handleEvent(e)}}},{25:25,26:26,30:30,45:45}],52:[function(t,e,n){"use strict";function i(t,e){var n=new u({ctx:t.ctx,options:e,chart:t});s.configure(t,n,e),s.addBox(t,n),t.titleBlock=n}var r=t(25),o=t(26),a=t(45),s=t(30),l=a.noop;r._set("global",{title:{display:!1,fontStyle:"bold",fullWidth:!0,lineHeight:1.2,padding:10,position:"top",text:"",weight:2e3}});var u=o.extend({initialize:function(t){var e=this;a.extend(e,t),e.legendHitBoxes=[]},beforeUpdate:l,update:function(t,e,n){var i=this;return i.beforeUpdate(),i.maxWidth=t,i.maxHeight=e,i.margins=n,i.beforeSetDimensions(),i.setDimensions(),i.afterSetDimensions(),i.beforeBuildLabels(),i.buildLabels(),i.afterBuildLabels(),i.beforeFit(),i.fit(),i.afterFit(),i.afterUpdate(),i.minSize},afterUpdate:l,beforeSetDimensions:l,setDimensions:function(){var t=this;t.isHorizontal()?(t.width=t.maxWidth,t.left=0,t.right=t.width):(t.height=t.maxHeight,t.top=0,t.bottom=t.height),t.paddingLeft=0,t.paddingTop=0,t.paddingRight=0,t.paddingBottom=0,t.minSize={width:0,height:0}},afterSetDimensions:l,beforeBuildLabels:l,buildLabels:l,afterBuildLabels:l,beforeFit:l,fit:function(){var t=this,e=a.valueOrDefault,n=t.options,i=n.display,o=e(n.fontSize,r.global.defaultFontSize),s=t.minSize,l=a.isArray(n.text)?n.text.length:1,u=a.options.toLineHeight(n.lineHeight,o),c=i?l*u+2*n.padding:0;t.isHorizontal()?(s.width=t.maxWidth,s.height=c):(s.width=c,s.height=t.maxHeight),t.width=s.width,t.height=s.height},afterFit:l,isHorizontal:function(){var t=this.options.position;return"top"===t||"bottom"===t},draw:function(){var t=this,e=t.ctx,n=a.valueOrDefault,i=t.options,o=r.global;if(i.display){var s,l,u,c=n(i.fontSize,o.defaultFontSize),h=n(i.fontStyle,o.defaultFontStyle),d=n(i.fontFamily,o.defaultFontFamily),f=a.fontString(c,h,d),p=a.options.toLineHeight(i.lineHeight,c),g=p/2+i.padding,m=0,v=t.top,y=t.left,b=t.bottom,_=t.right;e.fillStyle=n(i.fontColor,o.defaultFontColor),e.font=f,t.isHorizontal()?(l=y+(_-y)/2,u=v+g,s=_-y):(l="left"===i.position?y+g:_-g,u=v+(b-v)/2,s=b-v,m=Math.PI*("left"===i.position?-.5:.5)),e.save(),e.translate(l,u),e.rotate(m),e.textAlign="center",e.textBaseline="middle";var x=i.text;if(a.isArray(x))for(var w=0,D=0;D<x.length;++D)e.fillText(x[D],0,w,s),w+=p;else e.fillText(x,0,0,s);e.restore()}}});e.exports={id:"title",_element:u,beforeInit:function(t){var e=t.options.title;e&&i(t,e)},beforeUpdate:function(t){var e=t.options.title,n=t.titleBlock;e?(a.mergeIf(e,r.global.title),n?(s.configure(t,n,e),n.options=e):i(t,e)):n&&(s.removeBox(t,n),delete t.titleBlock)}}},{25:25,26:26,30:30,45:45}],53:[function(t,e,n){"use strict";e.exports=function(t){var e={position:"bottom"},n=t.Scale.extend({getLabels:function(){var t=this.chart.data;return this.options.labels||(this.isHorizontal()?t.xLabels:t.yLabels)||t.labels},determineDataLimits:function(){var t=this,e=t.getLabels();t.minIndex=0,t.maxIndex=e.length-1;var n;void 0!==t.options.ticks.min&&(n=e.indexOf(t.options.ticks.min),t.minIndex=n!==-1?n:t.minIndex),void 0!==t.options.ticks.max&&(n=e.indexOf(t.options.ticks.max),t.maxIndex=n!==-1?n:t.maxIndex),t.min=e[t.minIndex],t.max=e[t.maxIndex]},buildTicks:function(){var t=this,e=t.getLabels();t.ticks=0===t.minIndex&&t.maxIndex===e.length-1?e:e.slice(t.minIndex,t.maxIndex+1)},getLabelForIndex:function(t,e){var n=this,i=n.chart.data,r=n.isHorizontal();return i.yLabels&&!r?n.getRightValue(i.datasets[e].data[t]):n.ticks[t-n.minIndex]},getPixelForValue:function(t,e){var n,i=this,r=i.options.offset,o=Math.max(i.maxIndex+1-i.minIndex-(r?0:1),1);if(void 0!==t&&null!==t&&(n=i.isHorizontal()?t.x:t.y),void 0!==n||void 0!==t&&isNaN(e)){var a=i.getLabels();t=n||t;var s=a.indexOf(t);e=s!==-1?s:e}if(i.isHorizontal()){var l=i.width/o,u=l*(e-i.minIndex);return r&&(u+=l/2),i.left+Math.round(u)}var c=i.height/o,h=c*(e-i.minIndex);return r&&(h+=c/2),i.top+Math.round(h)},getPixelForTick:function(t){return this.getPixelForValue(this.ticks[t],t+this.minIndex,null)},getValueForPixel:function(t){var e,n=this,i=n.options.offset,r=Math.max(n._ticks.length-(i?0:1),1),o=n.isHorizontal(),a=(o?n.width:n.height)/r;return t-=o?n.left:n.top,i&&(t-=a/2),e=t<=0?0:Math.round(t/a),e+n.minIndex},getBasePixel:function(){return this.bottom}});t.scaleService.registerScaleType("category",n,e)}},{}],54:[function(t,e,n){"use strict";var i=t(25),r=t(45),o=t(34);e.exports=function(t){var e={position:"left",ticks:{callback:o.formatters.linear}},n=t.LinearScaleBase.extend({determineDataLimits:function(){function t(t){return s?t.xAxisID===e.id:t.yAxisID===e.id}var e=this,n=e.options,i=e.chart,o=i.data,a=o.datasets,s=e.isHorizontal(),l=0,u=1;e.min=null,e.max=null;var c=n.stacked;if(void 0===c&&r.each(a,function(e,n){if(!c){var r=i.getDatasetMeta(n);i.isDatasetVisible(n)&&t(r)&&void 0!==r.stack&&(c=!0)}}),n.stacked||c){var h={};r.each(a,function(o,a){var s=i.getDatasetMeta(a),l=[s.type,void 0===n.stacked&&void 0===s.stack?a:"",s.stack].join(".");void 0===h[l]&&(h[l]={positiveValues:[],negativeValues:[]});var u=h[l].positiveValues,c=h[l].negativeValues;i.isDatasetVisible(a)&&t(s)&&r.each(o.data,function(t,i){var r=+e.getRightValue(t);isNaN(r)||s.data[i].hidden||(u[i]=u[i]||0,c[i]=c[i]||0,n.relativePoints?u[i]=100:r<0?c[i]+=r:u[i]+=r)})}),r.each(h,function(t){var n=t.positiveValues.concat(t.negativeValues),i=r.min(n),o=r.max(n);e.min=null===e.min?i:Math.min(e.min,i),e.max=null===e.max?o:Math.max(e.max,o)})}else r.each(a,function(n,o){var a=i.getDatasetMeta(o);i.isDatasetVisible(o)&&t(a)&&r.each(n.data,function(t,n){var i=+e.getRightValue(t);isNaN(i)||a.data[n].hidden||(null===e.min?e.min=i:i<e.min&&(e.min=i),null===e.max?e.max=i:i>e.max&&(e.max=i))})});e.min=isFinite(e.min)&&!isNaN(e.min)?e.min:l,e.max=isFinite(e.max)&&!isNaN(e.max)?e.max:u,this.handleTickRangeOptions()},getTickLimit:function(){var t,e=this,n=e.options.ticks;if(e.isHorizontal())t=Math.min(n.maxTicksLimit?n.maxTicksLimit:11,Math.ceil(e.width/50));else{var o=r.valueOrDefault(n.fontSize,i.global.defaultFontSize);t=Math.min(n.maxTicksLimit?n.maxTicksLimit:11,Math.ceil(e.height/(2*o)))}return t},handleDirectionalChanges:function(){this.isHorizontal()||this.ticks.reverse()},getLabelForIndex:function(t,e){return+this.getRightValue(this.chart.data.datasets[e].data[t])},getPixelForValue:function(t){var e,n=this,i=n.start,r=+n.getRightValue(t),o=n.end-i;return e=n.isHorizontal()?n.left+n.width/o*(r-i):n.bottom-n.height/o*(r-i)},getValueForPixel:function(t){var e=this,n=e.isHorizontal(),i=n?e.width:e.height,r=(n?t-e.left:e.bottom-t)/i;return e.start+(e.end-e.start)*r},getPixelForTick:function(t){return this.getPixelForValue(this.ticksAsNumbers[t])}});t.scaleService.registerScaleType("linear",n,e)}},{25:25,34:34,45:45}],55:[function(t,e,n){"use strict";function i(t,e){var n,i=[];if(t.stepSize&&t.stepSize>0)n=t.stepSize;else{var o=r.niceNum(e.max-e.min,!1);n=r.niceNum(o/(t.maxTicks-1),!0)}var a=Math.floor(e.min/n)*n,s=Math.ceil(e.max/n)*n;t.min&&t.max&&t.stepSize&&r.almostWhole((t.max-t.min)/t.stepSize,n/1e3)&&(a=t.min,s=t.max);var l=(s-a)/n;l=r.almostEquals(l,Math.round(l),n/1e3)?Math.round(l):Math.ceil(l);var u=1;n<1&&(u=Math.pow(10,n.toString().length-2),a=Math.round(a*u)/u,s=Math.round(s*u)/u),i.push(void 0!==t.min?t.min:a);for(var c=1;c<l;++c)i.push(Math.round((a+c*n)*u)/u);return i.push(void 0!==t.max?t.max:s),i}var r=t(45);e.exports=function(t){var e=r.noop;t.LinearScaleBase=t.Scale.extend({getRightValue:function(e){return"string"==typeof e?+e:t.Scale.prototype.getRightValue.call(this,e)},handleTickRangeOptions:function(){var t=this,e=t.options,n=e.ticks;if(n.beginAtZero){var i=r.sign(t.min),o=r.sign(t.max);i<0&&o<0?t.max=0:i>0&&o>0&&(t.min=0)}var a=void 0!==n.min||void 0!==n.suggestedMin,s=void 0!==n.max||void 0!==n.suggestedMax;void 0!==n.min?t.min=n.min:void 0!==n.suggestedMin&&(null===t.min?t.min=n.suggestedMin:t.min=Math.min(t.min,n.suggestedMin)),void 0!==n.max?t.max=n.max:void 0!==n.suggestedMax&&(null===t.max?t.max=n.suggestedMax:t.max=Math.max(t.max,n.suggestedMax)),a!==s&&t.min>=t.max&&(a?t.max=t.min+1:t.min=t.max-1),t.min===t.max&&(t.max++,n.beginAtZero||t.min--)},getTickLimit:e,handleDirectionalChanges:e,buildTicks:function(){var t=this,e=t.options,n=e.ticks,o=t.getTickLimit();o=Math.max(2,o);var a={maxTicks:o,min:n.min,max:n.max,stepSize:r.valueOrDefault(n.fixedStepSize,n.stepSize)},s=t.ticks=i(a,t);t.handleDirectionalChanges(),t.max=r.max(s),t.min=r.min(s),n.reverse?(s.reverse(),t.start=t.max,t.end=t.min):(t.start=t.min,t.end=t.max)},convertTicksToLabels:function(){var e=this;e.ticksAsNumbers=e.ticks.slice(),e.zeroLineIndex=e.ticks.indexOf(0),t.Scale.prototype.convertTicksToLabels.call(e)}})}},{45:45}],56:[function(t,e,n){"use strict";function i(t,e){var n,i,o=[],a=r.valueOrDefault,s=a(t.min,Math.pow(10,Math.floor(r.log10(e.min)))),l=Math.floor(r.log10(e.max)),u=Math.ceil(e.max/Math.pow(10,l));0===s?(n=Math.floor(r.log10(e.minNotZero)),i=Math.floor(e.minNotZero/Math.pow(10,n)),o.push(s),s=i*Math.pow(10,n)):(n=Math.floor(r.log10(s)),i=Math.floor(s/Math.pow(10,n)));var c=n<0?Math.pow(10,Math.abs(n)):1;do o.push(s),++i,10===i&&(i=1,++n,c=n>=0?1:c),s=Math.round(i*Math.pow(10,n)*c)/c;while(n<l||n===l&&i<u);var h=a(t.max,s);return o.push(h),o}var r=t(45),o=t(34);e.exports=function(t){var e={position:"left",ticks:{callback:o.formatters.logarithmic}},n=t.Scale.extend({determineDataLimits:function(){function t(t){return s?t.xAxisID===e.id:t.yAxisID===e.id}var e=this,n=e.options,i=e.chart,o=i.data,a=o.datasets,s=e.isHorizontal();e.min=null,e.max=null,e.minNotZero=null;var l=n.stacked;if(void 0===l&&r.each(a,function(e,n){if(!l){var r=i.getDatasetMeta(n);i.isDatasetVisible(n)&&t(r)&&void 0!==r.stack&&(l=!0)}}),n.stacked||l){var u={};r.each(a,function(o,a){var s=i.getDatasetMeta(a),l=[s.type,void 0===n.stacked&&void 0===s.stack?a:"",s.stack].join(".");i.isDatasetVisible(a)&&t(s)&&(void 0===u[l]&&(u[l]=[]),r.each(o.data,function(t,n){var i=u[l],r=+e.getRightValue(t);isNaN(r)||s.data[n].hidden||r<0||(i[n]=i[n]||0,i[n]+=r)}))}),r.each(u,function(t){if(t.length>0){var n=r.min(t),i=r.max(t);e.min=null===e.min?n:Math.min(e.min,n),e.max=null===e.max?i:Math.max(e.max,i)}})}else r.each(a,function(n,o){var a=i.getDatasetMeta(o);i.isDatasetVisible(o)&&t(a)&&r.each(n.data,function(t,n){var i=+e.getRightValue(t);isNaN(i)||a.data[n].hidden||i<0||(null===e.min?e.min=i:i<e.min&&(e.min=i),null===e.max?e.max=i:i>e.max&&(e.max=i),0!==i&&(null===e.minNotZero||i<e.minNotZero)&&(e.minNotZero=i))})});this.handleTickRangeOptions()},handleTickRangeOptions:function(){var t=this,e=t.options,n=e.ticks,i=r.valueOrDefault,o=1,a=10;t.min=i(n.min,t.min),t.max=i(n.max,t.max),t.min===t.max&&(0!==t.min&&null!==t.min?(t.min=Math.pow(10,Math.floor(r.log10(t.min))-1),t.max=Math.pow(10,Math.floor(r.log10(t.max))+1)):(t.min=o,t.max=a)),null===t.min&&(t.min=Math.pow(10,Math.floor(r.log10(t.max))-1)),null===t.max&&(t.max=0!==t.min?Math.pow(10,Math.floor(r.log10(t.min))+1):a),null===t.minNotZero&&(t.min>0?t.minNotZero=t.min:t.max<1?t.minNotZero=Math.pow(10,Math.floor(r.log10(t.max))):t.minNotZero=o)},buildTicks:function(){var t=this,e=t.options,n=e.ticks,o=!t.isHorizontal(),a={min:n.min,max:n.max},s=t.ticks=i(a,t);t.max=r.max(s),t.min=r.min(s),n.reverse?(o=!o,t.start=t.max,t.end=t.min):(t.start=t.min,t.end=t.max),o&&s.reverse()},convertTicksToLabels:function(){this.tickValues=this.ticks.slice(),t.Scale.prototype.convertTicksToLabels.call(this)},getLabelForIndex:function(t,e){return+this.getRightValue(this.chart.data.datasets[e].data[t])},getPixelForTick:function(t){return this.getPixelForValue(this.tickValues[t])},_getFirstTickValue:function(t){var e=Math.floor(r.log10(t)),n=Math.floor(t/Math.pow(10,e));return n*Math.pow(10,e)},getPixelForValue:function(e){var n,i,o,a,s,l=this,u=l.options.ticks.reverse,c=r.log10,h=l._getFirstTickValue(l.minNotZero),d=0;return e=+l.getRightValue(e),u?(o=l.end,a=l.start,s=-1):(o=l.start,a=l.end,s=1),l.isHorizontal()?(n=l.width,i=u?l.right:l.left):(n=l.height,s*=-1,i=u?l.top:l.bottom),e!==o&&(0===o&&(d=r.getValueOrDefault(l.options.ticks.fontSize,t.defaults.global.defaultFontSize),n-=d,o=h),0!==e&&(d+=n/(c(a)-c(o))*(c(e)-c(o))),i+=s*d),i},getValueForPixel:function(e){var n,i,o,a,s=this,l=s.options.ticks.reverse,u=r.log10,c=s._getFirstTickValue(s.minNotZero);if(l?(i=s.end,o=s.start):(i=s.start,o=s.end),s.isHorizontal()?(n=s.width,a=l?s.right-e:e-s.left):(n=s.height,a=l?e-s.top:s.bottom-e),a!==i){if(0===i){var h=r.getValueOrDefault(s.options.ticks.fontSize,t.defaults.global.defaultFontSize);a-=h,n-=h,i=c}a*=u(o)-u(i),a/=n,a=Math.pow(10,u(i)+a)}return a}});t.scaleService.registerScaleType("logarithmic",n,e)}},{34:34,45:45}],57:[function(t,e,n){"use strict";var i=t(25),r=t(45),o=t(34);e.exports=function(t){function e(t){var e=t.options;return e.angleLines.display||e.pointLabels.display?t.chart.data.labels.length:0}function n(t){var e=t.options.pointLabels,n=r.valueOrDefault(e.fontSize,m.defaultFontSize),i=r.valueOrDefault(e.fontStyle,m.defaultFontStyle),o=r.valueOrDefault(e.fontFamily,m.defaultFontFamily),a=r.fontString(n,i,o);return{size:n,style:i,family:o,font:a}}function a(t,e,n){return r.isArray(n)?{w:r.longestText(t,t.font,n),h:n.length*e+1.5*(n.length-1)*e}:{w:t.measureText(n).width,h:e}}function s(t,e,n,i,r){return t===i||t===r?{start:e-n/2,end:e+n/2}:t<i||t>r?{start:e-n-5,end:e}:{start:e,end:e+n+5}}function l(t){var i,o,l,u=n(t),c=Math.min(t.height/2,t.width/2),h={r:t.width,l:0,t:t.height,b:0},d={};t.ctx.font=u.font,t._pointLabelSizes=[];var f=e(t);for(i=0;i<f;i++){l=t.getPointPosition(i,c),o=a(t.ctx,u.size,t.pointLabels[i]||""),t._pointLabelSizes[i]=o;var p=t.getIndexAngle(i),g=r.toDegrees(p)%360,m=s(g,l.x,o.w,0,180),v=s(g,l.y,o.h,90,270);m.start<h.l&&(h.l=m.start,d.l=p),m.end>h.r&&(h.r=m.end,d.r=p),v.start<h.t&&(h.t=v.start,d.t=p),v.end>h.b&&(h.b=v.end,d.b=p)}t.setReductions(c,h,d)}function u(t){var e=Math.min(t.height/2,t.width/2);t.drawingArea=Math.round(e),t.setCenterPoint(0,0,0,0)}function c(t){return 0===t||180===t?"center":t<180?"left":"right"}function h(t,e,n,i){if(r.isArray(e))for(var o=n.y,a=1.5*i,s=0;s<e.length;++s)t.fillText(e[s],n.x,o),o+=a;else t.fillText(e,n.x,n.y)}function d(t,e,n){90===t||270===t?n.y-=e.h/2:(t>270||t<90)&&(n.y-=e.h)}function f(t){var i=t.ctx,o=t.options,a=o.angleLines,s=o.pointLabels;i.lineWidth=a.lineWidth,i.strokeStyle=a.color;var l=t.getDistanceFromCenterForValue(o.ticks.reverse?t.min:t.max),u=n(t);i.textBaseline="top";for(var f=e(t)-1;f>=0;f--){if(a.display){var p=t.getPointPosition(f,l);i.beginPath(),i.moveTo(t.xCenter,t.yCenter),i.lineTo(p.x,p.y),i.stroke(),i.closePath()}if(s.display){var g=t.getPointPosition(f,l+5),v=r.valueAtIndexOrDefault(s.fontColor,f,m.defaultFontColor);i.font=u.font,i.fillStyle=v;var y=t.getIndexAngle(f),b=r.toDegrees(y);i.textAlign=c(b),d(b,t._pointLabelSizes[f],g),h(i,t.pointLabels[f]||"",g,u.size)}}}function p(t,n,i,o){var a=t.ctx;if(a.strokeStyle=r.valueAtIndexOrDefault(n.color,o-1),a.lineWidth=r.valueAtIndexOrDefault(n.lineWidth,o-1),t.options.gridLines.circular)a.beginPath(),a.arc(t.xCenter,t.yCenter,i,0,2*Math.PI),a.closePath(),a.stroke();else{var s=e(t);if(0===s)return;a.beginPath();var l=t.getPointPosition(0,i);a.moveTo(l.x,l.y);for(var u=1;u<s;u++)l=t.getPointPosition(u,i),a.lineTo(l.x,l.y);a.closePath(),a.stroke()}}function g(t){return r.isNumber(t)?t:0}var m=i.global,v={display:!0,animate:!0,position:"chartArea",angleLines:{display:!0,color:"rgba(0, 0, 0, 0.1)",lineWidth:1},gridLines:{circular:!1},ticks:{showLabelBackdrop:!0,backdropColor:"rgba(255,255,255,0.75)",backdropPaddingY:2,backdropPaddingX:2,callback:o.formatters.linear},pointLabels:{display:!0,fontSize:10,callback:function(t){return t}}},y=t.LinearScaleBase.extend({setDimensions:function(){var t=this,e=t.options,n=e.ticks;t.width=t.maxWidth,t.height=t.maxHeight,t.xCenter=Math.round(t.width/2),t.yCenter=Math.round(t.height/2);var i=r.min([t.height,t.width]),o=r.valueOrDefault(n.fontSize,m.defaultFontSize);t.drawingArea=e.display?i/2-(o/2+n.backdropPaddingY):i/2},determineDataLimits:function(){var t=this,e=t.chart,n=Number.POSITIVE_INFINITY,i=Number.NEGATIVE_INFINITY;r.each(e.data.datasets,function(o,a){if(e.isDatasetVisible(a)){var s=e.getDatasetMeta(a);r.each(o.data,function(e,r){var o=+t.getRightValue(e);isNaN(o)||s.data[r].hidden||(n=Math.min(o,n),i=Math.max(o,i))})}}),t.min=n===Number.POSITIVE_INFINITY?0:n,t.max=i===Number.NEGATIVE_INFINITY?0:i,t.handleTickRangeOptions()},getTickLimit:function(){var t=this.options.ticks,e=r.valueOrDefault(t.fontSize,m.defaultFontSize);return Math.min(t.maxTicksLimit?t.maxTicksLimit:11,Math.ceil(this.drawingArea/(1.5*e)))},convertTicksToLabels:function(){var e=this;t.LinearScaleBase.prototype.convertTicksToLabels.call(e),e.pointLabels=e.chart.data.labels.map(e.options.pointLabels.callback,e)},getLabelForIndex:function(t,e){return+this.getRightValue(this.chart.data.datasets[e].data[t])},fit:function(){this.options.pointLabels.display?l(this):u(this)},setReductions:function(t,e,n){var i=this,r=e.l/Math.sin(n.l),o=Math.max(e.r-i.width,0)/Math.sin(n.r),a=-e.t/Math.cos(n.t),s=-Math.max(e.b-i.height,0)/Math.cos(n.b);r=g(r),o=g(o),a=g(a),s=g(s),i.drawingArea=Math.min(Math.round(t-(r+o)/2),Math.round(t-(a+s)/2)),i.setCenterPoint(r,o,a,s)},setCenterPoint:function(t,e,n,i){var r=this,o=r.width-e-r.drawingArea,a=t+r.drawingArea,s=n+r.drawingArea,l=r.height-i-r.drawingArea;r.xCenter=Math.round((a+o)/2+r.left),r.yCenter=Math.round((s+l)/2+r.top)},getIndexAngle:function(t){var n=2*Math.PI/e(this),i=this.chart.options&&this.chart.options.startAngle?this.chart.options.startAngle:0,r=i*Math.PI*2/360;return t*n+r},getDistanceFromCenterForValue:function(t){var e=this;if(null===t)return 0;var n=e.drawingArea/(e.max-e.min);return e.options.ticks.reverse?(e.max-t)*n:(t-e.min)*n},getPointPosition:function(t,e){var n=this,i=n.getIndexAngle(t)-Math.PI/2;return{x:Math.round(Math.cos(i)*e)+n.xCenter,y:Math.round(Math.sin(i)*e)+n.yCenter}},getPointPositionForValue:function(t,e){return this.getPointPosition(t,this.getDistanceFromCenterForValue(e))},getBasePosition:function(){var t=this,e=t.min,n=t.max;return t.getPointPositionForValue(0,t.beginAtZero?0:e<0&&n<0?n:e>0&&n>0?e:0)},draw:function(){var t=this,e=t.options,n=e.gridLines,i=e.ticks,o=r.valueOrDefault;if(e.display){var a=t.ctx,s=this.getIndexAngle(0),l=o(i.fontSize,m.defaultFontSize),u=o(i.fontStyle,m.defaultFontStyle),c=o(i.fontFamily,m.defaultFontFamily),h=r.fontString(l,u,c);r.each(t.ticks,function(e,r){if(r>0||i.reverse){var u=t.getDistanceFromCenterForValue(t.ticksAsNumbers[r]);if(n.display&&0!==r&&p(t,n,u,r),i.display){var c=o(i.fontColor,m.defaultFontColor);if(a.font=h,a.save(),a.translate(t.xCenter,t.yCenter),a.rotate(s),i.showLabelBackdrop){var d=a.measureText(e).width;a.fillStyle=i.backdropColor,a.fillRect(-d/2-i.backdropPaddingX,-u-l/2-i.backdropPaddingY,d+2*i.backdropPaddingX,l+2*i.backdropPaddingY)}a.textAlign="center",a.textBaseline="middle",a.fillStyle=c,a.fillText(e,0,-u),a.restore()}}}),(e.angleLines.display||e.pointLabels.display)&&f(t)}}});t.scaleService.registerScaleType("radialLinear",y,v)}},{25:25,34:34,45:45}],58:[function(t,e,n){"use strict";function i(t,e){return t-e}function r(t){var e,n,i,r={},o=[];for(e=0,n=t.length;e<n;++e)i=t[e],r[i]||(r[i]=!0,o.push(i));return o}function o(t,e,n,i){if("linear"===i||!t.length)return[{time:e,pos:0},{time:n,pos:1}];var r,o,a,s,l,u=[],c=[e];for(r=0,o=t.length;r<o;++r)s=t[r],s>e&&s<n&&c.push(s);for(c.push(n),r=0,o=c.length;r<o;++r)l=c[r+1],a=c[r-1],s=c[r],void 0!==a&&void 0!==l&&Math.round((l+a)/2)===s||u.push({time:s,pos:r/(o-1)});return u}function a(t,e,n){for(var i,r,o,a=0,s=t.length-1;a>=0&&a<=s;){if(i=a+s>>1,r=t[i-1]||null,o=t[i],!r)return{lo:null,hi:o};if(o[e]<n)a=i+1;else{if(!(r[e]>n))return{lo:r,hi:o};s=i-1}}return{lo:o,hi:null}}function s(t,e,n,i){var r=a(t,e,n),o=r.lo?r.hi?r.lo:t[t.length-2]:t[0],s=r.lo?r.hi?r.hi:t[t.length-1]:t[1],l=s[e]-o[e],u=l?(n-o[e])/l:0,c=(s[i]-o[i])*u;return o[i]+c}function l(t,e){var n=e.parser,i=e.parser||e.format;return"function"==typeof n?n(t):"string"==typeof t&&"string"==typeof i?y(t,i):(t instanceof y||(t=y(t)),t.isValid()?t:"function"==typeof i?i(t):t)}function u(t,e){if(_.isNullOrUndef(t))return null;var n=e.options.time,i=l(e.getRightValue(t),n);return i.isValid()?(n.round&&i.startOf(n.round),i.valueOf()):null}function c(t,e,n,i){var r,o,a,s=e-t,l=D[n],u=l.size,c=l.steps;if(!c)return Math.ceil(s/(i*u));for(r=0,o=c.length;r<o&&(a=c[r],!(Math.ceil(s/(u*a))<=i));++r);return a}function h(t,e,n,i){var r,o,a,s=S.length;for(r=S.indexOf(t);r<s-1;++r)if(o=D[S[r]],a=o.steps?o.steps[o.steps.length-1]:w,o.common&&Math.ceil((n-e)/(a*o.size))<=i)return S[r];return S[s-1]}function d(t,e,n,i){var r,o,a=y.duration(y(i).diff(y(n))),s=S.length;for(r=s-1;r>=S.indexOf(e);r--)if(o=S[r],D[o].common&&a.as(o)>=t.length)return o;return S[e?S.indexOf(e):0]}function f(t){for(var e=S.indexOf(t)+1,n=S.length;e<n;++e)if(D[S[e]].common)return S[e]}function p(t,e,n,i){var r,o=i.time,a=o.unit||h(o.minUnit,t,e,n),s=f(a),l=_.valueOrDefault(o.stepSize,o.unitStepSize),u="week"===a&&o.isoWeekday,d=i.ticks.major.enabled,p=D[a],g=y(t),m=y(e),v=[];for(l||(l=c(t,e,a,n)),u&&(g=g.isoWeekday(u),m=m.isoWeekday(u)),g=g.startOf(u?"day":a),m=m.startOf(u?"day":a),m<e&&m.add(1,a),r=y(g),d&&s&&!u&&!o.round&&(r.startOf(s),r.add(~~((g-r)/(p.size*l))*l,a));r<m;r.add(l,a))v.push(+r);return v.push(+r),v}function g(t,e,n,i,r){var o,a,l=0,u=0;return r.offset&&e.length&&(r.time.min||(o=e.length>1?e[1]:i,a=e[0],l=(s(t,"time",o,"pos")-s(t,"time",a,"pos"))/2),r.time.max||(o=e[e.length-1],a=e.length>1?e[e.length-2]:n,u=(s(t,"time",o,"pos")-s(t,"time",a,"pos"))/2)),{left:l,right:u}}function m(t,e){var n,i,r,o,a=[];for(n=0,i=t.length;n<i;++n)r=t[n],o=!!e&&r===+y(r).startOf(e),a.push({value:r,major:o});return a}function v(t,e){var n,i,r,o=t.length;for(n=0;n<o;n++){if(i=l(t[n],e),0!==i.millisecond())return"MMM D, YYYY h:mm:ss.SSS a";0===i.second()&&0===i.minute()&&0===i.hour()||(r=!0)}return r?"MMM D, YYYY h:mm:ss a":"MMM D, YYYY"}var y=t(1);y="function"==typeof y?y:window.moment;var b=t(25),_=t(45),x=Number.MIN_SAFE_INTEGER||-9007199254740991,w=Number.MAX_SAFE_INTEGER||9007199254740991,D={millisecond:{common:!0,size:1,steps:[1,2,5,10,20,50,100,250,500]},second:{common:!0,size:1e3,steps:[1,2,5,10,30]},minute:{common:!0,size:6e4,steps:[1,2,5,10,30]},hour:{common:!0,size:36e5,steps:[1,2,3,6,12]},day:{common:!0,size:864e5,steps:[1,2,5]},week:{common:!1,size:6048e5,steps:[1,2,3,4]},month:{common:!0,size:2628e6,steps:[1,2,3]},quarter:{common:!1,size:7884e6,steps:[1,2,3,4]},year:{common:!0,size:3154e7}},S=Object.keys(D);e.exports=function(t){var e={position:"bottom",distribution:"linear",bounds:"data",time:{parser:!1,format:!1,unit:!1,round:!1,displayFormat:!1,isoWeekday:!1,minUnit:"millisecond",displayFormats:{millisecond:"h:mm:ss.SSS a",second:"h:mm:ss a",minute:"h:mm a",hour:"hA",day:"MMM D",week:"ll",month:"MMM YYYY",quarter:"[Q]Q - YYYY",year:"YYYY"}},ticks:{autoSkip:!1,source:"auto",major:{enabled:!1}}},n=t.Scale.extend({initialize:function(){if(!y)throw new Error("Chart.js - Moment.js could not be found! You must include it before Chart.js to use the time scale. Download at https://momentjs.com");this.mergeTicksOptions(),t.Scale.prototype.initialize.call(this)},update:function(){var e=this,n=e.options;return n.time&&n.time.format&&console.warn("options.time.format is deprecated and replaced by options.time.parser."),t.Scale.prototype.update.apply(e,arguments)},getRightValue:function(e){return e&&void 0!==e.t&&(e=e.t),t.Scale.prototype.getRightValue.call(this,e)},determineDataLimits:function(){var t,e,n,o,a,s,l=this,c=l.chart,h=l.options.time,d=h.unit||"day",f=w,p=x,g=[],m=[],v=[];for(t=0,n=c.data.labels.length;t<n;++t)v.push(u(c.data.labels[t],l));for(t=0,n=(c.data.datasets||[]).length;t<n;++t)if(c.isDatasetVisible(t))if(a=c.data.datasets[t].data,_.isObject(a[0]))for(m[t]=[],e=0,o=a.length;e<o;++e)s=u(a[e],l),g.push(s),m[t][e]=s;else g.push.apply(g,v),m[t]=v.slice(0);else m[t]=[];v.length&&(v=r(v).sort(i),f=Math.min(f,v[0]),p=Math.max(p,v[v.length-1])),g.length&&(g=r(g).sort(i),f=Math.min(f,g[0]),p=Math.max(p,g[g.length-1])),f=u(h.min,l)||f,p=u(h.max,l)||p,f=f===w?+y().startOf(d):f,p=p===x?+y().endOf(d)+1:p,l.min=Math.min(f,p),l.max=Math.max(f+1,p),l._horizontal=l.isHorizontal(),l._table=[],l._timestamps={data:g,datasets:m,labels:v}},buildTicks:function(){var t,e,n,i=this,r=i.min,a=i.max,s=i.options,l=s.time,c=[],h=[];switch(s.ticks.source){case"data":c=i._timestamps.data;break;case"labels":c=i._timestamps.labels;break;case"auto":default:c=p(r,a,i.getLabelCapacity(r),s)}for("ticks"===s.bounds&&c.length&&(r=c[0],a=c[c.length-1]),r=u(l.min,i)||r,a=u(l.max,i)||a,t=0,e=c.length;t<e;++t)n=c[t],n>=r&&n<=a&&h.push(n);return i.min=r,i.max=a,i._unit=l.unit||d(h,l.minUnit,i.min,i.max),i._majorUnit=f(i._unit),i._table=o(i._timestamps.data,r,a,s.distribution),i._offsets=g(i._table,h,r,a,s),i._labelFormat=v(i._timestamps.data,l),m(h,i._majorUnit)},getLabelForIndex:function(t,e){var n=this,i=n.chart.data,r=n.options.time,o=i.labels&&t<i.labels.length?i.labels[t]:"",a=i.datasets[e].data[t];return _.isObject(a)&&(o=n.getRightValue(a)),r.tooltipFormat?l(o,r).format(r.tooltipFormat):"string"==typeof o?o:l(o,r).format(n._labelFormat)},tickFormatFunction:function(t,e,n,i){var r=this,o=r.options,a=t.valueOf(),s=o.time.displayFormats,l=s[r._unit],u=r._majorUnit,c=s[u],h=t.clone().startOf(u).valueOf(),d=o.ticks.major,f=d.enabled&&u&&c&&a===h,p=t.format(i?i:f?c:l),g=f?d:o.ticks.minor,m=_.valueOrDefault(g.callback,g.userCallback);return m?m(p,e,n):p},convertTicksToLabels:function(t){var e,n,i=[];for(e=0,n=t.length;e<n;++e)i.push(this.tickFormatFunction(y(t[e].value),e,t));return i},getPixelForOffset:function(t){var e=this,n=e._horizontal?e.width:e.height,i=e._horizontal?e.left:e.top,r=s(e._table,"time",t,"pos");return i+n*(e._offsets.left+r)/(e._offsets.left+1+e._offsets.right)},getPixelForValue:function(t,e,n){var i=this,r=null;if(void 0!==e&&void 0!==n&&(r=i._timestamps.datasets[n][e]),null===r&&(r=u(t,i)),null!==r)return i.getPixelForOffset(r)},getPixelForTick:function(t){var e=this.getTicks();return t>=0&&t<e.length?this.getPixelForOffset(e[t].value):null},getValueForPixel:function(t){var e=this,n=e._horizontal?e.width:e.height,i=e._horizontal?e.left:e.top,r=(n?(t-i)/n:0)*(e._offsets.left+1+e._offsets.left)-e._offsets.right,o=s(e._table,"pos",r,"time");return y(o)},getLabelWidth:function(t){var e=this,n=e.options.ticks,i=e.ctx.measureText(t).width,r=_.toRadians(n.maxRotation),o=Math.cos(r),a=Math.sin(r),s=_.valueOrDefault(n.fontSize,b.global.defaultFontSize);return i*o+s*a},getLabelCapacity:function(t){var e=this,n=e.options.time.displayFormats.millisecond,i=e.tickFormatFunction(y(t),0,[],n),r=e.getLabelWidth(i),o=e.isHorizontal()?e.width:e.height,a=Math.floor(o/r);return a>0?a:1}});t.scaleService.registerScaleType("time",n,e)}},{1:1,25:25,45:45}]},{},[7])(7)}),function(t){"use strict";"function"==typeof define&&define.amd?define(["jquery"],function(e){return t(e,window,document)}):"object"==typeof exports?module.exports=function(e,n){return e||(e=window),n||(n="undefined"!=typeof window?require("jquery"):require("jquery")(e)),t(n,e,e.document)}:t(jQuery,window,document)}(function(t,e,n,i){"use strict";function r(e){var n,i,o="a aa ai ao as b fn i m o s ",a={};t.each(e,function(t,s){n=t.match(/^([^A-Z]+?)([A-Z])/),n&&o.indexOf(n[1]+" ")!==-1&&(i=t.replace(n[0],n[2].toLowerCase()),a[i]=t,"o"===n[1]&&r(e[t]))}),e._hungarianMap=a}function o(e,n,a){e._hungarianMap||r(e);var s;t.each(n,function(r,l){s=e._hungarianMap[r],s===i||!a&&n[s]!==i||("o"===s.charAt(0)?(n[s]||(n[s]={}),t.extend(!0,n[s],n[r]),o(e[s],n[s],a)):n[s]=n[r])})}function a(t){var e=Xt.defaults.oLanguage,n=e.sDecimal;if(n&&Vt(n),t){var i=t.sZeroRecords;!t.sEmptyTable&&i&&"No data available in table"===e.sEmptyTable&&Pt(t,t,"sZeroRecords","sEmptyTable"),!t.sLoadingRecords&&i&&"Loading..."===e.sLoadingRecords&&Pt(t,t,"sZeroRecords","sLoadingRecords"),t.sInfoThousands&&(t.sThousands=t.sInfoThousands);var r=t.sDecimal;r&&n!==r&&Vt(r)}}function s(t){pe(t,"ordering","bSort"),pe(t,"orderMulti","bSortMulti"),pe(t,"orderClasses","bSortClasses"),pe(t,"orderCellsTop","bSortCellsTop"),pe(t,"order","aaSorting"),pe(t,"orderFixed","aaSortingFixed"),pe(t,"paging","bPaginate"),pe(t,"pagingType","sPaginationType"),pe(t,"pageLength","iDisplayLength"),pe(t,"searching","bFilter"),"boolean"==typeof t.sScrollX&&(t.sScrollX=t.sScrollX?"100%":""),"boolean"==typeof t.scrollX&&(t.scrollX=t.scrollX?"100%":"");var e=t.aoSearchCols;if(e)for(var n=0,i=e.length;n<i;n++)e[n]&&o(Xt.models.oSearch,e[n])}function l(e){pe(e,"orderable","bSortable"),pe(e,"orderData","aDataSort"),pe(e,"orderSequence","asSorting"),pe(e,"orderDataType","sortDataType");var n=e.aDataSort;"number"!=typeof n||t.isArray(n)||(e.aDataSort=[n])}function u(n){
if(!Xt.__browser){var i={};Xt.__browser=i;var r=t("<div/>").css({position:"fixed",top:0,left:t(e).scrollLeft()*-1,height:1,width:1,overflow:"hidden"}).append(t("<div/>").css({position:"absolute",top:1,left:1,width:100,overflow:"scroll"}).append(t("<div/>").css({width:"100%",height:10}))).appendTo("body"),o=r.children(),a=o.children();i.barWidth=o[0].offsetWidth-o[0].clientWidth,i.bScrollOversize=100===a[0].offsetWidth&&100!==o[0].clientWidth,i.bScrollbarLeft=1!==Math.round(a.offset().left),i.bBounding=!!r[0].getBoundingClientRect().width,r.remove()}t.extend(n.oBrowser,Xt.__browser),n.oScroll.iBarWidth=Xt.__browser.barWidth}function c(t,e,n,r,o,a){var s,l=r,u=!1;for(n!==i&&(s=n,u=!0);l!==o;)t.hasOwnProperty(l)&&(s=u?e(s,t[l],l,t):t[l],u=!0,l+=a);return s}function h(e,i){var r=Xt.defaults.column,o=e.aoColumns.length,a=t.extend({},Xt.models.oColumn,r,{nTh:i?i:n.createElement("th"),sTitle:r.sTitle?r.sTitle:i?i.innerHTML:"",aDataSort:r.aDataSort?r.aDataSort:[o],mData:r.mData?r.mData:o,idx:o});e.aoColumns.push(a);var s=e.aoPreSearchCols;s[o]=t.extend({},Xt.models.oSearch,s[o]),d(e,o,t(i).data())}function d(e,n,r){var a=e.aoColumns[n],s=e.oClasses,u=t(a.nTh);if(!a.sWidthOrig){a.sWidthOrig=u.attr("width")||null;var c=(u.attr("style")||"").match(/width:\s*(\d+[pxem%]+)/);c&&(a.sWidthOrig=c[1])}r!==i&&null!==r&&(l(r),o(Xt.defaults.column,r,!0),r.mDataProp===i||r.mData||(r.mData=r.mDataProp),r.sType&&(a._sManualType=r.sType),r.className&&!r.sClass&&(r.sClass=r.className),r.sClass&&u.addClass(r.sClass),t.extend(a,r),Pt(a,r,"sWidth","sWidthOrig"),r.iDataSort!==i&&(a.aDataSort=[r.iDataSort]),Pt(a,r,"aDataSort"));var h=a.mData,d=k(h),f=a.mRender?k(a.mRender):null,p=function(t){return"string"==typeof t&&t.indexOf("@")!==-1};a._bAttrSrc=t.isPlainObject(h)&&(p(h.sort)||p(h.type)||p(h.filter)),a._setter=null,a.fnGetData=function(t,e,n){var r=d(t,e,i,n);return f&&e?f(r,e,t,n):r},a.fnSetData=function(t,e,n){return A(h)(t,e,n)},"number"!=typeof h&&(e._rowReadObject=!0),e.oFeatures.bSort||(a.bSortable=!1,u.addClass(s.sSortableNone));var g=t.inArray("asc",a.asSorting)!==-1,m=t.inArray("desc",a.asSorting)!==-1;a.bSortable&&(g||m)?g&&!m?(a.sSortingClass=s.sSortableAsc,a.sSortingClassJUI=s.sSortJUIAscAllowed):!g&&m?(a.sSortingClass=s.sSortableDesc,a.sSortingClassJUI=s.sSortJUIDescAllowed):(a.sSortingClass=s.sSortable,a.sSortingClassJUI=s.sSortJUI):(a.sSortingClass=s.sSortableNone,a.sSortingClassJUI="")}function f(t){if(t.oFeatures.bAutoWidth!==!1){var e=t.aoColumns;vt(t);for(var n=0,i=e.length;n<i;n++)e[n].nTh.style.width=e[n].sWidth}var r=t.oScroll;""===r.sY&&""===r.sX||gt(t),Rt(t,null,"column-sizing",[t])}function p(t,e){var n=v(t,"bVisible");return"number"==typeof n[e]?n[e]:null}function g(e,n){var i=v(e,"bVisible"),r=t.inArray(n,i);return r!==-1?r:null}function m(e){var n=0;return t.each(e.aoColumns,function(e,i){i.bVisible&&"none"!==t(i.nTh).css("display")&&n++}),n}function v(e,n){var i=[];return t.map(e.aoColumns,function(t,e){t[n]&&i.push(e)}),i}function y(t){var e,n,r,o,a,s,l,u,c,h=t.aoColumns,d=t.aoData,f=Xt.ext.type.detect;for(e=0,n=h.length;e<n;e++)if(l=h[e],c=[],!l.sType&&l._sManualType)l.sType=l._sManualType;else if(!l.sType){for(r=0,o=f.length;r<o;r++){for(a=0,s=d.length;a<s&&(c[a]===i&&(c[a]=S(t,a,e,"type")),u=f[r](c[a],t),u||r===f.length-1)&&"html"!==u;a++);if(u){l.sType=u;break}}l.sType||(l.sType="string")}}function b(e,n,r,o){var a,s,l,u,c,d,f,p=e.aoColumns;if(n)for(a=n.length-1;a>=0;a--){f=n[a];var g=f.targets!==i?f.targets:f.aTargets;for(t.isArray(g)||(g=[g]),l=0,u=g.length;l<u;l++)if("number"==typeof g[l]&&g[l]>=0){for(;p.length<=g[l];)h(e);o(g[l],f)}else if("number"==typeof g[l]&&g[l]<0)o(p.length+g[l],f);else if("string"==typeof g[l])for(c=0,d=p.length;c<d;c++)("_all"==g[l]||t(p[c].nTh).hasClass(g[l]))&&o(c,f)}if(r)for(a=0,s=r.length;a<s;a++)o(a,r[a])}function _(e,n,r,o){var a=e.aoData.length,s=t.extend(!0,{},Xt.models.oRow,{src:r?"dom":"data",idx:a});s._aData=n,e.aoData.push(s);for(var l=e.aoColumns,u=0,c=l.length;u<c;u++)l[u].sType=null;e.aiDisplayMaster.push(a);var h=e.rowIdFn(n);return h!==i&&(e.aIds[h]=s),!r&&e.oFeatures.bDeferRender||N(e,a,r,o),a}function x(e,n){var i;return n instanceof t||(n=t(n)),n.map(function(t,n){return i=P(e,n),_(e,i.data,n,i.cells)})}function w(t,e){return e._DT_RowIndex!==i?e._DT_RowIndex:null}function D(e,n,i){return t.inArray(i,e.aoData[n].anCells)}function S(t,e,n,r){var o=t.iDraw,a=t.aoColumns[n],s=t.aoData[e]._aData,l=a.sDefaultContent,u=a.fnGetData(s,r,{settings:t,row:e,col:n});if(u===i)return t.iDrawError!=o&&null===l&&(Mt(t,0,"Requested unknown parameter "+("function"==typeof a.mData?"{function}":"'"+a.mData+"'")+" for row "+e+", column "+n,4),t.iDrawError=o),l;if(u!==s&&null!==u||null===l||r===i){if("function"==typeof u)return u.call(s)}else u=l;return null===u&&"display"==r?"":u}function C(t,e,n,i){var r=t.aoColumns[n],o=t.aoData[e]._aData;r.fnSetData(o,i,{settings:t,row:e,col:n})}function T(e){return t.map(e.match(/(\\.|[^\.])+/g)||[""],function(t){return t.replace(/\\\./g,".")})}function k(e){if(t.isPlainObject(e)){var n={};return t.each(e,function(t,e){e&&(n[t]=k(e))}),function(t,e,r,o){var a=n[e]||n._;return a!==i?a(t,e,r,o):t}}if(null===e)return function(t){return t};if("function"==typeof e)return function(t,n,i,r){return e(t,n,i,r)};if("string"!=typeof e||e.indexOf(".")===-1&&e.indexOf("[")===-1&&e.indexOf("(")===-1)return function(t,n){return t[e]};var r=function(e,n,o){var a,s,l,u;if(""!==o)for(var c=T(o),h=0,d=c.length;h<d;h++){if(a=c[h].match(ge),s=c[h].match(me),a){if(c[h]=c[h].replace(ge,""),""!==c[h]&&(e=e[c[h]]),l=[],c.splice(0,h+1),u=c.join("."),t.isArray(e))for(var f=0,p=e.length;f<p;f++)l.push(r(e[f],n,u));var g=a[0].substring(1,a[0].length-1);e=""===g?l:l.join(g);break}if(s)c[h]=c[h].replace(me,""),e=e[c[h]]();else{if(null===e||e[c[h]]===i)return i;e=e[c[h]]}}return e};return function(t,n){return r(t,n,e)}}function A(e){if(t.isPlainObject(e))return A(e._);if(null===e)return function(){};if("function"==typeof e)return function(t,n,i){e(t,"set",n,i)};if("string"!=typeof e||e.indexOf(".")===-1&&e.indexOf("[")===-1&&e.indexOf("(")===-1)return function(t,n){t[e]=n};var n=function(e,r,o){for(var a,s,l,u,c,h=T(o),d=h[h.length-1],f=0,p=h.length-1;f<p;f++){if(s=h[f].match(ge),l=h[f].match(me),s){if(h[f]=h[f].replace(ge,""),e[h[f]]=[],a=h.slice(),a.splice(0,f+1),c=a.join("."),t.isArray(r))for(var g=0,m=r.length;g<m;g++)u={},n(u,r[g],c),e[h[f]].push(u);else e[h[f]]=r;return}l&&(h[f]=h[f].replace(me,""),e=e[h[f]](r)),null!==e[h[f]]&&e[h[f]]!==i||(e[h[f]]={}),e=e[h[f]]}d.match(me)?e=e[d.replace(me,"")](r):e[d.replace(ge,"")]=r};return function(t,i){return n(t,i,e)}}function I(t){return se(t.aoData,"_aData")}function E(t){t.aoData.length=0,t.aiDisplayMaster.length=0,t.aiDisplay.length=0,t.aIds={}}function O(t,e,n){for(var r=-1,o=0,a=t.length;o<a;o++)t[o]==e?r=o:t[o]>e&&t[o]--;r!=-1&&n===i&&t.splice(r,1)}function M(t,e,n,r){var o,a,s=t.aoData[e],l=function(n,i){for(;n.childNodes.length;)n.removeChild(n.firstChild);n.innerHTML=S(t,e,i,"display")};if("dom"!==n&&(n&&"auto"!==n||"dom"!==s.src)){var u=s.anCells;if(u)if(r!==i)l(u[r],r);else for(o=0,a=u.length;o<a;o++)l(u[o],o)}else s._aData=P(t,s,r,r===i?i:s._aData).data;s._aSortData=null,s._aFilterData=null;var c=t.aoColumns;if(r!==i)c[r].sType=null;else{for(o=0,a=c.length;o<a;o++)c[o].sType=null;L(t,s)}}function P(e,n,r,o){var a,s,l,u=[],c=n.firstChild,h=0,d=e.aoColumns,f=e._rowReadObject;o=o!==i?o:f?{}:[];var p=function(t,e){if("string"==typeof t){var n=t.indexOf("@");if(n!==-1){var i=t.substring(n+1),r=A(t);r(o,e.getAttribute(i))}}},g=function(e){if(r===i||r===h)if(s=d[h],l=t.trim(e.innerHTML),s&&s._bAttrSrc){var n=A(s.mData._);n(o,l),p(s.mData.sort,e),p(s.mData.type,e),p(s.mData.filter,e)}else f?(s._setter||(s._setter=A(s.mData)),s._setter(o,l)):o[h]=l;h++};if(c)for(;c;)a=c.nodeName.toUpperCase(),"TD"!=a&&"TH"!=a||(g(c),u.push(c)),c=c.nextSibling;else{u=n.anCells;for(var m=0,v=u.length;m<v;m++)g(u[m])}var y=n.firstChild?n:n.nTr;if(y){var b=y.getAttribute("id");b&&A(e.rowId)(o,b)}return{data:o,cells:u}}function N(e,i,r,o){var a,s,l,u,c,h,d=e.aoData[i],f=d._aData,p=[];if(null===d.nTr){for(a=r||n.createElement("tr"),d.nTr=a,d.anCells=p,a._DT_RowIndex=i,L(e,d),u=0,c=e.aoColumns.length;u<c;u++)l=e.aoColumns[u],h=!r,s=h?n.createElement(l.sCellType):o[u],s._DT_CellIndex={row:i,column:u},p.push(s),!h&&(r&&!l.mRender&&l.mData===u||t.isPlainObject(l.mData)&&l.mData._===u+".display")||(s.innerHTML=S(e,i,u,"display")),l.sClass&&(s.className+=" "+l.sClass),l.bVisible&&!r?a.appendChild(s):!l.bVisible&&r&&s.parentNode.removeChild(s),l.fnCreatedCell&&l.fnCreatedCell.call(e.oInstance,s,S(e,i,u),f,i,u);Rt(e,"aoRowCreatedCallback",null,[a,f,i,p])}d.nTr.setAttribute("role","row")}function L(e,n){var i=n.nTr,r=n._aData;if(i){var o=e.rowIdFn(r);if(o&&(i.id=o),r.DT_RowClass){var a=r.DT_RowClass.split(" ");n.__rowc=n.__rowc?fe(n.__rowc.concat(a)):a,t(i).removeClass(n.__rowc.join(" ")).addClass(r.DT_RowClass)}r.DT_RowAttr&&t(i).attr(r.DT_RowAttr),r.DT_RowData&&t(i).data(r.DT_RowData)}}function F(e){var n,i,r,o,a,s=e.nTHead,l=e.nTFoot,u=0===t("th, td",s).length,c=e.oClasses,h=e.aoColumns;for(u&&(o=t("<tr/>").appendTo(s)),n=0,i=h.length;n<i;n++)a=h[n],r=t(a.nTh).addClass(a.sClass),u&&r.appendTo(o),e.oFeatures.bSort&&(r.addClass(a.sSortingClass),a.bSortable!==!1&&(r.attr("tabindex",e.iTabIndex).attr("aria-controls",e.sTableId),Tt(e,a.nTh,n))),a.sTitle!=r[0].innerHTML&&r.html(a.sTitle),Ht(e,"header")(e,r,a,c);if(u&&B(e.aoHeader,s),t(s).find(">tr").attr("role","row"),t(s).find(">tr>th, >tr>td").addClass(c.sHeaderTH),t(l).find(">tr>th, >tr>td").addClass(c.sFooterTH),null!==l){var d=e.aoFooter[0];for(n=0,i=d.length;n<i;n++)a=h[n],a.nTf=d[n].cell,a.sClass&&t(a.nTf).addClass(a.sClass)}}function R(e,n,r){var o,a,s,l,u,c,h,d,f,p=[],g=[],m=e.aoColumns.length;if(n){for(r===i&&(r=!1),o=0,a=n.length;o<a;o++){for(p[o]=n[o].slice(),p[o].nTr=n[o].nTr,s=m-1;s>=0;s--)e.aoColumns[s].bVisible||r||p[o].splice(s,1);g.push([])}for(o=0,a=p.length;o<a;o++){if(h=p[o].nTr)for(;c=h.firstChild;)h.removeChild(c);for(s=0,l=p[o].length;s<l;s++)if(d=1,f=1,g[o][s]===i){for(h.appendChild(p[o][s].cell),g[o][s]=1;p[o+d]!==i&&p[o][s].cell==p[o+d][s].cell;)g[o+d][s]=1,d++;for(;p[o][s+f]!==i&&p[o][s].cell==p[o][s+f].cell;){for(u=0;u<d;u++)g[o+u][s+f]=1;f++}t(p[o][s].cell).attr("rowspan",d).attr("colspan",f)}}}}function j(e){var n=Rt(e,"aoPreDrawCallback","preDraw",[e]);if(t.inArray(!1,n)!==-1)return void ft(e,!1);var r=[],o=0,a=e.asStripeClasses,s=a.length,l=(e.aoOpenRows.length,e.oLanguage),u=e.iInitDisplayStart,c="ssp"==Wt(e),h=e.aiDisplay;e.bDrawing=!0,u!==i&&u!==-1&&(e._iDisplayStart=c?u:u>=e.fnRecordsDisplay()?0:u,e.iInitDisplayStart=-1);var d=e._iDisplayStart,f=e.fnDisplayEnd();if(e.bDeferLoading)e.bDeferLoading=!1,e.iDraw++,ft(e,!1);else if(c){if(!e.bDestroying&&!q(e))return}else e.iDraw++;if(0!==h.length)for(var p=c?0:d,g=c?e.aoData.length:f,v=p;v<g;v++){var y=h[v],b=e.aoData[y];null===b.nTr&&N(e,y);var _=b.nTr;if(0!==s){var x=a[o%s];b._sRowStripe!=x&&(t(_).removeClass(b._sRowStripe).addClass(x),b._sRowStripe=x)}Rt(e,"aoRowCallback",null,[_,b._aData,o,v,y]),r.push(_),o++}else{var w=l.sZeroRecords;1==e.iDraw&&"ajax"==Wt(e)?w=l.sLoadingRecords:l.sEmptyTable&&0===e.fnRecordsTotal()&&(w=l.sEmptyTable),r[0]=t("<tr/>",{"class":s?a[0]:""}).append(t("<td />",{valign:"top",colSpan:m(e),"class":e.oClasses.sRowEmpty}).html(w))[0]}Rt(e,"aoHeaderCallback","header",[t(e.nTHead).children("tr")[0],I(e),d,f,h]),Rt(e,"aoFooterCallback","footer",[t(e.nTFoot).children("tr")[0],I(e),d,f,h]);var D=t(e.nTBody);D.children().detach(),D.append(t(r)),Rt(e,"aoDrawCallback","draw",[e]),e.bSorted=!1,e.bFiltered=!1,e.bDrawing=!1}function H(t,e){var n=t.oFeatures,i=n.bSort,r=n.bFilter;i&&Dt(t),r?K(t,t.oPreviousSearch):t.aiDisplay=t.aiDisplayMaster.slice(),e!==!0&&(t._iDisplayStart=0),t._drawHold=e,j(t),t._drawHold=!1}function W(e){var n=e.oClasses,i=t(e.nTable),r=t("<div/>").insertBefore(i),o=e.oFeatures,a=t("<div/>",{id:e.sTableId+"_wrapper","class":n.sWrapper+(e.nTFoot?"":" "+n.sNoFooter)});e.nHolding=r[0],e.nTableWrapper=a[0],e.nTableReinsertBefore=e.nTable.nextSibling;for(var s,l,u,c,h,d,f=e.sDom.split(""),p=0;p<f.length;p++){if(s=null,l=f[p],"<"==l){if(u=t("<div/>")[0],c=f[p+1],"'"==c||'"'==c){for(h="",d=2;f[p+d]!=c;)h+=f[p+d],d++;if("H"==h?h=n.sJUIHeader:"F"==h&&(h=n.sJUIFooter),h.indexOf(".")!=-1){var g=h.split(".");u.id=g[0].substr(1,g[0].length-1),u.className=g[1]}else"#"==h.charAt(0)?u.id=h.substr(1,h.length-1):u.className=h;p+=d}a.append(u),a=t(u)}else if(">"==l)a=a.parent();else if("l"==l&&o.bPaginate&&o.bLengthChange)s=ut(e);else if("f"==l&&o.bFilter)s=X(e);else if("r"==l&&o.bProcessing)s=dt(e);else if("t"==l)s=pt(e);else if("i"==l&&o.bInfo)s=it(e);else if("p"==l&&o.bPaginate)s=ct(e);else if(0!==Xt.ext.feature.length)for(var m=Xt.ext.feature,v=0,y=m.length;v<y;v++)if(l==m[v].cFeature){s=m[v].fnInit(e);break}if(s){var b=e.aanFeatures;b[l]||(b[l]=[]),b[l].push(s),a.append(s)}}r.replaceWith(a),e.nHolding=null}function B(e,n){var i,r,o,a,s,l,u,c,h,d,f,p=t(n).children("tr"),g=function(t,e,n){for(var i=t[e];i[n];)n++;return n};for(e.splice(0,e.length),o=0,l=p.length;o<l;o++)e.push([]);for(o=0,l=p.length;o<l;o++)for(i=p[o],c=0,r=i.firstChild;r;){if("TD"==r.nodeName.toUpperCase()||"TH"==r.nodeName.toUpperCase())for(h=1*r.getAttribute("colspan"),d=1*r.getAttribute("rowspan"),h=h&&0!==h&&1!==h?h:1,d=d&&0!==d&&1!==d?d:1,u=g(e,o,c),f=1===h,s=0;s<h;s++)for(a=0;a<d;a++)e[o+a][u+s]={cell:r,unique:f},e[o+a].nTr=i;r=r.nextSibling}}function V(t,e,n){var i=[];n||(n=t.aoHeader,e&&(n=[],B(n,e)));for(var r=0,o=n.length;r<o;r++)for(var a=0,s=n[r].length;a<s;a++)!n[r][a].unique||i[a]&&t.bSortCellsTop||(i[a]=n[r][a].cell);return i}function U(e,n,i){if(Rt(e,"aoServerParams","serverParams",[n]),n&&t.isArray(n)){var r={},o=/(.*?)\[\]$/;t.each(n,function(t,e){var n=e.name.match(o);if(n){var i=n[0];r[i]||(r[i]=[]),r[i].push(e.value)}else r[e.name]=e.value}),n=r}var a,s=e.ajax,l=e.oInstance,u=function(t){Rt(e,null,"xhr",[e,t,e.jqXHR]),i(t)};if(t.isPlainObject(s)&&s.data){a=s.data;var c="function"==typeof a?a(n,e):a;n="function"==typeof a&&c?c:t.extend(!0,n,c),delete s.data}var h={data:n,success:function(t){var n=t.error||t.sError;n&&Mt(e,0,n),e.json=t,u(t)},dataType:"json",cache:!1,type:e.sServerMethod,error:function(n,i,r){var o=Rt(e,null,"xhr",[e,null,e.jqXHR]);t.inArray(!0,o)===-1&&("parsererror"==i?Mt(e,0,"Invalid JSON response",1):4===n.readyState&&Mt(e,0,"Ajax error",7)),ft(e,!1)}};e.oAjaxData=n,Rt(e,null,"preXhr",[e,n]),e.fnServerData?e.fnServerData.call(l,e.sAjaxSource,t.map(n,function(t,e){return{name:e,value:t}}),u,e):e.sAjaxSource||"string"==typeof s?e.jqXHR=t.ajax(t.extend(h,{url:s||e.sAjaxSource})):"function"==typeof s?e.jqXHR=s.call(l,n,u,e):(e.jqXHR=t.ajax(t.extend(h,s)),s.data=a)}function q(t){return!t.bAjaxDataGet||(t.iDraw++,ft(t,!0),U(t,z(t),function(e){Y(t,e)}),!1)}function z(e){var n,i,r,o,a=e.aoColumns,s=a.length,l=e.oFeatures,u=e.oPreviousSearch,c=e.aoPreSearchCols,h=[],d=wt(e),f=e._iDisplayStart,p=l.bPaginate!==!1?e._iDisplayLength:-1,g=function(t,e){h.push({name:t,value:e})};g("sEcho",e.iDraw),g("iColumns",s),g("sColumns",se(a,"sName").join(",")),g("iDisplayStart",f),g("iDisplayLength",p);var m={draw:e.iDraw,columns:[],order:[],start:f,length:p,search:{value:u.sSearch,regex:u.bRegex}};for(n=0;n<s;n++)r=a[n],o=c[n],i="function"==typeof r.mData?"function":r.mData,m.columns.push({data:i,name:r.sName,searchable:r.bSearchable,orderable:r.bSortable,search:{value:o.sSearch,regex:o.bRegex}}),g("mDataProp_"+n,i),l.bFilter&&(g("sSearch_"+n,o.sSearch),g("bRegex_"+n,o.bRegex),g("bSearchable_"+n,r.bSearchable)),l.bSort&&g("bSortable_"+n,r.bSortable);l.bFilter&&(g("sSearch",u.sSearch),g("bRegex",u.bRegex)),l.bSort&&(t.each(d,function(t,e){m.order.push({column:e.col,dir:e.dir}),g("iSortCol_"+t,e.col),g("sSortDir_"+t,e.dir)}),g("iSortingCols",d.length));var v=Xt.ext.legacy.ajax;return null===v?e.sAjaxSource?h:m:v?h:m}function Y(t,e){var n=function(t,n){return e[t]!==i?e[t]:e[n]},r=G(t,e),o=n("sEcho","draw"),a=n("iTotalRecords","recordsTotal"),s=n("iTotalDisplayRecords","recordsFiltered");if(o){if(1*o<t.iDraw)return;t.iDraw=1*o}E(t),t._iRecordsTotal=parseInt(a,10),t._iRecordsDisplay=parseInt(s,10);for(var l=0,u=r.length;l<u;l++)_(t,r[l]);t.aiDisplay=t.aiDisplayMaster.slice(),t.bAjaxDataGet=!1,j(t),t._bInitComplete||st(t,e),t.bAjaxDataGet=!0,ft(t,!1)}function G(e,n){var r=t.isPlainObject(e.ajax)&&e.ajax.dataSrc!==i?e.ajax.dataSrc:e.sAjaxDataProp;return"data"===r?n.aaData||n[r]:""!==r?k(r)(n):n}function X(e){var i=e.oClasses,r=e.sTableId,o=e.oLanguage,a=e.oPreviousSearch,s=e.aanFeatures,l='<input type="search" class="'+i.sFilterInput+'"/>',u=o.sSearch;u=u.match(/_INPUT_/)?u.replace("_INPUT_",l):u+l;var c=t("<div/>",{id:s.f?null:r+"_filter","class":i.sFilter}).append(t("<label/>").append(u)),h=function(){var t=(s.f,this.value?this.value:"");t!=a.sSearch&&(K(e,{sSearch:t,bRegex:a.bRegex,bSmart:a.bSmart,bCaseInsensitive:a.bCaseInsensitive}),e._iDisplayStart=0,j(e))},d=null!==e.searchDelay?e.searchDelay:"ssp"===Wt(e)?400:0,f=t("input",c).val(a.sSearch).attr("placeholder",o.sSearchPlaceholder).on("keyup.DT search.DT input.DT paste.DT cut.DT",d?xe(h,d):h).on("keypress.DT",function(t){if(13==t.keyCode)return!1}).attr("aria-controls",r);return t(e.nTable).on("search.dt.DT",function(t,i){if(e===i)try{f[0]!==n.activeElement&&f.val(a.sSearch)}catch(r){}}),c[0]}function K(t,e,n){var r=t.oPreviousSearch,o=t.aoPreSearchCols,a=function(t){r.sSearch=t.sSearch,r.bRegex=t.bRegex,r.bSmart=t.bSmart,r.bCaseInsensitive=t.bCaseInsensitive},s=function(t){return t.bEscapeRegex!==i?!t.bEscapeRegex:t.bRegex};if(y(t),"ssp"!=Wt(t)){J(t,e.sSearch,n,s(e),e.bSmart,e.bCaseInsensitive),a(e);for(var l=0;l<o.length;l++)Q(t,o[l].sSearch,l,s(o[l]),o[l].bSmart,o[l].bCaseInsensitive);$(t)}else a(e);t.bFiltered=!0,Rt(t,null,"search",[t])}function $(e){for(var n,i,r=Xt.ext.search,o=e.aiDisplay,a=0,s=r.length;a<s;a++){for(var l=[],u=0,c=o.length;u<c;u++)i=o[u],n=e.aoData[i],r[a](e,n._aFilterData,i,n._aData,u)&&l.push(i);o.length=0,t.merge(o,l)}}function Q(t,e,n,i,r,o){if(""!==e){for(var a,s=[],l=t.aiDisplay,u=Z(e,i,r,o),c=0;c<l.length;c++)a=t.aoData[l[c]]._aFilterData[n],u.test(a)&&s.push(l[c]);t.aiDisplay=s}}function J(t,e,n,i,r,o){var a,s,l,u=Z(e,i,r,o),c=t.oPreviousSearch.sSearch,h=t.aiDisplayMaster,d=[];if(0!==Xt.ext.search.length&&(n=!0),s=tt(t),e.length<=0)t.aiDisplay=h.slice();else{for((s||n||i||c.length>e.length||0!==e.indexOf(c)||t.bSorted)&&(t.aiDisplay=h.slice()),a=t.aiDisplay,l=0;l<a.length;l++)u.test(t.aoData[a[l]]._sFilterRow)&&d.push(a[l]);t.aiDisplay=d}}function Z(e,n,i,r){if(e=n?e:ve(e),i){var o=t.map(e.match(/"[^"]+"|[^ ]+/g)||[""],function(t){if('"'===t.charAt(0)){var e=t.match(/^"(.*)"$/);t=e?e[1]:t}return t.replace('"',"")});e="^(?=.*?"+o.join(")(?=.*?")+").*$"}return new RegExp(e,r?"i":"")}function tt(t){var e,n,i,r,o,a,s,l,u=t.aoColumns,c=Xt.ext.type.search,h=!1;for(n=0,r=t.aoData.length;n<r;n++)if(l=t.aoData[n],!l._aFilterData){for(a=[],i=0,o=u.length;i<o;i++)e=u[i],e.bSearchable?(s=S(t,n,i,"filter"),c[e.sType]&&(s=c[e.sType](s)),null===s&&(s=""),"string"!=typeof s&&s.toString&&(s=s.toString())):s="",s.indexOf&&s.indexOf("&")!==-1&&(ye.innerHTML=s,s=be?ye.textContent:ye.innerText),s.replace&&(s=s.replace(/[\r\n\u2028]/g,"")),a.push(s);l._aFilterData=a,l._sFilterRow=a.join("  "),h=!0}return h}function et(t){return{search:t.sSearch,smart:t.bSmart,regex:t.bRegex,caseInsensitive:t.bCaseInsensitive}}function nt(t){return{sSearch:t.search,bSmart:t.smart,bRegex:t.regex,bCaseInsensitive:t.caseInsensitive}}function it(e){var n=e.sTableId,i=e.aanFeatures.i,r=t("<div/>",{"class":e.oClasses.sInfo,id:i?null:n+"_info"});return i||(e.aoDrawCallback.push({fn:rt,sName:"information"}),r.attr("role","status").attr("aria-live","polite"),t(e.nTable).attr("aria-describedby",n+"_info")),r[0]}function rt(e){var n=e.aanFeatures.i;if(0!==n.length){var i=e.oLanguage,r=e._iDisplayStart+1,o=e.fnDisplayEnd(),a=e.fnRecordsTotal(),s=e.fnRecordsDisplay(),l=s?i.sInfo:i.sInfoEmpty;s!==a&&(l+=" "+i.sInfoFiltered),l+=i.sInfoPostFix,l=ot(e,l);var u=i.fnInfoCallback;null!==u&&(l=u.call(e.oInstance,e,r,o,a,s,l)),t(n).html(l)}}function ot(t,e){var n=t.fnFormatNumber,i=t._iDisplayStart+1,r=t._iDisplayLength,o=t.fnRecordsDisplay(),a=r===-1;return e.replace(/_START_/g,n.call(t,i)).replace(/_END_/g,n.call(t,t.fnDisplayEnd())).replace(/_MAX_/g,n.call(t,t.fnRecordsTotal())).replace(/_TOTAL_/g,n.call(t,o)).replace(/_PAGE_/g,n.call(t,a?1:Math.ceil(i/r))).replace(/_PAGES_/g,n.call(t,a?1:Math.ceil(o/r)))}function at(t){var e,n,i,r=t.iInitDisplayStart,o=t.aoColumns,a=t.oFeatures,s=t.bDeferLoading;if(!t.bInitialised)return void setTimeout(function(){at(t)},200);for(W(t),F(t),R(t,t.aoHeader),R(t,t.aoFooter),ft(t,!0),a.bAutoWidth&&vt(t),e=0,n=o.length;e<n;e++)i=o[e],i.sWidth&&(i.nTh.style.width=xt(i.sWidth));Rt(t,null,"preInit",[t]),H(t);var l=Wt(t);("ssp"!=l||s)&&("ajax"==l?U(t,[],function(n){var i=G(t,n);for(e=0;e<i.length;e++)_(t,i[e]);t.iInitDisplayStart=r,H(t),ft(t,!1),st(t,n)},t):(ft(t,!1),st(t)))}function st(t,e){t._bInitComplete=!0,(e||t.oInit.aaData)&&f(t),Rt(t,null,"plugin-init",[t,e]),Rt(t,"aoInitComplete","init",[t,e])}function lt(t,e){var n=parseInt(e,10);t._iDisplayLength=n,jt(t),Rt(t,null,"length",[t,n])}function ut(e){for(var n=e.oClasses,i=e.sTableId,r=e.aLengthMenu,o=t.isArray(r[0]),a=o?r[0]:r,s=o?r[1]:r,l=t("<select/>",{name:i+"_length","aria-controls":i,"class":n.sLengthSelect}),u=0,c=a.length;u<c;u++)l[0][u]=new Option("number"==typeof s[u]?e.fnFormatNumber(s[u]):s[u],a[u]);var h=t("<div><label/></div>").addClass(n.sLength);return e.aanFeatures.l||(h[0].id=i+"_length"),h.children().append(e.oLanguage.sLengthMenu.replace("_MENU_",l[0].outerHTML)),t("select",h).val(e._iDisplayLength).on("change.DT",function(n){lt(e,t(this).val()),j(e)}),t(e.nTable).on("length.dt.DT",function(n,i,r){e===i&&t("select",h).val(r)}),h[0]}function ct(e){var n=e.sPaginationType,i=Xt.ext.pager[n],r="function"==typeof i,o=function(t){j(t)},a=t("<div/>").addClass(e.oClasses.sPaging+n)[0],s=e.aanFeatures;return r||i.fnInit(e,a,o),s.p||(a.id=e.sTableId+"_paginate",e.aoDrawCallback.push({fn:function(t){if(r){var e,n,a=t._iDisplayStart,l=t._iDisplayLength,u=t.fnRecordsDisplay(),c=l===-1,h=c?0:Math.ceil(a/l),d=c?1:Math.ceil(u/l),f=i(h,d);for(e=0,n=s.p.length;e<n;e++)Ht(t,"pageButton")(t,s.p[e],e,f,h,d)}else i.fnUpdate(t,o)},sName:"pagination"})),a}function ht(t,e,n){var i=t._iDisplayStart,r=t._iDisplayLength,o=t.fnRecordsDisplay();0===o||r===-1?i=0:"number"==typeof e?(i=e*r,i>o&&(i=0)):"first"==e?i=0:"previous"==e?(i=r>=0?i-r:0,i<0&&(i=0)):"next"==e?i+r<o&&(i+=r):"last"==e?i=Math.floor((o-1)/r)*r:Mt(t,0,"Unknown paging action: "+e,5);var a=t._iDisplayStart!==i;return t._iDisplayStart=i,a&&(Rt(t,null,"page",[t]),n&&j(t)),a}function dt(e){return t("<div/>",{id:e.aanFeatures.r?null:e.sTableId+"_processing","class":e.oClasses.sProcessing}).html(e.oLanguage.sProcessing).insertBefore(e.nTable)[0]}function ft(e,n){e.oFeatures.bProcessing&&t(e.aanFeatures.r).css("display",n?"block":"none"),Rt(e,null,"processing",[e,n])}function pt(e){var n=t(e.nTable);n.attr("role","grid");var i=e.oScroll;if(""===i.sX&&""===i.sY)return e.nTable;var r=i.sX,o=i.sY,a=e.oClasses,s=n.children("caption"),l=s.length?s[0]._captionSide:null,u=t(n[0].cloneNode(!1)),c=t(n[0].cloneNode(!1)),h=n.children("tfoot"),d="<div/>",f=function(t){return t?xt(t):null};h.length||(h=null);var p=t(d,{"class":a.sScrollWrapper}).append(t(d,{"class":a.sScrollHead}).css({overflow:"hidden",position:"relative",border:0,width:r?f(r):"100%"}).append(t(d,{"class":a.sScrollHeadInner}).css({"box-sizing":"content-box",width:i.sXInner||"100%"}).append(u.removeAttr("id").css("margin-left",0).append("top"===l?s:null).append(n.children("thead"))))).append(t(d,{"class":a.sScrollBody}).css({position:"relative",overflow:"auto",width:f(r)}).append(n));h&&p.append(t(d,{"class":a.sScrollFoot}).css({overflow:"hidden",border:0,width:r?f(r):"100%"}).append(t(d,{"class":a.sScrollFootInner}).append(c.removeAttr("id").css("margin-left",0).append("bottom"===l?s:null).append(n.children("tfoot")))));var g=p.children(),m=g[0],v=g[1],y=h?g[2]:null;return r&&t(v).on("scroll.DT",function(t){var e=this.scrollLeft;m.scrollLeft=e,h&&(y.scrollLeft=e)}),t(v).css(o&&i.bCollapse?"max-height":"height",o),e.nScrollHead=m,e.nScrollBody=v,e.nScrollFoot=y,e.aoDrawCallback.push({fn:gt,sName:"scrolling"}),p[0]}function gt(e){var n,r,o,a,s,l,u,c,h,d=e.oScroll,g=d.sX,m=d.sXInner,v=d.sY,y=d.iBarWidth,b=t(e.nScrollHead),_=b[0].style,x=b.children("div"),w=x[0].style,D=x.children("table"),S=e.nScrollBody,C=t(S),T=S.style,k=t(e.nScrollFoot),A=k.children("div"),I=A.children("table"),E=t(e.nTHead),O=t(e.nTable),M=O[0],P=M.style,N=e.nTFoot?t(e.nTFoot):null,L=e.oBrowser,F=L.bScrollOversize,R=se(e.aoColumns,"nTh"),j=[],H=[],W=[],B=[],U=function(t){var e=t.style;e.paddingTop="0",e.paddingBottom="0",e.borderTopWidth="0",e.borderBottomWidth="0",e.height=0},q=S.scrollHeight>S.clientHeight;if(e.scrollBarVis!==q&&e.scrollBarVis!==i)return e.scrollBarVis=q,void f(e);e.scrollBarVis=q,O.children("thead, tfoot").remove(),N&&(l=N.clone().prependTo(O),r=N.find("tr"),a=l.find("tr")),s=E.clone().prependTo(O),n=E.find("tr"),o=s.find("tr"),s.find("th, td").removeAttr("tabindex"),g||(T.width="100%",b[0].style.width="100%"),t.each(V(e,s),function(t,n){u=p(e,t),n.style.width=e.aoColumns[u].sWidth}),N&&mt(function(t){t.style.width=""},a),h=O.outerWidth(),""===g?(P.width="100%",F&&(O.find("tbody").height()>S.offsetHeight||"scroll"==C.css("overflow-y"))&&(P.width=xt(O.outerWidth()-y)),h=O.outerWidth()):""!==m&&(P.width=xt(m),h=O.outerWidth()),mt(U,o),mt(function(e){W.push(e.innerHTML),j.push(xt(t(e).css("width")))},o),mt(function(e,n){t.inArray(e,R)!==-1&&(e.style.width=j[n])},n),t(o).height(0),N&&(mt(U,a),mt(function(e){B.push(e.innerHTML),H.push(xt(t(e).css("width")))},a),mt(function(t,e){t.style.width=H[e]},r),t(a).height(0)),mt(function(t,e){t.innerHTML='<div class="dataTables_sizing">'+W[e]+"</div>",t.childNodes[0].style.height="0",t.childNodes[0].style.overflow="hidden",t.style.width=j[e]},o),N&&mt(function(t,e){t.innerHTML='<div class="dataTables_sizing">'+B[e]+"</div>",t.childNodes[0].style.height="0",t.childNodes[0].style.overflow="hidden",t.style.width=H[e]},a),O.outerWidth()<h?(c=S.scrollHeight>S.offsetHeight||"scroll"==C.css("overflow-y")?h+y:h,F&&(S.scrollHeight>S.offsetHeight||"scroll"==C.css("overflow-y"))&&(P.width=xt(c-y)),""!==g&&""===m||Mt(e,1,"Possible column misalignment",6)):c="100%",T.width=xt(c),_.width=xt(c),N&&(e.nScrollFoot.style.width=xt(c)),v||F&&(T.height=xt(M.offsetHeight+y));var z=O.outerWidth();D[0].style.width=xt(z),w.width=xt(z);var Y=O.height()>S.clientHeight||"scroll"==C.css("overflow-y"),G="padding"+(L.bScrollbarLeft?"Left":"Right");w[G]=Y?y+"px":"0px",N&&(I[0].style.width=xt(z),A[0].style.width=xt(z),A[0].style[G]=Y?y+"px":"0px"),O.children("colgroup").insertBefore(O.children("thead")),C.trigger("scroll"),!e.bSorted&&!e.bFiltered||e._drawHold||(S.scrollTop=0)}function mt(t,e,n){for(var i,r,o=0,a=0,s=e.length;a<s;){for(i=e[a].firstChild,r=n?n[a].firstChild:null;i;)1===i.nodeType&&(n?t(i,r,o):t(i,o),o++),i=i.nextSibling,r=n?r.nextSibling:null;a++}}function vt(n){var i,r,o,a=n.nTable,s=n.aoColumns,l=n.oScroll,u=l.sY,c=l.sX,h=l.sXInner,d=s.length,g=v(n,"bVisible"),y=t("th",n.nTHead),b=a.getAttribute("width"),_=a.parentNode,x=!1,w=n.oBrowser,D=w.bScrollOversize,S=a.style.width;for(S&&S.indexOf("%")!==-1&&(b=S),i=0;i<g.length;i++)r=s[g[i]],null!==r.sWidth&&(r.sWidth=yt(r.sWidthOrig,_),x=!0);if(D||!x&&!c&&!u&&d==m(n)&&d==y.length)for(i=0;i<d;i++){var C=p(n,i);null!==C&&(s[C].sWidth=xt(y.eq(i).width()))}else{var T=t(a).clone().css("visibility","hidden").removeAttr("id");T.find("tbody tr").remove();var k=t("<tr/>").appendTo(T.find("tbody"));for(T.find("thead, tfoot").remove(),T.append(t(n.nTHead).clone()).append(t(n.nTFoot).clone()),T.find("tfoot th, tfoot td").css("width",""),y=V(n,T.find("thead")[0]),i=0;i<g.length;i++)r=s[g[i]],y[i].style.width=null!==r.sWidthOrig&&""!==r.sWidthOrig?xt(r.sWidthOrig):"",r.sWidthOrig&&c&&t(y[i]).append(t("<div/>").css({width:r.sWidthOrig,margin:0,padding:0,border:0,height:1}));if(n.aoData.length)for(i=0;i<g.length;i++)o=g[i],r=s[o],t(bt(n,o)).clone(!1).append(r.sContentPadding).appendTo(k);t("[name]",T).removeAttr("name");var A=t("<div/>").css(c||u?{position:"absolute",top:0,left:0,height:1,right:0,overflow:"hidden"}:{}).append(T).appendTo(_);c&&h?T.width(h):c?(T.css("width","auto"),T.removeAttr("width"),T.width()<_.clientWidth&&b&&T.width(_.clientWidth)):u?T.width(_.clientWidth):b&&T.width(b);var I=0;for(i=0;i<g.length;i++){var E=t(y[i]),O=E.outerWidth()-E.width(),M=w.bBounding?Math.ceil(y[i].getBoundingClientRect().width):E.outerWidth();I+=M,s[g[i]].sWidth=xt(M-O)}a.style.width=xt(I),A.remove()}if(b&&(a.style.width=xt(b)),(b||c)&&!n._reszEvt){var P=function(){t(e).on("resize.DT-"+n.sInstance,xe(function(){f(n)}))};D?setTimeout(P,1e3):P(),n._reszEvt=!0}}function yt(e,i){if(!e)return 0;var r=t("<div/>").css("width",xt(e)).appendTo(i||n.body),o=r[0].offsetWidth;return r.remove(),o}function bt(e,n){var i=_t(e,n);if(i<0)return null;var r=e.aoData[i];return r.nTr?r.anCells[n]:t("<td/>").html(S(e,i,n,"display"))[0]}function _t(t,e){for(var n,i=-1,r=-1,o=0,a=t.aoData.length;o<a;o++)n=S(t,o,e,"display")+"",n=n.replace(_e,""),n=n.replace(/&nbsp;/g," "),n.length>i&&(i=n.length,r=o);return r}function xt(t){return null===t?"0px":"number"==typeof t?t<0?"0px":t+"px":t.match(/\d$/)?t+"px":t}function wt(e){var n,r,o,a,s,l,u,c=[],h=e.aoColumns,d=e.aaSortingFixed,f=t.isPlainObject(d),p=[],g=function(e){e.length&&!t.isArray(e[0])?p.push(e):t.merge(p,e)};for(t.isArray(d)&&g(d),f&&d.pre&&g(d.pre),g(e.aaSorting),f&&d.post&&g(d.post),n=0;n<p.length;n++)for(u=p[n][0],a=h[u].aDataSort,r=0,o=a.length;r<o;r++)s=a[r],l=h[s].sType||"string",p[n]._idx===i&&(p[n]._idx=t.inArray(p[n][1],h[s].asSorting)),c.push({src:u,col:s,dir:p[n][1],index:p[n]._idx,type:l,formatter:Xt.ext.type.order[l+"-pre"]});return c}function Dt(t){var e,n,i,r,o,a=[],s=Xt.ext.type.order,l=t.aoData,u=(t.aoColumns,0),c=t.aiDisplayMaster;for(y(t),o=wt(t),e=0,n=o.length;e<n;e++)r=o[e],r.formatter&&u++,At(t,r.col);if("ssp"!=Wt(t)&&0!==o.length){for(e=0,i=c.length;e<i;e++)a[c[e]]=e;u===o.length?c.sort(function(t,e){var n,i,r,s,u,c=o.length,h=l[t]._aSortData,d=l[e]._aSortData;for(r=0;r<c;r++)if(u=o[r],n=h[u.col],i=d[u.col],s=n<i?-1:n>i?1:0,0!==s)return"asc"===u.dir?s:-s;return n=a[t],i=a[e],n<i?-1:n>i?1:0}):c.sort(function(t,e){var n,i,r,u,c,h,d=o.length,f=l[t]._aSortData,p=l[e]._aSortData;for(r=0;r<d;r++)if(c=o[r],n=f[c.col],i=p[c.col],h=s[c.type+"-"+c.dir]||s["string-"+c.dir],u=h(n,i),0!==u)return u;return n=a[t],i=a[e],n<i?-1:n>i?1:0})}t.bSorted=!0}function St(t){for(var e,n,i=t.aoColumns,r=wt(t),o=t.oLanguage.oAria,a=0,s=i.length;a<s;a++){var l=i[a],u=l.asSorting,c=l.sTitle.replace(/<.*?>/g,""),h=l.nTh;h.removeAttribute("aria-sort"),l.bSortable?(r.length>0&&r[0].col==a?(h.setAttribute("aria-sort","asc"==r[0].dir?"ascending":"descending"),n=u[r[0].index+1]||u[0]):n=u[0],e=c+("asc"===n?o.sSortAscending:o.sSortDescending)):e=c,h.setAttribute("aria-label",e)}}function Ct(e,n,r,o){var a,s=e.aoColumns[n],l=e.aaSorting,u=s.asSorting,c=function(e,n){var r=e._idx;return r===i&&(r=t.inArray(e[1],u)),r+1<u.length?r+1:n?null:0};if("number"==typeof l[0]&&(l=e.aaSorting=[l]),r&&e.oFeatures.bSortMulti){var h=t.inArray(n,se(l,"0"));h!==-1?(a=c(l[h],!0),null===a&&1===l.length&&(a=0),null===a?l.splice(h,1):(l[h][1]=u[a],l[h]._idx=a)):(l.push([n,u[0],0]),l[l.length-1]._idx=0)}else l.length&&l[0][0]==n?(a=c(l[0]),l.length=1,l[0][1]=u[a],l[0]._idx=a):(l.length=0,l.push([n,u[0]]),l[0]._idx=0);H(e),"function"==typeof o&&o(e)}function Tt(t,e,n,i){var r=t.aoColumns[n];Lt(e,{},function(e){r.bSortable!==!1&&(t.oFeatures.bProcessing?(ft(t,!0),setTimeout(function(){Ct(t,n,e.shiftKey,i),"ssp"!==Wt(t)&&ft(t,!1)},0)):Ct(t,n,e.shiftKey,i))})}function kt(e){var n,i,r,o=e.aLastSort,a=e.oClasses.sSortColumn,s=wt(e),l=e.oFeatures;if(l.bSort&&l.bSortClasses){
for(n=0,i=o.length;n<i;n++)r=o[n].src,t(se(e.aoData,"anCells",r)).removeClass(a+(n<2?n+1:3));for(n=0,i=s.length;n<i;n++)r=s[n].src,t(se(e.aoData,"anCells",r)).addClass(a+(n<2?n+1:3))}e.aLastSort=s}function At(t,e){var n,i=t.aoColumns[e],r=Xt.ext.order[i.sSortDataType];r&&(n=r.call(t.oInstance,t,e,g(t,e)));for(var o,a,s=Xt.ext.type.order[i.sType+"-pre"],l=0,u=t.aoData.length;l<u;l++)o=t.aoData[l],o._aSortData||(o._aSortData=[]),o._aSortData[e]&&!r||(a=r?n[l]:S(t,l,e,"sort"),o._aSortData[e]=s?s(a):a)}function It(e){if(e.oFeatures.bStateSave&&!e.bDestroying){var n={time:+new Date,start:e._iDisplayStart,length:e._iDisplayLength,order:t.extend(!0,[],e.aaSorting),search:et(e.oPreviousSearch),columns:t.map(e.aoColumns,function(t,n){return{visible:t.bVisible,search:et(e.aoPreSearchCols[n])}})};Rt(e,"aoStateSaveParams","stateSaveParams",[e,n]),e.oSavedState=n,e.fnStateSaveCallback.call(e.oInstance,e,n)}}function Et(e,n,r){var o,a,s=e.aoColumns,l=function(n){if(!n||!n.time)return void r();var l=Rt(e,"aoStateLoadParams","stateLoadParams",[e,n]);if(t.inArray(!1,l)!==-1)return void r();var u=e.iStateDuration;if(u>0&&n.time<+new Date-1e3*u)return void r();if(n.columns&&s.length!==n.columns.length)return void r();if(e.oLoadedState=t.extend(!0,{},n),n.start!==i&&(e._iDisplayStart=n.start,e.iInitDisplayStart=n.start),n.length!==i&&(e._iDisplayLength=n.length),n.order!==i&&(e.aaSorting=[],t.each(n.order,function(t,n){e.aaSorting.push(n[0]>=s.length?[0,n[1]]:n)})),n.search!==i&&t.extend(e.oPreviousSearch,nt(n.search)),n.columns)for(o=0,a=n.columns.length;o<a;o++){var c=n.columns[o];c.visible!==i&&(s[o].bVisible=c.visible),c.search!==i&&t.extend(e.aoPreSearchCols[o],nt(c.search))}Rt(e,"aoStateLoaded","stateLoaded",[e,n]),r()};if(!e.oFeatures.bStateSave)return void r();var u=e.fnStateLoadCallback.call(e.oInstance,e,l);u!==i&&l(u)}function Ot(e){var n=Xt.settings,i=t.inArray(e,se(n,"nTable"));return i!==-1?n[i]:null}function Mt(t,n,i,r){if(i="DataTables warning: "+(t?"table id="+t.sTableId+" - ":"")+i,r&&(i+=". For more information about this error, please see http://datatables.net/tn/"+r),n)e.console&&console.log&&console.log(i);else{var o=Xt.ext,a=o.sErrMode||o.errMode;if(t&&Rt(t,null,"error",[t,r,i]),"alert"==a)alert(i);else{if("throw"==a)throw new Error(i);"function"==typeof a&&a(t,r,i)}}}function Pt(e,n,r,o){return t.isArray(r)?void t.each(r,function(i,r){t.isArray(r)?Pt(e,n,r[0],r[1]):Pt(e,n,r)}):(o===i&&(o=r),void(n[r]!==i&&(e[o]=n[r])))}function Nt(e,n,i){var r;for(var o in n)n.hasOwnProperty(o)&&(r=n[o],t.isPlainObject(r)?(t.isPlainObject(e[o])||(e[o]={}),t.extend(!0,e[o],r)):i&&"data"!==o&&"aaData"!==o&&t.isArray(r)?e[o]=r.slice():e[o]=r);return e}function Lt(e,n,i){t(e).on("click.DT",n,function(n){t(e).blur(),i(n)}).on("keypress.DT",n,function(t){13===t.which&&(t.preventDefault(),i(t))}).on("selectstart.DT",function(){return!1})}function Ft(t,e,n,i){n&&t[e].push({fn:n,sName:i})}function Rt(e,n,i,r){var o=[];if(n&&(o=t.map(e[n].slice().reverse(),function(t,n){return t.fn.apply(e.oInstance,r)})),null!==i){var a=t.Event(i+".dt");t(e.nTable).trigger(a,r),o.push(a.result)}return o}function jt(t){var e=t._iDisplayStart,n=t.fnDisplayEnd(),i=t._iDisplayLength;e>=n&&(e=n-i),e-=e%i,(i===-1||e<0)&&(e=0),t._iDisplayStart=e}function Ht(e,n){var i=e.renderer,r=Xt.ext.renderer[n];return t.isPlainObject(i)&&i[n]?r[i[n]]||r._:"string"==typeof i?r[i]||r._:r._}function Wt(t){return t.oFeatures.bServerSide?"ssp":t.ajax||t.sAjaxSource?"ajax":"dom"}function Bt(t,e){var n=[],i=qe.numbers_length,r=Math.floor(i/2);return e<=i?n=ue(0,e):t<=r?(n=ue(0,i-2),n.push("ellipsis"),n.push(e-1)):t>=e-1-r?(n=ue(e-(i-2),e),n.splice(0,0,"ellipsis"),n.splice(0,0,0)):(n=ue(t-r+2,t+r-1),n.push("ellipsis"),n.push(e-1),n.splice(0,0,"ellipsis"),n.splice(0,0,0)),n.DT_el="span",n}function Vt(e){t.each({num:function(t){return ze(t,e)},"num-fmt":function(t){return ze(t,e,te)},"html-num":function(t){return ze(t,e,Qt)},"html-num-fmt":function(t){return ze(t,e,Qt,te)}},function(t,n){qt.type.order[t+e+"-pre"]=n,t.match(/^html\-/)&&(qt.type.search[t+e]=qt.type.search.html)})}function Ut(t){return function(){var e=[Ot(this[Xt.ext.iApiIndex])].concat(Array.prototype.slice.call(arguments));return Xt.ext.internal[t].apply(this,e)}}var qt,zt,Yt,Gt,Xt=function(e){this.$=function(t,e){return this.api(!0).$(t,e)},this._=function(t,e){return this.api(!0).rows(t,e).data()},this.api=function(t){return new zt(t?Ot(this[qt.iApiIndex]):this)},this.fnAddData=function(e,n){var r=this.api(!0),o=t.isArray(e)&&(t.isArray(e[0])||t.isPlainObject(e[0]))?r.rows.add(e):r.row.add(e);return(n===i||n)&&r.draw(),o.flatten().toArray()},this.fnAdjustColumnSizing=function(t){var e=this.api(!0).columns.adjust(),n=e.settings()[0],r=n.oScroll;t===i||t?e.draw(!1):""===r.sX&&""===r.sY||gt(n)},this.fnClearTable=function(t){var e=this.api(!0).clear();(t===i||t)&&e.draw()},this.fnClose=function(t){this.api(!0).row(t).child.hide()},this.fnDeleteRow=function(t,e,n){var r=this.api(!0),o=r.rows(t),a=o.settings()[0],s=a.aoData[o[0][0]];return o.remove(),e&&e.call(this,a,s),(n===i||n)&&r.draw(),s},this.fnDestroy=function(t){this.api(!0).destroy(t)},this.fnDraw=function(t){this.api(!0).draw(t)},this.fnFilter=function(t,e,n,r,o,a){var s=this.api(!0);null===e||e===i?s.search(t,n,r,a):s.column(e).search(t,n,r,a),s.draw()},this.fnGetData=function(t,e){var n=this.api(!0);if(t!==i){var r=t.nodeName?t.nodeName.toLowerCase():"";return e!==i||"td"==r||"th"==r?n.cell(t,e).data():n.row(t).data()||null}return n.data().toArray()},this.fnGetNodes=function(t){var e=this.api(!0);return t!==i?e.row(t).node():e.rows().nodes().flatten().toArray()},this.fnGetPosition=function(t){var e=this.api(!0),n=t.nodeName.toUpperCase();if("TR"==n)return e.row(t).index();if("TD"==n||"TH"==n){var i=e.cell(t).index();return[i.row,i.columnVisible,i.column]}return null},this.fnIsOpen=function(t){return this.api(!0).row(t).child.isShown()},this.fnOpen=function(t,e,n){return this.api(!0).row(t).child(e,n).show().child()[0]},this.fnPageChange=function(t,e){var n=this.api(!0).page(t);(e===i||e)&&n.draw(!1)},this.fnSetColumnVis=function(t,e,n){var r=this.api(!0).column(t).visible(e);(n===i||n)&&r.columns.adjust().draw()},this.fnSettings=function(){return Ot(this[qt.iApiIndex])},this.fnSort=function(t){this.api(!0).order(t).draw()},this.fnSortListener=function(t,e,n){this.api(!0).order.listener(t,e,n)},this.fnUpdate=function(t,e,n,r,o){var a=this.api(!0);return n===i||null===n?a.row(e).data(t):a.cell(e,n).data(t),(o===i||o)&&a.columns.adjust(),(r===i||r)&&a.draw(),0},this.fnVersionCheck=qt.fnVersionCheck;var n=this,r=e===i,c=this.length;r&&(e={}),this.oApi=this.internal=qt.internal;for(var f in Xt.ext.internal)f&&(this[f]=Ut(f));return this.each(function(){var f,p={},g=c>1?Nt(p,e,!0):e,m=0,v=this.getAttribute("id"),y=!1,w=Xt.defaults,D=t(this);if("table"!=this.nodeName.toLowerCase())return void Mt(null,0,"Non-table node initialisation ("+this.nodeName+")",2);s(w),l(w.column),o(w,w,!0),o(w.column,w.column,!0),o(w,t.extend(g,D.data()),!0);var S=Xt.settings;for(m=0,f=S.length;m<f;m++){var C=S[m];if(C.nTable==this||C.nTHead&&C.nTHead.parentNode==this||C.nTFoot&&C.nTFoot.parentNode==this){var T=g.bRetrieve!==i?g.bRetrieve:w.bRetrieve,A=g.bDestroy!==i?g.bDestroy:w.bDestroy;if(r||T)return C.oInstance;if(A){C.oInstance.fnDestroy();break}return void Mt(C,0,"Cannot reinitialise DataTable",3)}if(C.sTableId==this.id){S.splice(m,1);break}}null!==v&&""!==v||(v="DataTables_Table_"+Xt.ext._unique++,this.id=v);var I=t.extend(!0,{},Xt.models.oSettings,{sDestroyWidth:D[0].style.width,sInstance:v,sTableId:v});I.nTable=this,I.oApi=n.internal,I.oInit=g,S.push(I),I.oInstance=1===n.length?n:D.dataTable(),s(g),a(g.oLanguage),g.aLengthMenu&&!g.iDisplayLength&&(g.iDisplayLength=t.isArray(g.aLengthMenu[0])?g.aLengthMenu[0][0]:g.aLengthMenu[0]),g=Nt(t.extend(!0,{},w),g),Pt(I.oFeatures,g,["bPaginate","bLengthChange","bFilter","bSort","bSortMulti","bInfo","bProcessing","bAutoWidth","bSortClasses","bServerSide","bDeferRender"]),Pt(I,g,["asStripeClasses","ajax","fnServerData","fnFormatNumber","sServerMethod","aaSorting","aaSortingFixed","aLengthMenu","sPaginationType","sAjaxSource","sAjaxDataProp","iStateDuration","sDom","bSortCellsTop","iTabIndex","fnStateLoadCallback","fnStateSaveCallback","renderer","searchDelay","rowId",["iCookieDuration","iStateDuration"],["oSearch","oPreviousSearch"],["aoSearchCols","aoPreSearchCols"],["iDisplayLength","_iDisplayLength"]]),Pt(I.oScroll,g,[["sScrollX","sX"],["sScrollXInner","sXInner"],["sScrollY","sY"],["bScrollCollapse","bCollapse"]]),Pt(I.oLanguage,g,"fnInfoCallback"),Ft(I,"aoDrawCallback",g.fnDrawCallback,"user"),Ft(I,"aoServerParams",g.fnServerParams,"user"),Ft(I,"aoStateSaveParams",g.fnStateSaveParams,"user"),Ft(I,"aoStateLoadParams",g.fnStateLoadParams,"user"),Ft(I,"aoStateLoaded",g.fnStateLoaded,"user"),Ft(I,"aoRowCallback",g.fnRowCallback,"user"),Ft(I,"aoRowCreatedCallback",g.fnCreatedRow,"user"),Ft(I,"aoHeaderCallback",g.fnHeaderCallback,"user"),Ft(I,"aoFooterCallback",g.fnFooterCallback,"user"),Ft(I,"aoInitComplete",g.fnInitComplete,"user"),Ft(I,"aoPreDrawCallback",g.fnPreDrawCallback,"user"),I.rowIdFn=k(g.rowId),u(I);var E=I.oClasses;if(t.extend(E,Xt.ext.classes,g.oClasses),D.addClass(E.sTable),I.iInitDisplayStart===i&&(I.iInitDisplayStart=g.iDisplayStart,I._iDisplayStart=g.iDisplayStart),null!==g.iDeferLoading){I.bDeferLoading=!0;var O=t.isArray(g.iDeferLoading);I._iRecordsDisplay=O?g.iDeferLoading[0]:g.iDeferLoading,I._iRecordsTotal=O?g.iDeferLoading[1]:g.iDeferLoading}var M=I.oLanguage;t.extend(!0,M,g.oLanguage),M.sUrl&&(t.ajax({dataType:"json",url:M.sUrl,success:function(e){a(e),o(w.oLanguage,e),t.extend(!0,M,e),at(I)},error:function(){at(I)}}),y=!0),null===g.asStripeClasses&&(I.asStripeClasses=[E.sStripeOdd,E.sStripeEven]);var P=I.asStripeClasses,N=D.children("tbody").find("tr").eq(0);t.inArray(!0,t.map(P,function(t,e){return N.hasClass(t)}))!==-1&&(t("tbody tr",this).removeClass(P.join(" ")),I.asDestroyStripes=P.slice());var L,F=[],R=this.getElementsByTagName("thead");if(0!==R.length&&(B(I.aoHeader,R[0]),F=V(I)),null===g.aoColumns)for(L=[],m=0,f=F.length;m<f;m++)L.push(null);else L=g.aoColumns;for(m=0,f=L.length;m<f;m++)h(I,F?F[m]:null);if(b(I,g.aoColumnDefs,L,function(t,e){d(I,t,e)}),N.length){var j=function(t,e){return null!==t.getAttribute("data-"+e)?e:null};t(N[0]).children("th, td").each(function(t,e){var n=I.aoColumns[t];if(n.mData===t){var r=j(e,"sort")||j(e,"order"),o=j(e,"filter")||j(e,"search");null===r&&null===o||(n.mData={_:t+".display",sort:null!==r?t+".@data-"+r:i,type:null!==r?t+".@data-"+r:i,filter:null!==o?t+".@data-"+o:i},d(I,t))}})}var H=I.oFeatures,W=function(){if(g.aaSorting===i){var e=I.aaSorting;for(m=0,f=e.length;m<f;m++)e[m][1]=I.aoColumns[m].asSorting[0]}kt(I),H.bSort&&Ft(I,"aoDrawCallback",function(){if(I.bSorted){var e=wt(I),n={};t.each(e,function(t,e){n[e.src]=e.dir}),Rt(I,null,"order",[I,e,n]),St(I)}}),Ft(I,"aoDrawCallback",function(){(I.bSorted||"ssp"===Wt(I)||H.bDeferRender)&&kt(I)},"sc");var n=D.children("caption").each(function(){this._captionSide=t(this).css("caption-side")}),r=D.children("thead");0===r.length&&(r=t("<thead/>").appendTo(D)),I.nTHead=r[0];var o=D.children("tbody");0===o.length&&(o=t("<tbody/>").appendTo(D)),I.nTBody=o[0];var a=D.children("tfoot");if(0===a.length&&n.length>0&&(""!==I.oScroll.sX||""!==I.oScroll.sY)&&(a=t("<tfoot/>").appendTo(D)),0===a.length||0===a.children().length?D.addClass(E.sNoFooter):a.length>0&&(I.nTFoot=a[0],B(I.aoFooter,I.nTFoot)),g.aaData)for(m=0;m<g.aaData.length;m++)_(I,g.aaData[m]);else(I.bDeferLoading||"dom"==Wt(I))&&x(I,t(I.nTBody).children("tr"));I.aiDisplay=I.aiDisplayMaster.slice(),I.bInitialised=!0,y===!1&&at(I)};g.bStateSave?(H.bStateSave=!0,Ft(I,"aoDrawCallback",It,"state_save"),Et(I,g,W)):W()}),n=null,this},Kt={},$t=/[\r\n\u2028]/g,Qt=/<.*?>/g,Jt=/^\d{2,4}[\.\/\-]\d{1,2}[\.\/\-]\d{1,2}([T ]{1}\d{1,2}[:\.]\d{2}([\.:]\d{2})?)?$/,Zt=new RegExp("(\\"+["/",".","*","+","?","|","(",")","[","]","{","}","\\","$","^","-"].join("|\\")+")","g"),te=/[',$£€¥%\u2009\u202F\u20BD\u20a9\u20BArfkɃΞ]/gi,ee=function(t){return!t||t===!0||"-"===t},ne=function(t){var e=parseInt(t,10);return!isNaN(e)&&isFinite(t)?e:null},ie=function(t,e){return Kt[e]||(Kt[e]=new RegExp(ve(e),"g")),"string"==typeof t&&"."!==e?t.replace(/\./g,"").replace(Kt[e],"."):t},re=function(t,e,n){var i="string"==typeof t;return!!ee(t)||(e&&i&&(t=ie(t,e)),n&&i&&(t=t.replace(te,"")),!isNaN(parseFloat(t))&&isFinite(t))},oe=function(t){return ee(t)||"string"==typeof t},ae=function(t,e,n){if(ee(t))return!0;var i=oe(t);return i?!!re(he(t),e,n)||null:null},se=function(t,e,n){var r=[],o=0,a=t.length;if(n!==i)for(;o<a;o++)t[o]&&t[o][e]&&r.push(t[o][e][n]);else for(;o<a;o++)t[o]&&r.push(t[o][e]);return r},le=function(t,e,n,r){var o=[],a=0,s=e.length;if(r!==i)for(;a<s;a++)t[e[a]][n]&&o.push(t[e[a]][n][r]);else for(;a<s;a++)o.push(t[e[a]][n]);return o},ue=function(t,e){var n,r=[];e===i?(e=0,n=t):(n=e,e=t);for(var o=e;o<n;o++)r.push(o);return r},ce=function(t){for(var e=[],n=0,i=t.length;n<i;n++)t[n]&&e.push(t[n]);return e},he=function(t){return t.replace(Qt,"")},de=function(t){if(t.length<2)return!0;for(var e=t.slice().sort(),n=e[0],i=1,r=e.length;i<r;i++){if(e[i]===n)return!1;n=e[i]}return!0},fe=function(t){if(de(t))return t.slice();var e,n,i,r=[],o=t.length,a=0;t:for(n=0;n<o;n++){for(e=t[n],i=0;i<a;i++)if(r[i]===e)continue t;r.push(e),a++}return r};Xt.util={throttle:function(t,e){var n,r,o=e!==i?e:200;return function(){var e=this,a=+new Date,s=arguments;n&&a<n+o?(clearTimeout(r),r=setTimeout(function(){n=i,t.apply(e,s)},o)):(n=a,t.apply(e,s))}},escapeRegex:function(t){return t.replace(Zt,"\\$1")}};var pe=function(t,e,n){t[e]!==i&&(t[n]=t[e])},ge=/\[.*?\]$/,me=/\(\)$/,ve=Xt.util.escapeRegex,ye=t("<div>")[0],be=ye.textContent!==i,_e=/<.*?>/g,xe=Xt.util.throttle,we=[],De=Array.prototype,Se=function(e){var n,i,r=Xt.settings,o=t.map(r,function(t,e){return t.nTable});return e?e.nTable&&e.oApi?[e]:e.nodeName&&"table"===e.nodeName.toLowerCase()?(n=t.inArray(e,o),n!==-1?[r[n]]:null):e&&"function"==typeof e.settings?e.settings().toArray():("string"==typeof e?i=t(e):e instanceof t&&(i=e),i?i.map(function(e){return n=t.inArray(this,o),n!==-1?r[n]:null}).toArray():void 0):[]};zt=function(e,n){if(!(this instanceof zt))return new zt(e,n);var i=[],r=function(t){var e=Se(t);e&&i.push.apply(i,e)};if(t.isArray(e))for(var o=0,a=e.length;o<a;o++)r(e[o]);else r(e);this.context=fe(i),n&&t.merge(this,n),this.selector={rows:null,cols:null,opts:null},zt.extend(this,this,we)},Xt.Api=zt,t.extend(zt.prototype,{any:function(){return 0!==this.count()},concat:De.concat,context:[],count:function(){return this.flatten().length},each:function(t){for(var e=0,n=this.length;e<n;e++)t.call(this,this[e],e,this);return this},eq:function(t){var e=this.context;return e.length>t?new zt(e[t],this[t]):null},filter:function(t){var e=[];if(De.filter)e=De.filter.call(this,t,this);else for(var n=0,i=this.length;n<i;n++)t.call(this,this[n],n,this)&&e.push(this[n]);return new zt(this.context,e)},flatten:function(){var t=[];return new zt(this.context,t.concat.apply(t,this.toArray()))},join:De.join,indexOf:De.indexOf||function(t,e){for(var n=e||0,i=this.length;n<i;n++)if(this[n]===t)return n;return-1},iterator:function(t,e,n,r){var o,a,s,l,u,c,h,d,f=[],p=this.context,g=this.selector;for("string"==typeof t&&(r=n,n=e,e=t,t=!1),a=0,s=p.length;a<s;a++){var m=new zt(p[a]);if("table"===e)o=n.call(m,p[a],a),o!==i&&f.push(o);else if("columns"===e||"rows"===e)o=n.call(m,p[a],this[a],a),o!==i&&f.push(o);else if("column"===e||"column-rows"===e||"row"===e||"cell"===e)for(h=this[a],"column-rows"===e&&(c=Ee(p[a],g.opts)),l=0,u=h.length;l<u;l++)d=h[l],o="cell"===e?n.call(m,p[a],d.row,d.column,a,l):n.call(m,p[a],d,a,l,c),o!==i&&f.push(o)}if(f.length||r){var v=new zt(p,t?f.concat.apply([],f):f),y=v.selector;return y.rows=g.rows,y.cols=g.cols,y.opts=g.opts,v}return this},lastIndexOf:De.lastIndexOf||function(t,e){return this.indexOf.apply(this.toArray.reverse(),arguments)},length:0,map:function(t){var e=[];if(De.map)e=De.map.call(this,t,this);else for(var n=0,i=this.length;n<i;n++)e.push(t.call(this,this[n],n));return new zt(this.context,e)},pluck:function(t){return this.map(function(e){return e[t]})},pop:De.pop,push:De.push,reduce:De.reduce||function(t,e){return c(this,t,e,0,this.length,1)},reduceRight:De.reduceRight||function(t,e){return c(this,t,e,this.length-1,-1,-1)},reverse:De.reverse,selector:null,shift:De.shift,slice:function(){return new zt(this.context,this)},sort:De.sort,splice:De.splice,toArray:function(){return De.slice.call(this)},to$:function(){return t(this)},toJQuery:function(){return t(this)},unique:function(){return new zt(this.context,fe(this))},unshift:De.unshift}),zt.extend=function(t,e,n){if(n.length&&e&&(e instanceof zt||e.__dt_wrapper)){var i,r,o,a=function(t,e,n){return function(){var i=e.apply(t,arguments);return zt.extend(i,i,n.methodExt),i}};for(i=0,r=n.length;i<r;i++)o=n[i],e[o.name]="function"===o.type?a(t,o.val,o):"object"===o.type?{}:o.val,e[o.name].__dt_wrapper=!0,zt.extend(t,e[o.name],o.propExt)}},zt.register=Yt=function(e,n){if(t.isArray(e))for(var i=0,r=e.length;i<r;i++)zt.register(e[i],n);else{var o,a,s,l,u=e.split("."),c=we,h=function(t,e){for(var n=0,i=t.length;n<i;n++)if(t[n].name===e)return t[n];return null};for(o=0,a=u.length;o<a;o++){l=u[o].indexOf("()")!==-1,s=l?u[o].replace("()",""):u[o];var d=h(c,s);d||(d={name:s,val:{},methodExt:[],propExt:[],type:"object"},c.push(d)),o===a-1?(d.val=n,d.type="function"==typeof n?"function":t.isPlainObject(n)?"object":"other"):c=l?d.methodExt:d.propExt}}},zt.registerPlural=Gt=function(e,n,r){zt.register(e,r),zt.register(n,function(){var e=r.apply(this,arguments);return e===this?this:e instanceof zt?e.length?t.isArray(e[0])?new zt(e.context,e[0]):e[0]:i:e})};var Ce=function(e,n){if("number"==typeof e)return[n[e]];var i=t.map(n,function(t,e){return t.nTable});return t(i).filter(e).map(function(e){var r=t.inArray(this,i);return n[r]}).toArray()};Yt("tables()",function(t){return t?new zt(Ce(t,this.context)):this}),Yt("table()",function(t){var e=this.tables(t),n=e.context;return n.length?new zt(n[0]):e}),Gt("tables().nodes()","table().node()",function(){return this.iterator("table",function(t){return t.nTable},1)}),Gt("tables().body()","table().body()",function(){return this.iterator("table",function(t){return t.nTBody},1)}),Gt("tables().header()","table().header()",function(){return this.iterator("table",function(t){return t.nTHead},1)}),Gt("tables().footer()","table().footer()",function(){return this.iterator("table",function(t){return t.nTFoot},1)}),Gt("tables().containers()","table().container()",function(){return this.iterator("table",function(t){return t.nTableWrapper},1)}),Yt("draw()",function(t){return this.iterator("table",function(e){"page"===t?j(e):("string"==typeof t&&(t="full-hold"!==t),H(e,t===!1))})}),Yt("page()",function(t){return t===i?this.page.info().page:this.iterator("table",function(e){ht(e,t)})}),Yt("page.info()",function(t){if(0===this.context.length)return i;var e=this.context[0],n=e._iDisplayStart,r=e.oFeatures.bPaginate?e._iDisplayLength:-1,o=e.fnRecordsDisplay(),a=r===-1;return{page:a?0:Math.floor(n/r),pages:a?1:Math.ceil(o/r),start:n,end:e.fnDisplayEnd(),length:r,recordsTotal:e.fnRecordsTotal(),recordsDisplay:o,serverSide:"ssp"===Wt(e)}}),Yt("page.len()",function(t){return t===i?0!==this.context.length?this.context[0]._iDisplayLength:i:this.iterator("table",function(e){lt(e,t)})});var Te=function(t,e,n){if(n){var i=new zt(t);i.one("draw",function(){n(i.ajax.json())})}if("ssp"==Wt(t))H(t,e);else{ft(t,!0);var r=t.jqXHR;r&&4!==r.readyState&&r.abort(),U(t,[],function(n){E(t);for(var i=G(t,n),r=0,o=i.length;r<o;r++)_(t,i[r]);H(t,e),ft(t,!1)})}};Yt("ajax.json()",function(){var t=this.context;if(t.length>0)return t[0].json}),Yt("ajax.params()",function(){var t=this.context;if(t.length>0)return t[0].oAjaxData}),Yt("ajax.reload()",function(t,e){return this.iterator("table",function(n){Te(n,e===!1,t)})}),Yt("ajax.url()",function(e){var n=this.context;return e===i?0===n.length?i:(n=n[0],n.ajax?t.isPlainObject(n.ajax)?n.ajax.url:n.ajax:n.sAjaxSource):this.iterator("table",function(n){t.isPlainObject(n.ajax)?n.ajax.url=e:n.ajax=e})}),Yt("ajax.url().load()",function(t,e){return this.iterator("table",function(n){Te(n,e===!1,t)})});var ke=function(e,n,r,o,a){var s,l,u,c,h,d,f=[],p=typeof n;for(n&&"string"!==p&&"function"!==p&&n.length!==i||(n=[n]),u=0,c=n.length;u<c;u++)for(l=n[u]&&n[u].split&&!n[u].match(/[\[\(:]/)?n[u].split(","):[n[u]],h=0,d=l.length;h<d;h++)s=r("string"==typeof l[h]?t.trim(l[h]):l[h]),s&&s.length&&(f=f.concat(s));var g=qt.selector[e];if(g.length)for(u=0,c=g.length;u<c;u++)f=g[u](o,a,f);return fe(f)},Ae=function(e){return e||(e={}),e.filter&&e.search===i&&(e.search=e.filter),t.extend({search:"none",order:"current",page:"all"},e)},Ie=function(t){for(var e=0,n=t.length;e<n;e++)if(t[e].length>0)return t[0]=t[e],t[0].length=1,t.length=1,t.context=[t.context[e]],t;return t.length=0,t},Ee=function(e,n){var i,r,o,a=[],s=e.aiDisplay,l=e.aiDisplayMaster,u=n.search,c=n.order,h=n.page;if("ssp"==Wt(e))return"removed"===u?[]:ue(0,l.length);if("current"==h)for(i=e._iDisplayStart,r=e.fnDisplayEnd();i<r;i++)a.push(s[i]);else if("current"==c||"applied"==c){if("none"==u)a=l.slice();else if("applied"==u)a=s.slice();else if("removed"==u){for(var d={},i=0,r=s.length;i<r;i++)d[s[i]]=null;a=t.map(l,function(t){return d.hasOwnProperty(t)?null:t})}}else if("index"==c||"original"==c)for(i=0,r=e.aoData.length;i<r;i++)"none"==u?a.push(i):(o=t.inArray(i,s),(o===-1&&"removed"==u||o>=0&&"applied"==u)&&a.push(i));return a},Oe=function(e,n,r){var o,a=function(n){var a=ne(n),s=e.aoData;if(null!==a&&!r)return[a];if(o||(o=Ee(e,r)),null!==a&&t.inArray(a,o)!==-1)return[a];if(null===n||n===i||""===n)return o;if("function"==typeof n)return t.map(o,function(t){var e=s[t];return n(t,e._aData,e.nTr)?t:null});if(n.nodeName){var l=n._DT_RowIndex,u=n._DT_CellIndex;if(l!==i)return s[l]&&s[l].nTr===n?[l]:[];if(u)return s[u.row]&&s[u.row].nTr===n.parentNode?[u.row]:[];var c=t(n).closest("*[data-dt-row]");return c.length?[c.data("dt-row")]:[]}if("string"==typeof n&&"#"===n.charAt(0)){var h=e.aIds[n.replace(/^#/,"")];if(h!==i)return[h.idx]}var d=ce(le(e.aoData,o,"nTr"));return t(d).filter(n).map(function(){return this._DT_RowIndex}).toArray()};return ke("row",n,a,e,r)};Yt("rows()",function(e,n){e===i?e="":t.isPlainObject(e)&&(n=e,e=""),n=Ae(n);var r=this.iterator("table",function(t){return Oe(t,e,n)},1);return r.selector.rows=e,r.selector.opts=n,r}),Yt("rows().nodes()",function(){return this.iterator("row",function(t,e){return t.aoData[e].nTr||i},1)}),Yt("rows().data()",function(){return this.iterator(!0,"rows",function(t,e){return le(t.aoData,e,"_aData")},1)}),Gt("rows().cache()","row().cache()",function(t){return this.iterator("row",function(e,n){var i=e.aoData[n];return"search"===t?i._aFilterData:i._aSortData},1)}),Gt("rows().invalidate()","row().invalidate()",function(t){return this.iterator("row",function(e,n){M(e,n,t)})}),Gt("rows().indexes()","row().index()",function(){return this.iterator("row",function(t,e){return e},1)}),Gt("rows().ids()","row().id()",function(t){for(var e=[],n=this.context,i=0,r=n.length;i<r;i++)for(var o=0,a=this[i].length;o<a;o++){var s=n[i].rowIdFn(n[i].aoData[this[i][o]]._aData);e.push((t===!0?"#":"")+s)}return new zt(n,e)}),Gt("rows().remove()","row().remove()",function(){var t=this;return this.iterator("row",function(e,n,r){var o,a,s,l,u,c,h=e.aoData,d=h[n];for(h.splice(n,1),o=0,a=h.length;o<a;o++)if(u=h[o],c=u.anCells,null!==u.nTr&&(u.nTr._DT_RowIndex=o),null!==c)for(s=0,l=c.length;s<l;s++)c[s]._DT_CellIndex.row=o;O(e.aiDisplayMaster,n),O(e.aiDisplay,n),O(t[r],n,!1),e._iRecordsDisplay>0&&e._iRecordsDisplay--,jt(e);var f=e.rowIdFn(d._aData);f!==i&&delete e.aIds[f]}),this.iterator("table",function(t){for(var e=0,n=t.aoData.length;e<n;e++)t.aoData[e].idx=e}),this}),Yt("rows.add()",function(e){var n=this.iterator("table",function(t){var n,i,r,o=[];for(i=0,r=e.length;i<r;i++)n=e[i],n.nodeName&&"TR"===n.nodeName.toUpperCase()?o.push(x(t,n)[0]):o.push(_(t,n));return o},1),i=this.rows(-1);return i.pop(),t.merge(i,n),i}),Yt("row()",function(t,e){return Ie(this.rows(t,e))}),Yt("row().data()",function(e){var n=this.context;if(e===i)return n.length&&this.length?n[0].aoData[this[0]]._aData:i;var r=n[0].aoData[this[0]];return r._aData=e,t.isArray(e)&&r.nTr.id&&A(n[0].rowId)(e,r.nTr.id),M(n[0],this[0],"data"),this}),Yt("row().node()",function(){var t=this.context;return t.length&&this.length?t[0].aoData[this[0]].nTr||null:null}),Yt("row.add()",function(e){e instanceof t&&e.length&&(e=e[0]);var n=this.iterator("table",function(t){return e.nodeName&&"TR"===e.nodeName.toUpperCase()?x(t,e)[0]:_(t,e)});return this.row(n[0])});var Me=function(e,n,i,r){var o=[],a=function(n,i){if(t.isArray(n)||n instanceof t)for(var r=0,s=n.length;r<s;r++)a(n[r],i);else if(n.nodeName&&"tr"===n.nodeName.toLowerCase())o.push(n);else{var l=t("<tr><td/></tr>").addClass(i);t("td",l).addClass(i).html(n)[0].colSpan=m(e),o.push(l[0])}};a(i,r),n._details&&n._details.detach(),n._details=t(o),n._detailsShow&&n._details.insertAfter(n.nTr)},Pe=function(t,e){var n=t.context;if(n.length){var r=n[0].aoData[e!==i?e:t[0]];r&&r._details&&(r._details.remove(),r._detailsShow=i,r._details=i)}},Ne=function(t,e){var n=t.context;if(n.length&&t.length){var i=n[0].aoData[t[0]];i._details&&(i._detailsShow=e,e?i._details.insertAfter(i.nTr):i._details.detach(),Le(n[0]))}},Le=function(t){var e=new zt(t),n=".dt.DT_details",i="draw"+n,r="column-visibility"+n,o="destroy"+n,a=t.aoData;e.off(i+" "+r+" "+o),se(a,"_details").length>0&&(e.on(i,function(n,i){t===i&&e.rows({page:"current"}).eq(0).each(function(t){var e=a[t];e._detailsShow&&e._details.insertAfter(e.nTr)})}),e.on(r,function(e,n,i,r){if(t===n)for(var o,s=m(n),l=0,u=a.length;l<u;l++)o=a[l],o._details&&o._details.children("td[colspan]").attr("colspan",s)}),e.on(o,function(n,i){if(t===i)for(var r=0,o=a.length;r<o;r++)a[r]._details&&Pe(e,r)}))},Fe="",Re=Fe+"row().child",je=Re+"()";Yt(je,function(t,e){var n=this.context;return t===i?n.length&&this.length?n[0].aoData[this[0]]._details:i:(t===!0?this.child.show():t===!1?Pe(this):n.length&&this.length&&Me(n[0],n[0].aoData[this[0]],t,e),this)}),Yt([Re+".show()",je+".show()"],function(t){return Ne(this,!0),this}),Yt([Re+".hide()",je+".hide()"],function(){return Ne(this,!1),this}),Yt([Re+".remove()",je+".remove()"],function(){return Pe(this),this}),Yt(Re+".isShown()",function(){var t=this.context;return!(!t.length||!this.length)&&(t[0].aoData[this[0]]._detailsShow||!1)});var He=/^([^:]+):(name|visIdx|visible)$/,We=function(t,e,n,i,r){for(var o=[],a=0,s=r.length;a<s;a++)o.push(S(t,r[a],e));return o},Be=function(e,n,i){var r=e.aoColumns,o=se(r,"sName"),a=se(r,"nTh"),s=function(n){var s=ne(n);if(""===n)return ue(r.length);if(null!==s)return[s>=0?s:r.length+s];if("function"==typeof n){var l=Ee(e,i);return t.map(r,function(t,i){return n(i,We(e,i,0,0,l),a[i])?i:null})}var u="string"==typeof n?n.match(He):"";if(u)switch(u[2]){case"visIdx":case"visible":var c=parseInt(u[1],10);if(c<0){var h=t.map(r,function(t,e){return t.bVisible?e:null});return[h[h.length+c]]}return[p(e,c)];case"name":return t.map(o,function(t,e){return t===u[1]?e:null});default:return[]}if(n.nodeName&&n._DT_CellIndex)return[n._DT_CellIndex.column];var d=t(a).filter(n).map(function(){return t.inArray(this,a)}).toArray();if(d.length||!n.nodeName)return d;var f=t(n).closest("*[data-dt-column]");return f.length?[f.data("dt-column")]:[]};return ke("column",n,s,e,i)},Ve=function(e,n,r){var o,a,s,l,u=e.aoColumns,c=u[n],h=e.aoData;if(r===i)return c.bVisible;if(c.bVisible!==r){if(r){var d=t.inArray(!0,se(u,"bVisible"),n+1);for(a=0,s=h.length;a<s;a++)l=h[a].nTr,o=h[a].anCells,l&&l.insertBefore(o[n],o[d]||null)}else t(se(e.aoData,"anCells",n)).detach();c.bVisible=r}};Yt("columns()",function(e,n){e===i?e="":t.isPlainObject(e)&&(n=e,e=""),n=Ae(n);var r=this.iterator("table",function(t){return Be(t,e,n)},1);return r.selector.cols=e,r.selector.opts=n,r}),Gt("columns().header()","column().header()",function(t,e){return this.iterator("column",function(t,e){return t.aoColumns[e].nTh},1)}),Gt("columns().footer()","column().footer()",function(t,e){return this.iterator("column",function(t,e){return t.aoColumns[e].nTf},1)}),Gt("columns().data()","column().data()",function(){return this.iterator("column-rows",We,1)}),Gt("columns().dataSrc()","column().dataSrc()",function(){return this.iterator("column",function(t,e){return t.aoColumns[e].mData},1)}),Gt("columns().cache()","column().cache()",function(t){return this.iterator("column-rows",function(e,n,i,r,o){return le(e.aoData,o,"search"===t?"_aFilterData":"_aSortData",n)},1)}),Gt("columns().nodes()","column().nodes()",function(){return this.iterator("column-rows",function(t,e,n,i,r){return le(t.aoData,r,"anCells",e)},1)}),Gt("columns().visible()","column().visible()",function(e,n){var r=this,o=this.iterator("column",function(t,n){return e===i?t.aoColumns[n].bVisible:void Ve(t,n,e)});return e!==i&&this.iterator("table",function(o){R(o,o.aoHeader),R(o,o.aoFooter),o.aiDisplay.length||t(o.nTBody).find("td[colspan]").attr("colspan",m(o)),It(o),r.iterator("column",function(t,i){Rt(t,null,"column-visibility",[t,i,e,n])}),(n===i||n)&&r.columns.adjust()}),o}),Gt("columns().indexes()","column().index()",function(t){return this.iterator("column",function(e,n){return"visible"===t?g(e,n):n},1)}),Yt("columns.adjust()",function(){return this.iterator("table",function(t){f(t)},1)}),Yt("column.index()",function(t,e){if(0!==this.context.length){var n=this.context[0];if("fromVisible"===t||"toData"===t)return p(n,e);if("fromData"===t||"toVisible"===t)return g(n,e)}}),Yt("column()",function(t,e){return Ie(this.columns(t,e))});var Ue=function(e,n,r){var o,a,s,l,u,c,h,d=e.aoData,f=Ee(e,r),p=ce(le(d,f,"anCells")),g=t([].concat.apply([],p)),m=e.aoColumns.length,v=function(n){var r="function"==typeof n;if(null===n||n===i||r){for(a=[],s=0,l=f.length;s<l;s++)for(o=f[s],u=0;u<m;u++)c={row:o,column:u},r?(h=d[o],n(c,S(e,o,u),h.anCells?h.anCells[u]:null)&&a.push(c)):a.push(c);return a}if(t.isPlainObject(n))return n.column!==i&&n.row!==i&&t.inArray(n.row,f)!==-1?[n]:[];var p=g.filter(n).map(function(t,e){return{row:e._DT_CellIndex.row,column:e._DT_CellIndex.column}}).toArray();return p.length||!n.nodeName?p:(h=t(n).closest("*[data-dt-row]"),h.length?[{row:h.data("dt-row"),column:h.data("dt-column")}]:[])};return ke("cell",n,v,e,r)};Yt("cells()",function(e,n,r){if(t.isPlainObject(e)&&(e.row===i?(r=e,e=null):(r=n,n=null)),t.isPlainObject(n)&&(r=n,n=null),null===n||n===i)return this.iterator("table",function(t){return Ue(t,e,Ae(r))});var o,a,s,l,u=r?{page:r.page,order:r.order,search:r.search}:{},c=this.columns(n,u),h=this.rows(e,u),d=this.iterator("table",function(t,e){var n=[];for(o=0,a=h[e].length;o<a;o++)for(s=0,l=c[e].length;s<l;s++)n.push({row:h[e][o],column:c[e][s]});return n},1),f=r&&r.selected?this.cells(d,r):d;return t.extend(f.selector,{cols:n,rows:e,opts:r}),f}),Gt("cells().nodes()","cell().node()",function(){return this.iterator("cell",function(t,e,n){var r=t.aoData[e];return r&&r.anCells?r.anCells[n]:i},1)}),Yt("cells().data()",function(){return this.iterator("cell",function(t,e,n){return S(t,e,n)},1)}),Gt("cells().cache()","cell().cache()",function(t){return t="search"===t?"_aFilterData":"_aSortData",this.iterator("cell",function(e,n,i){return e.aoData[n][t][i]},1)}),Gt("cells().render()","cell().render()",function(t){return this.iterator("cell",function(e,n,i){return S(e,n,i,t)},1)}),Gt("cells().indexes()","cell().index()",function(){return this.iterator("cell",function(t,e,n){return{row:e,column:n,columnVisible:g(t,n)}},1)}),Gt("cells().invalidate()","cell().invalidate()",function(t){return this.iterator("cell",function(e,n,i){M(e,n,t,i)})}),Yt("cell()",function(t,e,n){return Ie(this.cells(t,e,n))}),Yt("cell().data()",function(t){var e=this.context,n=this[0];return t===i?e.length&&n.length?S(e[0],n[0].row,n[0].column):i:(C(e[0],n[0].row,n[0].column,t),
M(e[0],n[0].row,"data",n[0].column),this)}),Yt("order()",function(e,n){var r=this.context;return e===i?0!==r.length?r[0].aaSorting:i:("number"==typeof e?e=[[e,n]]:e.length&&!t.isArray(e[0])&&(e=Array.prototype.slice.call(arguments)),this.iterator("table",function(t){t.aaSorting=e.slice()}))}),Yt("order.listener()",function(t,e,n){return this.iterator("table",function(i){Tt(i,t,e,n)})}),Yt("order.fixed()",function(e){if(!e){var n=this.context,r=n.length?n[0].aaSortingFixed:i;return t.isArray(r)?{pre:r}:r}return this.iterator("table",function(n){n.aaSortingFixed=t.extend(!0,{},e)})}),Yt(["columns().order()","column().order()"],function(e){var n=this;return this.iterator("table",function(i,r){var o=[];t.each(n[r],function(t,n){o.push([n,e])}),i.aaSorting=o})}),Yt("search()",function(e,n,r,o){var a=this.context;return e===i?0!==a.length?a[0].oPreviousSearch.sSearch:i:this.iterator("table",function(i){i.oFeatures.bFilter&&K(i,t.extend({},i.oPreviousSearch,{sSearch:e+"",bRegex:null!==n&&n,bSmart:null===r||r,bCaseInsensitive:null===o||o}),1)})}),Gt("columns().search()","column().search()",function(e,n,r,o){return this.iterator("column",function(a,s){var l=a.aoPreSearchCols;return e===i?l[s].sSearch:void(a.oFeatures.bFilter&&(t.extend(l[s],{sSearch:e+"",bRegex:null!==n&&n,bSmart:null===r||r,bCaseInsensitive:null===o||o}),K(a,a.oPreviousSearch,1)))})}),Yt("state()",function(){return this.context.length?this.context[0].oSavedState:null}),Yt("state.clear()",function(){return this.iterator("table",function(t){t.fnStateSaveCallback.call(t.oInstance,t,{})})}),Yt("state.loaded()",function(){return this.context.length?this.context[0].oLoadedState:null}),Yt("state.save()",function(){return this.iterator("table",function(t){It(t)})}),Xt.versionCheck=Xt.fnVersionCheck=function(t){for(var e,n,i=Xt.version.split("."),r=t.split("."),o=0,a=r.length;o<a;o++)if(e=parseInt(i[o],10)||0,n=parseInt(r[o],10)||0,e!==n)return e>n;return!0},Xt.isDataTable=Xt.fnIsDataTable=function(e){var n=t(e).get(0),i=!1;return e instanceof Xt.Api||(t.each(Xt.settings,function(e,r){var o=r.nScrollHead?t("table",r.nScrollHead)[0]:null,a=r.nScrollFoot?t("table",r.nScrollFoot)[0]:null;r.nTable!==n&&o!==n&&a!==n||(i=!0)}),i)},Xt.tables=Xt.fnTables=function(e){var n=!1;t.isPlainObject(e)&&(n=e.api,e=e.visible);var i=t.map(Xt.settings,function(n){if(!e||e&&t(n.nTable).is(":visible"))return n.nTable});return n?new zt(i):i},Xt.camelToHungarian=o,Yt("$()",function(e,n){var i=this.rows(n).nodes(),r=t(i);return t([].concat(r.filter(e).toArray(),r.find(e).toArray()))}),t.each(["on","one","off"],function(e,n){Yt(n+"()",function(){var e=Array.prototype.slice.call(arguments);e[0]=t.map(e[0].split(/\s/),function(t){return t.match(/\.dt\b/)?t:t+".dt"}).join(" ");var i=t(this.tables().nodes());return i[n].apply(i,e),this})}),Yt("clear()",function(){return this.iterator("table",function(t){E(t)})}),Yt("settings()",function(){return new zt(this.context,this.context)}),Yt("init()",function(){var t=this.context;return t.length?t[0].oInit:null}),Yt("data()",function(){return this.iterator("table",function(t){return se(t.aoData,"_aData")}).flatten()}),Yt("destroy()",function(n){return n=n||!1,this.iterator("table",function(i){var r,o=i.nTableWrapper.parentNode,a=i.oClasses,s=i.nTable,l=i.nTBody,u=i.nTHead,c=i.nTFoot,h=t(s),d=t(l),f=t(i.nTableWrapper),p=t.map(i.aoData,function(t){return t.nTr});i.bDestroying=!0,Rt(i,"aoDestroyCallback","destroy",[i]),n||new zt(i).columns().visible(!0),f.off(".DT").find(":not(tbody *)").off(".DT"),t(e).off(".DT-"+i.sInstance),s!=u.parentNode&&(h.children("thead").detach(),h.append(u)),c&&s!=c.parentNode&&(h.children("tfoot").detach(),h.append(c)),i.aaSorting=[],i.aaSortingFixed=[],kt(i),t(p).removeClass(i.asStripeClasses.join(" ")),t("th, td",u).removeClass(a.sSortable+" "+a.sSortableAsc+" "+a.sSortableDesc+" "+a.sSortableNone),d.children().detach(),d.append(p);var g=n?"remove":"detach";h[g](),f[g](),!n&&o&&(o.insertBefore(s,i.nTableReinsertBefore),h.css("width",i.sDestroyWidth).removeClass(a.sTable),r=i.asDestroyStripes.length,r&&d.children().each(function(e){t(this).addClass(i.asDestroyStripes[e%r])}));var m=t.inArray(i,Xt.settings);m!==-1&&Xt.settings.splice(m,1)})}),t.each(["column","row","cell"],function(t,e){Yt(e+"s().every()",function(t){var n=this.selector.opts,r=this;return this.iterator(e,function(o,a,s,l,u){t.call(r[e](a,"cell"===e?s:n,"cell"===e?n:i),a,s,l,u)})})}),Yt("i18n()",function(e,n,r){var o=this.context[0],a=k(e)(o.oLanguage);return a===i&&(a=n),r!==i&&t.isPlainObject(a)&&(a=a[r]!==i?a[r]:a._),a.replace("%d",r)}),Xt.version="1.10.20",Xt.settings=[],Xt.models={},Xt.models.oSearch={bCaseInsensitive:!0,sSearch:"",bRegex:!1,bSmart:!0},Xt.models.oRow={nTr:null,anCells:null,_aData:[],_aSortData:null,_aFilterData:null,_sFilterRow:null,_sRowStripe:"",src:null,idx:-1},Xt.models.oColumn={idx:null,aDataSort:null,asSorting:null,bSearchable:null,bSortable:null,bVisible:null,_sManualType:null,_bAttrSrc:!1,fnCreatedCell:null,fnGetData:null,fnSetData:null,mData:null,mRender:null,nTh:null,nTf:null,sClass:null,sContentPadding:null,sDefaultContent:null,sName:null,sSortDataType:"std",sSortingClass:null,sSortingClassJUI:null,sTitle:null,sType:null,sWidth:null,sWidthOrig:null},Xt.defaults={aaData:null,aaSorting:[[0,"asc"]],aaSortingFixed:[],ajax:null,aLengthMenu:[10,25,50,100],aoColumns:null,aoColumnDefs:null,aoSearchCols:[],asStripeClasses:null,bAutoWidth:!0,bDeferRender:!1,bDestroy:!1,bFilter:!0,bInfo:!0,bLengthChange:!0,bPaginate:!0,bProcessing:!1,bRetrieve:!1,bScrollCollapse:!1,bServerSide:!1,bSort:!0,bSortMulti:!0,bSortCellsTop:!1,bSortClasses:!0,bStateSave:!1,fnCreatedRow:null,fnDrawCallback:null,fnFooterCallback:null,fnFormatNumber:function(t){return t.toString().replace(/\B(?=(\d{3})+(?!\d))/g,this.oLanguage.sThousands)},fnHeaderCallback:null,fnInfoCallback:null,fnInitComplete:null,fnPreDrawCallback:null,fnRowCallback:null,fnServerData:null,fnServerParams:null,fnStateLoadCallback:function(t){try{return JSON.parse((t.iStateDuration===-1?sessionStorage:localStorage).getItem("DataTables_"+t.sInstance+"_"+location.pathname))}catch(e){}},fnStateLoadParams:null,fnStateLoaded:null,fnStateSaveCallback:function(t,e){try{(t.iStateDuration===-1?sessionStorage:localStorage).setItem("DataTables_"+t.sInstance+"_"+location.pathname,JSON.stringify(e))}catch(n){}},fnStateSaveParams:null,iStateDuration:7200,iDeferLoading:null,iDisplayLength:10,iDisplayStart:0,iTabIndex:0,oClasses:{},oLanguage:{oAria:{sSortAscending:": activate to sort column ascending",sSortDescending:": activate to sort column descending"},oPaginate:{sFirst:"First",sLast:"Last",sNext:"Next",sPrevious:"Previous"},sEmptyTable:"No data available in table",sInfo:"Showing _START_ to _END_ of _TOTAL_ entries",sInfoEmpty:"Showing 0 to 0 of 0 entries",sInfoFiltered:"(filtered from _MAX_ total entries)",sInfoPostFix:"",sDecimal:"",sThousands:",",sLengthMenu:"Show _MENU_ entries",sLoadingRecords:"Loading...",sProcessing:"Processing...",sSearch:"Search:",sSearchPlaceholder:"",sUrl:"",sZeroRecords:"No matching records found"},oSearch:t.extend({},Xt.models.oSearch),sAjaxDataProp:"data",sAjaxSource:null,sDom:"lfrtip",searchDelay:null,sPaginationType:"simple_numbers",sScrollX:"",sScrollXInner:"",sScrollY:"",sServerMethod:"GET",renderer:null,rowId:"DT_RowId"},r(Xt.defaults),Xt.defaults.column={aDataSort:null,iDataSort:-1,asSorting:["asc","desc"],bSearchable:!0,bSortable:!0,bVisible:!0,fnCreatedCell:null,mData:null,mRender:null,sCellType:"td",sClass:"",sContentPadding:"",sDefaultContent:null,sName:"",sSortDataType:"std",sTitle:null,sType:null,sWidth:null},r(Xt.defaults.column),Xt.models.oSettings={oFeatures:{bAutoWidth:null,bDeferRender:null,bFilter:null,bInfo:null,bLengthChange:null,bPaginate:null,bProcessing:null,bServerSide:null,bSort:null,bSortMulti:null,bSortClasses:null,bStateSave:null},oScroll:{bCollapse:null,iBarWidth:0,sX:null,sXInner:null,sY:null},oLanguage:{fnInfoCallback:null},oBrowser:{bScrollOversize:!1,bScrollbarLeft:!1,bBounding:!1,barWidth:0},ajax:null,aanFeatures:[],aoData:[],aiDisplay:[],aiDisplayMaster:[],aIds:{},aoColumns:[],aoHeader:[],aoFooter:[],oPreviousSearch:{},aoPreSearchCols:[],aaSorting:null,aaSortingFixed:[],asStripeClasses:null,asDestroyStripes:[],sDestroyWidth:0,aoRowCallback:[],aoHeaderCallback:[],aoFooterCallback:[],aoDrawCallback:[],aoRowCreatedCallback:[],aoPreDrawCallback:[],aoInitComplete:[],aoStateSaveParams:[],aoStateLoadParams:[],aoStateLoaded:[],sTableId:"",nTable:null,nTHead:null,nTFoot:null,nTBody:null,nTableWrapper:null,bDeferLoading:!1,bInitialised:!1,aoOpenRows:[],sDom:null,searchDelay:null,sPaginationType:"two_button",iStateDuration:0,aoStateSave:[],aoStateLoad:[],oSavedState:null,oLoadedState:null,sAjaxSource:null,sAjaxDataProp:null,bAjaxDataGet:!0,jqXHR:null,json:i,oAjaxData:i,fnServerData:null,aoServerParams:[],sServerMethod:null,fnFormatNumber:null,aLengthMenu:null,iDraw:0,bDrawing:!1,iDrawError:-1,_iDisplayLength:10,_iDisplayStart:0,_iRecordsTotal:0,_iRecordsDisplay:0,oClasses:{},bFiltered:!1,bSorted:!1,bSortCellsTop:null,oInit:null,aoDestroyCallback:[],fnRecordsTotal:function(){return"ssp"==Wt(this)?1*this._iRecordsTotal:this.aiDisplayMaster.length},fnRecordsDisplay:function(){return"ssp"==Wt(this)?1*this._iRecordsDisplay:this.aiDisplay.length},fnDisplayEnd:function(){var t=this._iDisplayLength,e=this._iDisplayStart,n=e+t,i=this.aiDisplay.length,r=this.oFeatures,o=r.bPaginate;return r.bServerSide?o===!1||t===-1?e+i:Math.min(e+t,this._iRecordsDisplay):!o||n>i||t===-1?i:n},oInstance:null,sInstance:null,iTabIndex:0,nScrollHead:null,nScrollFoot:null,aLastSort:[],oPlugins:{},rowIdFn:null,rowId:null},Xt.ext=qt={buttons:{},classes:{},build:"bs4/dt-1.10.20",errMode:"alert",feature:[],search:[],selector:{cell:[],column:[],row:[]},internal:{},legacy:{ajax:null},pager:{},renderer:{pageButton:{},header:{}},order:{},type:{detect:[],search:{},order:{}},_unique:0,fnVersionCheck:Xt.fnVersionCheck,iApiIndex:0,oJUIClasses:{},sVersion:Xt.version},t.extend(qt,{afnFiltering:qt.search,aTypes:qt.type.detect,ofnSearch:qt.type.search,oSort:qt.type.order,afnSortData:qt.order,aoFeatures:qt.feature,oApi:qt.internal,oStdClasses:qt.classes,oPagination:qt.pager}),t.extend(Xt.ext.classes,{sTable:"dataTable",sNoFooter:"no-footer",sPageButton:"paginate_button",sPageButtonActive:"current",sPageButtonDisabled:"disabled",sStripeOdd:"odd",sStripeEven:"even",sRowEmpty:"dataTables_empty",sWrapper:"dataTables_wrapper",sFilter:"dataTables_filter",sInfo:"dataTables_info",sPaging:"dataTables_paginate paging_",sLength:"dataTables_length",sProcessing:"dataTables_processing",sSortAsc:"sorting_asc",sSortDesc:"sorting_desc",sSortable:"sorting",sSortableAsc:"sorting_asc_disabled",sSortableDesc:"sorting_desc_disabled",sSortableNone:"sorting_disabled",sSortColumn:"sorting_",sFilterInput:"",sLengthSelect:"",sScrollWrapper:"dataTables_scroll",sScrollHead:"dataTables_scrollHead",sScrollHeadInner:"dataTables_scrollHeadInner",sScrollBody:"dataTables_scrollBody",sScrollFoot:"dataTables_scrollFoot",sScrollFootInner:"dataTables_scrollFootInner",sHeaderTH:"",sFooterTH:"",sSortJUIAsc:"",sSortJUIDesc:"",sSortJUI:"",sSortJUIAscAllowed:"",sSortJUIDescAllowed:"",sSortJUIWrapper:"",sSortIcon:"",sJUIHeader:"",sJUIFooter:""});var qe=Xt.ext.pager;t.extend(qe,{simple:function(t,e){return["previous","next"]},full:function(t,e){return["first","previous","next","last"]},numbers:function(t,e){return[Bt(t,e)]},simple_numbers:function(t,e){return["previous",Bt(t,e),"next"]},full_numbers:function(t,e){return["first","previous",Bt(t,e),"next","last"]},first_last_numbers:function(t,e){return["first",Bt(t,e),"last"]},_numbers:Bt,numbers_length:7}),t.extend(!0,Xt.ext.renderer,{pageButton:{_:function(e,r,o,a,s,l){var u,c,h,d=e.oClasses,f=e.oLanguage.oPaginate,p=e.oLanguage.oAria.paginate||{},g=0,m=function(n,i){var r,a,h,v,y,b=d.sPageButtonDisabled,_=function(t){ht(e,t.data.action,!0)};for(r=0,a=i.length;r<a;r++)if(v=i[r],t.isArray(v)){var x=t("<"+(v.DT_el||"div")+"/>").appendTo(n);m(x,v)}else{switch(u=null,c=v,y=e.iTabIndex,v){case"ellipsis":n.append('<span class="ellipsis">&#x2026;</span>');break;case"first":u=f.sFirst,0===s&&(y=-1,c+=" "+b);break;case"previous":u=f.sPrevious,0===s&&(y=-1,c+=" "+b);break;case"next":u=f.sNext,s===l-1&&(y=-1,c+=" "+b);break;case"last":u=f.sLast,s===l-1&&(y=-1,c+=" "+b);break;default:u=v+1,c=s===v?d.sPageButtonActive:""}null!==u&&(h=t("<a>",{"class":d.sPageButton+" "+c,"aria-controls":e.sTableId,"aria-label":p[v],"data-dt-idx":g,tabindex:y,id:0===o&&"string"==typeof v?e.sTableId+"_"+v:null}).html(u).appendTo(n),Lt(h,{action:v},_),g++)}};try{h=t(r).find(n.activeElement).data("dt-idx")}catch(v){}m(t(r).empty(),a),h!==i&&t(r).find("[data-dt-idx="+h+"]").focus()}}}),t.extend(Xt.ext.type.detect,[function(t,e){var n=e.oLanguage.sDecimal;return re(t,n)?"num"+n:null},function(t,e){if(t&&!(t instanceof Date)&&!Jt.test(t))return null;var n=Date.parse(t);return null!==n&&!isNaN(n)||ee(t)?"date":null},function(t,e){var n=e.oLanguage.sDecimal;return re(t,n,!0)?"num-fmt"+n:null},function(t,e){var n=e.oLanguage.sDecimal;return ae(t,n)?"html-num"+n:null},function(t,e){var n=e.oLanguage.sDecimal;return ae(t,n,!0)?"html-num-fmt"+n:null},function(t,e){return ee(t)||"string"==typeof t&&t.indexOf("<")!==-1?"html":null}]),t.extend(Xt.ext.type.search,{html:function(t){return ee(t)?t:"string"==typeof t?t.replace($t," ").replace(Qt,""):""},string:function(t){return ee(t)?t:"string"==typeof t?t.replace($t," "):t}});var ze=function(t,e,n,i){return 0===t||t&&"-"!==t?(e&&(t=ie(t,e)),t.replace&&(n&&(t=t.replace(n,"")),i&&(t=t.replace(i,""))),1*t):-(1/0)};t.extend(qt.type.order,{"date-pre":function(t){var e=Date.parse(t);return isNaN(e)?-(1/0):e},"html-pre":function(t){return ee(t)?"":t.replace?t.replace(/<.*?>/g,"").toLowerCase():t+""},"string-pre":function(t){return ee(t)?"":"string"==typeof t?t.toLowerCase():t.toString?t.toString():""},"string-asc":function(t,e){return t<e?-1:t>e?1:0},"string-desc":function(t,e){return t<e?1:t>e?-1:0}}),Vt(""),t.extend(!0,Xt.ext.renderer,{header:{_:function(e,n,i,r){t(e.nTable).on("order.dt.DT",function(t,o,a,s){if(e===o){var l=i.idx;n.removeClass(i.sSortingClass+" "+r.sSortAsc+" "+r.sSortDesc).addClass("asc"==s[l]?r.sSortAsc:"desc"==s[l]?r.sSortDesc:i.sSortingClass)}})},jqueryui:function(e,n,i,r){t("<div/>").addClass(r.sSortJUIWrapper).append(n.contents()).append(t("<span/>").addClass(r.sSortIcon+" "+i.sSortingClassJUI)).appendTo(n),t(e.nTable).on("order.dt.DT",function(t,o,a,s){if(e===o){var l=i.idx;n.removeClass(r.sSortAsc+" "+r.sSortDesc).addClass("asc"==s[l]?r.sSortAsc:"desc"==s[l]?r.sSortDesc:i.sSortingClass),n.find("span."+r.sSortIcon).removeClass(r.sSortJUIAsc+" "+r.sSortJUIDesc+" "+r.sSortJUI+" "+r.sSortJUIAscAllowed+" "+r.sSortJUIDescAllowed).addClass("asc"==s[l]?r.sSortJUIAsc:"desc"==s[l]?r.sSortJUIDesc:i.sSortingClassJUI)}})}}});var Ye=function(t){return"string"==typeof t?t.replace(/</g,"&lt;").replace(/>/g,"&gt;").replace(/"/g,"&quot;"):t};return Xt.render={number:function(t,e,n,i,r){return{display:function(o){if("number"!=typeof o&&"string"!=typeof o)return o;var a=o<0?"-":"",s=parseFloat(o);if(isNaN(s))return Ye(o);s=s.toFixed(n),o=Math.abs(s);var l=parseInt(o,10),u=n?e+(o-l).toFixed(n).substring(2):"";return a+(i||"")+l.toString().replace(/\B(?=(\d{3})+(?!\d))/g,t)+u+(r||"")}}},text:function(){return{display:Ye,filter:Ye}}},t.extend(Xt.ext.internal,{_fnExternApiFunc:Ut,_fnBuildAjax:U,_fnAjaxUpdate:q,_fnAjaxParameters:z,_fnAjaxUpdateDraw:Y,_fnAjaxDataSrc:G,_fnAddColumn:h,_fnColumnOptions:d,_fnAdjustColumnSizing:f,_fnVisibleToColumnIndex:p,_fnColumnIndexToVisible:g,_fnVisbleColumns:m,_fnGetColumns:v,_fnColumnTypes:y,_fnApplyColumnDefs:b,_fnHungarianMap:r,_fnCamelToHungarian:o,_fnLanguageCompat:a,_fnBrowserDetect:u,_fnAddData:_,_fnAddTr:x,_fnNodeToDataIndex:w,_fnNodeToColumnIndex:D,_fnGetCellData:S,_fnSetCellData:C,_fnSplitObjNotation:T,_fnGetObjectDataFn:k,_fnSetObjectDataFn:A,_fnGetDataMaster:I,_fnClearTable:E,_fnDeleteIndex:O,_fnInvalidate:M,_fnGetRowElements:P,_fnCreateTr:N,_fnBuildHead:F,_fnDrawHead:R,_fnDraw:j,_fnReDraw:H,_fnAddOptionsHtml:W,_fnDetectHeader:B,_fnGetUniqueThs:V,_fnFeatureHtmlFilter:X,_fnFilterComplete:K,_fnFilterCustom:$,_fnFilterColumn:Q,_fnFilter:J,_fnFilterCreateSearch:Z,_fnEscapeRegex:ve,_fnFilterData:tt,_fnFeatureHtmlInfo:it,_fnUpdateInfo:rt,_fnInfoMacros:ot,_fnInitialise:at,_fnInitComplete:st,_fnLengthChange:lt,_fnFeatureHtmlLength:ut,_fnFeatureHtmlPaginate:ct,_fnPageChange:ht,_fnFeatureHtmlProcessing:dt,_fnProcessingDisplay:ft,_fnFeatureHtmlTable:pt,_fnScrollDraw:gt,_fnApplyToChildren:mt,_fnCalculateColumnWidths:vt,_fnThrottle:xe,_fnConvertToWidth:yt,_fnGetWidestNode:bt,_fnGetMaxLenString:_t,_fnStringToCss:xt,_fnSortFlatten:wt,_fnSort:Dt,_fnSortAria:St,_fnSortListener:Ct,_fnSortAttachListener:Tt,_fnSortingClasses:kt,_fnSortData:At,_fnSaveState:It,_fnLoadState:Et,_fnSettingsFromNode:Ot,_fnLog:Mt,_fnMap:Pt,_fnBindAction:Lt,_fnCallbackReg:Ft,_fnCallbackFire:Rt,_fnLengthOverflow:jt,_fnRenderer:Ht,_fnDataSource:Wt,_fnRowAttributes:L,_fnExtend:Nt,_fnCalculateEnd:function(){}}),t.fn.dataTable=Xt,Xt.$=t,t.fn.dataTableSettings=Xt.settings,t.fn.dataTableExt=Xt.ext,t.fn.DataTable=function(e){return t(this).dataTable(e).api()},t.each(Xt,function(e,n){t.fn.DataTable[e]=n}),t.fn.dataTable}),function(t){"function"==typeof define&&define.amd?define(["jquery","datatables.net"],function(e){return t(e,window,document)}):"object"==typeof exports?module.exports=function(e,n){return e||(e=window),n&&n.fn.dataTable||(n=require("datatables.net")(e,n).$),t(n,e,e.document)}:t(jQuery,window,document)}(function(t,e,n,i){"use strict";var r=t.fn.dataTable;return t.extend(!0,r.defaults,{dom:"<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",renderer:"bootstrap"}),t.extend(r.ext.classes,{sWrapper:"dataTables_wrapper dt-bootstrap4",sFilterInput:"form-control form-control-sm",sLengthSelect:"custom-select custom-select-sm form-control form-control-sm",sProcessing:"dataTables_processing card",sPageButton:"paginate_button page-item"}),r.ext.renderer.pageButton.bootstrap=function(e,o,a,s,l,u){var c,h,d,f=new r.Api(e),p=e.oClasses,g=e.oLanguage.oPaginate,m=e.oLanguage.oAria.paginate||{},v=0,y=function(n,i){var r,o,s,d,b=function(e){e.preventDefault(),t(e.currentTarget).hasClass("disabled")||f.page()==e.data.action||f.page(e.data.action).draw("page")};for(r=0,o=i.length;r<o;r++)if(d=i[r],t.isArray(d))y(n,d);else{switch(c="",h="",d){case"ellipsis":c="&#x2026;",h="disabled";break;case"first":c=g.sFirst,h=d+(l>0?"":" disabled");break;case"previous":c=g.sPrevious,h=d+(l>0?"":" disabled");break;case"next":c=g.sNext,h=d+(l<u-1?"":" disabled");break;case"last":c=g.sLast,h=d+(l<u-1?"":" disabled");break;default:c=d+1,h=l===d?"active":""}c&&(s=t("<li>",{"class":p.sPageButton+" "+h,id:0===a&&"string"==typeof d?e.sTableId+"_"+d:null}).append(t("<a>",{href:"#","aria-controls":e.sTableId,"aria-label":m[d],"data-dt-idx":v,tabindex:e.iTabIndex,"class":"page-link"}).html(c)).appendTo(n),e.oApi._fnBindAction(s,{action:d},b),v++)}};try{d=t(o).find(n.activeElement).data("dt-idx")}catch(b){}y(t(o).empty().html('<ul class="pagination"/>').children("ul"),s),d!==i&&t(o).find("[data-dt-idx="+d+"]").focus()},r}),$(document).ready(function(){});



/*!
 * @copyright Copyright &copy; Kartik Visweswaran, Krajee.com, 2014 - 2016
 * @version 1.3.4
 *
 * Date formatter utility library that allows formatting date/time variables or Date objects using PHP DateTime format.
 * @see http://php.net/manual/en/function.date.php
 *
 * For more JQuery plugins visit http://plugins.krajee.com
 * For more Yii related demos visit http://demos.krajee.com
 */var DateFormatter;!function(){"use strict";var t,e,r,n,a,u,i;u=864e5,i=3600,t=function(t,e){return"string"==typeof t&&"string"==typeof e&&t.toLowerCase()===e.toLowerCase()},e=function(t,r,n){var a=n||"0",u=t.toString();return u.length<r?e(a+u,r):u},r=function(t){var e,n;for(t=t||{},e=1;e<arguments.length;e++)if(n=arguments[e])for(var a in n)n.hasOwnProperty(a)&&("object"==typeof n[a]?r(t[a],n[a]):t[a]=n[a]);return t},n=function(t,e){for(var r=0;r<e.length;r++)if(e[r].toLowerCase()===t.toLowerCase())return r;return-1},a={dateSettings:{days:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],daysShort:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],months:["January","February","March","April","May","June","July","August","September","October","November","December"],monthsShort:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],meridiem:["AM","PM"],ordinal:function(t){var e=t%10,r={1:"st",2:"nd",3:"rd"};return 1!==Math.floor(t%100/10)&&r[e]?r[e]:"th"}},separators:/[ \-+\/\.T:@]/g,validParts:/[dDjlNSwzWFmMntLoYyaABgGhHisueTIOPZcrU]/g,intParts:/[djwNzmnyYhHgGis]/g,tzParts:/\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,tzClip:/[^-+\dA-Z]/g},DateFormatter=function(t){var e=this,n=r(a,t);e.dateSettings=n.dateSettings,e.separators=n.separators,e.validParts=n.validParts,e.intParts=n.intParts,e.tzParts=n.tzParts,e.tzClip=n.tzClip},DateFormatter.prototype={constructor:DateFormatter,getMonth:function(t){var e,r=this;return e=n(t,r.dateSettings.monthsShort)+1,0===e&&(e=n(t,r.dateSettings.months)+1),e},parseDate:function(e,r){var n,a,u,i,s,o,c,f,l,h,d=this,g=!1,m=!1,p=d.dateSettings,y={date:null,year:null,month:null,day:null,hour:0,min:0,sec:0};if(!e)return null;if(e instanceof Date)return e;if("U"===r)return u=parseInt(e),u?new Date(1e3*u):e;switch(typeof e){case"number":return new Date(e);case"string":break;default:return null}if(n=r.match(d.validParts),!n||0===n.length)throw new Error("Invalid date format definition.");for(a=e.replace(d.separators,"\x00").split("\x00"),u=0;u<a.length;u++)switch(i=a[u],s=parseInt(i),n[u]){case"y":case"Y":if(!s)return null;l=i.length,y.year=2===l?parseInt((70>s?"20":"19")+i):s,g=!0;break;case"m":case"n":case"M":case"F":if(isNaN(s)){if(o=d.getMonth(i),!(o>0))return null;y.month=o}else{if(!(s>=1&&12>=s))return null;y.month=s}g=!0;break;case"d":case"j":if(!(s>=1&&31>=s))return null;y.day=s,g=!0;break;case"g":case"h":if(c=n.indexOf("a")>-1?n.indexOf("a"):n.indexOf("A")>-1?n.indexOf("A"):-1,h=a[c],c>-1)f=t(h,p.meridiem[0])?0:t(h,p.meridiem[1])?12:-1,s>=1&&12>=s&&f>-1?y.hour=s+f-1:s>=0&&23>=s&&(y.hour=s);else{if(!(s>=0&&23>=s))return null;y.hour=s}m=!0;break;case"G":case"H":if(!(s>=0&&23>=s))return null;y.hour=s,m=!0;break;case"i":if(!(s>=0&&59>=s))return null;y.min=s,m=!0;break;case"s":if(!(s>=0&&59>=s))return null;y.sec=s,m=!0}if(g===!0&&y.year&&y.month&&y.day)y.date=new Date(y.year,y.month-1,y.day,y.hour,y.min,y.sec,0);else{if(m!==!0)return null;y.date=new Date(0,0,0,y.hour,y.min,y.sec,0)}return y.date},guessDate:function(t,e){if("string"!=typeof t)return t;var r,n,a,u,i,s,o=this,c=t.replace(o.separators,"\x00").split("\x00"),f=/^[djmn]/g,l=e.match(o.validParts),h=new Date,d=0;if(!f.test(l[0]))return t;for(a=0;a<c.length;a++){if(d=2,i=c[a],s=parseInt(i.substr(0,2)),isNaN(s))return null;switch(a){case 0:"m"===l[0]||"n"===l[0]?h.setMonth(s-1):h.setDate(s);break;case 1:"m"===l[0]||"n"===l[0]?h.setDate(s):h.setMonth(s-1);break;case 2:if(n=h.getFullYear(),r=i.length,d=4>r?r:4,n=parseInt(4>r?n.toString().substr(0,4-r)+i:i.substr(0,4)),!n)return null;h.setFullYear(n);break;case 3:h.setHours(s);break;case 4:h.setMinutes(s);break;case 5:h.setSeconds(s)}u=i.substr(d),u.length>0&&c.splice(a+1,0,u)}return h},parseFormat:function(t,r){var n,a=this,s=a.dateSettings,o=/\\?(.?)/gi,c=function(t,e){return n[t]?n[t]():e};return n={d:function(){return e(n.j(),2)},D:function(){return s.daysShort[n.w()]},j:function(){return r.getDate()},l:function(){return s.days[n.w()]},N:function(){return n.w()||7},w:function(){return r.getDay()},z:function(){var t=new Date(n.Y(),n.n()-1,n.j()),e=new Date(n.Y(),0,1);return Math.round((t-e)/u)},W:function(){var t=new Date(n.Y(),n.n()-1,n.j()-n.N()+3),r=new Date(t.getFullYear(),0,4);return e(1+Math.round((t-r)/u/7),2)},F:function(){return s.months[r.getMonth()]},m:function(){return e(n.n(),2)},M:function(){return s.monthsShort[r.getMonth()]},n:function(){return r.getMonth()+1},t:function(){return new Date(n.Y(),n.n(),0).getDate()},L:function(){var t=n.Y();return t%4===0&&t%100!==0||t%400===0?1:0},o:function(){var t=n.n(),e=n.W(),r=n.Y();return r+(12===t&&9>e?1:1===t&&e>9?-1:0)},Y:function(){return r.getFullYear()},y:function(){return n.Y().toString().slice(-2)},a:function(){return n.A().toLowerCase()},A:function(){var t=n.G()<12?0:1;return s.meridiem[t]},B:function(){var t=r.getUTCHours()*i,n=60*r.getUTCMinutes(),a=r.getUTCSeconds();return e(Math.floor((t+n+a+i)/86.4)%1e3,3)},g:function(){return n.G()%12||12},G:function(){return r.getHours()},h:function(){return e(n.g(),2)},H:function(){return e(n.G(),2)},i:function(){return e(r.getMinutes(),2)},s:function(){return e(r.getSeconds(),2)},u:function(){return e(1e3*r.getMilliseconds(),6)},e:function(){var t=/\((.*)\)/.exec(String(r))[1];return t||"Coordinated Universal Time"},I:function(){var t=new Date(n.Y(),0),e=Date.UTC(n.Y(),0),r=new Date(n.Y(),6),a=Date.UTC(n.Y(),6);return t-e!==r-a?1:0},O:function(){var t=r.getTimezoneOffset(),n=Math.abs(t);return(t>0?"-":"+")+e(100*Math.floor(n/60)+n%60,4)},P:function(){var t=n.O();return t.substr(0,3)+":"+t.substr(3,2)},T:function(){var t=(String(r).match(a.tzParts)||[""]).pop().replace(a.tzClip,"");return t||"UTC"},Z:function(){return 60*-r.getTimezoneOffset()},c:function(){return"Y-m-d\\TH:i:sP".replace(o,c)},r:function(){return"D, d M Y H:i:s O".replace(o,c)},U:function(){return r.getTime()/1e3||0}},c(t,t)},formatDate:function(t,e){var r,n,a,u,i,s=this,o="",c="\\";if("string"==typeof t&&(t=s.parseDate(t,e),!t))return null;if(t instanceof Date){for(a=e.length,r=0;a>r;r++)i=e.charAt(r),"S"!==i&&i!==c&&(r>0&&e.charAt(r-1)===c?o+=i:(u=s.parseFormat(i,t),r!==a-1&&s.intParts.test(i)&&"S"===e.charAt(r+1)&&(n=parseInt(u)||0,u+=s.dateSettings.ordinal(n)),o+=u));return o}return""}}}();/**
 * @preserve jQuery DateTimePicker
 * @homepage http://xdsoft.net/jqplugins/datetimepicker/
 * @author Chupurnov Valeriy (<chupurnov@gmail.com>)
 */

/**
 * @param {jQuery} $
 */
var datetimepickerFactory = function ($) {
	'use strict';

	var default_options  = {
		i18n: {
			ar: { // Arabic
				months: [
					"كانون الثاني", "شباط", "آذار", "نيسان", "مايو", "حزيران", "تموز", "آب", "أيلول", "تشرين الأول", "تشرين الثاني", "كانون الأول"
				],
				dayOfWeekShort: [
					"ن", "ث", "ع", "خ", "ج", "س", "ح"
				],
				dayOfWeek: ["الأحد", "الاثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة", "السبت", "الأحد"]
			},
			ro: { // Romanian
				months: [
					"Ianuarie", "Februarie", "Martie", "Aprilie", "Mai", "Iunie", "Iulie", "August", "Septembrie", "Octombrie", "Noiembrie", "Decembrie"
				],
				dayOfWeekShort: [
					"Du", "Lu", "Ma", "Mi", "Jo", "Vi", "Sâ"
				],
				dayOfWeek: ["Duminică", "Luni", "Marţi", "Miercuri", "Joi", "Vineri", "Sâmbătă"]
			},
			id: { // Indonesian
				months: [
					"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"
				],
				dayOfWeekShort: [
					"Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab"
				],
				dayOfWeek: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"]
			},
			is: { // Icelandic
				months: [
					"Janúar", "Febrúar", "Mars", "Apríl", "Maí", "Júní", "Júlí", "Ágúst", "September", "Október", "Nóvember", "Desember"
				],
				dayOfWeekShort: [
					"Sun", "Mán", "Þrið", "Mið", "Fim", "Fös", "Lau"
				],
				dayOfWeek: ["Sunnudagur", "Mánudagur", "Þriðjudagur", "Miðvikudagur", "Fimmtudagur", "Föstudagur", "Laugardagur"]
			},
			bg: { // Bulgarian
				months: [
					"Януари", "Февруари", "Март", "Април", "Май", "Юни", "Юли", "Август", "Септември", "Октомври", "Ноември", "Декември"
				],
				dayOfWeekShort: [
					"Нд", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"
				],
				dayOfWeek: ["Неделя", "Понеделник", "Вторник", "Сряда", "Четвъртък", "Петък", "Събота"]
			},
			fa: { // Persian/Farsi
				months: [
					'فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور', 'مهر', 'آبان', 'آذر', 'دی', 'بهمن', 'اسفند'
				],
				dayOfWeekShort: [
					'یکشنبه', 'دوشنبه', 'سه شنبه', 'چهارشنبه', 'پنجشنبه', 'جمعه', 'شنبه'
				],
				dayOfWeek: ["یک‌شنبه", "دوشنبه", "سه‌شنبه", "چهارشنبه", "پنج‌شنبه", "جمعه", "شنبه", "یک‌شنبه"]
			},
			ru: { // Russian
				months: [
					'Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
				],
				dayOfWeekShort: [
					"Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"
				],
				dayOfWeek: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"]
			},
			uk: { // Ukrainian
				months: [
					'Січень', 'Лютий', 'Березень', 'Квітень', 'Травень', 'Червень', 'Липень', 'Серпень', 'Вересень', 'Жовтень', 'Листопад', 'Грудень'
				],
				dayOfWeekShort: [
					"Ндл", "Пнд", "Втр", "Срд", "Чтв", "Птн", "Сбт"
				],
				dayOfWeek: ["Неділя", "Понеділок", "Вівторок", "Середа", "Четвер", "П'ятниця", "Субота"]
			},
			en: { // English
				months: [
					"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
				],
				dayOfWeekShort: [
					"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
				],
				dayOfWeek: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
			},
			el: { // Ελληνικά
				months: [
					"Ιανουάριος", "Φεβρουάριος", "Μάρτιος", "Απρίλιος", "Μάιος", "Ιούνιος", "Ιούλιος", "Αύγουστος", "Σεπτέμβριος", "Οκτώβριος", "Νοέμβριος", "Δεκέμβριος"
				],
				dayOfWeekShort: [
					"Κυρ", "Δευ", "Τρι", "Τετ", "Πεμ", "Παρ", "Σαβ"
				],
				dayOfWeek: ["Κυριακή", "Δευτέρα", "Τρίτη", "Τετάρτη", "Πέμπτη", "Παρασκευή", "Σάββατο"]
			},
			de: { // German
				months: [
					'Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'
				],
				dayOfWeekShort: [
					"So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"
				],
				dayOfWeek: ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"]
			},
			nl: { // Dutch
				months: [
					"januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december"
				],
				dayOfWeekShort: [
					"zo", "ma", "di", "wo", "do", "vr", "za"
				],
				dayOfWeek: ["zondag", "maandag", "dinsdag", "woensdag", "donderdag", "vrijdag", "zaterdag"]
			},
			tr: { // Turkish
				months: [
					"Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"
				],
				dayOfWeekShort: [
					"Paz", "Pts", "Sal", "Çar", "Per", "Cum", "Cts"
				],
				dayOfWeek: ["Pazar", "Pazartesi", "Salı", "Çarşamba", "Perşembe", "Cuma", "Cumartesi"]
			},
			fr: { //French
				months: [
					"Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"
				],
				dayOfWeekShort: [
					"Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"
				],
				dayOfWeek: ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"]
			},
			es: { // Spanish
				months: [
					"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
				],
				dayOfWeekShort: [
					"Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"
				],
				dayOfWeek: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"]
			},
			th: { // Thai
				months: [
					'มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
				],
				dayOfWeekShort: [
					'อา.', 'จ.', 'อ.', 'พ.', 'พฤ.', 'ศ.', 'ส.'
				],
				dayOfWeek: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัส", "ศุกร์", "เสาร์", "อาทิตย์"]
			},
			pl: { // Polish
				months: [
					"styczeń", "luty", "marzec", "kwiecień", "maj", "czerwiec", "lipiec", "sierpień", "wrzesień", "październik", "listopad", "grudzień"
				],
				dayOfWeekShort: [
					"nd", "pn", "wt", "śr", "cz", "pt", "sb"
				],
				dayOfWeek: ["niedziela", "poniedziałek", "wtorek", "środa", "czwartek", "piątek", "sobota"]
			},
			pt: { // Portuguese
				months: [
					"Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"
				],
				dayOfWeekShort: [
					"Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"
				],
				dayOfWeek: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"]
			},
			ch: { // Simplified Chinese
				months: [
					"一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"
				],
				dayOfWeekShort: [
					"日", "一", "二", "三", "四", "五", "六"
				]
			},
			se: { // Swedish
				months: [
					"Januari", "Februari", "Mars", "April", "Maj", "Juni", "Juli", "Augusti", "September",  "Oktober", "November", "December"
				],
				dayOfWeekShort: [
					"Sön", "Mån", "Tis", "Ons", "Tor", "Fre", "Lör"
				]
			},
			km: { // Khmer (ភាសាខ្មែរ)
				months: [
					"មករា​", "កុម្ភៈ", "មិនា​", "មេសា​", "ឧសភា​", "មិថុនា​", "កក្កដា​", "សីហា​", "កញ្ញា​", "តុលា​", "វិច្ឆិកា", "ធ្នូ​"
				],
				dayOfWeekShort: ["អាទិ​", "ច័ន្ទ​", "អង្គារ​", "ពុធ​", "ព្រហ​​", "សុក្រ​", "សៅរ៍"],
				dayOfWeek: ["អាទិត្យ​", "ច័ន្ទ​", "អង្គារ​", "ពុធ​", "ព្រហស្បតិ៍​", "សុក្រ​", "សៅរ៍"]
			},
			kr: { // Korean
				months: [
					"1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"
				],
				dayOfWeekShort: [
					"일", "월", "화", "수", "목", "금", "토"
				],
				dayOfWeek: ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"]
			},
			it: { // Italian
				months: [
					"Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"
				],
				dayOfWeekShort: [
					"Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab"
				],
				dayOfWeek: ["Domenica", "Lunedì", "Martedì", "Mercoledì", "Giovedì", "Venerdì", "Sabato"]
			},
			da: { // Dansk
				months: [
					"Januar", "Februar", "Marts", "April", "Maj", "Juni", "Juli", "August", "September", "Oktober", "November", "December"
				],
				dayOfWeekShort: [
					"Søn", "Man", "Tir", "Ons", "Tor", "Fre", "Lør"
				],
				dayOfWeek: ["søndag", "mandag", "tirsdag", "onsdag", "torsdag", "fredag", "lørdag"]
			},
			no: { // Norwegian
				months: [
					"Januar", "Februar", "Mars", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Desember"
				],
				dayOfWeekShort: [
					"Søn", "Man", "Tir", "Ons", "Tor", "Fre", "Lør"
				],
				dayOfWeek: ['Søndag', 'Mandag', 'Tirsdag', 'Onsdag', 'Torsdag', 'Fredag', 'Lørdag']
			},
			ja: { // Japanese
				months: [
					"1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"
				],
				dayOfWeekShort: [
					"日", "月", "火", "水", "木", "金", "土"
				],
				dayOfWeek: ["日曜", "月曜", "火曜", "水曜", "木曜", "金曜", "土曜"]
			},
			vi: { // Vietnamese
				months: [
					"Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"
				],
				dayOfWeekShort: [
					"CN", "T2", "T3", "T4", "T5", "T6", "T7"
				],
				dayOfWeek: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"]
			},
			sl: { // Slovenščina
				months: [
					"Januar", "Februar", "Marec", "April", "Maj", "Junij", "Julij", "Avgust", "September", "Oktober", "November", "December"
				],
				dayOfWeekShort: [
					"Ned", "Pon", "Tor", "Sre", "Čet", "Pet", "Sob"
				],
				dayOfWeek: ["Nedelja", "Ponedeljek", "Torek", "Sreda", "Četrtek", "Petek", "Sobota"]
			},
			cs: { // Čeština
				months: [
					"Leden", "Únor", "Březen", "Duben", "Květen", "Červen", "Červenec", "Srpen", "Září", "Říjen", "Listopad", "Prosinec"
				],
				dayOfWeekShort: [
					"Ne", "Po", "Út", "St", "Čt", "Pá", "So"
				]
			},
			hu: { // Hungarian
				months: [
					"Január", "Február", "Március", "Április", "Május", "Június", "Július", "Augusztus", "Szeptember", "Október", "November", "December"
				],
				dayOfWeekShort: [
					"Va", "Hé", "Ke", "Sze", "Cs", "Pé", "Szo"
				],
				dayOfWeek: ["vasárnap", "hétfő", "kedd", "szerda", "csütörtök", "péntek", "szombat"]
			},
			az: { //Azerbaijanian (Azeri)
				months: [
					"Yanvar", "Fevral", "Mart", "Aprel", "May", "Iyun", "Iyul", "Avqust", "Sentyabr", "Oktyabr", "Noyabr", "Dekabr"
				],
				dayOfWeekShort: [
					"B", "Be", "Ça", "Ç", "Ca", "C", "Ş"
				],
				dayOfWeek: ["Bazar", "Bazar ertəsi", "Çərşənbə axşamı", "Çərşənbə", "Cümə axşamı", "Cümə", "Şənbə"]
			},
			bs: { //Bosanski
				months: [
					"Januar", "Februar", "Mart", "April", "Maj", "Jun", "Jul", "Avgust", "Septembar", "Oktobar", "Novembar", "Decembar"
				],
				dayOfWeekShort: [
					"Ned", "Pon", "Uto", "Sri", "Čet", "Pet", "Sub"
				],
				dayOfWeek: ["Nedjelja","Ponedjeljak", "Utorak", "Srijeda", "Četvrtak", "Petak", "Subota"]
			},
			ca: { //Català
				months: [
					"Gener", "Febrer", "Març", "Abril", "Maig", "Juny", "Juliol", "Agost", "Setembre", "Octubre", "Novembre", "Desembre"
				],
				dayOfWeekShort: [
					"Dg", "Dl", "Dt", "Dc", "Dj", "Dv", "Ds"
				],
				dayOfWeek: ["Diumenge", "Dilluns", "Dimarts", "Dimecres", "Dijous", "Divendres", "Dissabte"]
			},
			'en-GB': { //English (British)
				months: [
					"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
				],
				dayOfWeekShort: [
					"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
				],
				dayOfWeek: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
			},
			et: { //"Eesti"
				months: [
					"Jaanuar", "Veebruar", "Märts", "Aprill", "Mai", "Juuni", "Juuli", "August", "September", "Oktoober", "November", "Detsember"
				],
				dayOfWeekShort: [
					"P", "E", "T", "K", "N", "R", "L"
				],
				dayOfWeek: ["Pühapäev", "Esmaspäev", "Teisipäev", "Kolmapäev", "Neljapäev", "Reede", "Laupäev"]
			},
			eu: { //Euskara
				months: [
					"Urtarrila", "Otsaila", "Martxoa", "Apirila", "Maiatza", "Ekaina", "Uztaila", "Abuztua", "Iraila", "Urria", "Azaroa", "Abendua"
				],
				dayOfWeekShort: [
					"Ig.", "Al.", "Ar.", "Az.", "Og.", "Or.", "La."
				],
				dayOfWeek: ['Igandea', 'Astelehena', 'Asteartea', 'Asteazkena', 'Osteguna', 'Ostirala', 'Larunbata']
			},
			fi: { //Finnish (Suomi)
				months: [
					"Tammikuu", "Helmikuu", "Maaliskuu", "Huhtikuu", "Toukokuu", "Kesäkuu", "Heinäkuu", "Elokuu", "Syyskuu", "Lokakuu", "Marraskuu", "Joulukuu"
				],
				dayOfWeekShort: [
					"Su", "Ma", "Ti", "Ke", "To", "Pe", "La"
				],
				dayOfWeek: ["sunnuntai", "maanantai", "tiistai", "keskiviikko", "torstai", "perjantai", "lauantai"]
			},
			gl: { //Galego
				months: [
					"Xan", "Feb", "Maz", "Abr", "Mai", "Xun", "Xul", "Ago", "Set", "Out", "Nov", "Dec"
				],
				dayOfWeekShort: [
					"Dom", "Lun", "Mar", "Mer", "Xov", "Ven", "Sab"
				],
				dayOfWeek: ["Domingo", "Luns", "Martes", "Mércores", "Xoves", "Venres", "Sábado"]
			},
			hr: { //Hrvatski
				months: [
					"Siječanj", "Veljača", "Ožujak", "Travanj", "Svibanj", "Lipanj", "Srpanj", "Kolovoz", "Rujan", "Listopad", "Studeni", "Prosinac"
				],
				dayOfWeekShort: [
					"Ned", "Pon", "Uto", "Sri", "Čet", "Pet", "Sub"
				],
				dayOfWeek: ["Nedjelja", "Ponedjeljak", "Utorak", "Srijeda", "Četvrtak", "Petak", "Subota"]
			},
			ko: { //Korean (한국어)
				months: [
					"1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"
				],
				dayOfWeekShort: [
					"일", "월", "화", "수", "목", "금", "토"
				],
				dayOfWeek: ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"]
			},
			lt: { //Lithuanian (lietuvių)
				months: [
					"Sausio", "Vasario", "Kovo", "Balandžio", "Gegužės", "Birželio", "Liepos", "Rugpjūčio", "Rugsėjo", "Spalio", "Lapkričio", "Gruodžio"
				],
				dayOfWeekShort: [
					"Sek", "Pir", "Ant", "Tre", "Ket", "Pen", "Šeš"
				],
				dayOfWeek: ["Sekmadienis", "Pirmadienis", "Antradienis", "Trečiadienis", "Ketvirtadienis", "Penktadienis", "Šeštadienis"]
			},
			lv: { //Latvian (Latviešu)
				months: [
					"Janvāris", "Februāris", "Marts", "Aprīlis ", "Maijs", "Jūnijs", "Jūlijs", "Augusts", "Septembris", "Oktobris", "Novembris", "Decembris"
				],
				dayOfWeekShort: [
					"Sv", "Pr", "Ot", "Tr", "Ct", "Pk", "St"
				],
				dayOfWeek: ["Svētdiena", "Pirmdiena", "Otrdiena", "Trešdiena", "Ceturtdiena", "Piektdiena", "Sestdiena"]
			},
			mk: { //Macedonian (Македонски)
				months: [
					"јануари", "февруари", "март", "април", "мај", "јуни", "јули", "август", "септември", "октомври", "ноември", "декември"
				],
				dayOfWeekShort: [
					"нед", "пон", "вто", "сре", "чет", "пет", "саб"
				],
				dayOfWeek: ["Недела", "Понеделник", "Вторник", "Среда", "Четврток", "Петок", "Сабота"]
			},
			mn: { //Mongolian (Монгол)
				months: [
					"1-р сар", "2-р сар", "3-р сар", "4-р сар", "5-р сар", "6-р сар", "7-р сар", "8-р сар", "9-р сар", "10-р сар", "11-р сар", "12-р сар"
				],
				dayOfWeekShort: [
					"Дав", "Мяг", "Лха", "Пүр", "Бсн", "Бям", "Ням"
				],
				dayOfWeek: ["Даваа", "Мягмар", "Лхагва", "Пүрэв", "Баасан", "Бямба", "Ням"]
			},
			'pt-BR': { //Português(Brasil)
				months: [
					"Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"
				],
				dayOfWeekShort: [
					"Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb"
				],
				dayOfWeek: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"]
			},
			sk: { //Slovenčina
				months: [
					"Január", "Február", "Marec", "Apríl", "Máj", "Jún", "Júl", "August", "September", "Október", "November", "December"
				],
				dayOfWeekShort: [
					"Ne", "Po", "Ut", "St", "Št", "Pi", "So"
				],
				dayOfWeek: ["Nedeľa", "Pondelok", "Utorok", "Streda", "Štvrtok", "Piatok", "Sobota"]
			},
			sq: { //Albanian (Shqip)
				months: [
					"Janar", "Shkurt", "Mars", "Prill", "Maj", "Qershor", "Korrik", "Gusht", "Shtator", "Tetor", "Nëntor", "Dhjetor"
				],
				dayOfWeekShort: [
					"Die", "Hën", "Mar", "Mër", "Enj", "Pre", "Shtu"
				],
				dayOfWeek: ["E Diel", "E Hënë", "E Martē", "E Mërkurë", "E Enjte", "E Premte", "E Shtunë"]
			},
			'sr-YU': { //Serbian (Srpski)
				months: [
					"Januar", "Februar", "Mart", "April", "Maj", "Jun", "Jul", "Avgust", "Septembar", "Oktobar", "Novembar", "Decembar"
				],
				dayOfWeekShort: [
					"Ned", "Pon", "Uto", "Sre", "čet", "Pet", "Sub"
				],
				dayOfWeek: ["Nedelja","Ponedeljak", "Utorak", "Sreda", "Četvrtak", "Petak", "Subota"]
			},
			sr: { //Serbian Cyrillic (Српски)
				months: [
					"јануар", "фебруар", "март", "април", "мај", "јун", "јул", "август", "септембар", "октобар", "новембар", "децембар"
				],
				dayOfWeekShort: [
					"нед", "пон", "уто", "сре", "чет", "пет", "суб"
				],
				dayOfWeek: ["Недеља","Понедељак", "Уторак", "Среда", "Четвртак", "Петак", "Субота"]
			},
			sv: { //Svenska
				months: [
					"Januari", "Februari", "Mars", "April", "Maj", "Juni", "Juli", "Augusti", "September", "Oktober", "November", "December"
				],
				dayOfWeekShort: [
					"Sön", "Mån", "Tis", "Ons", "Tor", "Fre", "Lör"
				],
				dayOfWeek: ["Söndag", "Måndag", "Tisdag", "Onsdag", "Torsdag", "Fredag", "Lördag"]
			},
			'zh-TW': { //Traditional Chinese (繁體中文)
				months: [
					"一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"
				],
				dayOfWeekShort: [
					"日", "一", "二", "三", "四", "五", "六"
				],
				dayOfWeek: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"]
			},
			zh: { //Simplified Chinese (简体中文)
				months: [
					"一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"
				],
				dayOfWeekShort: [
					"日", "一", "二", "三", "四", "五", "六"
				],
				dayOfWeek: ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"]
			},
			ug:{ // Uyghur(ئۇيغۇرچە)
				months: [
					"1-ئاي","2-ئاي","3-ئاي","4-ئاي","5-ئاي","6-ئاي","7-ئاي","8-ئاي","9-ئاي","10-ئاي","11-ئاي","12-ئاي"
				],
				dayOfWeek: [
					"يەكشەنبە", "دۈشەنبە","سەيشەنبە","چارشەنبە","پەيشەنبە","جۈمە","شەنبە"
				]
			},
			he: { //Hebrew (עברית)
				months: [
					'ינואר', 'פברואר', 'מרץ', 'אפריל', 'מאי', 'יוני', 'יולי', 'אוגוסט', 'ספטמבר', 'אוקטובר', 'נובמבר', 'דצמבר'
				],
				dayOfWeekShort: [
					'א\'', 'ב\'', 'ג\'', 'ד\'', 'ה\'', 'ו\'', 'שבת'
				],
				dayOfWeek: ["ראשון", "שני", "שלישי", "רביעי", "חמישי", "שישי", "שבת", "ראשון"]
            },
            kk:{
                months:["Желтоқсан","Қаңтар","Ақпан","Наурыз","Сәуір","Мамыр","Маусым","Шілде","Тамыз","Қыркүйек","Қазан","Қараша"],
                dayOfWeekShort:["жс" , "дс" , "сс" , "ср" , "бс" , "жм" , "сб"],
                dayOfWeek:["Жексенбі" , "Дүйсенбі","Сейсенбі","Сәрсенбі","Бейсенбі","Жұма","Сенбі"]
            },
			hy: { // Armenian
				months: [
					"Հունվար", "Փետրվար", "Մարտ", "Ապրիլ", "Մայիս", "Հունիս", "Հուլիս", "Օգոստոս", "Սեպտեմբեր", "Հոկտեմբեր", "Նոյեմբեր", "Դեկտեմբեր"
				],
				dayOfWeekShort: [
					"Կի", "Երկ", "Երք", "Չոր", "Հնգ", "Ուրբ", "Շբթ"
				],
				dayOfWeek: ["Կիրակի", "Երկուշաբթի", "Երեքշաբթի", "Չորեքշաբթի", "Հինգշաբթի", "Ուրբաթ", "Շաբաթ"]
			},
			kg: { // Kyrgyz
				months: [
					'Үчтүн айы', 'Бирдин айы', 'Жалган Куран', 'Чын Куран', 'Бугу', 'Кулжа', 'Теке', 'Баш Оона', 'Аяк Оона', 'Тогуздун айы', 'Жетинин айы', 'Бештин айы'
				],
				dayOfWeekShort: [
					"Жек", "Дүй", "Шей", "Шар", "Бей", "Жум", "Ише"
				],
				dayOfWeek: [
					"Жекшемб", "Дүйшөмб", "Шейшемб", "Шаршемб", "Бейшемби", "Жума", "Ишенб"
				]
			},
			rm: { // Romansh
				months: [
					"Schaner", "Favrer", "Mars", "Avrigl", "Matg", "Zercladur", "Fanadur", "Avust", "Settember", "October", "November", "December"
				],
				dayOfWeekShort: [
					"Du", "Gli", "Ma", "Me", "Gie", "Ve", "So"
				],
				dayOfWeek: [
					"Dumengia", "Glindesdi", "Mardi", "Mesemna", "Gievgia", "Venderdi", "Sonda"
				]
			},
			ka: { // Georgian
				months: [
					'იანვარი', 'თებერვალი', 'მარტი', 'აპრილი', 'მაისი', 'ივნისი', 'ივლისი', 'აგვისტო', 'სექტემბერი', 'ოქტომბერი', 'ნოემბერი', 'დეკემბერი'
				],
				dayOfWeekShort: [
					"კვ", "ორშ", "სამშ", "ოთხ", "ხუთ", "პარ", "შაბ"
				],
				dayOfWeek: ["კვირა", "ორშაბათი", "სამშაბათი", "ოთხშაბათი", "ხუთშაბათი", "პარასკევი", "შაბათი"]
			}
		},

		ownerDocument: document,
		contentWindow: window,

		value: '',
		rtl: false,

		format:	'Y/m/d H:i',
		formatTime:	'H:i',
		formatDate:	'Y/m/d',

		startDate:	false, // new Date(), '1986/12/08', '-1970/01/05','-1970/01/05',
		step: 60,
		monthChangeSpinner: true,

		closeOnDateSelect: false,
		closeOnTimeSelect: true,
		closeOnWithoutClick: true,
		closeOnInputClick: true,
		openOnFocus: true,

		timepicker: true,
		datepicker: true,
		weeks: false,

		defaultTime: false,	// use formatTime format (ex. '10:00' for formatTime:	'H:i')
		defaultDate: false,	// use formatDate format (ex new Date() or '1986/12/08' or '-1970/01/05' or '-1970/01/05')

		minDate: false,
		maxDate: false,
		minTime: false,
		maxTime: false,
		minDateTime: false,
		maxDateTime: false,

		allowTimes: [],
		opened: false,
		initTime: true,
		inline: false,
		theme: '',
		touchMovedThreshold: 5,

		onSelectDate: function () {},
		onSelectTime: function () {},
		onChangeMonth: function () {},
		onGetWeekOfYear: function () {},
		onChangeYear: function () {},
		onChangeDateTime: function () {},
		onShow: function () {},
		onClose: function () {},
		onGenerate: function () {},

		withoutCopyright: true,
		inverseButton: false,
		hours12: false,
		next: 'xdsoft_next',
		prev : 'xdsoft_prev',
		dayOfWeekStart: 0,
		parentID: 'body',
		timeHeightInTimePicker: 25,
		timepickerScrollbar: true,
		todayButton: true,
		prevButton: true,
		nextButton: true,
		defaultSelect: true,

		scrollMonth: true,
		scrollTime: true,
		scrollInput: true,

		lazyInit: false,
		mask: false,
		validateOnBlur: true,
		allowBlank: true,
		yearStart: 1950,
		yearEnd: 2050,
		monthStart: 0,
		monthEnd: 11,
		style: '',
		id: '',
		fixed: false,
		roundTime: 'round', // ceil, floor
		className: '',
		weekends: [],
		highlightedDates: [],
		highlightedPeriods: [],
		allowDates : [],
		allowDateRe : null,
		disabledDates : [],
		disabledWeekDays: [],
		yearOffset: 0,
		beforeShowDay: null,

		enterLikeTab: true,
		showApplyButton: false
	};

	var dateHelper = null,
		defaultDateHelper = null,
		globalLocaleDefault = 'en',
		globalLocale = 'en';

	var dateFormatterOptionsDefault = {
		meridiem: ['AM', 'PM']
	};

	var initDateFormatter = function(){
		var locale = default_options.i18n[globalLocale],
			opts = {
				days: locale.dayOfWeek,
				daysShort: locale.dayOfWeekShort,
				months: locale.months,
				monthsShort: $.map(locale.months, function(n){ return n.substring(0, 3) })
			};

		if (typeof DateFormatter === 'function') {
			dateHelper = defaultDateHelper = new DateFormatter({
				dateSettings: $.extend({}, dateFormatterOptionsDefault, opts)
			});
		}
	};

	var dateFormatters = {
		moment: {
			default_options:{
				format: 'YYYY/MM/DD HH:mm',
				formatDate: 'YYYY/MM/DD',
				formatTime: 'HH:mm',
			},
			formatter: {
				parseDate: function (date, format) {
					if(isFormatStandard(format)){
						return defaultDateHelper.parseDate(date, format);
					} 
					var d = moment(date, format);
					return d.isValid() ? d.toDate() : false;
				},

				formatDate: function (date, format) {
					if(isFormatStandard(format)){
						return defaultDateHelper.formatDate(date, format);
					} 
					return moment(date).format(format);
				},

				formatMask: function(format){
					return format
						.replace(/Y{4}/g, '9999')
						.replace(/Y{2}/g, '99')
						.replace(/M{2}/g, '19')
						.replace(/D{2}/g, '39')
						.replace(/H{2}/g, '29')
						.replace(/m{2}/g, '59')
						.replace(/s{2}/g, '59');
				},
			}
		}
	}

	// for locale settings
	$.datetimepicker = {
		setLocale: function(locale){
			var newLocale = default_options.i18n[locale] ? locale : globalLocaleDefault;
			if (globalLocale !== newLocale) {
				globalLocale = newLocale;
				// reinit date formatter
				initDateFormatter();
			}
		},

		setDateFormatter: function(dateFormatter) {
			if(typeof dateFormatter === 'string' && dateFormatters.hasOwnProperty(dateFormatter)){
				var df = dateFormatters[dateFormatter];
				$.extend(default_options, df.default_options);
				dateHelper = df.formatter; 
			}
			else {
				dateHelper = dateFormatter;
			}
		},
	};

	var standardFormats = {
		RFC_2822: 'D, d M Y H:i:s O',
		ATOM: 'Y-m-d\TH:i:sP',
		ISO_8601: 'Y-m-d\TH:i:sO',
		RFC_822: 'D, d M y H:i:s O',
		RFC_850: 'l, d-M-y H:i:s T',
		RFC_1036: 'D, d M y H:i:s O',
		RFC_1123: 'D, d M Y H:i:s O',
		RSS: 'D, d M Y H:i:s O',
		W3C: 'Y-m-d\TH:i:sP'
	}

	var isFormatStandard = function(format){
		return Object.values(standardFormats).indexOf(format) === -1 ? false : true;
	}

	$.extend($.datetimepicker, standardFormats);

	// first init date formatter
	initDateFormatter();

	// fix for ie8
	if (!window.getComputedStyle) {
		window.getComputedStyle = function (el) {
			this.el = el;
			this.getPropertyValue = function (prop) {
				var re = /(-([a-z]))/g;
				if (prop === 'float') {
					prop = 'styleFloat';
				}
				if (re.test(prop)) {
					prop = prop.replace(re, function (a, b, c) {
						return c.toUpperCase();
					});
				}
				return el.currentStyle[prop] || null;
			};
			return this;
		};
	}
	if (!Array.prototype.indexOf) {
		Array.prototype.indexOf = function (obj, start) {
			var i, j;
			for (i = (start || 0), j = this.length; i < j; i += 1) {
				if (this[i] === obj) { return i; }
			}
			return -1;
		};
	}

	Date.prototype.countDaysInMonth = function () {
		return new Date(this.getFullYear(), this.getMonth() + 1, 0).getDate();
	};

	$.fn.xdsoftScroller = function (options, percent) {
		return this.each(function () {
			var timeboxparent = $(this),
				pointerEventToXY = function (e) {
					var out = {x: 0, y: 0},
						touch;
					if (e.type === 'touchstart' || e.type === 'touchmove' || e.type === 'touchend' || e.type === 'touchcancel') {
						touch  = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
						out.x = touch.clientX;
						out.y = touch.clientY;
					} else if (e.type === 'mousedown' || e.type === 'mouseup' || e.type === 'mousemove' || e.type === 'mouseover' || e.type === 'mouseout' || e.type === 'mouseenter' || e.type === 'mouseleave') {
						out.x = e.clientX;
						out.y = e.clientY;
					}
					return out;
				},
				timebox,
				parentHeight,
				height,
				scrollbar,
				scroller,
				maximumOffset = 100,
				start = false,
				startY = 0,
				startTop = 0,
				h1 = 0,
				touchStart = false,
				startTopScroll = 0,
				calcOffset = function () {};

			if (percent === 'hide') {
				timeboxparent.find('.xdsoft_scrollbar').hide();
				return;
			}

			if (!$(this).hasClass('xdsoft_scroller_box')) {
				timebox = timeboxparent.children().eq(0);
				parentHeight = timeboxparent[0].clientHeight;
				height = timebox[0].offsetHeight;
				scrollbar = $('<div class="xdsoft_scrollbar"></div>');
				scroller = $('<div class="xdsoft_scroller"></div>');
				scrollbar.append(scroller);

				timeboxparent.addClass('xdsoft_scroller_box').append(scrollbar);
				calcOffset = function calcOffset(event) {
					var offset = pointerEventToXY(event).y - startY + startTopScroll;
					if (offset < 0) {
						offset = 0;
					}
					if (offset + scroller[0].offsetHeight > h1) {
						offset = h1 - scroller[0].offsetHeight;
					}
					timeboxparent.trigger('scroll_element.xdsoft_scroller', [maximumOffset ? offset / maximumOffset : 0]);
				};

				scroller
					.on('touchstart.xdsoft_scroller mousedown.xdsoft_scroller', function (event) {
						if (!parentHeight) {
							timeboxparent.trigger('resize_scroll.xdsoft_scroller', [percent]);
						}

						startY = pointerEventToXY(event).y;
						startTopScroll = parseInt(scroller.css('margin-top'), 10);
						h1 = scrollbar[0].offsetHeight;

						if (event.type === 'mousedown' || event.type === 'touchstart') {
							if (options.ownerDocument) {
								$(options.ownerDocument.body).addClass('xdsoft_noselect');
							}
							$([options.ownerDocument.body, options.contentWindow]).on('touchend mouseup.xdsoft_scroller', function arguments_callee() {
								$([options.ownerDocument.body, options.contentWindow]).off('touchend mouseup.xdsoft_scroller', arguments_callee)
									.off('mousemove.xdsoft_scroller', calcOffset)
									.removeClass('xdsoft_noselect');
							});
							$(options.ownerDocument.body).on('mousemove.xdsoft_scroller', calcOffset);
						} else {
							touchStart = true;
							event.stopPropagation();
							event.preventDefault();
						}
					})
					.on('touchmove', function (event) {
						if (touchStart) {
							event.preventDefault();
							calcOffset(event);
						}
					})
					.on('touchend touchcancel', function () {
						touchStart =  false;
						startTopScroll = 0;
					});

				timeboxparent
					.on('scroll_element.xdsoft_scroller', function (event, percentage) {
						if (!parentHeight) {
							timeboxparent.trigger('resize_scroll.xdsoft_scroller', [percentage, true]);
						}
						percentage = percentage > 1 ? 1 : (percentage < 0 || isNaN(percentage)) ? 0 : percentage;

						scroller.css('margin-top', maximumOffset * percentage);

						setTimeout(function () {
							timebox.css('marginTop', -parseInt((timebox[0].offsetHeight - parentHeight) * percentage, 10));
						}, 10);
					})
					.on('resize_scroll.xdsoft_scroller', function (event, percentage, noTriggerScroll) {
						var percent, sh;
						parentHeight = timeboxparent[0].clientHeight;
						height = timebox[0].offsetHeight;
						percent = parentHeight / height;
						sh = percent * scrollbar[0].offsetHeight;
						if (percent > 1) {
							scroller.hide();
						} else {
							scroller.show();
							scroller.css('height', parseInt(sh > 10 ? sh : 10, 10));
							maximumOffset = scrollbar[0].offsetHeight - scroller[0].offsetHeight;
							if (noTriggerScroll !== true) {
								timeboxparent.trigger('scroll_element.xdsoft_scroller', [percentage || Math.abs(parseInt(timebox.css('marginTop'), 10)) / (height - parentHeight)]);
							}
						}
					});

				timeboxparent.on('mousewheel', function (event) {
					var top = Math.abs(parseInt(timebox.css('marginTop'), 10));

					top = top - (event.deltaY * 20);
					if (top < 0) {
						top = 0;
					}

					timeboxparent.trigger('scroll_element.xdsoft_scroller', [top / (height - parentHeight)]);
					event.stopPropagation();
					return false;
				});

				timeboxparent.on('touchstart', function (event) {
					start = pointerEventToXY(event);
					startTop = Math.abs(parseInt(timebox.css('marginTop'), 10));
				});

				timeboxparent.on('touchmove', function (event) {
					if (start) {
						event.preventDefault();
						var coord = pointerEventToXY(event);
						timeboxparent.trigger('scroll_element.xdsoft_scroller', [(startTop - (coord.y - start.y)) / (height - parentHeight)]);
					}
				});

				timeboxparent.on('touchend touchcancel', function () {
					start = false;
					startTop = 0;
				});
			}
			timeboxparent.trigger('resize_scroll.xdsoft_scroller', [percent]);
		});
	};

	$.fn.datetimepicker = function (opt, opt2) {
		var result = this,
			KEY0 = 48,
			KEY9 = 57,
			_KEY0 = 96,
			_KEY9 = 105,
			CTRLKEY = 17,
			DEL = 46,
			ENTER = 13,
			ESC = 27,
			BACKSPACE = 8,
			ARROWLEFT = 37,
			ARROWUP = 38,
			ARROWRIGHT = 39,
			ARROWDOWN = 40,
			TAB = 9,
			F5 = 116,
			AKEY = 65,
			CKEY = 67,
			VKEY = 86,
			ZKEY = 90,
			YKEY = 89,
			ctrlDown	=	false,
			options = ($.isPlainObject(opt) || !opt) ? $.extend(true, {}, default_options, opt) : $.extend(true, {}, default_options),

			lazyInitTimer = 0,
			createDateTimePicker,
			destroyDateTimePicker,

			lazyInit = function (input) {
				input
					.on('open.xdsoft focusin.xdsoft mousedown.xdsoft touchstart', function initOnActionCallback() {
						if (input.is(':disabled') || input.data('xdsoft_datetimepicker')) {
							return;
						}
						clearTimeout(lazyInitTimer);
						lazyInitTimer = setTimeout(function () {

							if (!input.data('xdsoft_datetimepicker')) {
								createDateTimePicker(input);
							}
							input
								.off('open.xdsoft focusin.xdsoft mousedown.xdsoft touchstart', initOnActionCallback)
								.trigger('open.xdsoft');
						}, 100);
					});
			};

		createDateTimePicker = function (input) {
			var datetimepicker = $('<div class="xdsoft_datetimepicker xdsoft_noselect"></div>'),
				xdsoft_copyright = $('<div class="xdsoft_copyright"><a target="_blank" href="http://xdsoft.net/jqplugins/datetimepicker/">xdsoft.net</a></div>'),
				datepicker = $('<div class="xdsoft_datepicker active"></div>'),
				month_picker = $('<div class="xdsoft_monthpicker"><button type="button" class="xdsoft_prev"></button><button type="button" class="xdsoft_today_button"></button>' +
					'<div class="xdsoft_label xdsoft_month"><span></span><i></i></div>' +
					'<div class="xdsoft_label xdsoft_year"><span></span><i></i></div>' +
					'<button type="button" class="xdsoft_next"></button></div>'),
				calendar = $('<div class="xdsoft_calendar"></div>'),
				timepicker = $('<div class="xdsoft_timepicker active"><button type="button" class="xdsoft_prev"></button><div class="xdsoft_time_box"></div><button type="button" class="xdsoft_next"></button></div>'),
				timeboxparent = timepicker.find('.xdsoft_time_box').eq(0),
				timebox = $('<div class="xdsoft_time_variant"></div>'),
				applyButton = $('<button type="button" class="xdsoft_save_selected blue-gradient-button">Save Selected</button>'),

				monthselect = $('<div class="xdsoft_select xdsoft_monthselect"><div></div></div>'),
				yearselect = $('<div class="xdsoft_select xdsoft_yearselect"><div></div></div>'),
				triggerAfterOpen = false,
				XDSoft_datetime,

				xchangeTimer,
				timerclick,
				current_time_index,
				setPos,
				timer = 0,
				_xdsoft_datetime,
				forEachAncestorOf;

			if (options.id) {
				datetimepicker.attr('id', options.id);
			}
			if (options.style) {
				datetimepicker.attr('style', options.style);
			}
			if (options.weeks) {
				datetimepicker.addClass('xdsoft_showweeks');
			}
			if (options.rtl) {
				datetimepicker.addClass('xdsoft_rtl');
			}

			datetimepicker.addClass('xdsoft_' + options.theme);
			datetimepicker.addClass(options.className);

			month_picker
				.find('.xdsoft_month span')
				.after(monthselect);
			month_picker
				.find('.xdsoft_year span')
				.after(yearselect);

			month_picker
				.find('.xdsoft_month,.xdsoft_year')
				.on('touchstart mousedown.xdsoft', function (event) {
					var select = $(this).find('.xdsoft_select').eq(0),
						val = 0,
						top = 0,
						visible = select.is(':visible'),
						items,
						i;

					month_picker
						.find('.xdsoft_select')
						.hide();
					if (_xdsoft_datetime.currentTime) {
						val = _xdsoft_datetime.currentTime[$(this).hasClass('xdsoft_month') ? 'getMonth' : 'getFullYear']();
					}

					select[visible ? 'hide' : 'show']();
					for (items = select.find('div.xdsoft_option'), i = 0; i < items.length; i += 1) {
						if (items.eq(i).data('value') === val) {
							break;
						} else {
							top += items[0].offsetHeight;
						}
					}

					select.xdsoftScroller(options, top / (select.children()[0].offsetHeight - (select[0].clientHeight)));
					event.stopPropagation();
					return false;
				});

			var handleTouchMoved = function (event) {
				var evt = event.originalEvent;
				var touchPosition = evt.touches ? evt.touches[0] : evt;
				this.touchStartPosition = this.touchStartPosition || touchPosition;
				var xMovement = Math.abs(this.touchStartPosition.clientX - touchPosition.clientX);
				var yMovement = Math.abs(this.touchStartPosition.clientY - touchPosition.clientY);
				var distance = Math.sqrt(xMovement * xMovement + yMovement * yMovement);
				if(distance > options.touchMovedThreshold) {
					this.touchMoved = true;
				}
			}

			month_picker
				.find('.xdsoft_select')
				.xdsoftScroller(options)
				.on('touchstart mousedown.xdsoft', function (event) {
					var evt = event.originalEvent;
					this.touchMoved = false;
					this.touchStartPosition = evt.touches ? evt.touches[0] : evt;
					event.stopPropagation();
					event.preventDefault();
				})
				.on('touchmove', '.xdsoft_option', handleTouchMoved)
				.on('touchend mousedown.xdsoft', '.xdsoft_option', function () {
					if (!this.touchMoved) {
						if (_xdsoft_datetime.currentTime === undefined || _xdsoft_datetime.currentTime === null) {
							_xdsoft_datetime.currentTime = _xdsoft_datetime.now();
						}

						var year = _xdsoft_datetime.currentTime.getFullYear();
						if (_xdsoft_datetime && _xdsoft_datetime.currentTime) {
							_xdsoft_datetime.currentTime[$(this).parent().parent().hasClass('xdsoft_monthselect') ? 'setMonth' : 'setFullYear']($(this).data('value'));
						}

						$(this).parent().parent().hide();

						datetimepicker.trigger('xchange.xdsoft');
						if (options.onChangeMonth && $.isFunction(options.onChangeMonth)) {
							options.onChangeMonth.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'));
						}

						if (year !== _xdsoft_datetime.currentTime.getFullYear() && $.isFunction(options.onChangeYear)) {
							options.onChangeYear.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'));
						}
					}
				});

			datetimepicker.getValue = function () {
				return _xdsoft_datetime.getCurrentTime();
			};

			datetimepicker.setOptions = function (_options) {
				var highlightedDates = {};

				options = $.extend(true, {}, options, _options);

				if (_options.allowTimes && $.isArray(_options.allowTimes) && _options.allowTimes.length) {
					options.allowTimes = $.extend(true, [], _options.allowTimes);
				}

				if (_options.weekends && $.isArray(_options.weekends) && _options.weekends.length) {
					options.weekends = $.extend(true, [], _options.weekends);
				}

				if (_options.allowDates && $.isArray(_options.allowDates) && _options.allowDates.length) {
					options.allowDates = $.extend(true, [], _options.allowDates);
				}

				if (_options.allowDateRe && Object.prototype.toString.call(_options.allowDateRe)==="[object String]") {
					options.allowDateRe = new RegExp(_options.allowDateRe);
				}

				if (_options.highlightedDates && $.isArray(_options.highlightedDates) && _options.highlightedDates.length) {
					$.each(_options.highlightedDates, function (index, value) {
						var splitData = $.map(value.split(','), $.trim),
							exDesc,
							hDate = new HighlightedDate(dateHelper.parseDate(splitData[0], options.formatDate), splitData[1], splitData[2]), // date, desc, style
							keyDate = dateHelper.formatDate(hDate.date, options.formatDate);
						if (highlightedDates[keyDate] !== undefined) {
							exDesc = highlightedDates[keyDate].desc;
							if (exDesc && exDesc.length && hDate.desc && hDate.desc.length) {
								highlightedDates[keyDate].desc = exDesc + "\n" + hDate.desc;
							}
						} else {
							highlightedDates[keyDate] = hDate;
						}
					});

					options.highlightedDates = $.extend(true, [], highlightedDates);
				}

				if (_options.highlightedPeriods && $.isArray(_options.highlightedPeriods) && _options.highlightedPeriods.length) {
					highlightedDates = $.extend(true, [], options.highlightedDates);
					$.each(_options.highlightedPeriods, function (index, value) {
						var dateTest, // start date
							dateEnd,
							desc,
							hDate,
							keyDate,
							exDesc,
							style;
						if ($.isArray(value)) {
							dateTest = value[0];
							dateEnd = value[1];
							desc = value[2];
							style = value[3];
						}
						else {
							var splitData = $.map(value.split(','), $.trim);
							dateTest = dateHelper.parseDate(splitData[0], options.formatDate);
							dateEnd = dateHelper.parseDate(splitData[1], options.formatDate);
							desc = splitData[2];
							style = splitData[3];
						}

						while (dateTest <= dateEnd) {
							hDate = new HighlightedDate(dateTest, desc, style);
							keyDate = dateHelper.formatDate(dateTest, options.formatDate);
							dateTest.setDate(dateTest.getDate() + 1);
							if (highlightedDates[keyDate] !== undefined) {
								exDesc = highlightedDates[keyDate].desc;
								if (exDesc && exDesc.length && hDate.desc && hDate.desc.length) {
									highlightedDates[keyDate].desc = exDesc + "\n" + hDate.desc;
								}
							} else {
								highlightedDates[keyDate] = hDate;
							}
						}
					});

					options.highlightedDates = $.extend(true, [], highlightedDates);
				}

				if (_options.disabledDates && $.isArray(_options.disabledDates) && _options.disabledDates.length) {
					options.disabledDates = $.extend(true, [], _options.disabledDates);
				}

				if (_options.disabledWeekDays && $.isArray(_options.disabledWeekDays) && _options.disabledWeekDays.length) {
					options.disabledWeekDays = $.extend(true, [], _options.disabledWeekDays);
				}

				if ((options.open || options.opened) && (!options.inline)) {
					input.trigger('open.xdsoft');
				}

				if (options.inline) {
					triggerAfterOpen = true;
					datetimepicker.addClass('xdsoft_inline');
					input.after(datetimepicker).hide();
				}

				if (options.inverseButton) {
					options.next = 'xdsoft_prev';
					options.prev = 'xdsoft_next';
				}

				if (options.datepicker) {
					datepicker.addClass('active');
				} else {
					datepicker.removeClass('active');
				}

				if (options.timepicker) {
					timepicker.addClass('active');
				} else {
					timepicker.removeClass('active');
				}

				if (options.value) {
					_xdsoft_datetime.setCurrentTime(options.value);
					if (input && input.val) {
						input.val(_xdsoft_datetime.str);
					}
				}

				if (isNaN(options.dayOfWeekStart)) {
					options.dayOfWeekStart = 0;
				} else {
					options.dayOfWeekStart = parseInt(options.dayOfWeekStart, 10) % 7;
				}

				if (!options.timepickerScrollbar) {
					timeboxparent.xdsoftScroller(options, 'hide');
				}

				if (options.minDate && /^[\+\-](.*)$/.test(options.minDate)) {
					options.minDate = dateHelper.formatDate(_xdsoft_datetime.strToDateTime(options.minDate), options.formatDate);
				}

				if (options.maxDate &&  /^[\+\-](.*)$/.test(options.maxDate)) {
					options.maxDate = dateHelper.formatDate(_xdsoft_datetime.strToDateTime(options.maxDate), options.formatDate);
				}

                if (options.minDateTime &&  /^\+(.*)$/.test(options.minDateTime)) {
                	options.minDateTime = _xdsoft_datetime.strToDateTime(options.minDateTime).dateFormat(options.formatDate);
                }

                if (options.maxDateTime &&  /^\+(.*)$/.test(options.maxDateTime)) {
                	options.maxDateTime = _xdsoft_datetime.strToDateTime(options.maxDateTime).dateFormat(options.formatDate);
                }

				applyButton.toggle(options.showApplyButton);

				month_picker
					.find('.xdsoft_today_button')
					.css('visibility', !options.todayButton ? 'hidden' : 'visible');

				month_picker
					.find('.' + options.prev)
					.css('visibility', !options.prevButton ? 'hidden' : 'visible');

				month_picker
					.find('.' + options.next)
					.css('visibility', !options.nextButton ? 'hidden' : 'visible');

				setMask(options);

				if (options.validateOnBlur) {
					input
						.off('blur.xdsoft')
						.on('blur.xdsoft', function () {
							if (options.allowBlank && (!$.trim($(this).val()).length ||
									(typeof options.mask === "string" && $.trim($(this).val()) === options.mask.replace(/[0-9]/g, '_')))) {
								$(this).val(null);
								datetimepicker.data('xdsoft_datetime').empty();
							} else {
								var d = dateHelper.parseDate($(this).val(), options.format);
								if (d) { // parseDate() may skip some invalid parts like date or time, so make it clear for user: show parsed date/time
									$(this).val(dateHelper.formatDate(d, options.format));
								} else {
									var splittedHours   = +([$(this).val()[0], $(this).val()[1]].join('')),
										splittedMinutes = +([$(this).val()[2], $(this).val()[3]].join(''));

									// parse the numbers as 0312 => 03:12
									if (!options.datepicker && options.timepicker && splittedHours >= 0 && splittedHours < 24 && splittedMinutes >= 0 && splittedMinutes < 60) {
										$(this).val([splittedHours, splittedMinutes].map(function (item) {
											return item > 9 ? item : '0' + item;
										}).join(':'));
									} else {
										$(this).val(dateHelper.formatDate(_xdsoft_datetime.now(), options.format));
									}
								}
								datetimepicker.data('xdsoft_datetime').setCurrentTime($(this).val());
							}

							datetimepicker.trigger('changedatetime.xdsoft');
							datetimepicker.trigger('close.xdsoft');
						});
				}
				options.dayOfWeekStartPrev = (options.dayOfWeekStart === 0) ? 6 : options.dayOfWeekStart - 1;

				datetimepicker
					.trigger('xchange.xdsoft')
					.trigger('afterOpen.xdsoft');
			};

			datetimepicker
				.data('options', options)
				.on('touchstart mousedown.xdsoft', function (event) {
					event.stopPropagation();
					event.preventDefault();
					yearselect.hide();
					monthselect.hide();
					return false;
				});

			//scroll_element = timepicker.find('.xdsoft_time_box');
			timeboxparent.append(timebox);
			timeboxparent.xdsoftScroller(options);

			datetimepicker.on('afterOpen.xdsoft', function () {
				timeboxparent.xdsoftScroller(options);
			});

			datetimepicker
				.append(datepicker)
				.append(timepicker);

			if (options.withoutCopyright !== true) {
				datetimepicker
					.append(xdsoft_copyright);
			}

			datepicker
				.append(month_picker)
				.append(calendar)
				.append(applyButton);

			$(options.parentID)
				.append(datetimepicker);

			XDSoft_datetime = function () {
				var _this = this;
				_this.now = function (norecursion) {
					var d = new Date(),
						date,
						time;

					if (!norecursion && options.defaultDate) {
						date = _this.strToDateTime(options.defaultDate);
						d.setFullYear(date.getFullYear());
						d.setMonth(date.getMonth());
						d.setDate(date.getDate());
					}

					d.setFullYear(d.getFullYear());

					if (!norecursion && options.defaultTime) {
						time = _this.strtotime(options.defaultTime);
						d.setHours(time.getHours());
						d.setMinutes(time.getMinutes());
						d.setSeconds(time.getSeconds());
						d.setMilliseconds(time.getMilliseconds());
					}
					return d;
				};

				_this.isValidDate = function (d) {
					if (Object.prototype.toString.call(d) !== "[object Date]") {
						return false;
					}
					return !isNaN(d.getTime());
				};

				_this.setCurrentTime = function (dTime, requireValidDate) {
					if (typeof dTime === 'string') {
						_this.currentTime = _this.strToDateTime(dTime);
					}
					else if (_this.isValidDate(dTime)) {
						_this.currentTime = dTime;
					}
					else if (!dTime && !requireValidDate && options.allowBlank && !options.inline) {
						_this.currentTime = null;
					}
					else {
						_this.currentTime = _this.now();
					}

					datetimepicker.trigger('xchange.xdsoft');
				};

				_this.empty = function () {
					_this.currentTime = null;
				};

				_this.getCurrentTime = function () {
					return _this.currentTime;
				};

				_this.nextMonth = function () {

					if (_this.currentTime === undefined || _this.currentTime === null) {
						_this.currentTime = _this.now();
					}

					var month = _this.currentTime.getMonth() + 1,
						year;
					if (month === 12) {
						_this.currentTime.setFullYear(_this.currentTime.getFullYear() + 1);
						month = 0;
					}

					year = _this.currentTime.getFullYear();

					_this.currentTime.setDate(
						Math.min(
							new Date(_this.currentTime.getFullYear(), month + 1, 0).getDate(),
							_this.currentTime.getDate()
						)
					);
					_this.currentTime.setMonth(month);

					if (options.onChangeMonth && $.isFunction(options.onChangeMonth)) {
						options.onChangeMonth.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'));
					}

					if (year !== _this.currentTime.getFullYear() && $.isFunction(options.onChangeYear)) {
						options.onChangeYear.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'));
					}

					datetimepicker.trigger('xchange.xdsoft');
					return month;
				};

				_this.prevMonth = function () {

					if (_this.currentTime === undefined || _this.currentTime === null) {
						_this.currentTime = _this.now();
					}

					var month = _this.currentTime.getMonth() - 1;
					if (month === -1) {
						_this.currentTime.setFullYear(_this.currentTime.getFullYear() - 1);
						month = 11;
					}
					_this.currentTime.setDate(
						Math.min(
							new Date(_this.currentTime.getFullYear(), month + 1, 0).getDate(),
							_this.currentTime.getDate()
						)
					);
					_this.currentTime.setMonth(month);
					if (options.onChangeMonth && $.isFunction(options.onChangeMonth)) {
						options.onChangeMonth.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'));
					}
					datetimepicker.trigger('xchange.xdsoft');
					return month;
				};

				_this.getWeekOfYear = function (datetime) {
					if (options.onGetWeekOfYear && $.isFunction(options.onGetWeekOfYear)) {
						var week = options.onGetWeekOfYear.call(datetimepicker, datetime);
						if (typeof week !== 'undefined') {
							return week;
						}
					}
					var onejan = new Date(datetime.getFullYear(), 0, 1);

					//First week of the year is th one with the first Thursday according to ISO8601
					if (onejan.getDay() !== 4) {
						onejan.setMonth(0, 1 + ((4 - onejan.getDay()+ 7) % 7));
					}

					return Math.ceil((((datetime - onejan) / 86400000) + onejan.getDay() + 1) / 7);
				};

				_this.strToDateTime = function (sDateTime) {
					var tmpDate = [], timeOffset, currentTime;

					if (sDateTime && sDateTime instanceof Date && _this.isValidDate(sDateTime)) {
						return sDateTime;
					}

					tmpDate = /^([+-]{1})(.*)$/.exec(sDateTime);

					if (tmpDate) {
						tmpDate[2] = dateHelper.parseDate(tmpDate[2], options.formatDate);
					}

					if (tmpDate  && tmpDate[2]) {
						timeOffset = tmpDate[2].getTime() - (tmpDate[2].getTimezoneOffset()) * 60000;
						currentTime = new Date((_this.now(true)).getTime() + parseInt(tmpDate[1] + '1', 10) * timeOffset);
					} else {
						currentTime = sDateTime ? dateHelper.parseDate(sDateTime, options.format) : _this.now();
					}

					if (!_this.isValidDate(currentTime)) {
						currentTime = _this.now();
					}

					return currentTime;
				};

				_this.strToDate = function (sDate) {
					if (sDate && sDate instanceof Date && _this.isValidDate(sDate)) {
						return sDate;
					}

					var currentTime = sDate ? dateHelper.parseDate(sDate, options.formatDate) : _this.now(true);
					if (!_this.isValidDate(currentTime)) {
						currentTime = _this.now(true);
					}
					return currentTime;
				};

				_this.strtotime = function (sTime) {
					if (sTime && sTime instanceof Date && _this.isValidDate(sTime)) {
						return sTime;
					}
					var currentTime = sTime ? dateHelper.parseDate(sTime, options.formatTime) : _this.now(true);
					if (!_this.isValidDate(currentTime)) {
						currentTime = _this.now(true);
					}
					return currentTime;
				};

				_this.str = function () {
					var format = options.format;
					if (options.yearOffset) {
						format = format.replace('Y', _this.currentTime.getFullYear() + options.yearOffset);
						format = format.replace('y', String(_this.currentTime.getFullYear() + options.yearOffset).substring(2, 4));
					}
					return dateHelper.formatDate(_this.currentTime, format);
				};
				_this.currentTime = this.now();
			};

			_xdsoft_datetime = new XDSoft_datetime();

			applyButton.on('touchend click', function (e) {//pathbrite
				e.preventDefault();
				datetimepicker.data('changed', true);
				_xdsoft_datetime.setCurrentTime(getCurrentValue());
				input.val(_xdsoft_datetime.str());
				datetimepicker.trigger('close.xdsoft');
			});
			month_picker
				.find('.xdsoft_today_button')
				.on('touchend mousedown.xdsoft', function () {
					datetimepicker.data('changed', true);
					_xdsoft_datetime.setCurrentTime(0, true);
					datetimepicker.trigger('afterOpen.xdsoft');
				}).on('dblclick.xdsoft', function () {
				var currentDate = _xdsoft_datetime.getCurrentTime(), minDate, maxDate;
				currentDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate());
				minDate = _xdsoft_datetime.strToDate(options.minDate);
				minDate = new Date(minDate.getFullYear(), minDate.getMonth(), minDate.getDate());
				if (currentDate < minDate) {
					return;
				}
				maxDate = _xdsoft_datetime.strToDate(options.maxDate);
				maxDate = new Date(maxDate.getFullYear(), maxDate.getMonth(), maxDate.getDate());
				if (currentDate > maxDate) {
					return;
				}
				input.val(_xdsoft_datetime.str());
				input.trigger('change');
				datetimepicker.trigger('close.xdsoft');
			});
			month_picker
				.find('.xdsoft_prev,.xdsoft_next')
				.on('touchend mousedown.xdsoft', function () {
					var $this = $(this),
						timer = 0,
						stop = false;

					(function arguments_callee1(v) {
						if ($this.hasClass(options.next)) {
							_xdsoft_datetime.nextMonth();
						} else if ($this.hasClass(options.prev)) {
							_xdsoft_datetime.prevMonth();
						}
						if (options.monthChangeSpinner) {
							if (!stop) {
								timer = setTimeout(arguments_callee1, v || 100);
							}
						}
					}(500));

					$([options.ownerDocument.body, options.contentWindow]).on('touchend mouseup.xdsoft', function arguments_callee2() {
						clearTimeout(timer);
						stop = true;
						$([options.ownerDocument.body, options.contentWindow]).off('touchend mouseup.xdsoft', arguments_callee2);
					});
				});

			timepicker
				.find('.xdsoft_prev,.xdsoft_next')
				.on('touchend mousedown.xdsoft', function () {
					var $this = $(this),
						timer = 0,
						stop = false,
						period = 110;
					(function arguments_callee4(v) {
						var pheight = timeboxparent[0].clientHeight,
							height = timebox[0].offsetHeight,
							top = Math.abs(parseInt(timebox.css('marginTop'), 10));
						if ($this.hasClass(options.next) && (height - pheight) - options.timeHeightInTimePicker >= top) {
							timebox.css('marginTop', '-' + (top + options.timeHeightInTimePicker) + 'px');
						} else if ($this.hasClass(options.prev) && top - options.timeHeightInTimePicker >= 0) {
							timebox.css('marginTop', '-' + (top - options.timeHeightInTimePicker) + 'px');
						}
						/**
						 * Fixed bug:
						 * When using css3 transition, it will cause a bug that you cannot scroll the timepicker list.
						 * The reason is that the transition-duration time, if you set it to 0, all things fine, otherwise, this
						 * would cause a bug when you use jquery.css method.
						 * Let's say: * { transition: all .5s ease; }
						 * jquery timebox.css('marginTop') will return the original value which is before you clicking the next/prev button,
						 * meanwhile the timebox[0].style.marginTop will return the right value which is after you clicking the
						 * next/prev button.
						 *
						 * What we should do:
						 * Replace timebox.css('marginTop') with timebox[0].style.marginTop.
						 */
						timeboxparent.trigger('scroll_element.xdsoft_scroller', [Math.abs(parseInt(timebox[0].style.marginTop, 10) / (height - pheight))]);
						period = (period > 10) ? 10 : period - 10;
						if (!stop) {
							timer = setTimeout(arguments_callee4, v || period);
						}
					}(500));
					$([options.ownerDocument.body, options.contentWindow]).on('touchend mouseup.xdsoft', function arguments_callee5() {
						clearTimeout(timer);
						stop = true;
						$([options.ownerDocument.body, options.contentWindow])
							.off('touchend mouseup.xdsoft', arguments_callee5);
					});
				});

			xchangeTimer = 0;
			// base handler - generating a calendar and timepicker
			datetimepicker
				.on('xchange.xdsoft', function (event) {
					clearTimeout(xchangeTimer);
					xchangeTimer = setTimeout(function () {

						if (_xdsoft_datetime.currentTime === undefined || _xdsoft_datetime.currentTime === null) {
							_xdsoft_datetime.currentTime = _xdsoft_datetime.now();
						}

						var table =	'',
							start = new Date(_xdsoft_datetime.currentTime.getFullYear(), _xdsoft_datetime.currentTime.getMonth(), 1, 12, 0, 0),
							i = 0,
							j,
							today = _xdsoft_datetime.now(),
							maxDate = false,
							minDate = false,
							minDateTime = false,
							maxDateTime = false,
							hDate,
							day,
							d,
							y,
							m,
							w,
							classes = [],
							customDateSettings,
							newRow = true,
							time = '',
							h,
							line_time,
							description;

						while (start.getDay() !== options.dayOfWeekStart) {
							start.setDate(start.getDate() - 1);
						}

						table += '<table><thead><tr>';

						if (options.weeks) {
							table += '<th></th>';
						}

						for (j = 0; j < 7; j += 1) {
							table += '<th>' + options.i18n[globalLocale].dayOfWeekShort[(j + options.dayOfWeekStart) % 7] + '</th>';
						}

						table += '</tr></thead>';
						table += '<tbody>';

						if (options.maxDate !== false) {
							maxDate = _xdsoft_datetime.strToDate(options.maxDate);
							maxDate = new Date(maxDate.getFullYear(), maxDate.getMonth(), maxDate.getDate(), 23, 59, 59, 999);
						}

						if (options.minDate !== false) {
							minDate = _xdsoft_datetime.strToDate(options.minDate);
							minDate = new Date(minDate.getFullYear(), minDate.getMonth(), minDate.getDate());
						}

                        if (options.minDateTime !== false) {
							minDateTime = _xdsoft_datetime.strToDate(options.minDateTime);
							minDateTime = new Date(minDateTime.getFullYear(), minDateTime.getMonth(), minDateTime.getDate(), minDateTime.getHours(), minDateTime.getMinutes(), minDateTime.getSeconds());
						}

                        if (options.maxDateTime !== false) {
							maxDateTime = _xdsoft_datetime.strToDate(options.maxDateTime);
							maxDateTime = new Date(maxDateTime.getFullYear(), maxDateTime.getMonth(), maxDateTime.getDate(), maxDateTime.getHours(), maxDateTime.getMinutes(), maxDateTime.getSeconds());
						}

						var maxDateTimeDay;
						if (maxDateTime !== false) {
							maxDateTimeDay = ((maxDateTime.getFullYear() * 12) + maxDateTime.getMonth()) * 31 + maxDateTime.getDate();
						}

						while (i < _xdsoft_datetime.currentTime.countDaysInMonth() || start.getDay() !== options.dayOfWeekStart || _xdsoft_datetime.currentTime.getMonth() === start.getMonth()) {
							classes = [];
							i += 1;

							day = start.getDay();
							d = start.getDate();
							y = start.getFullYear();
							m = start.getMonth();
							w = _xdsoft_datetime.getWeekOfYear(start);
							description = '';

							classes.push('xdsoft_date');

							if (options.beforeShowDay && $.isFunction(options.beforeShowDay.call)) {
								customDateSettings = options.beforeShowDay.call(datetimepicker, start);
							} else {
								customDateSettings = null;
							}

							if(options.allowDateRe && Object.prototype.toString.call(options.allowDateRe) === "[object RegExp]"){
								if(!options.allowDateRe.test(dateHelper.formatDate(start, options.formatDate))){
									classes.push('xdsoft_disabled');
								}
							}
							
							if(options.allowDates && options.allowDates.length>0){
								if(options.allowDates.indexOf(dateHelper.formatDate(start, options.formatDate)) === -1){
									classes.push('xdsoft_disabled');
								}
							}
							
							var currentDay = ((start.getFullYear() * 12) + start.getMonth()) * 31 + start.getDate();
							if ((maxDate !== false && start > maxDate) || (minDateTime !== false && start < minDateTime)  || (minDate !== false && start < minDate) || (maxDateTime !== false && currentDay > maxDateTimeDay) || (customDateSettings && customDateSettings[0] === false)) {
								classes.push('xdsoft_disabled');
							}
							
							if (options.disabledDates.indexOf(dateHelper.formatDate(start, options.formatDate)) !== -1) {
								classes.push('xdsoft_disabled');
							}
							
							if (options.disabledWeekDays.indexOf(day) !== -1) {
								classes.push('xdsoft_disabled');
							}
							
							if (input.is('[disabled]')) {
								classes.push('xdsoft_disabled');
							}

							if (customDateSettings && customDateSettings[1] !== "") {
								classes.push(customDateSettings[1]);
							}

							if (_xdsoft_datetime.currentTime.getMonth() !== m) {
								classes.push('xdsoft_other_month');
							}

							if ((options.defaultSelect || datetimepicker.data('changed')) && dateHelper.formatDate(_xdsoft_datetime.currentTime, options.formatDate) === dateHelper.formatDate(start, options.formatDate)) {
								classes.push('xdsoft_current');
							}

							if (dateHelper.formatDate(today, options.formatDate) === dateHelper.formatDate(start, options.formatDate)) {
								classes.push('xdsoft_today');
							}

							if (start.getDay() === 0 || start.getDay() === 6 || options.weekends.indexOf(dateHelper.formatDate(start, options.formatDate)) !== -1) {
								classes.push('xdsoft_weekend');
							}

							if (options.highlightedDates[dateHelper.formatDate(start, options.formatDate)] !== undefined) {
								hDate = options.highlightedDates[dateHelper.formatDate(start, options.formatDate)];
								classes.push(hDate.style === undefined ? 'xdsoft_highlighted_default' : hDate.style);
								description = hDate.desc === undefined ? '' : hDate.desc;
							}

							if (options.beforeShowDay && $.isFunction(options.beforeShowDay)) {
								classes.push(options.beforeShowDay(start));
							}

							if (newRow) {
								table += '<tr>';
								newRow = false;
								if (options.weeks) {
									table += '<th>' + w + '</th>';
								}
							}

							table += '<td data-date="' + d + '" data-month="' + m + '" data-year="' + y + '"' + ' class="xdsoft_date xdsoft_day_of_week' + start.getDay() + ' ' + classes.join(' ') + '" title="' + description + '">' +
								'<div>' + d + '</div>' +
								'</td>';

							if (start.getDay() === options.dayOfWeekStartPrev) {
								table += '</tr>';
								newRow = true;
							}

							start.setDate(d + 1);
						}
						table += '</tbody></table>';

						calendar.html(table);

						month_picker.find('.xdsoft_label span').eq(0).text(options.i18n[globalLocale].months[_xdsoft_datetime.currentTime.getMonth()]);
						month_picker.find('.xdsoft_label span').eq(1).text(_xdsoft_datetime.currentTime.getFullYear() + options.yearOffset);

						// generate timebox
						time = '';
						h = '';
						m = '';

						var minTimeMinutesOfDay = 0;
						if (options.minTime !== false) {
						    var t = _xdsoft_datetime.strtotime(options.minTime);
						    minTimeMinutesOfDay = 60 * t.getHours() + t.getMinutes();
						}
						var maxTimeMinutesOfDay = 24 * 60;
						if (options.maxTime !== false) {
						    var t = _xdsoft_datetime.strtotime(options.maxTime);
						    maxTimeMinutesOfDay = 60 * t.getHours() + t.getMinutes();
						}

						if (options.minDateTime !== false) {
							var t = _xdsoft_datetime.strToDateTime(options.minDateTime);
						        var currentDayIsMinDateTimeDay = dateHelper.formatDate(_xdsoft_datetime.currentTime, options.formatDate) === dateHelper.formatDate(t, options.formatDate);
							if (currentDayIsMinDateTimeDay) {
								var m = 60 * t.getHours() + t.getMinutes();
								if (m > minTimeMinutesOfDay) minTimeMinutesOfDay = m;
							}
						}

						if (options.maxDateTime !== false) {
							var t = _xdsoft_datetime.strToDateTime(options.maxDateTime);
						        var currentDayIsMaxDateTimeDay = dateHelper.formatDate(_xdsoft_datetime.currentTime, options.formatDate) === dateHelper.formatDate(t, options.formatDate);
							if (currentDayIsMaxDateTimeDay) {
								var m = 60 * t.getHours() + t.getMinutes();
								if (m < maxTimeMinutesOfDay) maxTimeMinutesOfDay = m;
							}
						}

						line_time = function line_time(h, m) {
							var now = _xdsoft_datetime.now(), current_time,
								isALlowTimesInit = options.allowTimes && $.isArray(options.allowTimes) && options.allowTimes.length;
							now.setHours(h);
							h = parseInt(now.getHours(), 10);
							now.setMinutes(m);
							m = parseInt(now.getMinutes(), 10);
							classes = [];
							var currentMinutesOfDay = 60 * h + m;
							if (input.is('[disabled]') || (currentMinutesOfDay >= maxTimeMinutesOfDay) || (currentMinutesOfDay < minTimeMinutesOfDay)) {
								classes.push('xdsoft_disabled');
							}

							current_time = new Date(_xdsoft_datetime.currentTime);
							current_time.setHours(parseInt(_xdsoft_datetime.currentTime.getHours(), 10));

							if (!isALlowTimesInit) {
								current_time.setMinutes(Math[options.roundTime](_xdsoft_datetime.currentTime.getMinutes() / options.step) * options.step);
							}

							if ((options.initTime || options.defaultSelect || datetimepicker.data('changed')) && current_time.getHours() === parseInt(h, 10) && ((!isALlowTimesInit && options.step > 59) || current_time.getMinutes() === parseInt(m, 10))) {
								if (options.defaultSelect || datetimepicker.data('changed')) {
									classes.push('xdsoft_current');
								} else if (options.initTime) {
									classes.push('xdsoft_init_time');
								}
							}
							if (parseInt(today.getHours(), 10) === parseInt(h, 10) && parseInt(today.getMinutes(), 10) === parseInt(m, 10)) {
								classes.push('xdsoft_today');
							}
							time += '<div class="xdsoft_time ' + classes.join(' ') + '" data-hour="' + h + '" data-minute="' + m + '">' + dateHelper.formatDate(now, options.formatTime) + '</div>';
						};

						if (!options.allowTimes || !$.isArray(options.allowTimes) || !options.allowTimes.length) {
							for (i = 0, j = 0; i < (options.hours12 ? 12 : 24); i += 1) {
								for (j = 0; j < 60; j += options.step) {
								        var currentMinutesOfDay = i * 60 + j;
								        if (currentMinutesOfDay < minTimeMinutesOfDay) continue;
								        if (currentMinutesOfDay >= maxTimeMinutesOfDay) continue;
									h = (i < 10 ? '0' : '') + i;
									m = (j < 10 ? '0' : '') + j;
									line_time(h, m);
								}
							}
						} else {
							for (i = 0; i < options.allowTimes.length; i += 1) {
								h = _xdsoft_datetime.strtotime(options.allowTimes[i]).getHours();
								m = _xdsoft_datetime.strtotime(options.allowTimes[i]).getMinutes();
								line_time(h, m);
							}
						}

						timebox.html(time);

						opt = '';

						for (i = parseInt(options.yearStart, 10); i <= parseInt(options.yearEnd, 10); i += 1) {
							opt += '<div class="xdsoft_option ' + (_xdsoft_datetime.currentTime.getFullYear() === i ? 'xdsoft_current' : '') + '" data-value="' + i + '">' + (i + options.yearOffset) + '</div>';
						}
						yearselect.children().eq(0)
							.html(opt);

						for (i = parseInt(options.monthStart, 10), opt = ''; i <= parseInt(options.monthEnd, 10); i += 1) {
							opt += '<div class="xdsoft_option ' + (_xdsoft_datetime.currentTime.getMonth() === i ? 'xdsoft_current' : '') + '" data-value="' + i + '">' + options.i18n[globalLocale].months[i] + '</div>';
						}
						monthselect.children().eq(0).html(opt);
						$(datetimepicker)
							.trigger('generate.xdsoft');
					}, 10);
					event.stopPropagation();
				})
				.on('afterOpen.xdsoft', function () {
					if (options.timepicker) {
						var classType, pheight, height, top;
						if (timebox.find('.xdsoft_current').length) {
							classType = '.xdsoft_current';
						} else if (timebox.find('.xdsoft_init_time').length) {
							classType = '.xdsoft_init_time';
						}
						if (classType) {
							pheight = timeboxparent[0].clientHeight;
							height = timebox[0].offsetHeight;
							top = timebox.find(classType).index() * options.timeHeightInTimePicker + 1;
							if ((height - pheight) < top) {
								top = height - pheight;
							}
							timeboxparent.trigger('scroll_element.xdsoft_scroller', [parseInt(top, 10) / (height - pheight)]);
						} else {
							timeboxparent.trigger('scroll_element.xdsoft_scroller', [0]);
						}
					}
				});

			timerclick = 0;
			calendar
				.on('touchend click.xdsoft', 'td', function (xdevent) {
					xdevent.stopPropagation();  // Prevents closing of Pop-ups, Modals and Flyouts in Bootstrap
					timerclick += 1;
					var $this = $(this),
						currentTime = _xdsoft_datetime.currentTime;

					if (currentTime === undefined || currentTime === null) {
						_xdsoft_datetime.currentTime = _xdsoft_datetime.now();
						currentTime = _xdsoft_datetime.currentTime;
					}

					if ($this.hasClass('xdsoft_disabled')) {
						return false;
					}

					currentTime.setDate(1);
					currentTime.setFullYear($this.data('year'));
					currentTime.setMonth($this.data('month'));
					currentTime.setDate($this.data('date'));

					datetimepicker.trigger('select.xdsoft', [currentTime]);

					input.val(_xdsoft_datetime.str());

					if (options.onSelectDate &&	$.isFunction(options.onSelectDate)) {
						options.onSelectDate.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'), xdevent);
					}

					datetimepicker.data('changed', true);
					datetimepicker.trigger('xchange.xdsoft');
					datetimepicker.trigger('changedatetime.xdsoft');
					if ((timerclick > 1 || (options.closeOnDateSelect === true || (options.closeOnDateSelect === false && !options.timepicker))) && !options.inline) {
						datetimepicker.trigger('close.xdsoft');
					}
					setTimeout(function () {
						timerclick = 0;
					}, 200);
				});

			timebox
				.on('touchstart', 'div', function (xdevent) {
					this.touchMoved = false;
				})
				.on('touchmove', 'div', handleTouchMoved)
				.on('touchend click.xdsoft', 'div', function (xdevent) {
					if (!this.touchMoved) {
						xdevent.stopPropagation();
						var $this = $(this),
							currentTime = _xdsoft_datetime.currentTime;

						if (currentTime === undefined || currentTime === null) {
							_xdsoft_datetime.currentTime = _xdsoft_datetime.now();
							currentTime = _xdsoft_datetime.currentTime;
						}

						if ($this.hasClass('xdsoft_disabled')) {
							return false;
						}
						currentTime.setHours($this.data('hour'));
						currentTime.setMinutes($this.data('minute'));
						datetimepicker.trigger('select.xdsoft', [currentTime]);

						datetimepicker.data('input').val(_xdsoft_datetime.str());

						if (options.onSelectTime && $.isFunction(options.onSelectTime)) {
							options.onSelectTime.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'), xdevent);
						}
						datetimepicker.data('changed', true);
						datetimepicker.trigger('xchange.xdsoft');
						datetimepicker.trigger('changedatetime.xdsoft');
						if (options.inline !== true && options.closeOnTimeSelect === true) {
							datetimepicker.trigger('close.xdsoft');
						}
					}
				});

			datepicker
				.on('mousewheel.xdsoft', function (event) {
					if (!options.scrollMonth) {
						return true;
					}
					if (event.deltaY < 0) {
						_xdsoft_datetime.nextMonth();
					} else {
						_xdsoft_datetime.prevMonth();
					}
					return false;
				});

			input
				.on('mousewheel.xdsoft', function (event) {
					if (!options.scrollInput) {
						return true;
					}
					if (!options.datepicker && options.timepicker) {
						current_time_index = timebox.find('.xdsoft_current').length ? timebox.find('.xdsoft_current').eq(0).index() : 0;
						if (current_time_index + event.deltaY >= 0 && current_time_index + event.deltaY < timebox.children().length) {
							current_time_index += event.deltaY;
						}
						if (timebox.children().eq(current_time_index).length) {
							timebox.children().eq(current_time_index).trigger('mousedown');
						}
						return false;
					}
					if (options.datepicker && !options.timepicker) {
						datepicker.trigger(event, [event.deltaY, event.deltaX, event.deltaY]);
						if (input.val) {
							input.val(_xdsoft_datetime.str());
						}
						datetimepicker.trigger('changedatetime.xdsoft');
						return false;
					}
				});

			datetimepicker
				.on('changedatetime.xdsoft', function (event) {
					if (options.onChangeDateTime && $.isFunction(options.onChangeDateTime)) {
						var $input = datetimepicker.data('input');
						options.onChangeDateTime.call(datetimepicker, _xdsoft_datetime.currentTime, $input, event);
						delete options.value;
						$input.trigger('change');
					}
				})
				.on('generate.xdsoft', function () {
					if (options.onGenerate && $.isFunction(options.onGenerate)) {
						options.onGenerate.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'));
					}
					if (triggerAfterOpen) {
						datetimepicker.trigger('afterOpen.xdsoft');
						triggerAfterOpen = false;
					}
				})
				.on('click.xdsoft', function (xdevent) {
					xdevent.stopPropagation();
				});

			current_time_index = 0;

			/**
			 * Runs the callback for each of the specified node's ancestors.
			 *
			 * Return FALSE from the callback to stop ascending.
			 *
			 * @param {DOMNode} node
			 * @param {Function} callback
			 * @returns {undefined}
			 */
			forEachAncestorOf = function (node, callback) {
				do {
					node = node.parentNode;

					if (!node || callback(node) === false) {
						break;
					}
				} while (node.nodeName !== 'HTML');
			};

			/**
			 * Sets the position of the picker.
			 *
			 * @returns {undefined}
			 */
			setPos = function () {
				var dateInputOffset,
					dateInputElem,
					verticalPosition,
					left,
					position,
					datetimepickerElem,
					dateInputHasFixedAncestor,
					$dateInput,
					windowWidth,
					verticalAnchorEdge,
					datetimepickerCss,
					windowHeight,
					windowScrollTop;

				$dateInput = datetimepicker.data('input');
				dateInputOffset = $dateInput.offset();
				dateInputElem = $dateInput[0];

				verticalAnchorEdge = 'top';
				verticalPosition = (dateInputOffset.top + dateInputElem.offsetHeight) - 1;
				left = dateInputOffset.left;
				position = "absolute";

				windowWidth = $(options.contentWindow).width();
				windowHeight = $(options.contentWindow).height();
				windowScrollTop = $(options.contentWindow).scrollTop();

				if ((options.ownerDocument.documentElement.clientWidth - dateInputOffset.left) < datepicker.parent().outerWidth(true)) {
					var diff = datepicker.parent().outerWidth(true) - dateInputElem.offsetWidth;
					left = left - diff;
				}

				if ($dateInput.parent().css('direction') === 'rtl') {
					left -= (datetimepicker.outerWidth() - $dateInput.outerWidth());
				}

				if (options.fixed) {
					verticalPosition -= windowScrollTop;
					left -= $(options.contentWindow).scrollLeft();
					position = "fixed";
				} else {
					dateInputHasFixedAncestor = false;

					forEachAncestorOf(dateInputElem, function (ancestorNode) {
						if (ancestorNode === null) {
							return false;
						}

						if (options.contentWindow.getComputedStyle(ancestorNode).getPropertyValue('position') === 'fixed') {
							dateInputHasFixedAncestor = true;
							return false;
						}
					});

					if (dateInputHasFixedAncestor) {
						position = 'fixed';

						//If the picker won't fit entirely within the viewport then display it above the date input.
						if (verticalPosition + datetimepicker.outerHeight() > windowHeight + windowScrollTop) {
							verticalAnchorEdge = 'bottom';
							verticalPosition = (windowHeight + windowScrollTop) - dateInputOffset.top;
						} else {
							verticalPosition -= windowScrollTop;
						}
					} else {
						if (verticalPosition + datetimepicker[0].offsetHeight > windowHeight + windowScrollTop) {
							verticalPosition = dateInputOffset.top - datetimepicker[0].offsetHeight + 1;
						}
					}

					if (verticalPosition < 0) {
						verticalPosition = 0;
					}

					if (left + dateInputElem.offsetWidth > windowWidth) {
						left = windowWidth - dateInputElem.offsetWidth;
					}
				}

				datetimepickerElem = datetimepicker[0];

				forEachAncestorOf(datetimepickerElem, function (ancestorNode) {
					var ancestorNodePosition;

					ancestorNodePosition = options.contentWindow.getComputedStyle(ancestorNode).getPropertyValue('position');

					if (ancestorNodePosition === 'relative' && windowWidth >= ancestorNode.offsetWidth) {
						left = left - ((windowWidth - ancestorNode.offsetWidth) / 2);
						return false;
					}
				});

				datetimepickerCss = {
					position: position,
					left: left,
					top: '',  //Initialize to prevent previous values interfering with new ones.
					bottom: ''  //Initialize to prevent previous values interfering with new ones.
				};

				datetimepickerCss[verticalAnchorEdge] = verticalPosition;

				datetimepicker.css(datetimepickerCss);
			};

			datetimepicker
				.on('open.xdsoft', function (event) {
					var onShow = true;
					if (options.onShow && $.isFunction(options.onShow)) {
						onShow = options.onShow.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'), event);
					}
					if (onShow !== false) {
						datetimepicker.show();
						setPos();
						$(options.contentWindow)
							.off('resize.xdsoft', setPos)
							.on('resize.xdsoft', setPos);

						if (options.closeOnWithoutClick) {
							$([options.ownerDocument.body, options.contentWindow]).on('touchstart mousedown.xdsoft', function arguments_callee6() {
								datetimepicker.trigger('close.xdsoft');
								$([options.ownerDocument.body, options.contentWindow]).off('touchstart mousedown.xdsoft', arguments_callee6);
							});
						}
					}
				})
				.on('close.xdsoft', function (event) {
					var onClose = true;
					month_picker
						.find('.xdsoft_month,.xdsoft_year')
						.find('.xdsoft_select')
						.hide();
					if (options.onClose && $.isFunction(options.onClose)) {
						onClose = options.onClose.call(datetimepicker, _xdsoft_datetime.currentTime, datetimepicker.data('input'), event);
					}
					if (onClose !== false && !options.opened && !options.inline) {
						datetimepicker.hide();
					}
					event.stopPropagation();
				})
				.on('toggle.xdsoft', function () {
					if (datetimepicker.is(':visible')) {
						datetimepicker.trigger('close.xdsoft');
					} else {
						datetimepicker.trigger('open.xdsoft');
					}
				})
				.data('input', input);

			timer = 0;

			datetimepicker.data('xdsoft_datetime', _xdsoft_datetime);
			datetimepicker.setOptions(options);

			function getCurrentValue() {
				var ct = false, time;

				if (options.startDate) {
					ct = _xdsoft_datetime.strToDate(options.startDate);
				} else {
					ct = options.value || ((input && input.val && input.val()) ? input.val() : '');
					if (ct) {
						ct = _xdsoft_datetime.strToDateTime(ct);
						if (options.yearOffset) {
							ct = new Date(ct.getFullYear() - options.yearOffset, ct.getMonth(), ct.getDate(), ct.getHours(), ct.getMinutes(), ct.getSeconds(), ct.getMilliseconds());
						}
					} else if (options.defaultDate) {
						ct = _xdsoft_datetime.strToDateTime(options.defaultDate);
						if (options.defaultTime) {
							time = _xdsoft_datetime.strtotime(options.defaultTime);
							ct.setHours(time.getHours());
							ct.setMinutes(time.getMinutes());
						}
					}
				}

				if (ct && _xdsoft_datetime.isValidDate(ct)) {
					datetimepicker.data('changed', true);
				} else {
					ct = '';
				}

				return ct || 0;
			}

			function setMask(options) {

				var isValidValue = function (mask, value) {
						var reg = mask
							.replace(/([\[\]\/\{\}\(\)\-\.\+]{1})/g, '\\$1')
							.replace(/_/g, '{digit+}')
							.replace(/([0-9]{1})/g, '{digit$1}')
							.replace(/\{digit([0-9]{1})\}/g, '[0-$1_]{1}')
							.replace(/\{digit[\+]\}/g, '[0-9_]{1}');
						return (new RegExp(reg)).test(value);
					},
					getCaretPos = function (input) {
						try {
							if (options.ownerDocument.selection && options.ownerDocument.selection.createRange) {
								var range = options.ownerDocument.selection.createRange();
								return range.getBookmark().charCodeAt(2) - 2;
							}
							if (input.setSelectionRange) {
								return input.selectionStart;
							}
						} catch (e) {
							return 0;
						}
					},
					setCaretPos = function (node, pos) {
						node = (typeof node === "string" || node instanceof String) ? options.ownerDocument.getElementById(node) : node;
						if (!node) {
							return false;
						}
						if (node.createTextRange) {
							var textRange = node.createTextRange();
							textRange.collapse(true);
							textRange.moveEnd('character', pos);
							textRange.moveStart('character', pos);
							textRange.select();
							return true;
						}
						if (node.setSelectionRange) {
							node.setSelectionRange(pos, pos);
							return true;
						}
						return false;
					};

				if(options.mask) {
					input.off('keydown.xdsoft');
				}

				if (options.mask === true) {
					if (dateHelper.formatMask) {
						options.mask = dateHelper.formatMask(options.format)
					} else {
						options.mask = options.format
							.replace(/Y/g, '9999')
							.replace(/F/g, '9999')
							.replace(/m/g, '19')
							.replace(/d/g, '39')
							.replace(/H/g, '29')
							.replace(/i/g, '59')
							.replace(/s/g, '59');
					}
				}

				if ($.type(options.mask) === 'string') {
					if (!isValidValue(options.mask, input.val())) {
						input.val(options.mask.replace(/[0-9]/g, '_'));
						setCaretPos(input[0], 0);
					}

					input.on('paste.xdsoft', function (event) {
					    // couple options here
					    // 1. return false - tell them they can't paste
					    // 2. insert over current characters - minimal validation
					    // 3. full fledged parsing and validation
					    // let's go option 2 for now

					    // fires multiple times for some reason

					    // https://stackoverflow.com/a/30496488/1366033
					    var clipboardData = event.clipboardData || event.originalEvent.clipboardData || window.clipboardData,
						pastedData = clipboardData.getData('text'),
						val = this.value,
						pos = this.selectionStart

					    var valueBeforeCursor = val.substr(0, pos);
					    var valueAfterPaste = val.substr(pos + pastedData.length);

					    val = valueBeforeCursor + pastedData + valueAfterPaste;           
					    pos += pastedData.length;

					    if (isValidValue(options.mask, val)) {
						this.value = val;
						setCaretPos(this, pos);
					    } else if ($.trim(val) === '') {
						this.value = options.mask.replace(/[0-9]/g, '_');
					    } else {
						input.trigger('error_input.xdsoft');
					    }

					    event.preventDefault();
					    return false;
					  });

					  input.on('keydown.xdsoft', function (event) {
					    var val = this.value,
						key = event.which,
						pos = this.selectionStart,
						selEnd = this.selectionEnd,
						hasSel = pos !== selEnd,
						digit;

					    // only alow these characters
					    if (((key >=  KEY0 && key <=  KEY9)  ||
						 (key >= _KEY0 && key <= _KEY9)) || 
						 (key === BACKSPACE || key === DEL)) {

					      // get char to insert which is new character or placeholder ('_')
					      digit = (key === BACKSPACE || key === DEL) ? '_' :
							  String.fromCharCode((_KEY0 <= key && key <= _KEY9) ? key - KEY0 : key);

						// we're deleting something, we're not at the start, and have normal cursor, move back one
						// if we have a selection length, cursor actually sits behind deletable char, not in front
						if (key === BACKSPACE && pos && !hasSel) {
						    pos -= 1;
						}

						// don't stop on a separator, continue whatever direction you were going
						//   value char - keep incrementing position while on separator char and we still have room
						//   del char   - keep decrementing position while on separator char and we still have room
						while (true) {
						  var maskValueAtCurPos = options.mask.substr(pos, 1);
						  var posShorterThanMaskLength = pos < options.mask.length;
						  var posGreaterThanZero = pos > 0;
						  var notNumberOrPlaceholder = /[^0-9_]/;
						  var curPosOnSep = notNumberOrPlaceholder.test(maskValueAtCurPos);
						  var continueMovingPosition = curPosOnSep && posShorterThanMaskLength && posGreaterThanZero

						  // if we hit a real char, stay where we are
						  if (!continueMovingPosition) break;

						  // hitting backspace in a selection, you can possibly go back any further - go forward
						  pos += (key === BACKSPACE && !hasSel) ? -1 : 1;

						}


						if (hasSel) {
						  // pos might have moved so re-calc length
						  var selLength = selEnd - pos

						  // if we have a selection length we will wipe out entire selection and replace with default template for that range
						  var defaultBlank = options.mask.replace(/[0-9]/g, '_');
						  var defaultBlankSelectionReplacement = defaultBlank.substr(pos, selLength); 
						  var selReplacementRemainder = defaultBlankSelectionReplacement.substr(1) // might be empty

						  var valueBeforeSel = val.substr(0, pos);
						  var insertChars = digit + selReplacementRemainder;
						  var charsAfterSelection = val.substr(pos + selLength);

						  val = valueBeforeSel + insertChars + charsAfterSelection

						} else {
						  var valueBeforeCursor = val.substr(0, pos);
						  var insertChar = digit;
						  var valueAfterNextChar = val.substr(pos + 1);

						  val = valueBeforeCursor + insertChar + valueAfterNextChar
						}

						if ($.trim(val) === '') {
						  // if empty, set to default
						    val = defaultBlank
						} else {
						  // if at the last character don't need to do anything
						    if (pos === options.mask.length) {
							event.preventDefault();
							return false;
						    }
						}

						// resume cursor location
						pos += (key === BACKSPACE) ? 0 : 1;
						// don't stop on a separator, continue whatever direction you were going
						while (/[^0-9_]/.test(options.mask.substr(pos, 1)) && pos < options.mask.length && pos > 0) {
						    pos += (key === BACKSPACE) ? 0 : 1;
						}

						if (isValidValue(options.mask, val)) {
						    this.value = val;
						    setCaretPos(this, pos);
						} else if ($.trim(val) === '') {
						    this.value = options.mask.replace(/[0-9]/g, '_');
						} else {
						    input.trigger('error_input.xdsoft');
						}
					    } else {
						if (([AKEY, CKEY, VKEY, ZKEY, YKEY].indexOf(key) !== -1 && ctrlDown) || [ESC, ARROWUP, ARROWDOWN, ARROWLEFT, ARROWRIGHT, F5, CTRLKEY, TAB, ENTER].indexOf(key) !== -1) {
						    return true;
						}
					    }

					    event.preventDefault();
					    return false;
					  });
				}
			}

			_xdsoft_datetime.setCurrentTime(getCurrentValue());

			input
				.data('xdsoft_datetimepicker', datetimepicker)
				.on('open.xdsoft focusin.xdsoft mousedown.xdsoft touchstart', function () {
					if (input.is(':disabled') || (input.data('xdsoft_datetimepicker').is(':visible') && options.closeOnInputClick)) {
						return;
					}
					if (!options.openOnFocus) {
						return;
					}
					clearTimeout(timer);
					timer = setTimeout(function () {
						if (input.is(':disabled')) {
							return;
						}

						triggerAfterOpen = true;
						_xdsoft_datetime.setCurrentTime(getCurrentValue(), true);
						if(options.mask) {
							setMask(options);
						}
						datetimepicker.trigger('open.xdsoft');
					}, 100);
				})
				.on('keydown.xdsoft', function (event) {
					var elementSelector,
						key = event.which;
					if ([ENTER].indexOf(key) !== -1 && options.enterLikeTab) {
						elementSelector = $("input:visible,textarea:visible,button:visible,a:visible");
						datetimepicker.trigger('close.xdsoft');
						elementSelector.eq(elementSelector.index(this) + 1).focus();
						return false;
					}
					if ([TAB].indexOf(key) !== -1) {
						datetimepicker.trigger('close.xdsoft');
						return true;
					}
				})
				.on('blur.xdsoft', function () {
					datetimepicker.trigger('close.xdsoft');
				});
		};
		destroyDateTimePicker = function (input) {
			var datetimepicker = input.data('xdsoft_datetimepicker');
			if (datetimepicker) {
				datetimepicker.data('xdsoft_datetime', null);
				datetimepicker.remove();
				input
					.data('xdsoft_datetimepicker', null)
					.off('.xdsoft');
				$(options.contentWindow).off('resize.xdsoft');
				$([options.contentWindow, options.ownerDocument.body]).off('mousedown.xdsoft touchstart');
				if (input.unmousewheel) {
					input.unmousewheel();
				}
			}
		};
		$(options.ownerDocument)
			.off('keydown.xdsoftctrl keyup.xdsoftctrl')
			.on('keydown.xdsoftctrl', function (e) {
				if (e.keyCode === CTRLKEY) {
					ctrlDown = true;
				}
			})
			.on('keyup.xdsoftctrl', function (e) {
				if (e.keyCode === CTRLKEY) {
					ctrlDown = false;
				}
			});

		this.each(function () {
			var datetimepicker = $(this).data('xdsoft_datetimepicker'), $input;
			if (datetimepicker) {
				if ($.type(opt) === 'string') {
					switch (opt) {
						case 'show':
							$(this).select().focus();
							datetimepicker.trigger('open.xdsoft');
							break;
						case 'hide':
							datetimepicker.trigger('close.xdsoft');
							break;
						case 'toggle':
							datetimepicker.trigger('toggle.xdsoft');
							break;
						case 'destroy':
							destroyDateTimePicker($(this));
							break;
						case 'reset':
							this.value = this.defaultValue;
							if (!this.value || !datetimepicker.data('xdsoft_datetime').isValidDate(dateHelper.parseDate(this.value, options.format))) {
								datetimepicker.data('changed', false);
							}
							datetimepicker.data('xdsoft_datetime').setCurrentTime(this.value);
							break;
						case 'validate':
							$input = datetimepicker.data('input');
							$input.trigger('blur.xdsoft');
							break;
						default:
							if (datetimepicker[opt] && $.isFunction(datetimepicker[opt])) {
								result = datetimepicker[opt](opt2);
							}
					}
				} else {
					datetimepicker
						.setOptions(opt);
				}
				return 0;
			}
			if ($.type(opt) !== 'string') {
				if (!options.lazyInit || options.open || options.inline) {
					createDateTimePicker($(this));
				} else {
					lazyInit($(this));
				}
			}
		});

		return result;
	};

	$.fn.datetimepicker.defaults = default_options;

	function HighlightedDate(date, desc, style) {
		"use strict";
		this.date = date;
		this.desc = desc;
		this.style = style;
	}
};
;(function (factory) {
	if ( typeof define === 'function' && define.amd ) {
		// AMD. Register as an anonymous module.
		define(['jquery', 'jquery-mousewheel'], factory);
	} else if (typeof exports === 'object') {
		// Node/CommonJS style for Browserify
		module.exports = factory(require('jquery'));;
	} else {
		// Browser globals
		factory(jQuery);
	}
}(datetimepickerFactory));


/*!
 * jQuery Mousewheel 3.1.13
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 */

(function (factory) {
    if ( typeof define === 'function' && define.amd ) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        // Node/CommonJS style for Browserify
        module.exports = factory;
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {

    var toFix  = ['wheel', 'mousewheel', 'DOMMouseScroll', 'MozMousePixelScroll'],
        toBind = ( 'onwheel' in document || document.documentMode >= 9 ) ?
                    ['wheel'] : ['mousewheel', 'DomMouseScroll', 'MozMousePixelScroll'],
        slice  = Array.prototype.slice,
        nullLowestDeltaTimeout, lowestDelta;

    if ( $.event.fixHooks ) {
        for ( var i = toFix.length; i; ) {
            $.event.fixHooks[ toFix[--i] ] = $.event.mouseHooks;
        }
    }

    var special = $.event.special.mousewheel = {
        version: '3.1.12',

        setup: function() {
            if ( this.addEventListener ) {
                for ( var i = toBind.length; i; ) {
                    this.addEventListener( toBind[--i], handler, false );
                }
            } else {
                this.onmousewheel = handler;
            }
            // Store the line height and page height for this particular element
            $.data(this, 'mousewheel-line-height', special.getLineHeight(this));
            $.data(this, 'mousewheel-page-height', special.getPageHeight(this));
        },

        teardown: function() {
            if ( this.removeEventListener ) {
                for ( var i = toBind.length; i; ) {
                    this.removeEventListener( toBind[--i], handler, false );
                }
            } else {
                this.onmousewheel = null;
            }
            // Clean up the data we added to the element
            $.removeData(this, 'mousewheel-line-height');
            $.removeData(this, 'mousewheel-page-height');
        },

        getLineHeight: function(elem) {
            var $elem = $(elem),
                $parent = $elem['offsetParent' in $.fn ? 'offsetParent' : 'parent']();
            if (!$parent.length) {
                $parent = $('body');
            }
            return parseInt($parent.css('fontSize'), 10) || parseInt($elem.css('fontSize'), 10) || 16;
        },

        getPageHeight: function(elem) {
            return $(elem).height();
        },

        settings: {
            adjustOldDeltas: true, // see shouldAdjustOldDeltas() below
            normalizeOffset: true  // calls getBoundingClientRect for each event
        }
    };

    $.fn.extend({
        mousewheel: function(fn) {
            return fn ? this.bind('mousewheel', fn) : this.trigger('mousewheel');
        },

        unmousewheel: function(fn) {
            return this.unbind('mousewheel', fn);
        }
    });


    function handler(event) {
        var orgEvent   = event || window.event,
            args       = slice.call(arguments, 1),
            delta      = 0,
            deltaX     = 0,
            deltaY     = 0,
            absDelta   = 0,
            offsetX    = 0,
            offsetY    = 0;
        event = $.event.fix(orgEvent);
        event.type = 'mousewheel';

        // Old school scrollwheel delta
        if ( 'detail'      in orgEvent ) { deltaY = orgEvent.detail * -1;      }
        if ( 'wheelDelta'  in orgEvent ) { deltaY = orgEvent.wheelDelta;       }
        if ( 'wheelDeltaY' in orgEvent ) { deltaY = orgEvent.wheelDeltaY;      }
        if ( 'wheelDeltaX' in orgEvent ) { deltaX = orgEvent.wheelDeltaX * -1; }

        // Firefox < 17 horizontal scrolling related to DOMMouseScroll event
        if ( 'axis' in orgEvent && orgEvent.axis === orgEvent.HORIZONTAL_AXIS ) {
            deltaX = deltaY * -1;
            deltaY = 0;
        }

        // Set delta to be deltaY or deltaX if deltaY is 0 for backwards compatabilitiy
        delta = deltaY === 0 ? deltaX : deltaY;

        // New school wheel delta (wheel event)
        if ( 'deltaY' in orgEvent ) {
            deltaY = orgEvent.deltaY * -1;
            delta  = deltaY;
        }
        if ( 'deltaX' in orgEvent ) {
            deltaX = orgEvent.deltaX;
            if ( deltaY === 0 ) { delta  = deltaX * -1; }
        }

        // No change actually happened, no reason to go any further
        if ( deltaY === 0 && deltaX === 0 ) { return; }

        // Need to convert lines and pages to pixels if we aren't already in pixels
        // There are three delta modes:
        //   * deltaMode 0 is by pixels, nothing to do
        //   * deltaMode 1 is by lines
        //   * deltaMode 2 is by pages
        if ( orgEvent.deltaMode === 1 ) {
            var lineHeight = $.data(this, 'mousewheel-line-height');
            delta  *= lineHeight;
            deltaY *= lineHeight;
            deltaX *= lineHeight;
        } else if ( orgEvent.deltaMode === 2 ) {
            var pageHeight = $.data(this, 'mousewheel-page-height');
            delta  *= pageHeight;
            deltaY *= pageHeight;
            deltaX *= pageHeight;
        }

        // Store lowest absolute delta to normalize the delta values
        absDelta = Math.max( Math.abs(deltaY), Math.abs(deltaX) );

        if ( !lowestDelta || absDelta < lowestDelta ) {
            lowestDelta = absDelta;

            // Adjust older deltas if necessary
            if ( shouldAdjustOldDeltas(orgEvent, absDelta) ) {
                lowestDelta /= 40;
            }
        }

        // Adjust older deltas if necessary
        if ( shouldAdjustOldDeltas(orgEvent, absDelta) ) {
            // Divide all the things by 40!
            delta  /= 40;
            deltaX /= 40;
            deltaY /= 40;
        }

        // Get a whole, normalized value for the deltas
        delta  = Math[ delta  >= 1 ? 'floor' : 'ceil' ](delta  / lowestDelta);
        deltaX = Math[ deltaX >= 1 ? 'floor' : 'ceil' ](deltaX / lowestDelta);
        deltaY = Math[ deltaY >= 1 ? 'floor' : 'ceil' ](deltaY / lowestDelta);

        // Normalise offsetX and offsetY properties
        if ( special.settings.normalizeOffset && this.getBoundingClientRect ) {
            var boundingRect = this.getBoundingClientRect();
            offsetX = event.clientX - boundingRect.left;
            offsetY = event.clientY - boundingRect.top;
        }

        // Add information to the event object
        event.deltaX = deltaX;
        event.deltaY = deltaY;
        event.deltaFactor = lowestDelta;
        event.offsetX = offsetX;
        event.offsetY = offsetY;
        // Go ahead and set deltaMode to 0 since we converted to pixels
        // Although this is a little odd since we overwrite the deltaX/Y
        // properties with normalized deltas.
        event.deltaMode = 0;

        // Add event and delta to the front of the arguments
        args.unshift(event, delta, deltaX, deltaY);

        // Clearout lowestDelta after sometime to better
        // handle multiple device types that give different
        // a different lowestDelta
        // Ex: trackpad = 3 and mouse wheel = 120
        if (nullLowestDeltaTimeout) { clearTimeout(nullLowestDeltaTimeout); }
        nullLowestDeltaTimeout = setTimeout(nullLowestDelta, 200);

        return ($.event.dispatch || $.event.handle).apply(this, args);
    }

    function nullLowestDelta() {
        lowestDelta = null;
    }

    function shouldAdjustOldDeltas(orgEvent, absDelta) {
        // If this is an older event and the delta is divisable by 120,
        // then we are assuming that the browser is treating this as an
        // older mouse wheel event and that we should divide the deltas
        // by 40 to try and get a more usable deltaFactor.
        // Side note, this actually impacts the reported scroll distance
        // in older browsers and can cause scrolling to be slower than native.
        // Turn this off by setting $.event.special.mousewheel.settings.adjustOldDeltas to false.
        return special.settings.adjustOldDeltas && orgEvent.type === 'mousewheel' && absDelta % 120 === 0;
    }

}));