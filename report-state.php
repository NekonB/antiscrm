<?php

    // var toDT = function(date) {
    //     Date.prototype.toLocalISOString = function() {
    
    //     // Helper for padding
    //         function pad(n, len) {
    //         return ('000' + n).slice(-len);
    //         }
        
    //         // If not called on a Date instance, or timevalue is NaN, return undefined
    //         if (isNaN(this) || Object.prototype.toString.call(this) != '[object Date]') return;
        
    //         // Otherwise, return an ISO format string with the current system timezone offset
    //         var d = this;
    //         var os = d.getTimezoneOffset();
    //         var sign = (os > 0? '-' : '+');
    //         os = Math.abs(os);
            
    //         return pad(d.getFullYear(), 4) + '-' +
    //             pad(d.getMonth() + 1, 2) + '-' +
    //             pad(d.getDate(), 2) +
    //             'T' + 
    //             pad(d.getHours(), 2) + ':' +
    //             pad(d.getMinutes(), 2) + ':' +
    //             pad(d.getSeconds(), 2) 
                
    //             // Note sign of ECMASCript offsets are opposite to ISO 8601
    //     }
    ob_start();
    session_start();
    require './vendor/autoload.php';
    require_once './connectionCDR.php';
    if ($_SERVER['REQUEST_METHOD'] == 'GET') {
        $params = json_decode($_GET['params']);
        $interval = json_decode($_GET['interval']);
        $db =date_create( $interval->dtb);
        $db = date_format($db,'Y-m-d H:i:s');
        $de =date_create( $interval->dte);
        $de = date_format($de,'Y-m-d H:i:s');
        $strOperators ="";
        $data = (object)array();
        foreach($_SESSION['operators'] as $el) {
            $strOperators = implode(',' , $_SESSION['operators']);
        }
        $sql = "SELECT c.uniqueid as uid, c.calldate as dtb, c.src as num, c.dst as agent, c.disposition as result, SEC_TO_TIME(c.billsec) as longTime, c.recordingfile as recfile FROM cdr as c WHERE c.calldate BETWEEN ? and ? and  c.dst IN (".$strOperators.") " ;
        if($params->agent){
            $sql=$sql." AND c.dst ='".$params->agent."'" ;
        }
        
        if($params->res){
            if($params->res=="wellcomeLoss"){
                $sql = "SELECT c.uniqueid as uid, c.calldate as dtb, c.src as num, c.dst as agent, c.disposition as result, SEC_TO_TIME(c.billsec) as longTime, c.recordingfile as recfile FROM cdr as c WHERE c.did=? and c.calldate BETWEEN ? and ? and c.lastapp='Playback' ";
                $stmt=$cdr->prepare($sql);
                $stmt->execute(array($_SESSION['number'], $db, $de));
                $data = $stmt->fetchAll();
            }
            else if ($params->res=="queueLoss") {
                $sql = "SELECT c.uniqueid as uid, c.calldate as dtb, c.src as num, c.dst as agent, c.disposition as result, SEC_TO_TIME(c.billsec) as longTime, c.recordingfile as recfile FROM cdr as c WHERE c.did=? and c.calldate BETWEEN ? and ? and c.disposition='NO ANSWER' ";
                $stmt=$cdr->prepare($sql);
                $stmt->execute(array($_SESSION['number'], $db, $de));
                $data=$stmt->fetchAll();
            }
            else if ($params->res=="notWorkTimeLoss") {
                
                $sql = "SELECT c.uniqueid as uid, c.calldate as dtb, c.src as num, c.dst as agent, c.disposition as result, SEC_TO_TIME(c.billsec) as longTime, c.recordingfile as recfile FROM cdr as c WHERE c.did=? and c.calldate BETWEEN ? and ? and c.dcontext='not-work-time-gorod' ";
                $stmt=$cdr->prepare($sql);
                $stmt->execute(array($_SESSION['number'], $db, $de));
                $data=$stmt->fetchAll();
            }
            else if($params->res=='NO ANSWER') {
                $sql = "SELECT c.uniqueid as uid, c.calldate as dtb, c.src as num, c.dst as agent, c.disposition as result, SEC_TO_TIME(c.billsec) as longTime, c.recordingfile as recfile FROM cdr as c WHERE c.calldate BETWEEN ? and ? and  c.dst IN (".$strOperators.") and c.lastapp = 'Dial' and c.disposition='NO ANSWER' ";
                if($params->agent){
                    $sql=$sql." AND c.dst =? ";
                }
                $stmt=$cdr->prepare($sql);
                if($params->agent) {
                    $stmt->execute(array( $db, $de , $params->agent));
                }
                $stmt->execute(array( $db, $de));
                $data=$stmt->fetchAll();
            }
            else if ($params->res=='ANSWERED') {
                $sql = "SELECT c.uniqueid as uid, c.calldate as dtb, c.src as num, c.dst as agent, c.disposition as result, SEC_TO_TIME(c.billsec) as longTime, c.recordingfile as recfile FROM cdr as c WHERE c.calldate BETWEEN ? and ? and  c.dst IN (".$strOperators.") and c.lastapp = 'Dial' and c.disposition='ANSWERED' ";
                if($params->agent){
                    $sql=$sql." AND c.dst =? ";
                }
                $stmt=$cdr->prepare($sql);
                if($params->agent) {
                    $stmt->execute(array( $db, $de , $params->agent));
                }
                $stmt->execute(array( $db, $de));
                $data=$stmt->fetchAll();
            }
            
        }
        else {
            $stmt = $cdr->prepare($sql,array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $stmt->execute(array($db , $de));
            $data = $stmt->fetchAll();

        }
        
        $objPHPExcel = new PHPExcel();


        $objPHPExcel->setActiveSheetIndex(0);
        $active = $objPHPExcel->getActiveSheet();

        $active->mergeCells('A1:G1');
        $active->getCell('A1')->setValue("Статистика Колл-центра за ".$db." - ".$de)
            ->getStyle()
            ->getFont()
            ->setSize(16)
            ->setBold(true);

        $active->getCell('A1')
            ->getStyle()
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $row = 3;
        $active->getCellByColumnAndRow(0,$row)->setValue("some");
        foreach($data as $el) {
            $active->getCellByColumnAndRow(0,$row)->setValue($el->dtb);
            $active->getCellByColumnAndRow(1,$row)->setValue($el->num);
            $active->getCellByColumnAndRow(2,$row)->setValue($el->agent);
            $active->getCellByColumnAndRow(3,$row)->setValue($el->result);
            $active->getCellByColumnAndRow(4,$row)->setValue($el->longTime);

            $row+=1;
        } 

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save("report-state.xls");
        header('location: /report-state.xls');
        echo json_encode("report-state.xls");
    }   