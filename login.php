<?php require_once './connectionCMonitor.php'; ?>
<?php
    ob_start();
    session_start();
    $_SESSION['locale'] = $_GET['l']?$_GET['l']:'ru';
    $flocal= file_get_contents("./langs/".$_SESSION['locale'].".json");
    $trans=json_decode($flocal);
    
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if(isset($_POST['locale'])) {
            $_SESSION['locale']=$_POST['locale'];
        }
        else {
            $user = $_POST['username'];
            $password  = $_POST['password'];
            $sql = "SELECT number, password as pass FROM callcenters WHERE number ='".$user."' LIMIT 1";
            $stmt= $cdr->query($sql);
            $row = $stmt->fetch();
            $islogin = md5($password)==$row['pass'];

            if(!$row['number']) {
                echo '<div class="alert alert-warning alert-dismissible fade show" role="alert">';
                echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
                echo '<span aria-hidden="true">&times;</span>';
                echo '</button>';
                echo "<strong>".$trans->lossnumber."</strong>" ;
                echo '</div>';
            }
            else {
                if(!$islogin) {
                    echo '<div class="alert alert-warning alert-dismissible fade show" role="alert">';
                    echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
                    echo '<span aria-hidden="true">&times;</span>';
                    echo '</button>';
                    echo "<strong>". $trans->losspass."</strong>" ;
                    echo '</div>';
                }
                else {
                    $sql = "SELECT c.number, op.num,c.id as cid FROM callcenters as c  INNER JOIN operators as op ON  op.callcenter_id = c.id WHERE number ='".$row['number']."'";
                    $stmt = $cdr->query($sql);
                    $l->num_rows;
                    $_SESSION['operators'] = array();
                    while($r = $stmt->fetch()) {
                        array_push($_SESSION['operators'] , $r['num']);
                        $_SESSION['number'] = $r['number'];
                        
                    }
                    // $_SESSION['CID'] = $r['cid'];
                    $sql ="SELECT q.queue FROM callcenters as c INNER JOIN queuenums as q ON c.id = q.callcenter_id WHERE c.number = '".$row['number']."'";
                    $stmt = $cdr->query($sql);
                    $_SESSION['queues'] = array();
                    while($row = $stmt->fetch()) {
                        array_push($_SESSION['queues'], $row['queue']);
                        
                    }
                    $_SESSION['locale'] = 'ru';
                    // for ($row_no = $result->num_rows - 1; $row_no >= 0; $row_no--) {
                    //     $result->data_seek($row_no);
                    //     $row = $result->fetch_assoc();
                        
                    // }
                    
                    header("location: ./index.php");
                }     
            }
        }
    }
        
    
?>
<?php require_once './header.php';?>
<body>
    <div class="container ">
        <div class="row">
            <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div class="d-flex flex-column mt-5">
                    <div class="d-flex justify-content-center">
                        <div>
                            <img src="icons/logologin.png" class="img-fluid" alt="">

                        </div>
                        
                    </div>
                    <h6 class="text-center"><?php echo $trans->statecallcenter ?></h6>
                </div>

                <div class="card card-signin my-5">
                    <div class="card-body">
                        <h5 class="card-title text-center"><?php echo $trans->signin ?></h5>
                        <form class="form-signin" action="./login.php" method="POST">
                            <div class="form-label-group">
                                <div class="btn-group btn-group-toggle container" data-toggle="buttons">
                                    <label class="btn btn-secondary langch" data-lang="kk">
                                        <input type="radio" name="lang" id="kk" autocomplete="off" checked> <img src="./icons/Kazakhstan.png"/> Қазақ
                                    </label>
                                    <label class="btn btn-secondary langch" data-lang="ru">
                                        <input type="radio" name="lang" id="ru" autocomplete="off"> <img src="./icons/Russia.png"/> Русский
                                    </label>
                                    <label class="btn btn-secondary langch" data-lang="en">
                                        <input type="radio" name="lang" id="en" autocomplete="off"><img src="./icons/United-Kingdom.png"/>  English
                                    </label>
                                </div>
                            </div>
                            <div class="form-label-group">
                                <label for="inputEmail"><?php echo $trans->email ?></label>
                                <input name="username" type="text" id="inputEmail" class="form-control" placeholder="" autocomplete="on" required autofocus>
                            </div>
                            <div class="form-label-group">
                                <label for="inputPassword"><?php echo $trans->password ?></label>
                                <input name="password" type="password" id="inputPassword" class="form-control" autocomplete="on" placeholder="" required>
                            </div>
                            <button class="btn btn-lg mt-2 btn-primary btn-block text-uppercase" type="submit"><?php echo $trans->login ?></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
    $(document).ready(function() {
        $(".alert").alert();

        $('.langch[data-lang="<?php echo $_SESSION['locale'] ?>"]').addClass('active').addClass('focus');
        $('.langch').click(function () {
            location.replace('./login.php?l='+$(this).data('lang'));
        });
    });
      
    </script>
</body>
</html>