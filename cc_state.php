<?php 
    ob_start();
    session_start();
    if(!isset($_SESSION['number'])) {
        header("location: login.php");
    }
    $flocal= file_get_contents("./langs/".$_SESSION['locale'].".json");
    $trans=json_decode($flocal);
?>
<?php require_once './header.php';?>
<style>
    tr:hover {
        cursor:pointer;
    }
</style>
<body>
	<div class="container-fluid ">
        <div class=" row ">
            <!-- {{> leftpanel}} -->
            <?php  require_once './leftpanel.php';?>
            <div class="right-panel col-xl-10 offset-xl-2 col-lg-9 offset-lg-3 col-md-12 col-sm-12">
                <div class="content container-fluid row">
                    <!-- {{> header}} -->
                    <div class="header col-12 row d-flex justify-content-between" >
                    <div class="">
                        <h3 class="text-muted"><?php echo $trans->statecallcenter; ?> (<?php echo $_SESSION['number'] ?>)</h3>
                    </div>
                    <div class="">
                        <div class="btn-group btn-group-toggle container" data-toggle="buttons">
                            <label class="btn btn-secondary langch" data-lang="kk">
                                <input type="radio" name="lang" id="kk" autocomplete="off" checked> <img src="./icons/Kazakhstan.png"/> Қазақ
                            </label>
                            <label class="btn btn-secondary langch" data-lang="ru">
                                <input type="radio" name="lang" id="ru" autocomplete="off"> <img src="./icons/Russia.png"/> Русский
                            </label>
                            <label class="btn btn-secondary langch" data-lang="en">
                                <input type="radio" name="lang" id="en" autocomplete="off"><img src="./icons/United-Kingdom.png"/>  English
                            </label>
                        </div>
                    </div>
                    <div class="items-block  container-fluid mt-3">
						<div class="row mt-2" >
							<div class="col-2">
								<label for="groupSelectUsers"><?php echo $trans->ops ?></label>
								<select class="form-control" id="groupSelectUsers">
									<option value="0"><?php echo $trans->all ?></option>
										<?php
											$operators = $_SESSION['operators'];
											foreach($operators as $el) {
												echo "<option value='".$el."'>".$el."</option>";
											}
										?>
								</select>
							</div>

							<div class="gr">
								<label for=""><?php echo $trans->on ?></label>
								<div class="row">
									<div class="intervals-button"><button data-diff="84192000" id="allTime" class="btn btn-dark"><?php echo $trans->all ?></button></div>
									<div class="intervals-button"><button data-diff="0" id="day" class="btn btn-dark"><?php echo $trans->ontoday ?></button></div>
									<div class="intervals-button"><button data-diff="604800" id="week" class="btn btn-dark"><?php echo $trans->onweek ?></button></div>
									<div class="intervals-button"><button data-diff="2419200" id="month" class="btn btn-dark"><?php echo $trans->onmonth ?></button></div>
								</div>
							</div>
							
						</div>
						<div class="row mt-2 col-6">
							<label for="datepicker"><?php echo $trans->chooseinterval ?></label>
							<div class=" input-daterange input-group" id="datepicker">
								<span><?php echo $trans->date ?></span>
								<span class="text-datepicker"><?php echo $trans->to ?></span>
								<input type="text" value="<?php echo date('m/d/yy 00:00:00') ?>" class="input-sm form-control form-control-sm" id="picStart" />
								<span class="text-datepicker"><?php echo $trans->do ?></span>
								<input type="text" value="<?php echo date('m/d/yy 23:59:59') ?>" class="input-sm  form-control form-control-sm" id="picEnd" />
							</div>
						</div>
						<div class="mt-2 row">
							<div class="col-3">
								<label for=""><?php echo $trans->bystatus ?></label>
								<select class="form-control" id="resultSelect">
									<option value="0"><?php echo $trans->all ?></option>
									<option value="ANSWERED"><?php echo $trans->confirms ?></option>
									<option value="NO ANSWER"><?php echo $trans->lossinqueue_ ?></option>
									<option value="wellcomeLoss"><?php echo $trans->losswelcome ?></option>
									<option value="notWorkTimeLosss"><?php echo $trans->lossinnotworktime ?></option>                                
								</select>
							</div>
						</div>
						<div class=" mt-2 ">
							<button id="sort" class="btn btn-primary"><?php echo $trans->search ?></button> <span style="display: none;" class=" btn  loader"></span>
                            <!-- <button id="report" class="btn btn-primary">Создать отчет в Excel</button> -->
                        </div>
					</div>
                   

					<div class="allCalls mt-5 container-fluid">
						<table class="table table-sm table-bordered table-striped table-hover" id="allCalls"> 
							<thead>
								<tr>
									<th><?php echo $trans->datecall ?></th>
									<th><?php echo $trans->numberabon ?></th>
									<th><?php echo $trans->agent ?></th>
									<th><?php echo $trans->result ?></th>
									<th><?php echo $trans->duration ?></th>
									<th><?php echo $trans->record?></th>
								</tr>
							</thead>
							<tbody>
					
							</tbody>
						</table>
					</div>
					<!-- Button trigger modal -->

					<!-- Modal -->
					<div class="modal fade " id="infoCallModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
						<div class="modal-md modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header ">
									<h5 class="modal-title center"><?php echo $trans->infbycall ?> <span></span></h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body d-flex justify-content-center">
                                    <div class="d-flex flex-column">
                                        <div class="time mb-3"><p class="font-weight-bold"><?php echo $trans->datecall ?>:  <span class="font-weight-normal"></span></p> </div>
                                        <div class="agent mb-3"><p class="font-weight-bold"><?php echo $trans->agent ?>: <span class="font-weight-normal"></span></p></div>
                                        <div class="audio"><audio controls><source></source></audio></div>
                                    </div>
								</div>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.js"></script> -->
	<script>

    var toDT = function(date) {
        Date.prototype.toLocalISOString = function() {
    
        // Helper for padding
            function pad(n, len) {
            return ('000' + n).slice(-len);
            }
        
            // If not called on a Date instance, or timevalue is NaN, return undefined
            if (isNaN(this) || Object.prototype.toString.call(this) != '[object Date]') return;
        
            // Otherwise, return an ISO format string with the current system timezone offset
            var d = this;
            var os = d.getTimezoneOffset();
            var sign = (os > 0? '-' : '+');
            os = Math.abs(os);
            
            return pad(d.getFullYear(), 4) + '-' +
                pad(d.getMonth() + 1, 2) + '-' +
                pad(d.getDate(), 2) +
                'T' + 
                pad(d.getHours(), 2) + ':' +
                pad(d.getMinutes(), 2) + ':' +
                pad(d.getSeconds(), 2) 
                
                // Note sign of ECMASCript offsets are opposite to ISO 8601
        }

        var dt = date.toLocalISOString().replace('T', ' ');
        return dt;
    }

    var toHHMMSS = function(sec) {
        let sec_num = parseInt(sec/1000, 10); 
        let hours  = Math.floor(sec_num / 3600);
        let minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        let seconds = sec_num - (hours * 3600) - (minutes * 60);

        let Ohours = hours + '';
        let Ominutes = minutes + '';
        let Oseconds = seconds + '';
        if (minutes < 10) {
            Ominutes = "0" + minutes;
        }
        if (seconds < 10) {
            Oseconds = "0" + seconds;
        }
        var time = Ominutes + ':' + Oseconds;
        return time;
    }

    $.ajaxSetup({
        beforeSend: function() {
            $('.loader').show();
        },
        complete: function() {
            $('.loader').hide();
        },
        error: function(jqXHR, textStatus, errorThrown) {
            $('.loader').hide();
        }
    })
    // $('.langch[data-lang]').removeClass('active').removeClass('focus');
    $('.langch[data-lang="<?php echo $_SESSION['locale'] ?>"]').addClass('active').addClass('focus');
    $('.langch').click(function () {
        $.ajax({
            type:'POST',
            url:'./setlocale.php',
            data:{local:$(this).data('lang')},
            dataType:'json',
            success:function(response) {
                location.reload();
                console.log(response);
            }
        })
    });


    var history = $('#allCalls').dataTable({
        "language":{
            "url":'./assets/<?php echo $_SESSION['locale'];?>.json'
        },
        "pageLength": 100
    });
    var historyApi = $('#allCalls').dataTable().api();
    
    
    
    
    
    
    $(document).ready(function() {
       
        jQuery.datetimepicker.setLocale($('#lang').val());
        jQuery('#picStart').datetimepicker({
            dayOfWeekStart:1
        });

        jQuery('#picEnd').datetimepicker({
            dayOfWeekStart:1
        });


        function getParams(data) {
            var url = new URLSearchParams(location.search);
            return url.get(data);
        }
        
        if(getParams('redirect')==1) {
            $('#picStart').val(toDT( new Date(getParams('db').replace('%20' , ' '))));
            $('#picEnd').val(toDT( new Date(getParams('de').replace('%20' , ' '))));
            $('#groupSelectUsers').val(getParams('agent')?getParams('agent'):0);
            $('#resultSelect').val(getParams('status')?getParams('status'):0);
            requestCalls();
        }
        else{
            var db = new Date().setHours(0,0,0);
            var de = new Date();
            $('#picStart').val(toDT(new Date(db)));
            $('#picEnd').val(toDT(new Date(de)));

            $("#groupSelectUsers").chosen({
                width:"100%",disable_search:true
            })
            $("#typeSelect").chosen({
                width:"100%",disable_search:true
            })
            $("#resultSelect").chosen({
                width:"100%",disable_search:true
            })
        }
        
        historyApi.order([0 , 'desc']).draw();
        
    })


    var printResult = function (res) {
        var _res="";
        switch (res) {
            case 'NO ANSWER':
                _res = "<?php echo $trans->loss ?>"
                break;
        
            case 'ANSWERED':
                _res = "<?php echo $trans->confirm ?>"
                break;
            case 'BUSY':
                _res ="<?php echo $trans->busy ?>"
        }
        return _res;
    }

  
    
    var requestCalls = function(interval) { 
        function pad(n, len) {
            return ('000' + n).slice(-len);
        }

        var agent = $('#groupSelectUsers').val();
        var result = $('#resultSelect').val();
        var params = {}
        agent!=0?params.agent=agent:null;
        result!=0?params.res=result:null;
        if(!interval) {
            var interval = {
                dtb:$('#picStart').val(),
                dte:$('#picEnd').val(),
            };
        }
        else {
            var d_b = new Date(interval.dtb)
            var d_e = new Date(interval.dte)
            $('#picStart').val(toDT(new Date(d_b))); 
            $('#picEnd').val(toDT(new Date(d_e)));
        }

        interval.dtb =toDT(new Date (new Date(interval.dtb)))
        interval.dte = toDT(new Date(new Date(interval.dte)))


        var isAudio = function(el) {
            return el=='none'?'':'<i class="fa fa-play"></i>';
        }

        var printAudio = function(url,el)  {  
            
            if(el!='none') {
                // console.log(el);
                return url;
            }
            else {
                return "";
            }
            
        }

        $.ajax({
            type:'get',
            url:'./ajax/sortstate.php',
            dataType:'json',
            data:{params: JSON.stringify(params) , interval:JSON.stringify(interval)},
            success:function(response) {
                var allCalls = response;
                console.log(response);
                historyApi.clear().draw();
                var row = "";
                allCalls.forEach(function(el , i) {
                    // console.log(el.time);
                    file =el.recfile?el.recfile:'none';
                    var url = new Date(el.dtb).getFullYear()+'/'+('0' + (new Date(el.dtb).getMonth()+1)).slice(-2)+'/'+('0' + (new Date(el.dtb).getDate())).slice(-2)+'/' + file;
                    var row = '<tr data-rec="'+printAudio(url,file)+'" data-uid="'+el.uid+'">' +
                        '<td class="time">'+el.dtb+'</td>' +
                        '<td class="num">'+el.num+'</td>' +
                        '<td class="agent">'+el.agent+'</td>' +
                        "<td>"+printResult(el.result)+"</td>" +
                        '<td>' +el.longTime+ '</td>' +
                        '<td>'+isAudio(file)+'</td>'+
                    '</tr>';
                    $('#allCalls').append(row).dataTable();  
                    historyApi.row.add($(row));
                    // 
                });
                 
                historyApi.order([0 , 'desc']).draw();
            }
            
        })
    }


    /**
     * Play record file
     */
    $('#infoCallModal').on('hidden.bs.modal', function () {
        $(".audio audio").prop('src' ,"");
        document.querySelectorAll(".audio audio")[0].pause();
    })
    $(document).delegate('tr[data-uid]' , 'click' , function () {
        $(".audio audio").prop('src' , "./recfiles/"+$(this).data('rec'));
        $(".modal-header h5 span").text(" ("+$(this).children('.num').text()+")");
        $('.modal .time span').text($(this).children('.time').text());
        $('.modal .agent span').text($(this).children('.agent').text());
        $('#infoCallModal').modal();

    });

    var sort = function(obj) {
        var diff = $(this).data('diff');

        var startDay = new Date();
        var toDay = new Date();
        if($(this).prop('id') =='day') {   
            var startDay = new Date();
            var toDay = new Date();
        }
        if($(this).prop('id') =='week') {
             
            var startDay = new Date();
            var toDay = new Date();
            
        }

        startDay =  parseInt(new Date(new Date(startDay).setHours(0,0,0)).getTime()) - parseInt(diff*1000);
        console.log(startDay)
        requestCalls({dtb:new Date(startDay) , dte:new Date(toDay)});
    }

    $(document).delegate('#allTime' , 'click' , sort)
    $(document).delegate('#day' , 'click' , sort)
    $(document).delegate('#week' , 'click' , sort)
    $(document).delegate('#month' , 'click' , sort)


    function createReport(interval) {
        function pad(n, len) {
            return ('000' + n).slice(-len);
        }

        var agent = $('#groupSelectUsers').val();
        var result = $('#resultSelect').val();
        var params = {}
        agent!=0?params.agent=agent:null;
        result!=0?params.res=result:null;
        if(!interval) {
            var interval = {
                dtb:$('#picStart').val(),
                dte:$('#picEnd').val(),
            };
        }
        else {
            var d_b = new Date(interval.dtb)
            var d_e = new Date(interval.dte)
            $('#picStart').val(toDT(new Date(d_b))); 
            $('#picEnd').val(toDT(new Date(d_e)));
        }

        interval.dtb =toDT(new Date (new Date(interval.dtb)))
        interval.dte = toDT(new Date(new Date(interval.dte)))


        var isAudio = function(el) {
            return el=='none'?'':'<i class="fa fa-play"></i>';
        }

        var printAudio = function(url,el)  {  
            
            if(el!='none') {
                // console.log(el);
                return url;
            }
            else {
                return "";
            }
            
        }

        $.ajax({
            type:'get',
            url:'./ajax/sortstate.php',
            dataType:'json',
            data:{params: JSON.stringify(params) , interval:JSON.stringify(interval)},
            success:function(response) {
                var allCalls = response;
                console.log(response);
                historyApi.clear().draw();
                var row = "";
                allCalls.forEach(function(el , i) {
                    // console.log(el.time);
                    file =el.recfile?el.recfile:'none';
                    var url = new Date(el.dtb).getFullYear()+'/'+('0' + (new Date(el.dtb).getMonth()+1)).slice(-2)+'/'+('0' + (new Date(el.dtb).getDate())).slice(-2)+'/' + file;
                    var row = '<tr data-rec="'+printAudio(url,file)+'" data-uid="'+el.uid+'">' +
                        '<td class="time">'+el.dtb+'</td>' +
                        '<td class="num">'+el.num+'</td>' +
                        '<td class="agent">'+el.agent+'</td>' +
                        "<td>"+printResult(el.result)+"</td>" +
                        '<td>' +el.longTime+ '</td>' +
                        '<td>'+isAudio(file)+'</td>'+
                    '</tr>';
                    $('#allCalls').append(row).dataTable();  
                    historyApi.row.add($(row));
                    // 
                });
                 
                historyApi.order([0 , 'desc']).draw();
            }
            
        })

        $.ajax({
            type:'get',
            url:'./report-state.php',
            dataType:'json',
            data:{params: JSON.stringify(params) , interval:JSON.stringify(interval)},
            success:function(response) {
                console.log(response);
                
            }
        });
    }

    $('#sort').click(function() {
        requestCalls()
    });
    $('#report').click(function() {
        createReport();
        location.replace('./report-state.xls');
    });
    

</script>
</body>
</html>