var ami = require('asterisk-manager')('5038','192.168.10.253','nikon','689368faf59f18a8905d09b3b22b16a6', true);
ami.connect();

var start = function(evt) {
    ami.on('newchannel' , function (evt) {
        console.log('\n***NEW CHANNEL***');
        console.log('id ' + evt.uniqueid);
        console.log('num ' + evt.calleridnum);
    });
    ami.on('queuecallerjoin' , function (evt) {
        console.log('\n***JOIN***');
        console.log('id ' + evt.uniqueid);
        console.log('num ' + evt.calleridnum);
        console.log('positon ' + evt.position);
    });
    ami.on('queuecallerleave' , function (evt) {
        console.log('\n***LEAVE***');
        console.log('id ' + evt.uniqueid);
        console.log('num ' + evt.calleridnum);
    });
    ami.on('agentanswer' , function (evt) {
        console.log('\n***ANSWER***');
        console.log('id ' + evt.uniqueid);
        console.log('num ' + evt.calleridnum);
    });
    ami.on('agentcalled' , function (evt) {
        console.log('\n***CALLED***');
        console.log('id ' + evt.uniqueid);
        console.log('num ' + evt.calleridnum);
        console.log('agent ' + evt.membername);
    });
    ami.on('agentcomplete' , function (evt) {
        console.log('\n***COMPLETE***');
        console.log('id ' + evt.uniqueid);
        console.log('num ' + evt.calleridnum);
        console.log('agent ' + evt.membername);
        console.log('reason ' + evt.reason)

    });
    ami.on('agentringnoanswer' , function (evt) {
        console.log('\n***NO ANSWER***');
        console.log('id ' + evt.uniqueid);
        console.log('num ' + evt.calleridnum);
        console.log('agent ' + evt.membername);
    });
    ami.on('agentconnect' , function (evt) {
        console.log('\n***CONNECT***');
        console.log('id ' + evt.uniqueid);
        console.log('num ' + evt.calleridnum);
    });
}
ami.on('fullybooted' , start)