
<?php 
    ob_start();
    session_start();
    if(!isset($_SESSION['number'])) {
        header("location: login.php?l=ru");
    }
    $flocal= file_get_contents("./langs/".$_SESSION['locale'].".json");
    $trans=json_decode($flocal);
?>
<?php require_once './header.php';?>
<?php require_once './connectionCMonitor.php';?>
<body>
    <div class="container-fluid ">
        <div class=" row ">
            <!-- {{> leftpanel}} -->
            <?php  require_once './leftpanel.php';?>
            <div class="right-panel col-xl-10 offset-xl-2 col-lg-9 offset-lg-3 col-md-12 col-sm-12">
                <div class="content container-fluid row">
                    <!-- {{> header}} -->
                    <div class="header col-12 row d-flex justify-content-between" >
                    <div class="">
                        <h3 class="text-muted"><?php echo $trans->onlinemonitoring; ?> (<?php echo $_SESSION['number'] ?>)</h3>
                    </div>
                    <div class="">
                        <div class="btn-group btn-group-toggle container" data-toggle="buttons">
                            <label class="btn btn-secondary langch" data-lang="kk">
                                <input type="radio" name="lang" id="kk" autocomplete="off" checked> <img src="./icons/Kazakhstan.png"/> Қазақ
                            </label>
                            <label class="btn btn-secondary langch" data-lang="ru">
                                <input type="radio" name="lang" id="ru" autocomplete="off"> <img src="./icons/Russia.png"/> Русский
                            </label>
                            <label class="btn btn-secondary langch" data-lang="en">
                                <input type="radio" name="lang" id="en" autocomplete="off"><img src="./icons/United-Kingdom.png"/>  English
                            </label>
                        </div>
                    </div>    
                </div>
                    <!-- <div class="header col-12"><h3 class="text-muted">Онлайн мониторинг (<?php echo $_SESSION['number'] ?>)</h3></div> -->
                    <div class="row col-md-12 col-sm-12 col-lg-12 col-12" >
                        <h4 class="text-muted"><?php echo $trans->opsinsys;?></h4>
                        <table id="agents" class="table table-bordered table-striped table-hover table-sm">
                            <thead class="thead-dark">
                                <tr>
                                    <th class="w-50"><?php echo $trans->numberop;?></th>
                                    <th class="w-25"><?php echo $trans->state;?></th>
                                    <th class="w-25"><?php echo $trans->status; ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $request = curl_init("http:/localhost:8088/ari/endpoints?api_key=3gpstat-ari:c25ad526ba0a98eabcc26371d92074d7");
                                    curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
                                    $endpoints = curl_exec($request);
                                    curl_close($request);
                                    $result = json_decode($endpoints);
                                    $sql = "SELECT * FROM operators WHERE num=?" ;
                                    foreach($result as $el) {
                                        
                                        if(in_array($el->resource , $_SESSION['operators'])) {
                                            if($el->state=="online") {
                                                $stmt = $cdr->prepare($sql);
                                                $stmt->execute(array($el->resource));
                                                $row = $stmt->fetch();
                                                echo "<tr data-sip='".$el->resource."'>";
                                                    echo "<td class='dev'>".$el->resource."</td>";
                                                    echo "<td class='state'>".$el->state."</td>";
                                                    echo "<td data-time='0' class='status'><img width='24' height='24' src=''><span class='status-text'>".$row['status']."</span><span class='timer'></span></td>";
                                                echo "</tr>";
                                            }
                                        }
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="row col-md-12 col-sm-12 col-lg-12 col-12">
                        
                            <h4 class="text-muted"> <?php echo $trans->queuecalls;?></h4>
                            <table id="queueCalls" class="table  table-bordered table-sm">
                                <thead class="thead-dark" >
                                    <tr>
                                        <th>
                                            <?php echo $trans->numberabon ?>
                                        </th>
                                        <th>
                                            <?php echo $trans->waitinqueue ?>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>                                    
                                </tbody>
                            </table>
                          
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="assets/js/main/socket.js"></script>
    <script>
        var toHHMMSS = function(sec) {
            let sec_num = parseInt(sec, 10); 
            let hours  = Math.floor(sec_num / 3600);
            let minutes = Math.floor((sec_num - (hours * 3600)) / 60);
            let seconds = sec_num - (hours * 3600) - (minutes * 60);

            let Ohours = hours + '';
            let Ominutes = minutes + '';
            let Oseconds = seconds + '';
            if (minutes < 10) {
                Ominutes = "0" + minutes;
            }
            if (seconds < 10) {
                Oseconds = "0" + seconds;
            }
            var time = Ominutes + ':' + Oseconds;
            return time;
        }
        var createTimer = function (sec=0) {  
            return function () {return sec++;}
        }
        var timerList = [];
        var queues = [];
        $.ajax({
            type:'POST',
            url:'ajax/que.php',
            dataType:'json',
            success:function(response) {
                queues = response;
            }
        })

        var printStatus = function (_status) {
            var status="";
            switch (_status) {
                case 'Idle':
                    status="<?php echo $trans->notinuse;?>";
                    break;
                case 'Ringing':
                    status="<?php echo $trans->ringing;?>";
                    break;
                case 'InUse':
                    status="<?php echo $trans->busy ?>";
                    break;
                case 'Pause':
                    status="<?php echo $trans->onpause;?>";
                    break;
                case 'Unavailable':
                    status="<?php echo $trans->pause;?>";
                    break;

            }
            return status;
        }

        var request = function() {
            $.ajax({
                type:'POST',
                url:'./ajax/monitor.php',
                dataType:'json',
                success:function(response) {
                   
                    var states=[];
                    var q = response.que;
                    console.log(q);
                    

                    states  = response.states;

                    console.log(states);
                    states.forEach(function (el ,i) {
                        console.log(el.lastevent);
                        if(el.lastevent) {
                            timerList.push({id:el.num , timer:createTimer(new Date().getTime()/1000 - new Date(el.lastevent).getTime()/1000 ) , isAgent:true})
                        }
                        if(el.status=='InUse') {
                            $('#agents tr[data-sip='+el.num+'] td.status').html('<img width="24" height="24" src="./ico/'+el.status+'.svg" ><span class="status-text">'+printStatus(el.status) +"</span> "+ '<span class="timer"></span><span class="number text-primary">'+"("+ el.caller +")"+'</span>' + " ");
                        }
                        else {
                            $('#agents tr[data-sip='+el.num+'] td.status').html('<img width="24" height="24" src="./ico/'+el.status+'.svg" ><span class="status-text">'+printStatus(el.status) +"</span> "+ '<span class="timer"></span><span class="number text-primary"></span>' + " ");           
                        }
                        $('#agents tr[data-sip='+el.num+'] td.status').data('time' , el.lastevent)
                    });

                    q.forEach(function (el) {
                        el.forEach(function (e) {
                            timerList.push({id:e.uid , timer:createTimer(new Date().getTime()/1000 - parseInt(new Date(e.dtb).getTime()/1000)) });
                            $('#queueCalls tbody').append("<tr data-uid='"+e.uid+"'><td>"+e.number+"</td><td class='primary-text timer'></td></tr>");
                        });
                    });
                }                    
            })
        }

        $(document).ready(function () {

            $('.langch[data-lang="<?php echo $_SESSION['locale'] ?>"]').addClass('active').addClass('focus');
            $('.langch').click(function () {
                $.ajax({
                    type:'POST',
                    url:'./setlocale.php',
                    data:{local:$(this).data('lang')},
                    dataType:'json',
                    success:function(response) {
                        location.reload();
                        console.log(response);
                    }
                })
            });


            var sio = io.connect("http://89.218.233.62:3001");
            sio.emit('requestJoin' , {msg:'msg'});
            sio.on('setQue' , function (data) {
                if(queues.includes(data.queue)) {
                    timerList.push({id:data.uid , timer:createTimer()})
                    $('#queueCalls tbody').append("<tr data-uid='"+data.uid+"'><td>"+data.number+"</td><td class='primary-text timer'></td></tr>");
                }
            })
            sio.on('delQue' , function(data) {
                console.log(data);
                timerList.forEach(el=>{
                    if(el.id == data.uid) {
                        el.timer = createTimer(0);
                    }
                });
                $('#queueCalls tr[data-uid="'+data.uid+'"]').remove();
                $('#agents tr[data-sip="'+data.num+'"] td.status .number').text(' ('+data.caller+')');
            });
            sio.on('dcs',function(data) {
                timerList.forEach(el => {
                    if(el.id == data.agent) {
                        el.timer = createTimer(0);
                    }
                });
                
                $('#agents tr[data-sip='+data.agent+'] td.status').html('<img width="24" height="24" src="./ico/'+data.status+'.svg" ><span class="status-text">'+printStatus(data.status) +"</span> "+'<span class="timer"></span><span class="number text-primary"></span>');
                if(data.status !='InUse') {
                    $('#agents tr[data-sip='+data.agent+'] td.status .number').empty();
                }
            });

            request();
            console.log(timerList);
            
            setInterval(function () {
                timerList.forEach((el,i)=>{
                    if(el.isAgent) {
                        $('#agents tr[data-sip="'+el.id+'"] td.status .timer').text(toHHMMSS(el.timer()));
                    }else {
                        $('#queueCalls tr[data-uid="'+el.id+'"] td.timer').text(toHHMMSS(el.timer()));
                    }
                });
            } , 1000);
        });
    </script>
</body>
</html>