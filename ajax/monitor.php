<?php
    ob_start();
    session_start();
    require_once '../connectionCMonitor.php';
    $data = (object)array();
    $sql = "SELECT * from callcenters WHERE number=? ";
    $stmt = $cdr->prepare($sql);
    $stmt->execute(array($_SESSION['number']));
    $id = (object)array();
    $id->id=$stmt->fetchColumn();
    // echo json_encode($id);
    $sql = "SELECT * FROM operators WHERE callcenter_id =?";
    $stmt = $cdr->prepare($sql);
    $stmt->execute(array($id->id));
    $data-> states = $stmt->fetchAll();

    $qye = array();
    foreach($_SESSION['queues'] as $q) {
        $sql="SELECT * FROM queue WHERE queuenum = ?";
        $stmt=$cdr->prepare($sql);
        $stmt->execute(array($q));
        array_push($qye,$stmt->fetchAll());
    }
    
    $data->que=$qye;

    echo json_encode($data);