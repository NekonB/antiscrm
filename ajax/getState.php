
<?php 
    require_once '../connectionCDR.php';

    
    ob_start();
    session_start();
    $dtb = $_POST['db'];
    $dte = $_POST['de'];
    $number = $_SESSION['number'];

    $strOperators ="";
    

    $db =date_create($dtb);
    $db = date_format($db,'Y-m-d H:i:s');
    $de =date_create( $dte);
    $de = date_format($de,'Y-m-d H:i:s');

    $data = (object) array();
    $data->agents = $_SESSION['operators'];

    /**
     * query - all calls
     */
    $sql="";
    $sql .= "SELECT " ;
        $sql .= "count(c.lastapp ='Queue' or null) + count(c.lastapp='Playback' or null) as allCalls, ";
        $sql .= "count(c.lastapp ='Playback' or null) as wellcomeLoss, ";
        $sql .= "count(c.lastapp='Queue' and c.disposition='NO ANSWER' or null) as lossInQueue, ";
        $sql .= "count(c.dcontext LIKE 'not-work-time%'  or null) as lossNotWorktime, ";
//        $sql .= "count(c.lastapp = 'Dial' and c.disposition='ANSWERED' or null) as confirm, ";
        $sql .= "count(c.disposition='BUSY' or null) as busyLoss ";
    $sql .= "FROM cdr as c ";
    $sql .= "WHERE ";
        $sql .= "c.calldate between '".$db."' and '".$de."' and c.did = '".$number."'";
    
    $stmt = $cdr->query($sql);
    $data -> allCalls = $stmt->fetchObject();
    $data -> allCalls -> loss = ((int)$data -> allCalls->wellcomeLoss + (int)$data -> allCalls->lossInQueue + (int)$data -> allCalls->lossNotWorktime);
    $data -> allCalls -> confirm =0;
    $stmt->closeCursor();

    /**
     * query - by operators
     */
    
    $byOperCall = array();
    foreach($_SESSION['operators'] as $oper) {
        $sql = "SELECT count(*) as allCalls, count(c.disposition = 'ANSWERED' and c.lastapp='Dial' or null) as confirm, count(c.disposition = 'NO ANSWER' or c.disposition='BUSY' or c.lastapp != 'Dial' or null) as loss, count(c.disposition = 'NO ANSWER' and c.lastapp='Dial' or null) as operatorLoss, count(c.lastapp != 'Dial' or null) as overload, c.dst as agent  FROM cdr as c WHERE c.calldate between ? and ? and c.dst = ? ";
        $stmt = $cdr->prepare($sql);
        $stmt->execute(array($db,$de,$oper));
        array_push($byOperCall , $stmt->fetchObject());
    }
    $data-> byOperCall =$byOperCall;
    $stmt->closeCursor();


    foreach($data->byOperCall as $el) {
        $data->allCalls->confirm+=$el->confirm;
    } 

    /**
     * All AVG Q
     */
    $data -> avgQ = (object)array();
    $sql = "SELECT avg(c.duration) as avgQ FROM cdr as c WHERE c.calldate between ? and ? and c.did = ? and c.lastapp ='Queue'";
    $stmt = $cdr->prepare($sql);
    $stmt->execute(array($db,$de,$number));
    $data -> avgQ -> avgQ = $stmt->fetchObject();
    $sql = "SELECT max(time(c.duration)) as avgQ FROM cdr as c WHERE c.calldate between ? and ? and c.did = ? and c.lastapp ='Queue' ";
    $stmt = $cdr->prepare($sql);
    $stmt->execute(array($db,$de,$number));
    $data -> avgQ -> maxQ = $stmt->fetchObject();
    $sql = "SELECT min(time(c.duration)) as avgQ FROM cdr as c WHERE c.calldate between ? and ? and c.did = ? and c.lastapp ='Queue' ";
    $stmt = $cdr->prepare($sql);
    $stmt->execute(array($db,$de,$number));
    $data -> avgQ -> minQ = $stmt->fetchObject();
    $stmt->closeCursor();


    $data -> longAll = (object)array();
    $AL = array();
    $minL = array();
    $maxL = array();
    foreach($_SESSION['operators'] as $oper) {
        $sql = "SELECT avg(c.duration) as avgL , c.dst as agent FROM cdr as c WHERE c.calldate between ? and ?  and c.dst = ? and c.disposition = 'ANSWERED' and c.lastapp ='Dial'";
        $stmt = $cdr->prepare($sql);
        $stmt->execute(array($db,$de,$oper));
        array_push($AL , $stmt->fetchObject());
        $sql = "SELECT min(time(c.duration)) as avgL , c.dst as agent FROM cdr as c WHERE c.calldate between ? and ?  and c.dst = ? and c.disposition = 'ANSWERED' and c.lastapp ='Dial'";
        $stmt = $cdr->prepare($sql);
        $stmt->execute(array($db,$de,$oper));
        array_push($minL , $stmt->fetchObject());
        $sql = "SELECT max(time(c.duration)) as avgL , c.dst as agent FROM cdr as c WHERE c.calldate between ? and ?  and c.dst = ? and c.disposition = 'ANSWERED' and c.lastapp ='Dial'";
        $stmt = $cdr->prepare($sql);
        $stmt->execute(array($db,$de,$oper));
        array_push($maxL , $stmt->fetchObject());
    }
    $data-> longAll -> avgL = $AL;
    $data-> longAll -> maxL = $maxL;
    $data-> longAll -> minL = $minL;

    $avgByO = array();
    foreach($_SESSION['operators'] as $oper) {
        $sql="SELECT avg(c.duration) as avgQ , max(time(c.duration or null)) as maxQ , min(time(c.duration or null)) as minQ , c.dst as agent FROM cdr as c WHERE c.calldate between ? and ? and c.dst = ?";
        $stmt= $cdr->prepare($sql);
        $stmt->execute(array($db,$de,$oper));
        array_push($avgByO , $stmt->fetchObject());
    }
    $data-> byOperAVGO = $avgByO;
    $longBy = array();
    foreach($_SESSION['operators'] as $oper) {
        $sql="SELECT avg(c.duration) as avgL , max(time(c.duration)) as maxL , min(time(c.duration)) as minL , c.dst as agent FROM cdr as c WHERE c.calldate between ? and ? and c.dst = ?  and c.disposition ='ANSWERED' and c.lastapp = 'Dial' ";
        $stmt= $cdr->prepare($sql);
        $stmt->execute(array($db,$de,$oper));
        array_push($longBy , $stmt->fetchObject());
    }
    $data-> longByO = $longBy;
    echo json_encode($data);
?>
