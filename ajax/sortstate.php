<?php 
    ob_start();
    session_start();
    require_once '../connectionCDR.php';
    if ($_SERVER['REQUEST_METHOD'] == 'GET') {
        $params = json_decode($_GET['params']);
        $interval = json_decode($_GET['interval']);
        $db =date_create( $interval->dtb);
        $db = date_format($db,'Y-m-d H:i:s');
        $de =date_create( $interval->dte);
        $de = date_format($de,'Y-m-d H:i:s');
        $strOperators ="";
        $data = (object)array();
        foreach($_SESSION['operators'] as $el) {
            $strOperators = implode(',' , $_SESSION['operators']);
        }
        $sql = "SELECT c.uniqueid as uid, c.calldate as dtb, c.src as num, c.dst as agent, c.disposition as result, SEC_TO_TIME(c.billsec) as longTime, c.recordingfile as recfile FROM cdr as c WHERE c.calldate BETWEEN ? and ? and  c.dst IN (".$strOperators.") " ;
        if($params->agent){
            $sql=$sql." AND c.dst ='".$params->agent."'" ;
        }
        
        if($params->res){
            if($params->res=="wellcomeLoss"){
                $sql = "SELECT c.uniqueid as uid, c.calldate as dtb, c.src as num, c.dst as agent, c.disposition as result, SEC_TO_TIME(c.billsec) as longTime, c.recordingfile as recfile FROM cdr as c WHERE c.did=? and c.calldate BETWEEN ? and ? and c.lastapp='Playback' ";
                $stmt=$cdr->prepare($sql);
                $stmt->execute(array($_SESSION['number'], $db, $de));
                $data = $stmt->fetchAll();
            }
            else if ($params->res=="queueLoss") {
                $sql = "SELECT c.uniqueid as uid, c.calldate as dtb, c.src as num, c.dst as agent, c.disposition as result, SEC_TO_TIME(c.billsec) as longTime, c.recordingfile as recfile FROM cdr as c WHERE c.did=? and c.calldate BETWEEN ? and ? and c.disposition='NO ANSWER' ";
                $stmt=$cdr->prepare($sql);
                $stmt->execute(array($_SESSION['number'], $db, $de));
                $data=$stmt->fetchAll();
            }
            else if ($params->res=="notWorkTimeLoss") {
                
                $sql = "SELECT c.uniqueid as uid, c.calldate as dtb, c.src as num, c.dst as agent, c.disposition as result, SEC_TO_TIME(c.billsec) as longTime, c.recordingfile as recfile FROM cdr as c WHERE c.did=? and c.calldate BETWEEN ? and ? and c.dcontext LIKE 'not-work-time%' ";
                $stmt=$cdr->prepare($sql);
                $stmt->execute(array($_SESSION['number'], $db, $de));
                $data=$stmt->fetchAll();
            }
            else if($params->res=='NO ANSWER') {
                $sql = "SELECT c.uniqueid as uid, c.calldate as dtb, c.src as num, c.dst as agent, c.disposition as result, SEC_TO_TIME(c.billsec) as longTime, c.recordingfile as recfile FROM cdr as c WHERE c.calldate BETWEEN ? and ? and  c.dst IN (".$strOperators.") and c.lastapp = 'Dial' and c.disposition='NO ANSWER' ";
                if($params->agent){
                    $sql=$sql." AND c.dst =? ";
                }
                $stmt=$cdr->prepare($sql);
                if($params->agent) {
                    $stmt->execute(array( $db, $de , $params->agent));
                }
                $stmt->execute(array( $db, $de));
                $data=$stmt->fetchAll();
            }
            else if ($params->res=='ANSWERED') {
                $sql = "SELECT c.uniqueid as uid, c.calldate as dtb, c.src as num, c.dst as agent, c.disposition as result, SEC_TO_TIME(c.billsec) as longTime, c.recordingfile as recfile FROM cdr as c WHERE c.calldate BETWEEN ? and ? and  c.dst IN (".$strOperators.") and c.lastapp = 'Dial' and c.disposition='ANSWERED' ";
                if($params->agent){
                    $sql=$sql." AND c.dst =? ";
                }
                $stmt=$cdr->prepare($sql);
                if($params->agent) {
                    $stmt->execute(array( $db, $de , $params->agent));
                }
                $stmt->execute(array( $db, $de));
                $data=$stmt->fetchAll();
            }
            
        }
        else {
            $stmt = $cdr->prepare($sql,array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $stmt->execute(array($db , $de));
            $data = $stmt->fetchAll();
            // $sql = "SELECT c.uniqueid as uid, c.calldate as dtb, c.src as num, c.dst as agent, c.disposition as result, SEC_TO_TIME(c.billsec) as longTime, c.recordingfile as recfile FROM cdr as c WHERE c.did=? and c.calldate BETWEEN ? and ? and c.disposition='NO ANSWER' ";
            // $stmt=$cdr->prepare($sql);
            // $stmt->execute(array($_SESSION['number'], $db, $de));
            // $res = $stmt->fetchAll();
            // foreach($res as $el){
            //     array_push($data , $el);
            // } 
            // array_merge($data , $stmt->fetchAll());

            // $sql = "SELECT c.uniqueid as uid, c.calldate as dtb, c.src as num, c.dst as agent, c.disposition as result, SEC_TO_TIME(c.billsec) as longTime, c.recordingfile as recfile FROM cdr as c WHERE c.did=? and c.calldate BETWEEN ? and ? and c.disposition='NO ANSWER' ";
            // $stmt=$cdr->prepare($sql);
            // $stmt->execute(array($_SESSION['number'], $db, $de));

            // foreach($res as $el){
            //     array_push($data , $el);
            // } 


            // $sql = "SELECT c.uniqueid as uid, c.calldate as dtb, c.src as num, c.dst as agent, c.disposition as result, SEC_TO_TIME(c.billsec) as longTime, c.recordingfile as recfile FROM cdr as c WHERE c.did=? and c.calldate BETWEEN ? and ? and c.dcontext='not-work-time-gorod' ";
            // $stmt=$cdr->prepare($sql);
            // $stmt->execute(array($_SESSION['number'], $db, $de));

            // foreach($res as $el){
            //     array_push($data , $el);
            // } 
            
        }
        
        echo json_encode($data);
    }   